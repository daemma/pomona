### ############################################################################
##! @file       Makefile
##! @brief      Recipes for running CMake
##! @author     "Emma Hague" <dev@daemma.io>
##! @date       2017-11-22
##! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
##!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>

CMAKEEXE = /usr/bin/cmake
BUILDDIR = build
.SILENT   :
.PHONY    : clean rebuild doc
all       : build

bdir      :
	  @test -d $(BUILDDIR) || mkdir $(BUILDDIR)

build     : bdir
	  @cd $(BUILDDIR)               && \
	  $(CMAKEEXE) ..                && \
	  $(CMAKEEXE) --build . -- -j 4 && \
	  cd ..

clean     :
	  @rm -rf $(BUILDDIR)/*

rebuild   : clean build

install   : build
	  @cd $(BUILDDIR) && \
	  make install    && \
	  cd ..

uninstall : install
	  @cd $(BUILDDIR) && \
	  make install    && \
	  cd ..

test      : 
	  @cd $(BUILDDIR)                && \
	  $(CMAKEEXE) -DBUILD_TEST=ON .. && \
	  $(CMAKEEXE) --build . -- -j 4  && \
	  make test && \
	  cd ..

dist      :
	  @cd $(BUILDDIR) && \
	  $(CMAKEEXE) ..  && \
	  make clean-doc  && \
	  make dist       && \
	  cd ..

doc       :
	  @cd $(BUILDDIR) && \
	  $(CMAKEEXE) ..  && \
	  make clean-doc  && \
	  make doc        && \
	  cd ..

print     : bdir
	  @cd $(BUILDDIR)          && \
	  cmake .. -DPRINT_VARS=ON && \
	  cd ..

### end Makefile
### ############################################################################
