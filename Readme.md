<!--#########################################################################-->
<!-- @file       Readme.md -->
<!-- @brief      Project overview and information. -->
<!-- @author     "Emma Hague" <dev@daemma.io> -->
<!-- @date       2017-11-22 -->
<!-- @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io> -->
<!--             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt> -->

[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/daemma/dotfiles/blob/master/License.txt) 
![Version](https://img.shields.io/:version-0.0.3-green.svg)

# Pomona
Pomona was the Roman goddess of fruit and nut trees.
This software is a basic C/C++ utility library for converting data file types, 
among other fruitful tasks.

## Dependencies
### Meta
You'll need the typical C/C++ tool set: `make`, `cmake`, `gcc`, etc. (or somethings isomorphic).

### Cereal
The [cereal](https://uscilab.github.io/cereal/) header collection is a masterful implementation 
of C++ class I/O using three formats: JSON, XML, and binary. 
This package is available on github and most package managers. 
It is required for this library.

### yaml-cpp (optional)
The [yaml-cpp](https://github.com/jbeder/yaml-cpp) package is a nice way to read/write 
[YAML 1.2](http://www.yaml.org/spec/1.2/spec.html) in C++. 
As of this writing, the version in most package managers requires the large and odious `boost`. 
This is not true for the git version, so it is better to download and install from there.

### CERN's root-system (optional)
[Root](https://root.cern.ch/) is CERN's massive and rich data analysis library. 
It's in a league of it's own and can be optionally baked into this package. 
You can find root in many package managers but it is generally better to download and compile the 
latest version for yourself.

### Google's gtest (optional)
The command `make test` will compile and run the library tests, via
Google's gtest suite.

### Valgrind
Here's some tips on how to use [Valgrind](http://www.valgrind.org/) with this library: 
[Valgrind and root](https://root-forum.cern.ch/t/valgrind-and-root/28506)

## Build
Building this library follows the typical `cmake` pattern:
```
 $ mkdir build
 $ cd build
 $ cmake ..
 $ cmake --build .
```
Alternatively, you can use the included `Makefile` as a short-cut:
```
 $ make
```

### Configuration Options
You can edit the `CMakelists.txt` file to suite your needs: the primary build options 
	are listed near the top of the file.
Alternatively, you can tweak the build at configure-time with the typical 
`cmake -D<var>=ON|OFF` syntax.

| Variable               | Description   |
| ---------------------- |:------------- |
| BUILD_SHARED_ONLY      | Build only the shared object library | 
| BUILD_STATIC_LIB       | Build static library |
| BUILD_APPS             | Build application executables |
| BUILD_TEST             | Build test executables |
| USE_YAML               | Build with YAML-file support |
| USE_CERNROOT           | Build with CERN's root-system |
| USE_LOGMSG             | Build with message logging service |
| USE_DOXY               | Build doxygen target |
| CMAKE_RULE_MESSAGES    | Add a progress messages |
| CMAKE_VERBOSE_MAKEFILE | Show each command line as it is used |
| CMAKE_PRINT_CONFIGS    | Print configuration variables |


## Credits
The icon image for this project is taken from the painting *Pomona* (c. 1700) by Nicolas Fouché 
as obtained from [wikipedia](https://commons.wikimedia.org/wiki/File:Nicolas_Fouch%C3%A9_001.jpg).
It is a public domain work of art.

## License
[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/daemma/dotfiles/blob/master/License.txt) 

Pomona is for converting data file types, among other fruitful tasks.

Copyright (C) 2019 "Emma Hague" <dev@daemma.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.



<!--end Readme.md -->
<!--#########################################################################-->
