//  ############################################################################
//! @file       AbsObject.hh
//! @brief      Header file for a generic abstract base class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ABSOBJECT_HH
#define POMONA_ABSOBJECT_HH
#include "Types.hh"  // fundamental types

namespace Pomona {

//  ****************************************************************************
//! Generic abstract base class for this library.
class AbsObject
{
public: 
  AbsObject()                            = default;
  AbsObject(const AbsObject&)            = default;
  AbsObject(AbsObject&&)                 = default;
  AbsObject& operator=(const AbsObject&) = default;
  AbsObject& operator=(AbsObject&&)      = default;
  inline virtual ~AbsObject(){}

  //! cloner (pure virtual)
  virtual AbsObject* clone() const = 0;

  // class helpers
  inline virtual const String_t className() const { return "AbsObject"; }
  //! Is this the same class-name
  inline virtual Bool_t isA(const String_t& cn) const
  { return (className() == cn); }
  //! Is this the same class-type/name as other?
  inline virtual Bool_t isSameClass(const AbsObject& other) const
  { return isA(other.className()); }

  //! Clear: set values to defaults
  inline virtual void clear(){}
  //! Are all the values equal to default?
  inline virtual Bool_t isEmpty() const
  { return kTRUE; }

protected: 
#ifdef CERNROOT  // Using root
  ClassDef(Pomona::AbsObject,0);
#endif           // end using root
}; 

//  ****************************************************************************
//! @struct is_object
/** @brief A template struct to dertmine if the template parameter inherits 
    from AbsObject. 
*/
template <class T>
struct is_absobject : std::is_base_of<AbsObject, T>{ };

//  ****************************************************************************
//! Template function to  dertmine if inheritence from AbsObject. 
template <class T>
Bool_t IsAbsObject(const T&){ return is_absobject<T>::value; }

} // end namespace Pomona

#endif // end POMONA_ABSOBJECT_HH


//  end AbsObject.cxx
//  ############################################################################
