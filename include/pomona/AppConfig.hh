//  ############################################################################
//! @file       AppConfig.hh
//! @brief      Header for configuration app
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-26
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_APP_CONFIG_HH
#define POMONA_APP_CONFIG_HH
#include "Application.hh"  // pomona: base class

namespace Pomona {

namespace App {

//  ****************************************************************************
//! Utility and interface for configuration application.
class Config : public Application 
{
public: 
  Config();
  inline virtual ~Config(){ }

  virtual Int_t run();

protected: 
  virtual Bool_t  addOptions();
  virtual void    printHelpUsage(std::ostream& os) const;

private: 
  Config(const Config& other)          = delete;
  Config(Config&& other)               = delete;
  Config& operator=(const Config& rhs) = delete;
  Config& operator=(Config&& rhs)      = delete;
}; 

} // end namespace App

} // end namespace Pomona

#endif // end POMONA_APP_CONFIG_HH

//  end AppConfig.hh
//  ############################################################################
