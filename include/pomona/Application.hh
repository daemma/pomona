//  ############################################################################
//! @file       Application.hh
//! @brief      Header for generic application class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-25
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_APP_APPLICATION_HH
#define POMONA_APP_APPLICATION_HH
#include "Stopwatch.hh"  // pomona: process timer
#include "OptionMap.hh"  // pomona: command-line options
#include "HostPath.hh"   // pomona: host OS path(s)
#include <iosfwd>        // std: i/o streamer forward declarations
#ifdef CERNROOT          // Using root
class TCanvas;           // root: plotting canvas
#endif                   // end using root

namespace Pomona {

//  ****************************************************************************
//! Applications namespace
namespace App {

//  ****************************************************************************
//! Utility and interface for building specific applications.
class Application : public Named 
{
public: 
  Application(const Char_t* appClassName = "Application");
  inline virtual ~Application(){ }

  // accessors
  const OptionMap& options() const;
        OptionMap& options();
  const Stopwatch& timer() const;
  const UInt_t&    pid() const;

  // run the app
  virtual Int_t main(Int_t argc, Char_t** argv);
  virtual Int_t run();

  // Output helpers
  virtual Bool_t cdOut(const String& subDir, const Bool_t& print = kFALSE) const;
  virtual Bool_t cdOut(const Char_t* subDir, const Bool_t& print = kFALSE) const;
  virtual void   cdTop() const;
  virtual void   cdUp() const;

#ifdef CERNROOT                 // Using root
  virtual void saveCanvas(const TCanvas& canvas, 
			  const Bool_t& printInfo = kFALSE,
			  const Bool_t& aliasOutDir = kTRUE) const;
#endif                          // end using root

protected: 
  virtual Bool_t addOptions();

  virtual void printSectionSep  (const Char_t* sectionName = "") const;
  virtual void printHelpText    (std::ostream& os) const;
  virtual void printHelpTextVerb(std::ostream& os) const;
  virtual void printHelpUsage   (std::ostream& os) const;
  virtual void printHelpOptions (std::ostream& os, const Char_t* ind = "  ") const;

  virtual void  runExitableOptions() const;
  virtual void  setLogLevelOptions() const;
  virtual void  setRootSysOptions() const;
  virtual void  setAppOptions();
  virtual void  begin();
  virtual Int_t end();

  static void PrintLibVersion  (std::ostream& os);
  static void PrintLibCopyright(std::ostream& os);
  static void PrintLibInfo     ();
  static void PrintLibInfo     (std::ostream& os);
  static void PrintYamlVersion (std::ostream& os);
  static void Exit(const Int_t& code = 0, const Bool_t& performCleanup = kTRUE);

protected: 
  //! Options
  OptionMap mOpts;    
  //! Run-time timer
  Stopwatch mTimer;
  //! Process ID number
  UInt_t    mPid;
  //! Log file-name (if requested)
  mutable String    mLogFile;
  //! Output directory    
  mutable HostPath  mOutDir;
  //! Top-level output directory   
  mutable HostPath  mOutDirTop;  

private: 
  Application(const Application& other)          = delete;
  Application(Application&& other)               = delete;
  Application& operator=(const Application& rhs) = delete;
  Application& operator=(Application&& rhs)      = delete;
}; 

} // end namespace App

} // end namespace Pomona

#endif // end POMONA_APP_APPLICATION_HH

//  end Application.hh
//  ############################################################################
