//  ############################################################################
//! @file       ClassDef.hh
//! @brief      Class definition macro(s).
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_CLASSDEF_HH
#define POMONA_CLASSDEF_HH
#include "Types.hh"     // pomona: fundamental types
#include "IoCereal.hh"  // pomona: cereal-ization
#include "IoYaml.hh"    // pomona: yaml-ization
#ifdef CERNROOT         // Using root
#include "TObject.h"    // root: TObject
#endif                  // end using root

//  ****************************************************************************
//! @def CLASS_DEF_BASE(name,id) Class definition basic macro.
#define CLASS_DEF_BASE(name,id)		 \
  public:				 \
    virtual const String_t className() const;  \
    virtual AbsObject* clone() const;	 \
    CLASS_DEF_CEREAL()                   

//  ****************************************************************************
//! @def CLASS_DEF_ROOT(name,id) Class definition root-system macro.
#define CLASS_DEF_ROOT(name,id)	ClassDef(name,id);

//  ****************************************************************************
//! @def CLASS_DEF(name,id) Class definition macro.
#ifdef CERNROOT            // Using root
#  ifdef YAMLCPP             // Using yaml-cpp
#    define CLASS_DEF(name,id) \
       CLASS_DEF_BASE(name,id)  \
       CLASS_DEF_YAML(name) \
       CLASS_DEF_ROOT(name,id)
#  else                      // Not using yaml-cpp
#    define CLASS_DEF(name,id) \
       CLASS_DEF_BASE(name,id)  \
       CLASS_DEF_ROOT(name,id)
#  endif                     // end not using yaml-cpp
#else                      // Not using root
#  ifdef YAMLCPP             // Using yaml-cpp
#    define CLASS_DEF(name,id) \
       CLASS_DEF_BASE(name,id)  \
       CLASS_DEF_YAML(name) 
#  else                      // Not using yaml-cpp
#    define CLASS_DEF(name,id) \
       CLASS_DEF_BASE(name,id)  
#  endif // end not using yaml-cpp
#endif // end not using root


#endif // end POMONA_CLASSDEF_HH

//  end ClassDef.hh
//  ############################################################################



