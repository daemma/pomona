//  ############################################################################
//! @file       ClassImp.hh
//! @brief      Class implementation macro(s).
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_CLASSIMP_HH
#define POMONA_CLASSIMP_HH
#include "Types.hh"  // pomona: fundamental types

//  ****************************************************************************
//! @def STRIPNS(var) Strip the namespace (Pomona::) from imput
#define STRIPNS(var) \
  String_t(#var).substr(8, String_t(#var).length())

//  ****************************************************************************
//! @def CLASS_IMP_BASE(name) Class implementation base macro.
#define CLASS_IMP_BASE(name)						\
  const String_t     name::className() const { return STRIPNS(name); }	\
  Pomona::AbsObject* name::clone() const { return new name(*this); }

//  ****************************************************************************
//! @def CLASS_IMP(name) Class implementation macro.
#ifdef CERNROOT         // Using root
#define CLASS_IMP(name)	\
  CLASS_IMP_BASE(name)	\
  ClassImp(name)
#else                   // Not using root
#define CLASS_IMP(name) \
  CLASS_IMP_BASE(name)
#endif // end not using root

#endif // end POMONA_CLASSIMP_HH


//  end ClassImp.hh
//  ############################################################################
