//  ############################################################################
//! @file       Comparable.hh
//! @brief      Header for utility class for implementing object comparison
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-03-27
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_COMPARABLE_HH
#define POMONA_COMPARABLE_HH
#include "Types.hh"  // fundamental types

namespace Pomona {

//  ****************************************************************************
//! Utility class for implementing object comparison.
class Comparable 
{
public: 
  Comparable()                             = default;
  Comparable(const Comparable&)            = default;
  Comparable(Comparable&&)                 = default;
  Comparable& operator=(const Comparable&) = default;
  Comparable& operator=(Comparable&&)      = default;
  inline virtual ~Comparable(){ }

  //! Pointer equivalence
  inline virtual Bool_t isSame(const Comparable& other) const
  { return (this == &other); }
  //! Equivalence
  inline virtual Bool_t isEqual(const Comparable& other) const
  { return isSame(other); }
  //! Is sortable object?
  inline virtual Bool_t isSortable() const
  { return kFALSE; }
  //! Compare this object to other (pure virtual)
  /** Derived implementations should return:
      - <0 (-1) if this is smaller than other
      - >0 (+1) if this is larger than other
      -  0 else 
  */
  inline virtual Int_t  compare(const Comparable& /*other*/) const
  { return 0; }

  //! Operator: equivalence 
  inline virtual Bool_t operator==(const Comparable& rhs)
  { return isEqual(rhs); }
  //! Operator: anti-equivalence
  virtual Bool_t operator!=(const Comparable& rhs)
  { return !isEqual(rhs); }
  //! Operator: less-than
  virtual Bool_t operator< (const Comparable& rhs)
  { return (compare(rhs) < 0); }
  //! Operator: less-than or equal
  virtual Bool_t operator<=(const Comparable& rhs)
  { return (compare(rhs) <= 0); }
  //! Operator: greater-than
  virtual Bool_t operator> (const Comparable& rhs)
  { return (compare(rhs) > 0); }
  //! Operator: greater-than or equal
  virtual Bool_t operator>=(const Comparable& rhs)
  { return (compare(rhs) >= 0); }

protected: 
#ifdef CERNROOT  // Using root
  ClassDef(Pomona::Comparable,0);
#endif           // end using root
}; 

//! Operator: equivalence 
inline Bool_t operator==(const Comparable& lhs, const Comparable& rhs)
{ return lhs.isEqual(rhs); }

//! Operator: anti-equivalence 
inline Bool_t operator!=(const Comparable& lhs, const Comparable& rhs)
{ return !lhs.isEqual(rhs); }

//! Operator: less than
inline Bool_t operator< (const Comparable& lhs, const Comparable& rhs)
{ return (lhs.compare(rhs) < 0); }

//! Operator: less than or equal
inline Bool_t operator<=(const Comparable& lhs, const Comparable& rhs)
{ return (lhs.compare(rhs) <= 0); }

//! Operator: greater than
inline Bool_t operator> (const Comparable& lhs, const Comparable& rhs)
{ return (lhs.compare(rhs) > 0); }

//! Operator: greater than or equal
inline Bool_t operator>=(const Comparable& lhs, const Comparable& rhs)
{ return (lhs.compare(rhs) >= 0); }

} // end namespace Pomona

#endif // end POMONA_COMPARABLE_HH


//  end Comparable.hh
//  ############################################################################
