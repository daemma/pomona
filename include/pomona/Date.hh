//  ############################################################################
//! @file       Date.hh
//! @brief      Header for calendar date utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_DATE_HH
#define POMONA_DATE_HH
#include "Object.hh"  // pomona: base class
#include <ctime>      // stdc: tm, gmtime

namespace Pomona {

// forward declaration
class TimePoint;

//  ****************************************************************************
//! Alias for stdlib time struct tm.
/** Equivalent to struct C/C++ standard library time struct  
    <a href="cplusplus.com/reference/ctime/tm/">tm</a>.
*/
typedef struct tm Tm_t;

//  ****************************************************************************
//! Alias for stdlib time struct time_t
/** Equivalent to struct C/C++ standard library time arithmetic type  
    <a href="cplusplus.com/reference/ctime/time_t/">time_t</a>.
*/
typedef time_t Time_t;

//  ****************************************************************************
//! A reinterpretation of stdlib struct tm.
/*! This is a funky conglomeration of C++11 and C98 methods. 
    Hopefully it's better than using the raw things and stuff ...
*/
class Date : public Object {
public:
  UShort_t mMSec;    //!< milliseconds after the second 0-999
  UInt8_t  mSec;     //!< seconds after the minute      0-60
  UInt8_t  mMin;     //!< minutes after the hour        0-59
  UInt8_t  mHour;    //!< hours since midnight          0-23
  UInt8_t  mDay;     //!< day of the month              1-31
  UInt8_t  mMonth;   //!< months after January          1-12
  UShort_t mYear;    //!< years since zero              0-??
  UInt8_t  mTzMin;   //!< Time-Zone: minutes offset     0-59
  Int8_t   mTzHour;  //!< Time-Zone: hours offset       -12-12
  String_t mTzAbr;   //!< Time-Zone abbreviation        UTC-???
  Bool_t   mIsDst;   //!< Daylight Saving Time flag     false-true

public:
  Date();
  explicit Date(const Tm_t& tmDate);
  explicit Date(const Time_t& uts);
  explicit Date(const TimePoint& uts);
  Date(const Date& other);
  Date(Date&& other);
  Date& operator=(const Date& rhs);
  Date& operator=(Date&& rhs);
  Date& operator=(const Tm_t& rhs);
  Date& operator=(const Time_t& rhs);
  Date& operator=(const TimePoint& rhs);
  inline virtual ~Date(){ }

  // AbsObject
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  // Comparable
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable
  virtual UShort_t defaultPrintContent() const;
  virtual void     printValue(std::ostream& os) const;

  // modify
  void set     (const Tm_t& tmDate);
  void set     (const Time_t& uts);
  void setLocal(const Time_t& local);
  void set     (const TimePoint& uts);
  void setLocal(const TimePoint& local);
  void setNow  ();

  // access
  Tm_t      asTm() const;
  Time_t    asTime() const;
  TimePoint asTimePoint() const;
  String_t  asIso8601(Bool_t withMillisec = kTRUE) const;
  String_t  dateStr() const;
  String_t  timeStr(Bool_t withMillisec = kTRUE) const;
  String_t  tzStr() const;
  String_t  asDescription() const;

  // utilities
  static Date     Zero();
  static Date     Now();
  static Date     NowLocal();
  static String_t Description(const Tm_t& tmDate);
  static String_t TzAbbrev(const Tm_t& tmDate);
  static String_t TzStr(const Tm_t& tmDate);
  static void     TzParseStr(const String_t& tzStr, Int8_t& hour, UInt8_t& min);
  static void     TzValues(const Tm_t& tmDate, Int8_t& hour, UInt8_t& min);
  static Int_t    TzSeconds(const Tm_t& tmDate);
  static Time_t   GmTime(const Tm_t& timeInfo);

private:
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(CNVP(mMSec)
      ,CNVP(mSec)
      ,CNVP(mMin)
      ,CNVP(mHour)
      ,CNVP(mDay)
      ,CNVP(mMonth)
      ,CNVP(mYear)
      ,CNVP(mTzMin)
      ,CNVP(mTzHour)
      ,CNVP(mTzAbr)
      ,CNVP(mIsDst));
  }

private:
  CLASS_DEF(Pomona::Date,1)
};

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::Date> 
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Date& rhs)
  {
    Node node;              // re-casting for compatability with yaml-cpp
    node["MSec"]   =           rhs.mMSec;
    node["Sec"]    = (UShort_t)rhs.mSec;
    node["Min"]    = (UShort_t)rhs.mMin;
    node["Hour"]   = (UShort_t)rhs.mHour;
    node["Day"]    = (UShort_t)rhs.mDay;
    node["Month"]  = (UShort_t)rhs.mMonth;
    node["Year"]   =           rhs.mYear;
    node["TzMin"]  = (UShort_t)rhs.mTzMin;
    node["TzHour"] = (Short_t) rhs.mTzHour;
    node["TzAbr"]  =           rhs.mTzAbr;
    node["IsDst"]  =           rhs.mIsDst;
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::Date& rhs)
  {
    if(!node.IsMap()|| node.size() != 11) return kFALSE;
    rhs.mMSec   = node["MSec"]  .as<UShort_t>();
    rhs.mSec    = node["Sec"]   .as<UShort_t>();
    rhs.mMin    = node["Min"]   .as<UShort_t>();
    rhs.mHour   = node["Hour"]  .as<UShort_t>();
    rhs.mDay    = node["Day"]   .as<UShort_t>();
    rhs.mMonth  = node["Month"] .as<UShort_t>();
    rhs.mYear   = node["Year"]  .as<UShort_t>();
    rhs.mTzMin  = node["TzMin"] .as<UShort_t>();
    rhs.mTzHour = node["TzHour"].as<Short_t>();
    rhs.mTzAbr  = node["TzAbr"] .as<String_t>();
    rhs.mIsDst  = node["IsDst"] .as<Bool_t>();
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_DATE_HH

//  end Date.hh
//  ############################################################################
