//  ############################################################################################
//! @file       Enum.hh
//! @brief      Template class to hold an enumerated value, with reference to meta-data
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2018-05-25
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ENUM_HH
#define POMONA_ENUM_HH
#include "Object.hh"      // pomona: base class
#include "LogService.hh"  // pomona: log message service
#ifdef CERNROOT           // Using root
#include "TTree.h"        // root: data tree
#endif                    // end using root

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for enable if T is an enumeration type
template <typename T> 
using EiiE = typename std::enable_if< std::is_enum<T>::value >::type;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for underlying type 
template <typename T> 
using Under = typename std::underlying_type<T>::type;

//  ****************************************************************************
//! Object-ified enumeration value
template <typename T> 
class Enum : public Object
{
public: 
  Enum(const T& value = T());
  Enum(const Enum& other);
  Enum(Enum&& other);
  Enum<T>& operator=(const Enum<T>&  rhs);
  Enum<T>& operator=(      Enum<T>&& rhs);
  Enum<T>& operator=(const T&  rhs);
  Enum<T>& operator=(      T&& rhs);
  Enum<T>& operator=(const Under<T>&  rhs);
  Enum<T>& operator=(      Under<T>&& rhs);
  inline virtual ~Enum(){}

  // AbsObject overloads
  virtual const String_t className() const;
  virtual void       clear();
  virtual Bool_t     isEmpty() const;
  // Comparable overloads
  virtual Bool_t     isEqual(const Comparable& other) const;
  virtual Bool_t     isSortable() const;
  virtual Int_t      compare(const Comparable& other) const;
  // Printable overloads
  virtual UShort_t   defaultPrintContent() const;
  virtual void       printValue(std::ostream& os) const;

  // accessors
  const T& get() const;
  void     set(const T& val);
  // special access
  explicit operator Under<T>();
  const T& operator()() const;
  const Under<T>& operator+() const;
  const Under<T>& getValueAsUnder() const;
  void            setValueFromUnder(const Under<T>& uval);
  template <typename U, typename = EiiI<U> > const U& getValueAs() const;
  template <typename U, typename = EiiI<U> > void     setValueFrom(const U& uval);

  // Bitwise operators
  Under<T> operator~() const;
  Under<T> operator|(const Enum<T>& rhs) const;
  Under<T> operator|(const T& rhs) const;
  Under<T> operator|(const Under<T>& rhs) const;
  Under<T> operator&(const Enum<T>& rhs) const;
  Under<T> operator&(const T& rhs) const;
  Under<T> operator&(const Under<T>& rhs) const;

  // Equivalence operators
  Bool_t operator==(const T& rhs);
  Bool_t operator!=(const T& rhs);
  Bool_t operator<=(const T& rhs);
  Bool_t operator< (const T& rhs);
  Bool_t operator>=(const T& rhs);
  Bool_t operator> (const T& rhs);
  
  // static utilities
  template <typename U, typename = EiiI<U> > static U As(const T& eval);
  template <typename U, typename = EiiI<U> > static T From(const U& uval);
  
#ifdef CERNROOT  // Using root
  // CERN-ROOT helpers
  Bool_t createBranches(TTree& tree, const String& bName = "") const;
  Bool_t setBranchAddrs(TTree& tree, const String& bName = "") const;
#endif           // end using root

protected:
  virtual T& get();
  virtual T& operator()();

private:
  //! Enumeration value
  T mValue;
  
private: 
  //! cereal::access must be friendly
  friend class cereal::access;
  //! Cerealization: save minimal
  template <class Archive>
  Int_t save_minimal(const Archive&) const { return getValueAs<Int_t>(); }
  //! Cerealization: load minimal
  template <class Archive>
  void load_minimal(const Archive&, const Int_t& value){ setValueFrom<Int_t>(value); }
  //! YAML::convert must be friendly
  friend class YAML::convert< Enum<T> >; 
}; 

// External operators
template <typename T> Under<T>& operator|=(Under<T>& lhs, const Enum<T>& rhs);
template <typename T> Under<T>& operator&=(Under<T>& lhs, const Enum<T>& rhs);

//  ****************************************************************************
//  ****************************************************************************
//  Template implementations
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: default
template <typename T> 
Enum<T>::Enum(const T& val)
  : Object()
  , mValue(val)
{
  static_assert(std::is_enum<T>::value,              "Enum<T>::Enum: Template type is not an enumeration.");
  static_assert(std::is_integral< Under<T> >::value, "Enum<T>::Enum: Template type is not integral-based.");
  static_assert(sizeof(Under<T>) <= sizeof(Int_t),   "Enum<T>::Enum: Template type is not <= 32-bit.");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: copy
template <typename T> 
Enum<T>::Enum(const Enum& other)
  : Object(other)
  , mValue(other.mValue)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: move
template <typename T> 
Enum<T>::Enum(Enum&& other)
  : Object(other)
  , mValue(other.mValue)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(const Enum<T>& rhs)
{
  if(this != &rhs){
    Object::operator=(rhs);
    mValue = rhs.mValue;
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(Enum<T>&& rhs)
{
  if(this != &rhs){
    Object::operator=(rhs);
    mValue = rhs.mValue;
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(const T& rhs)
{
  mValue = rhs;
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(T&& rhs)
{
  mValue = rhs;
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(const Under<T>& rhs)
{
  mValue = From(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <typename T> 
Enum<T>& Enum<T>::operator=(Under<T>&& rhs)
{
  mValue = From(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the class-name
template <typename T> 
const String_t Enum<T>::className() const
{
  return ("Enum<" + String(typeid(T).name()) + ">");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set value to default
template <typename T> 
void Enum<T>::clear()
{
  Object::clear();
  mValue = T();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the value equal to default?
template <typename T> 
Bool_t Enum<T>::isEmpty() const 
{
  return (  Object::isEmpty() 
	 && mValue == T()
	 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! equality
/*! @note Only compares object member. */
template <typename T> 
Bool_t Enum<T>::isEqual(const Comparable& other) const
{
  if(isSame(other)) return kTRUE;
  try{ // attempt to cast
    const Enum<T>& o = dynamic_cast<const Enum<T>&>(other); 
    if(mValue != o.get())  return kFALSE;
    return kTRUE;
  }
  catch(const std::bad_cast& bc){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
template <typename T> 
Bool_t Enum<T>::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/*! @note Only compares object members. */
template <typename T> 
Int_t Enum<T>::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{  // attempt cast
      const Enum<T>& o = dynamic_cast<const Enum<T>&>(other); 
      if     (getValueAsUnder() < o.getValueAsUnder()) return -1;
      else if(getValueAsUnder() > o.getValueAsUnder()) return  1;
      else                   return  0;
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default Print content 
template <typename T> 
UShort_t Enum<T>::defaultPrintContent() const
{
  return kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print the "value"
template <typename T> 
void Enum<T>::printValue(std::ostream& os) const
{
  os << getValueAs<Int_t>();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type
template <typename T> 
T& Enum<T>::get()       
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type (const)
template <typename T> 
const T& Enum<T>::get() const 
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type (const)
template <typename T> 
const T& Enum<T>::operator()() const 
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type
template <typename T> 
T& Enum<T>::operator()()
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Unary plus (integer promotion)
template <typename T> 
const Under<T>& Enum<T>::operator+() const
{
  return static_cast< const Under<T>& >(mValue);    
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Static cast conversion operator
template <typename T> 
Enum<T>::operator Under<T>()
{
  return static_cast< Under<T> >(mValue);    
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access value as arbitrary integral type
template <typename T> 
template <typename U, typename>
const U& Enum<T>::getValueAs() const
{
  try{ return dynamic_cast<const U&>(mValue); }
  catch(const std::bad_cast& bc){
    loutE(kInput) << "Enum<T>::getValueAs<U>: Bad cast" << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access value as unerlying enumeration type
template <typename T> 
const Under<T>& Enum<T>::getValueAsUnder() const
{
  return dynamic_cast< const Under<T>& >(mValue);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Return enumeration-value from unerlying enumeration type
template <typename T> 
template <typename U, typename>
void Enum<T>::setValueFrom(const U& uval)
{
  try{ mValue = dynamic_cast<T>(uval); }
  catch(const std::bad_cast& bc){
    loutE(kInput) << "Enum<T>::setValue<U>: Bad cast" << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set object from template-type
template <typename T> 
void Enum<T>::set(const T& val)
{ 
  mValue = val; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set value from unerlying enumeration type
template <typename T> 
void Enum<T>::setValueFromUnder(const Under<T>& uval)
{
  mValue = dynamic_cast<const T&>(uval); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Interprete value as arbitrary integral type
template <typename T> 
template <typename U, typename>
U Enum<T>::As(const T& eval)
{
  try{ return dynamic_cast<U>(eval); }
  catch(const std::bad_cast& bc){
    loutE(kInput) << "Enum<T>::As<U>: Bad cast" << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Return enumeration-value from unerlying enumeration type
template <typename T> 
template <typename U, typename>
T Enum<T>::From(const U& uval)
{
  try{ return dynamic_cast<T>(uval); }
  catch(const std::bad_cast& bc){
    loutE(kInput) << "Enum<T>::From<U>: Bad cast" << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise NOT of enumeration
template <typename T> 
Under<T> Enum<T>::operator~() const
{
  return ~getValueAsUnder();   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of Enum
template <typename T> 
Under<T> Enum<T>::operator|(const Enum<T>& rhs) const
{
  return getValueAsUnder() | rhs.getValueAsUnder();   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of enumeration
template <typename T> 
Under<T> Enum<T>::operator|(const T& rhs) const
{
  return getValueAsUnder() | As< Under<T> >(rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of enumeration with base type
template <typename T> 
Under<T> Enum<T>::operator|(const Under<T>& rhs) const
{
  return getValueAsUnder() | rhs;   
}

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! Bitwise OR assignment of enumeration
// template <typename T> 
// Under<T>& Enum<T>::operator|=(const Enum<T>& rhs)
// {
//   return getValueAsUnder() |= rhs.getValueAsUnder();   
// }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of Enum
template <typename T> 
Under<T> Enum<T>::operator&(const Enum<T>& rhs) const
{
  return getValueAsUnder() & rhs.getValueAsUnder();   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of enumeration
template <typename T> 
Under<T> Enum<T>::operator&(const T& rhs) const
{
  return getValueAsUnder() & As< Under<T> >(rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of enumeration with base type
template <typename T> 
Under<T> Enum<T>::operator&(const Under<T>& rhs) const
{
  return getValueAsUnder() & rhs;   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Equivalence operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator==(const T& rhs)
{
  return (get() == rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Anti-equivalence operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator!=(const T& rhs)
{
  return (get() != rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Less than or equal operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator<=(const T& rhs)
{
  return (getValueAsUnder() <= As< Under<T> >(rhs));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Less than operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator<(const T& rhs)
{
  return (getValueAsUnder() < As< Under<T> >(rhs));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Less than or equal operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator>=(const T& rhs)
{
  return (getValueAsUnder() >= As< Under<T> >(rhs));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Less than operator with enumeration
template <typename T> 
Bool_t Enum<T>::operator>(const T& rhs)
{
  return (getValueAsUnder() > As< Under<T> >(rhs));
}

//  ****************************************************************************
//  ****************************************************************************
//  ****************************************************************************
#ifdef CERNROOT  // Using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Create the banches of a *flat* TTree corresponding to this object
template <typename T> 
Bool_t Enum<T>::createBranches(TTree& tree, const String& bName) const
{
  Bool_t suc(kFALSE);
  // determine the branch type
  String l("D");
  if     (typeid(Under<T>) == typeid(Bool_t))   l = "O";
  else if(typeid(Under<T>) == typeid(Int8_t))   l = "B";
  else if(typeid(Under<T>) == typeid(UInt8_t))  l = "b";
  else throw Exception("Unknown arithmetic type", "createBranches", "Enum<T>");
  // create the branch
  suc |= (0 != tree.Branch(bName.c_str(), (void*)&mValue, (bName+"/"+l).c_str()));
  // Warn on failure
  if(!suc) 
    loutW(kData) << "Enum<T>::createBranches: At least on branch failed." << endl;
  return suc;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Create the banches of a *flat* TTree corresponding to this object
template <typename T> 
Bool_t Enum<T>::setBranchAddrs(TTree& tree, const String& bName) const
{
  Bool_t suc(kFALSE);
  suc |= (0 <= tree.SetBranchAddress(bName.c_str(), (void*)&mValue));
  // Warn on failure
  if(!suc) 
    loutW(kData) << "Enum<T>::setBranchAddrs: At least on branch failed." << endl;
  return suc;
}

#endif // end using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR assignment of base type and Enum.
template <typename T>
Under<T>& operator|=(Under<T>& lhs, const Enum<T>& rhs)
{
  return lhs |= rhs.getValueAsUnder();   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND assignment of base type and Enum.
template <typename T>
Under<T>& operator&=(Under<T>& lhs, const Enum<T>& rhs)
{
  return lhs &= rhs.getValueAsUnder();   
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive, typename T> 
struct specialize<Archive, Pomona::Enum<T>, specialization::member_load_save_minimal>{}; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <typename T> struct convert< Pomona::Enum<T> >
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Enum<T>& rhs)
  {
    // write vector to node
    Node node;
    node = rhs.template as<Int_t>();  // UInt8_t don't seem to write :-/
    return std::move(node);
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::Enum<T>& rhs)
  {
    if(!node.IsScalar() || node.size() != 1) return kFALSE;
    Int_t val(node.as<Int_t>());
    rhs.template setValue<Int_t>(val);
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_ENUM_HH


//  end Enum.hh
//  ############################################################################
