//  ############################################################################
//! @file       Exception.hh
//! @brief      Enhanced exception handler
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_EXCEPTION_HH
#define POMONA_EXCEPTION_HH
#include "Types.hh"  // fundamental types
#include <exception>      // std: exception
#include <utility>        // std: utilities

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Enhanced exception handling class.
class Exception : public std::exception {
public:
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Default constructor
  inline Exception(const char* message   = "", 
		   const char* method    = "", 
		   const char* className = "") noexcept
    : std::exception() 
    , mClass(className) 
    , mMethod(method)
    , mMessage(message)
  { }
 
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Copy constructor.
  inline explicit Exception(const Exception& other) noexcept
    : std::exception(other)
    , mClass(other.mClass)
    , mMethod(other.mMethod)
    , mMessage(other.mMessage)
  { }
    

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Copy constructor from std::exception.
  inline explicit Exception(const std::exception& other) noexcept
    : std::exception(other)
    , mClass("")
    , mMethod("")
    , mMessage(other.what())
  { }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Move constructor.
  inline Exception(Exception&& other) noexcept
    : std::exception(std::move(other))
    , mClass(std::move(other.mClass))
    , mMethod(std::move(other.mMethod))
    , mMessage(std::move(other.mMessage))
  { }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Copy assignemnt operator
  inline Exception& operator=(const Exception& rhs) noexcept
  {
    if(this != &rhs){
      // copy
      std::exception::operator=(rhs); 
      mClass   = rhs.mClass; 
      mMethod  = rhs.mMethod; 
      mMessage = rhs.mMessage; 
    }
    return *this; 
  }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Copy assignemnt operator from std::exception
  inline Exception& operator=(const std::exception& rhs) noexcept
  { 
    if(this != &rhs){
      // copy
      exception::operator=(rhs); 
      mMessage = rhs.what(); 
    }
    return *this; 
  }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Move assignemnt operator
  inline Exception& operator=(Exception&& rhs) noexcept
  { 
    if(this != &rhs){
      // copy
      exception::operator=(rhs); 
      mClass   = rhs.mClass; 
      mMethod  = rhs.mMethod; 
      mMessage = rhs.mMessage;
      // clear rhs
      rhs.clear(); 
    }
    return *this; 
  }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Destructor
  inline virtual ~Exception(){ }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  const char* what() const noexcept
  {
    String_t theWhat("");
    if(!mClass.empty()){
      if(!mMethod.empty()) theWhat = mClass + "::"  + mMethod + ": " + mMessage;
      else                 theWhat = mClass + ":: "                  + mMessage;
    }
    else{
      if(!mMethod.empty()) theWhat =                  mMethod + ": " + mMessage;
      else                 theWhat =                                   mMessage;
    }
    return theWhat.c_str(); 
  }


  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Clear the text values
  inline void clear() noexcept
  { 
    mClass.clear(); 
    mMethod.clear(); 
    mMessage.clear();  
  }

private:
  String_t mClass;    //!< class associated with the exception
  String_t mMethod;   //!< method invoking the exception
  String_t mMessage;  //!< explainatory
};

} // end namespace Pomona

#endif // POMONA_EXCEPTION_HH

//  end Exception.hh
//  ############################################################################
