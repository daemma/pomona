//  ############################################################################
//! @file       HostPath.hh
//! @brief      Header for host system path handling
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_HOSTPATH_HH
#define POMONA_HOSTPATH_HH
#include "String.hh"      // pomona: base-class
#include "HostSystem.hh"  // pomona: Host OS interface

namespace Pomona {

//  ****************************************************************************
//! Host operating-system file or directory path utility.
class HostPath : public String
{
public: 
  explicit HostPath(const String& path = "");
  HostPath(const Char_t* path);
  HostPath(const HostPath& other);
  HostPath(HostPath&& other);
  HostPath& operator=(const HostPath& rhs);
  HostPath& operator=(HostPath&& rhs);
  HostPath& operator=(const String& rhs);
  HostPath& operator=(const Char_t* rhs);
  inline virtual ~HostPath(){ }

  // Comparable overloads
  virtual Int_t compare(const Comparable& other) const;
  // Printable overloads
  virtual UShort_t defaultPrintContent() const;
  
  // path sub-structure(s)
  HostPath dirName () const;
  HostPath baseName() const;
  String   extName () const;
  String   asAliased(const HostPath& aliasPath) const;
  Bool_t   access(const HostSystem::eAccessMode& mode = HostSystem::eAccessMode::kExists) const;
  Bool_t   exist() const;
  Bool_t   read() const;
  Bool_t   write() const;
  Bool_t   execute() const;

  // modifiers
  HostPath& append    (const HostPath& path);
  HostPath& prepend   (const HostPath& path);
  HostPath& replaceExt(const String& ext = "");
  HostPath& stripExt  ();

  // Arithmetic operators
  HostPath& operator+=(const String& rhs);
  HostPath& operator+=(const Char_t* rhs);

private: 
  CLASS_DEF(Pomona::HostPath,1)
};

// external arithmetic operators
HostPath operator+(const HostPath& lhs, const String& rhs);
HostPath operator+(const HostPath& lhs, const Char_t* rhs);

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive> 
struct specialize<Archive, Pomona::HostPath,
		  specialization::member_load_save_minimal>{}; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert< Pomona::HostPath >
{
  //! Encode into YAML Node
  static Node encode(const Pomona::HostPath& rhs)
  {
    return convert<Pomona::String>::encode(rhs);
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::HostPath& rhs)
  {
    return convert<Pomona::String>::decode(node, static_cast<Pomona::String&>(rhs));
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_HOSTPATH_HH

//  end HostPath.hh
//  ############################################################################
