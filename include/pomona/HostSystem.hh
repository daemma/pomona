//  ############################################################################
//! @file       HostSystem.hh
//! @brief      Header for host os-system utility class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_HOSTSYSTEM_HH
#define POMONA_HOSTSYSTEM_HH
#include "Types.hh"  // pomona: basic types
#ifdef CERNROOT      // Using root
class TSystem;       // root: forward declare root class
#endif               // end using root

namespace Pomona {

//  ****************************************************************************
//! Host operating system interface.
/** All member functions are static. */
class HostSystem
{
public: 
  inline HostSystem(){}
  inline virtual ~HostSystem(){ }

#ifdef CERNROOT  // Using root
  static TSystem& System();
#endif           // end using root

  static UInt_t Pid();
  static void   Exit(const Int_t& code = 0, const Bool_t& performCleanup = kTRUE);
  static Int_t  Exec(const Char_t* cmd);
  static Int_t  MkDir(const Char_t* dir);

  static const Char_t* BaseName   (const Char_t* path);
  static const Char_t* DirName    (const Char_t* path);
  static const Char_t* PrependPath(const Char_t* path, String_t& fileName);

  //  **************************************************************************
  //! @enum eAccessMode Access mode for a system path
  /*! @note Values can be ORed. */
  enum class eAccessMode : UInt8_t { 
    kExists  = 0x00,  //!< existence
    kExecute = 0x01,  //!< executable
    kWrite   = 0x02,  //!< writable
    kRead    = 0x04   //!< readable
  };
  
  static Bool_t Access(const String_t& dir, 
		       const eAccessMode& mode = eAccessMode::kExists);


protected: 
  static const Char_t* BaseNameImp   (const Char_t* path);
  static const Char_t* DirNameImp    (const Char_t* path);
  static const Char_t* PrependPathImp(const Char_t* path, String_t& fileName);

private: 
  HostSystem(const HostSystem& other)          = delete;
  HostSystem(HostSystem&& other)               = delete;
  HostSystem& operator=(const HostSystem& rhs) = delete;
  HostSystem& operator=(HostSystem&& rhs)      = delete;
}; 

} // end namespace Pomona

#endif // end POMONA_HOSTSYSTEM_HH

//  end HostSystem.hh
//  ############################################################################
