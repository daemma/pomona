//  ############################################################################################
//! @file       IoArchive.hh
//! @brief      Methods for Object I/O
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2019-03-29
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_ARCHIVE_HH
#define POMONA_IO_ARCHIVE_HH
#include "Object.hh"         // pomona: base class
#include "Named.hh"          // pomona: named base class
#include "IoArchiveType.hh"  // pomona: archive-type definitions
#include "LogService.hh"     // pomona: log message service
#include <fstream>           // std: file streamers
#ifdef CERNROOT              // Using root
#include "TFile.h"           // root: root files
#endif                       // end using root

namespace Pomona {

namespace Io {

//  ****************************************************************************
//! Utility template class for facilitating Object I/O
template <typename U> 
class Archive 
{
public:
  Archive(U& obj = U(), const eArchive& arcType = gDefArchive);
  inline virtual ~Archive(){}

  const U&        get() const;
  void            set(U& obj);
  const eArchive& getType() const;
  void            setType(const eArchive& arcType);

  Bool_t write(std::ostream& os) const;
  Bool_t write(const Char_t* fileName) const;

  Bool_t read(std::istream& is);
  Bool_t read(const Char_t* fileName);

protected:
  U& get();
  String_t name() const;
  template <class V> Bool_t writeCereal(std::ostream& os) const;
  template <class V> Bool_t readCereal(std::istream& is);
  
private:
  // Class is non-copyable
  Archive(const Archive& other)          = delete;
  Archive(Archive&& other)               = delete;
  // Class is non-assignable
  Archive& operator=(const Archive& rhs) = delete;
  Archive& operator=(Archive&& rhs)      = delete;

private:
  //! Pointer to Object for I/O
  U* mObject;
  //! Archive type
  eArchive mArcType;
}; 

//  ****************************************************************************
//  ****************************************************************************
//  Template implementations
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: default
template <typename U> 
Archive<U>::Archive(U& obj, const eArchive& arcType)
  : mObject(&obj)
  , mArcType(arcType)
{
  AssertObject<U>(obj);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object (const)
template <typename U> 
const U& Archive<U>::get() const
{ 
  if(mObject == nullptr)
    loutE(kObject) << "Archive::get: Null object pointer" << endl;;
  return (*mObject); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object (const)
template <typename U> 
U& Archive<U>::get()
{ 
  if(mObject == nullptr)
    loutE(kObject) << "Archive::get: Null object pointer" << endl;
  return (*mObject); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set object 
template <typename U> 
void Archive<U>::set(U& obj)
{ 
  mObject = &obj; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get archive-typex 
template <typename U> 
const eArchive& Archive<U>::getType() const
{ 
  return mArcType; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set archive-type 
template <typename U> 
void Archive<U>::setType(const eArchive& arcType)
{ 
  mArcType = arcType; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the object name 
template <typename U> 
String_t Archive<U>::name() const
{
  // return get().className();
  // return get().getName();
  // return (get().isNamed() ? get().getName() : get().className());
  String_t n = (get().isNamed() ? get().getName() : get().className());
#ifdef CERNROOT  // Using root
  if(getType() == eArchive::kRoo && !get().isNamed()) n = "Pomona::" + n;
#endif
  return n;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Write object to cereal out-stream 
template <typename U> 
template <class V>
Bool_t Archive<U>::writeCereal(std::ostream& os) const
{
  try{
    V oa(os); 
    oa(cereal::make_nvp(name(), get())); 
    return kTRUE;
  } catch(const std::exception& e){
    loutE(kFile) << "Archive::write(std::ostream): cereal archive failed: " << e.what() << endl;
    return kFALSE;
  }
}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Write object to out-stream 
template <typename U> 
Bool_t Archive<U>::write(std::ostream& os) const
{
  switch(mArcType){
  case eArchive::kJsn : return writeCereal<cereal::JSONOutputArchive>  (os);
  case eArchive::kXml : return writeCereal<cereal::XMLOutputArchive>   (os);
  case eArchive::kCnb : return writeCereal<cereal::BinaryOutputArchive>(os);
  case eArchive::kCsv : { get().printCsv(os, kTRUE); return kTRUE; }

#ifdef YAMLCPP  // using yaml-cpp 
  case eArchive::kYml : 
    {
      YAML::convert<U> converter;              // create converter
      YAML::Node node;                         // create yaml-cpp node
      node[name()] = converter.encode(get());  // embed object into node
      os << node;                              // stream node
      return kTRUE;                            // return success
    }
#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root
  case eArchive::kRoo : 
    {
      loutE(kFile) << "Archive::write(std::ostream): Cannot write root files to streams." << endl;
      return kFALSE;
    }
#endif          // end using root
  
  default : 
    {
      loutA(kData) << "Archive::write(std::ostream): Unkown archive type." << endl;
      return kFALSE;
    }
  } // end switch on archive-type
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Write object to file 
template <typename U> 
Bool_t Archive<U>::write(const Char_t* fileName) const
{
  Bool_t ret(kFALSE);

  // check input
  if(fileName == nullptr || fileName[0] == '\0'){
    loutE(kInput) << "Archive::write(const Char_t*): Empty file name" << endl;
    return ret;
  }
  
  // Create out file stream 
  std::ios_base::openmode mode = std::ios_base::out;
  if(mArcType == eArchive::kCnb) mode |= std::ios_base::binary;
  std::ofstream ofs(fileName, mode);
  if(!ofs.is_open()){
    loutE(kInput) << "Archive::write(const Char_t*): Failed to open file streamer" << endl;
    return kFALSE;
  }

  // write to the stream
  switch(mArcType){
  case eArchive::kJsn : 
  case eArchive::kXml : 
  case eArchive::kCnb : 
    { ret = write(ofs); break; }
  case eArchive::kCsv : 
    { get().printCsvHead(ofs, kTRUE); ret = write(ofs); break; }

#ifdef YAMLCPP  // using yaml-cpp 
  case eArchive::kYml : 
    {
      ofs << "%YAML 1.2" << endl;
      ofs << "---" << endl;
      ret = write(ofs);
      if(ret) ofs << endl << "..." << endl;
      break;
    }
#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root
  case eArchive::kRoo : 
    {
      TFile rf(fileName, "RECREATE"); rf.cd();
      get().Write(); 
      rf.Write(); 
      rf.Close();
      ret = kTRUE;
      break;
    }
#endif           // end using root
  
  default :
    {
      loutA(kData) << "Archive::write(const Char_t*): Unkown archive type." << endl;
      break;
    }
  } // end switch on archive-type

  // clean-up
  ofs.close();
  if(!ret) loutE(kFile) << "Archive::write(const Char_t*): Failed to write archive file" << endl;
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read object from cereal in-stream 
template <typename U> 
template <class V>
Bool_t Archive<U>::readCereal(std::istream& is)
{
  try{
    V ia(is); 
    ia(cereal::make_nvp(name(), get())); 
    return kTRUE;
  } catch(const std::exception& e){
    loutE(kFile) << "Archive::read(std::istream): cereal archive failed: " << e.what() << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Write object to out-stream 
template <typename U> 
Bool_t Archive<U>::read(std::istream& is)
{  
  switch(mArcType){
  case eArchive::kJsn : return readCereal<cereal::JSONInputArchive>  (is);
  case eArchive::kXml : return readCereal<cereal::XMLInputArchive>   (is);
  case eArchive::kCnb : return readCereal<cereal::BinaryInputArchive>(is);
  case eArchive::kCsv : 
    {
      loutE(kData) << "Archive::read(std::istream): Cannot read CSV files from streams." << endl;
      return kFALSE;
    }

#ifdef YAMLCPP  // using yaml-cpp 
  case eArchive::kYml : 
    {
      YAML::convert<U> converter;                  // create converter
      YAML::Node node = YAML::Load(is);            // create yaml-cpp node
      return converter.decode(node[name()], get());  // set member object from node
    }
#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root
  case eArchive::kRoo : 
    {
      loutE(kData) << "Archive::read(std::istream): Cannot read root files from streams." << endl;
      return kFALSE;
    }
#endif          // end using root
  
  default : 
    {
      loutA(kData) << "Archive::read(std::istream): Unkown archive type." << endl;
      return kFALSE;
    }
  } // end switch on archive-type
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Write object to file 
template <typename U> 
Bool_t Archive<U>::read(const Char_t* fileName)
{
  Bool_t ret(kFALSE);

  // check input
  if(fileName == nullptr || fileName[0] == '\0'){
    loutE(kInput) << "Archive::read(const Char_t*): Empty file name" << endl;
    return ret;
  }
  
  // Create out file stream 
  std::ios_base::openmode mode = std::ios_base::in;
  if(mArcType == eArchive::kCnb) mode |= std::ios_base::binary;
  std::ifstream ifs(fileName, mode);
  if(!ifs.is_open()){
    loutE(kInput) << "Archive::read(const Char_t*): Failed to open file streamer" << endl;
    return kFALSE;
  }

  switch(mArcType){
  case eArchive::kJsn : 
  case eArchive::kXml : 
  case eArchive::kCnb : 
#ifdef YAMLCPP  // using yaml-cpp 
  case eArchive::kYml : 
#endif          // end using yaml-cpp
    {
      ret = read(ifs);
      break;
    }

  case eArchive::kCsv : 
    {
      loutA(kData) << "Archive::read(const Char_t*): CSV reading not implemented!" << endl;
      break;
    }

#ifdef CERNROOT  // Using root
  case eArchive::kRoo : 
    {
      // Open the ROOT file
      TFile rf(fileName, "read"); rf.cd();
      if(rf.IsZombie()){
	loutE(kFile) << "Archive::read(const Char_t*): " 
		     << "Failed to open TFile: " << fileName << endl;
	ret = kFALSE;
	break;
      }
      // read the object
      auto objPtr = dynamic_cast<U*>(rf.Get(name().c_str()));
      if(objPtr != nullptr){ 
	(*mObject) = (*objPtr); 
	ret = kTRUE; 
      } 
      else{
	loutE(kFile) << "Archive::read(const Char_t*): Object not Get(ten) from root file" << endl;
	ret = kFALSE;
      }
      rf.Close();
      break;
    }
#endif           // end using root
  
  default :
    {
      loutA(kData) << "Archive::read(const Char_t*): Unkown archive type." << endl;
      break;
    }
  } // end switch on archive-type

  // ceal-up
  ifs.close();
  if(!ret) loutE(kFile) << "Archive::read(const Char_t*): Failed to read archive file" << endl;
  return ret;
}

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_ARCHIVE_HH

//  end IoArchive.hh
//  ############################################################################################




// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! Archive enumerations as an array
// static const std::array<eArchive,6> gArchiveEnums = 
//   {eArchive::kJsn
//   ,eArchive::kXml
//   ,eArchive::kCnb
//   ,eArchive::kYml
//   ,eArchive::kCsv
//   ,eArchive::kRoo
//   };

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! File extension associated with an archive
// inline String Extension(const eArchive& arc = gDefArchive)
// {
//   switch(arc){
//   case eArchive::kJsn : return ".json";
//   case eArchive::kXml : return ".xml";
//   case eArchive::kCnb : return ".cnb";
//   case eArchive::kYml : return ".yaml";
//   case eArchive::kCsv : return ".csv";
//   case eArchive::kRoo : return ".root";
//   default :            
//     loutA(kInput) << "Archive::Extension: Unkown archive type." << endl;
//     return Archive::Extension(gDefArchive);
//   } // end switch on archive type  
// }

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! An archive enumeration associated with file extension
// inline eArchive ArcEnum(const String& ext = ".json")
// {
//   for(auto& arc : gArchiveEnums) if(ext == Extension(arc)) return arc;
//   return gDefArchive;
// }

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! Adjust file-name extension associated with an archive
// inline void AdjustExtension(String& fileName, const eArchive& arc = gDefArchive)
// {
//   String ext = Extension(arc);
//   if(!fileName.endsWith(ext)){ fileName += ext; }
// }

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! Write object to a file
// template <class U>
// Bool_t Write(const HostPath& path, const U& obj, const eArchive& arc = gDefArchive)
// {
//   return Write<U>(path.get(), obj, arc);
// }

// //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// //! Write object to all file types
// template <class U> 
// Bool_t WriteAll(const HostPath& path, const U& obj)
// {
//   // Check inputs
//   if(path.get().isEmpty()){ 
//     loutE(kInput) << "Archive::WriteAll: Empty path." << endl;
//     return kFALSE;
//   }

//   Bool_t ret(kFALSE);
//   for(auto& arc : Archive::gArchiveEnums){
//     if(arc == eArchive::kYml && !Project::HasYaml())     continue;
//     if(arc == eArchive::kRoo && !Project::HasCernRoot()) continue;

//     HostPath odir = path.dirName();
//     if(odir.access(HostSystem::eAccessMode::kExists)){
//       HostPath ofn(path);
//       Archive::AdjustExtension(ofn.get(), arc);
//       ret |= Archive::Write<U>(ofn.get(), obj, arc);
//       if(ret) loutI(kFile) << "Wrote " << ofn << endl;
//       else    loutE(kFile) << "Archive::WriteAll: Failed to write " << ofn << endl;
//     }
//     else      loutE(kFile) << "Archive::WriteAll: Out dir not found " << odir << endl;
//   }
//   return ret;
// }
