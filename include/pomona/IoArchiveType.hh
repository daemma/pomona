//  ############################################################################
//! @file       IoArchiveType.hh
//! @brief      Header for data archive-ing
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_ARCHIVETYPE_HH
#define POMONA_IO_ARCHIVETYPE_HH
#include "Types.hh"  // pomona: fundamental types

namespace Pomona {

//  ****************************************************************************
//! File input & output methods and utilities
namespace Io {

//  ****************************************************************************
//! @enum eArchive Archive archive-type enumeration.
/*! @note Values are exclusive. */
enum class eArchive : UInt8_t { 
  kNon = 0x00,  //!< None
  kJsn = 0x01,  //!< JavaScript Object Notation
  kXml = 0x02,  //!< eXtensible Markup Language
  kCnb = 0x03,  //!< Cereal Native Binary
  kCsv = 0x04,  //!< Comma Seperated Values
#ifdef YAMLCPP  //   using yaml-cpp 
  kYml = 0x05,  //!< YAML
#endif          //   end using yaml-cpp
#ifdef CERNROOT //   Using root
  kRoo = 0x06,  //!< root-system binary
#endif          //   end using root
  kAll = 0xFF   //!< All
};

//  ****************************************************************************
//! Default cereal archive type
static const eArchive gDefArchive = eArchive::kJsn;

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_ARCHIVETYPE_HH


//  end Archive.hh
//  ############################################################################
