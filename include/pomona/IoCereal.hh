//  ############################################################################
//! @file       IoCereal.hh
//! @brief      Helpers for cereal-ization: headers
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_CEREAL_HH
#define POMONA_IO_CEREAL_HH
#include "cereal/cereal.hpp"             // cereal: main/master include
#include "cereal/access.hpp"             // cereal: access
#include "cereal/types/polymorphic.hpp"  // cereal: poly & registration
#include "cereal/archives/json.hpp"      // cereal: JSON archive
#include "cereal/archives/xml.hpp"       // cereal: XML archive
#include "cereal/archives/binary.hpp"    // cereal: binary archive

//! <a href="https://uscilab.github.io/cereal/">cereal</a>
namespace cereal { }

//  ****************************************************************************
//! @def CNVP(var) Name-value-pair macro.
/** Creates a name value pair for the variable var with almost(!) the same name 
    as the variable */
#define CNVP(var) \
  cereal::make_nvp(String_t(#var).substr(1, String_t(#var).length()), var)

//  ****************************************************************************
//! @def CLASS_DEF_IOCEREAL() Class definition basic macro.
#define CLASS_DEF_CEREAL()       \
  private:		         \
    friend class cereal::access;

#endif // end POMONA_IO_CEREAL_HH

//  end Cereal.hh
//  ############################################################################
