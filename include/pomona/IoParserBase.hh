//  ############################################################################
//! @file       IoParserBase.hh
//! @brief      Header for file parser helper base class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-03-31
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_PARSER_BASE_HH
#define POMONA_IO_PARSER_BASE_HH
#include "Types.hh"   // pomona: fundamental types
#include <iosfwd>     // std: i/o-stream forward declarations
#include <memory>     // std: unique_ptr
using std::ifstream;  // std: in file-streamer

namespace Pomona {

class String;    // foward declare enhanced string
class HostPath;  // foward declare host system file path

namespace Io {

//  ****************************************************************************
//! Methods and classes for reading arbitrary files from disk
namespace Parser {

//! Alias for an input file streamer, std::unique_ptr<std::ifstream>
typedef std::unique_ptr<std::ifstream> IfsPtr_t;

//  ****************************************************************************
//! A file parser utility class.
class Base 
{
protected: 
  Base(const Char_t*   path, const Bool_t& isBin = kFALSE);
  Base(const String_t& path, const Bool_t& isBin = kFALSE);
  Base(const String&   path, const Bool_t& isBin = kFALSE);
  Base(const HostPath& path, const Bool_t& isBin = kFALSE);

public: 
  virtual ~Base();

  // Accessors
  const String_t& path() const;
  const UInt64_t& fileSize() const;
        Float_t   fileSizeMB() const;
  const Bool_t&   isGood() const;
  const Bool_t&   isBinary() const;

  virtual const Bool_t& open();
  UInt64_t curPos() const;
  void close() const;

protected: 
  void setIsBinary(const Bool_t& val = kTRUE);
  const ifstream& ifs() const;
        ifstream& ifs();

  Bool_t   ifsIsNull() const;
  Bool_t   ifsIsOpen() const;
  Bool_t   ifsIsGood() const;
  UInt64_t calcSize() const;
  Bool_t   openStream();

private: 
  //! Path to file
  String_t mPath;
  //! Input streamer for file
  IfsPtr_t mIfs;
  //! File size
  UInt64_t mFileSize;

protected: 
  //! Does the file "currently look good"
  Bool_t mIsGood;
  //! Is the file binary?
  Bool_t mIsBin;

private: 
  Base(const Base& other)          = delete;
  Base(Base&& other)               = delete;
  Base& operator=(const Base& rhs) = delete;
  Base& operator=(Base&& rhs)      = delete;
}; 

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_PARSER_BASE_HH

//  end IoParserBase.hh
//  ############################################################################
