//  ############################################################################
//! @file       IoParserBin.hh
//! @brief      Header for binary file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_PARSER_BIN_HH
#define POMONA_IO_PARSER_BIN_HH
#include "IoParserBase.hh"   // pomona: base class
#include <vector>            // std: vector

namespace Pomona {

namespace Io {

namespace Parser {

//  ****************************************************************************
//! A binary file parser utility class.
class Bin : public Base
{
public: 
  Bin(const Char_t* path);
  Bin(const String_t& path);
  inline virtual ~Bin() {}

protected: 
  Bool_t readStr(String_t& val, const UInt_t& length = 1);
  Bool_t readChr(Char_t&   val);
  Bool_t readInt(Int_t&    val);
  Bool_t readInt(UInt_t&   val);
  Bool_t readFlt(Float_t&  val);
  Bool_t readVec(std::vector<Float_t>& vals, const UInt_t& num);

private: 
  Bin(const Bin& other)          = delete;
  Bin(Bin&& other)               = delete;
  Bin& operator=(const Bin& rhs) = delete;
  Bin& operator=(Bin&& rhs)      = delete;
}; 

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_PARSER_BIN_HH

//  end IoBin.hh
//  ############################################################################
