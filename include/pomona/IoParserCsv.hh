//  ############################################################################
//! @file       IoParserCsv.hh
//! @brief      Header for text file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-03-31
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_PARSER_CSV_HH
#define POMONA_IO_PARSER_CSV_HH
#include "IoParserTxt.hh"  // pomona: text paraser class

namespace Pomona {

namespace Io {

namespace Parser {

//! Alias for a parsed line from CSV file
typedef std::vector<String> Row_t;

//  ****************************************************************************
//! A text file parser utility class.
class Csv : public Txt
{
public: 
  Csv(const Char_t*   path, const Char_t& delim = ',');
  Csv(const String_t& path, const Char_t& delim = ',');
  Csv(const String&   path, const Char_t& delim = ',');
  Csv(const HostPath& path, const Char_t& delim = ',');
  inline virtual ~Csv(){}

  const Char_t& delim() const;
  void setDelim(const Char_t& delim);
  
  virtual Row_t getRow();

private: 
  Char_t mDelim;  //!< Column deliminator

private: 
  Csv(const Csv& other)          = delete;
  Csv(Csv&& other)               = delete;
  Csv& operator=(const Csv& rhs) = delete;
  Csv& operator=(Csv&& rhs)      = delete;
}; 

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_PARSER_CSV_HH


//  end IoParserCsv.hh
//  ############################################################################
