//  ############################################################################
//! @file       IoParserTxt.hh
//! @brief      Header for text file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_PARSER_TXT_HH
#define POMONA_IO_PARSER_TXT_HH
#include "IoParserBase.hh"  // pomona: paraser class
#include <vector>           // std: vector container

namespace Pomona {

namespace Io {

namespace Parser {

//! Alias for an input buffer, std::unique_ptr<Char_t[]>
typedef std::unique_ptr<Char_t[]> BufPtr_t;

//  ****************************************************************************
//! A text file parser utility class.
class Txt : public Base
{
public: 
  Txt(const Char_t*   path);
  Txt(const String_t& path);
  Txt(const String&   path);
  Txt(const HostPath& path);
  inline virtual ~Txt(){}

  void setBufferSize(const UShort_t& bufSz);

  virtual Bool_t getLine(String& line, const Char_t& delim = '\n');

  static const UShort_t& DefBufferSize();
  static std::vector<String> Tokenize(const String& str, const Char_t& delim = ',');
  
protected: 
  const UShort_t& bufferSize() const;
  const BufPtr_t& bufferPtr() const;
        BufPtr_t& bufferPtr();
  const Char_t*   buffer() const;
        Char_t*   buffer();

private: 
  void init(const UShort_t& bufSz);
  
private: 
  UShort_t  mBufSz;   //!< Size of the buffer array
  BufPtr_t  mBuffer;  //!< Buffer for reading lines

private: 
  //! Default size for line of text buffer character array
  static constexpr UShort_t mDefBufSize = 8192;

private: 
  Txt(const Txt& other)          = delete;
  Txt(Txt&& other)               = delete;
  Txt& operator=(const Txt& rhs) = delete;
  Txt& operator=(Txt&& rhs)      = delete;
}; 

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona

#endif // end POMONA_IO_PARSER_TXT_HH


//  end IoParserTxt.hh
//  ############################################################################
