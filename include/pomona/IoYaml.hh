//  ############################################################################
//! @file       IoYaml.hh
//! @brief      Helpers for yaml-ization: headers
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_IO_YAML_HH
#define POMONA_IO_YAML_HH
#ifdef YAMLCPP              // using yaml-cpp 
#include "yaml-cpp/yaml.h"  // yaml-cpp: API

//  ****************************************************************************
//! @def CLASS_DEF_YAML(name) Class definition yaml macro.
#define CLASS_DEF_YAML(name)          \
  private:		              \
    friend class YAML::convert<name>; 
#endif  // end using yaml-cpp

#endif // end POMONA_IO_YAML_HH

//  end Yaml.hh
//  ############################################################################
