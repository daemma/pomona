//  ############################################################################
//! @file       LogLevel.hh
//! @brief      Log message level enumeration
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_LOG_LEVEL_HH
#define POMONA_LOG_LEVEL_HH
#include "Types.hh"  // pomona: fundamental types

namespace Pomona {

//  ****************************************************************************
//! Namespace encapsulating all logging objects and methods.
namespace Log {

// Here's some example levels from: 
// https://github.com/kvz/bash3boilerplate/blob/master/main.sh
// debug: 
//  "Info useful to developers for debugging the application, 
//   not useful during operations."
// info:  
//  "Normal operational messages - 
//   may be harvested for reporting, measuring throughput, etc. - 
//   no action required."
// notice: 
//   "Events that are unusual but not error conditions - 
//    might be summarized in an email to developers or admins 
//    to spot potential problems - no immediate action required."
// warning: 
//   "Warning messages, not an error, but indication that an error 
//    will occur if action is not taken, e.g. file system 85% full - 
//    each item must be resolved within a given time. This is a debug message"
// error: 
//   "Non-urgent failures, these should be relayed to developers or admins; 
//    each item must be resolved within a given time."
// critical: 
//   "Should be corrected immediately, but indicates failure in a primary system, 
//    an example is a loss of a backup ISP connection."
// alert: 
//   "Should be corrected immediately, therefore notify staff who can fix the problem. 
//    An example would be the loss of a primary ISP connection."
// emergency: 
//   "A \"panic\" condition usually affecting multiple apps/servers/sites. 
//    At this level it would usually notify all tech staff on call."


//  ****************************************************************************
//! @enum eLevel Log message level.
/*! @note Values are exclusive. */
enum class eLevel : Int8_t { 
  kGuru     = 10, //!< Dump all the things!
  kVerbose  =  9, //!< Debug deep dive
  kDebug    =  8, //!< Useful for devs, not for ops
  kInfo     =  7, //!< Normal ops messages, i.e. throughput, etc.
  kProgress =  6, //!< Progess bar, for time intensive ops
  kNotice   =  5, //!< Unusual event, but not an error
  kWarning  =  4, //!< Strange event, error may be eminent 
  kError    =  3, //!< Error has occured, unexpceted behavior ahead
  kAlert    =  2, //!< Substantial error that should be corrected
  kCritical =  1, //!< Serious error that must be corrected immediatly
  kFatal    =  0, //!< You're done-ski
  kSilent   = -1  //!< Don't print any messages
};

//  ****************************************************************************
//! Default log-level
static const eLevel gDefLevel = eLevel::kInfo;

} // end namespace Log

} // end namespace Pomona

#endif // end POMONA_LOG_LEVEL_HH

//  end LogLevel.hh
//  ############################################################################
