//  ############################################################################
//! @file       LogProgressBar.hh
//! @brief      Header for ProgressBar class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_PROGRESSBAR_HH
#define POMONA_PROGRESSBAR_HH
#include "LogTopic.hh"       // pomona: log message topics
#include "Stopwatch.hh"  // pomona: process timer

namespace Pomona {

namespace Log {

//  ****************************************************************************
//! Utility class to facilitate printing elapsed progress.
class ProgressBar {
public:
  ProgressBar(const Log::eTopic& topic = Log::eTopic::kEval);
  inline virtual ~ProgressBar() { }

  const Float_t& fraction() const;

  void printFraction(const Float_t& fraction);
  void printFraction(const UShort_t& numerator, const UShort_t& denomenator);
  void printFraction(const UInt_t& numerator, const UInt_t& denomenator);
  void printFraction(const ULong64_t& numerator, const ULong64_t& denomenator);
  void printFinish();

  // misc utilities
  static void printProgressBarExample();

protected: 
  ProgressBar(const ProgressBar& other) = delete;
  ProgressBar(ProgressBar&& other) = delete;
  ProgressBar& operator=(const ProgressBar& rhs) = delete;
  ProgressBar& operator=(ProgressBar&& rhs) = delete;

private:
  //! log message topic
  Log::eTopic mTopic;
  //! total progress: [0., 1.)
  Float_t mFraction;
  //! Run-time timer
  Stopwatch mTimer;
};

} // end namespace Log

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Progress-bar alias for top-level namespace
typedef Log::ProgressBar ProgBar;

} // end namespace Pomona

#endif // end POMONA_PROGRESSBAR_HH


//  end LogProgressBar.hh
//  ############################################################################
