//  ############################################################################
//! @file       LogService.hh
//! @brief      Header for log message utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_LOG_SERVICE_HH
#define POMONA_LOG_SERVICE_HH
#include "LogLevel.hh"  // pomona: log message level enumeration
#include "LogTopic.hh"  // pomona: log message topic enumeration
#include <iostream>     // std: io-streamers
using std::cout;        // std: generic output stream
using std::endl;        // std: end-line

namespace Pomona {

namespace Log {

// Pomona Forward declarations
class  Object;
struct StreamConfig;

//  ****************************************************************************
//! A log-message utility service class (instanton).
class Service {
public:
  static Service& instance();
  virtual ~Service();

  // message level
  void setLevel(const eLevel& level);
  const eLevel& level() const;
  static const String_t&  LevelName (const eLevel& level);
  static const String_t&  LevelTitle(const eLevel& level);
  static const eLevel&    AsLevel   (const String_t& str);
  static void PrintLevel(std::ostream& os, const eLevel& level);
  static void PrintLevelHelp(std::ostream& os, const String_t& indent = "");
  static void PrintLevelExample();

  // message topic
  static const String_t&  TopicName (const eTopic& topic);
  static const String_t&  TopicTitle(const eTopic& topic);
  static void             PrintTopic(std::ostream& os, const eTopic& topic);

  // message prefix
  void setPrefix(const Char_t* str);
  const Char_t* prefix() const;
  void printPrefix(std::ostream& os) const;

  // message meta tags
  void setStyle(const UInt8_t& style);
  const UInt8_t& style() const;
  Bool_t isPrintTags() const;
  Bool_t isPrintPrefix() const;
  Bool_t isPrintPid() const;
  Bool_t isPrintDate() const;
  Bool_t isPrintTime() const;
  Bool_t isPrintColor() const;
  
  // message counts
  UInt_t msgCount() const;
  UInt_t msgCount(const eLevel& level) const;

  // streams
  size_t addStream(const eLevel& level, std::ostream* os = 0, 
		   const UShort_t& topic = gDefLogTopic) const;
  size_t addStream(const eLevel& level, const Char_t* fileName, 
		   Bool_t append = kFALSE, 
		   const UShort_t& topic = gDefLogTopic) const;
  std::ostream* progressStream(eTopic topic = eTopic::kNone);

  // logging
  std::ostream& log(const eLevel& level = gDefLevel, 
		    const eTopic& topic = eTopic::kNone) const;

#ifdef CERNROOT                   // Using root
  // root-system message level
  static void SetRootLevel(const Int_t& level);
  static void SetRootLevel(const String_t& level);
  static const Int_t& RootLevel();
  static void PrintRootLevelHelp(std::ostream& os, const String_t& indent = "");
#endif                            // end using root

private:
  explicit Service();
  Service(const Service& other)          = delete;
  Service(Service&& other)               = delete;
  Service& operator=(const Service& rhs) = delete;
  Service& operator=(Service&& rhs)      = delete;

  const StreamConfig& streamConfig(Int_t id) const;
  size_t addStream(const StreamConfig& sc) const;
  Int_t activeStream(eLevel level, eTopic topic, 
		     const Object* obj = 0) const;

private:
  //! Run-time log service verbosity
  eLevel mLevel;
  //! Print message tags
  UInt8_t mStyle;
  //! App-name log-message prefix tag
  String_t mPrefix;
};

std::ostream& operator<<(std::ostream& os, const eLevel& level);

} // end namespace Log

// External utlities and operators
std::ostream& Logger(const Log::eLevel& level = Log::gDefLevel, 
		     const Log::eTopic& topic = Log::eTopic::kNone);

} // end namespace Pomona


//  ****************************************************************************
//  Shortcut streamer definitions, etc.
#define logger()  Pomona::Log::Service::instance()
#define llevel()  logger().level()
#define logLIG(l) (llevel()>=Pomona::Log::eLevel::l)
#define lout(l,t) logger().log(Pomona::Log::eLevel::l, Pomona::Log::eTopic::t)
#define loutG(t)  lout(kGuru,     t)
#define loutV(t)  lout(kVerbose,  t)
#define loutD(t)  lout(kDebug,    t)
#define loutI(t)  lout(kInfo,     t)
#define loutP(t)  lout(kProgress, t)
#define loutN(t)  lout(kNotice,   t)
#define loutW(t)  lout(kWarning,  t)
#define loutE(t)  lout(kError,    t)
#define loutA(t)  lout(kAlert,    t)
#define loutC(t)  lout(kCritical, t)
#define loutF(t)  lout(kFatal,    t)

#endif // end POMONA_LOG_SERVICE_HH

//  end LogService.hh
//  ############################################################################
