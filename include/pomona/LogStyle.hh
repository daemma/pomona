//  ############################################################################
//! @file       LogStyle.hh
//! @brief      Log message style enumeration
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_LOG_STYLE_HH
#define POMONA_LOG_STYLE_HH
#include "Types.hh"  // pomona: fundamental types

namespace Pomona {

namespace Log {

//  ****************************************************************************
//! @enum eStyle Log message style.
/*! @note Values can be bit-wise ORed. */
enum class eStyle : UInt8_t { 
  kNone   = 0x00, //!< no tags
  kPrefix = 0x01, //!< app prefix
  kPid    = 0x02, //!< process ID
  kDate   = 0x04, //!< date
  kTime   = 0x08, //!< time
  kColor  = 0x10, //!< colorized
  kAll    = 0xFF  //!< all tags
};

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Cast const eStyle enumeration class reference as an (unsigned short) integer.
inline UInt8_t AsInt(const eStyle& ls)
{ 
  return static_cast<UInt8_t>(ls); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Cast eStyle enumeration class reference as an (unsigned short) integer.
inline UInt8_t AsInt(eStyle& ls)
{ 
  return static_cast<UInt8_t>(ls); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise NOT of eStyle enumerations.
inline UInt8_t operator~(const eStyle& ls)
{ 
  return ~AsInt(ls); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of eStyle enumerations.
inline UInt8_t operator|(const eStyle& lhs, const eStyle& rhs)
{ 
  return AsInt(lhs) | AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of eStyle enumeration and unsigned short integer.
inline UInt8_t operator|(const eStyle& lhs, const UInt8_t& rhs)
{ 
  return AsInt(lhs) | rhs; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of unsigned short integerand eStyle enumerations.
inline UInt8_t operator|(const UInt8_t& lhs, const eStyle& rhs)
{ 
  return lhs | AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR assignment of unsigned short integerand eStyle enumerations.
inline UInt8_t& operator|=(UInt8_t& lhs, const eStyle& rhs)
{ 
  return lhs |= AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of eStyle enumerations.
inline UInt8_t operator&(const eStyle& lhs, const eStyle& rhs)
{ 
  return AsInt(lhs) & AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of eStyle enumeration and unsigned short integer.
inline UInt8_t operator&(const eStyle& lhs, const UInt8_t& rhs)
{ 
  return AsInt(lhs) & rhs; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of unsigned short integerand eStyle enumerations.
inline UInt8_t operator&(const UInt8_t& lhs, const eStyle& rhs)
{ 
  return lhs & AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND assignment of unsigned short integerand eStyle enumerations.
inline UInt8_t& operator&=(UInt8_t& lhs, const eStyle& rhs)
{ 
  return lhs &= AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Equivalence of unsigned short integer and eStyle enumerations.
inline Bool_t operator==(const UInt8_t& lhs, const eStyle& rhs)
{ 
  return (lhs == AsInt(rhs)); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Anti-equivalence of unsigned short integer and eStyle enumerations.
inline Bool_t operator!=(const UInt8_t& lhs, const eStyle& rhs)
{ 
  return !(lhs == AsInt(rhs)); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default log-style
static const UInt8_t gDefStyle = (eStyle::kPrefix | eStyle::kColor);

} // end namespace Log

} // end namespace Pomona

#endif // end POMONA_LOG_STYLE_HH


//  end LogStyle.hh
//  ############################################################################
