//  ############################################################################
//! @file       LogTerminalColors.hh
//! @brief      Tools and utilities for colorizing terminal output
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_LOG_TERMINALCOLORS_HH
#define POMONA_LOG_TERMINALCOLORS_HH
#include "Types.hh"  // pomona: fundamental types
#include <iostream>       // std: io-streamers

namespace Pomona {

namespace Log {

//  ****************************************************************************
//! @enum eTermColor Terminal output styles
/*! @note Values are exclusive. */
enum class eTermStyle : UInt8_t { 
  kRegular   =  0,  //!< 
  kBright    =  1,  //!< 
  kDim       =  2,  //!< 
  kUnderline =  4,  //!< 
};

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! eTermStyle output streamer
inline std::ostream& operator<<(std::ostream& os, const eTermStyle& style)
{
  os << static_cast<UShort_t>(style);
  return os;
}

//  ****************************************************************************
//! @enum eTermColor Terminal output colors
/** @note Values are exclusive. */
enum class eTermColor : Int8_t { 
  kFgBlack    =  30, //!< black
  kFgRed      =  31, //!< red
  kFgGreen    =  32, //!< green
  kFgYellow   =  33, //!< yellow
  kFgBlue     =  34, //!< blue
  kFgViolet   =  35, //!< violet
  kFgCyan     =  36, //!< cyan
  kFgWhite    =  37, //!< white

  kFgHiBlack  =  90, //!< black, hi-contrast
  kFgHiRed    =  91, //!< red, hi-contrast
  kFgHiGreen  =  92, //!< green, hi-contrast
  kFgHiYellow =  93, //!< yellow, hi-contrast
  kFgHiBlue   =  94, //!< blue, hi-contrast
  kFgHiViolet =  95, //!< violet, hi-contrast
  kFgHiCyan   =  96, //!< cyan, hi-contrast
  kFgHiWhite  =  97, //!< white, hi-contrast

  kBgBlack    =  40, //!< black background
  kBgRed      =  41, //!< red background
  kBgGreen    =  42, //!< green background
  kBgYellow   =  43, //!< yellow background
  kBgBlue     =  44, //!< blue background
  kBgViolet   =  45, //!< violet background
  kBgCyan     =  46, //!< cyan background
  kBgWhite    =  47, //!< white background

  kBgHiBlack  =  100, //!< black background, hi-contrast
  kBgHiRed    =  101, //!< red background, hi-contrast
  kBgHiGreen  =  102, //!< green background, hi-contrast
  kBgHiYellow =  103, //!< yellow background, hi-contrast
  kBgHiBlue   =  104, //!< blue background, hi-contrast
  kBgHiViolet =  105, //!< violet background, hi-contrast
  kBgHiCyan   =  106, //!< cyan background, hi-contrast
  kBgHiWhite  =  107  //!< white background, hi-contrast
};

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! eTermColor output streamer
inline std::ostream& operator<<(std::ostream& os, const eTermColor& color)
{
  os << static_cast<Short_t>(color);
  return os;
}

//  ****************************************************************************
//! A utility for colorizing terminal output.
class TermColorMod {
public:
  //! Printed color
  eTermColor mColor;
  //! Printing style
  eTermStyle mStyle;
  //! Escape sequence
  static constexpr const Char_t* mEscapeSeq = "\033[";
  //! Color off code 
  static constexpr const Char_t* mColorOff  = "\033[0m";

public:
  TermColorMod(const eTermColor& color = eTermColor::kFgWhite,
	       const eTermStyle& style = eTermStyle::kRegular) 
    : mColor(color),
      mStyle(style)
  {}
  TermColorMod(const TermColorMod& other) = default;
  TermColorMod(TermColorMod&& other) = default;
  TermColorMod& operator=(const TermColorMod& rhs) = default;
  TermColorMod& operator=(TermColorMod&& rhs) = default;

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Print color modifiers to the stream
  friend inline std::ostream& operator<<(std::ostream& os, const TermColorMod& mod) 
  {
    os <<  mod.mEscapeSeq << mod.mStyle << ";" << mod.mColor << "m";
    return os;
  }
  
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //! Print "color off" code to stream
  static inline void ColorOff(std::ostream& os)
  {
    os << mColorOff;
  }
};

} // end namespace Log

} // end namespace Pomona

#endif // end POMONA_LOG_TERMINALCOLORS_HH


//  end LogTerminalColors.hh
//  ############################################################################
