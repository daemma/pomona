//  ############################################################################
//! @file       LogTopic.hh
//! @brief      Log message topic enumeration
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-08-16
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_LOG_TOPIC_HH
#define POMONA_LOG_TOPIC_HH
#include "Types.hh"  // pomona: fundamental types

namespace Pomona {

namespace Log {

//  ****************************************************************************
//! @enum eTopic Log message topic.
/*! @note Values can be bit-wise ORed. */
enum class eTopic : UShort_t { 
  kNone      = 0x0000, //!< no topic
  kInput     = 0x0001, //!< input arguments
  kEval      = 0x0002, //!< evalutaion
  kObject    = 0x0004, //!< object handling
  kData      = 0x0008, //!< data handling
  kFile      = 0x0010, //!< file I/O
  kPlot      = 0x0020, //!< plotting
  kMini      = 0x0040, //!< minimization
  kFit       = 0x0080, //!< fitting
  kInterp    = 0x0100, //!< interpolation
  kSimu      = 0x0200, //!< simulation
  kML        = 0x0400, //!< machine learning
  kAll       = 0xFFFF  //!< all topics
};

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Cast const eTopic enumeration class reference as an (unsigned short) integer.
inline UShort_t AsInt(const eTopic& lt)
{ 
  return static_cast<UShort_t>(lt); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Cast eTopic enumeration class reference as an (unsigned short) integer.
inline UShort_t AsInt(eTopic& lt)
{ 
  return static_cast<UShort_t>(lt); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise NOT of eTopic enumerations.
inline UShort_t operator~(const eTopic& lt)
{ 
  return ~AsInt(lt); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of eTopic enumerations.
inline UShort_t operator|(const eTopic& lhs, const eTopic& rhs)
{ 
  return AsInt(lhs) | AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of eTopic enumeration and unsigned short integer.
inline UShort_t operator|(const eTopic& lhs, const UShort_t& rhs)
{ 
  return AsInt(lhs) | rhs; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR of unsigned short integerand eTopic enumerations.
inline UShort_t operator|(const UShort_t& lhs, const eTopic& rhs)
{ 
  return lhs | AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise OR assignment of unsigned short integerand eTopic enumerations.
inline UShort_t& operator|=(UShort_t& lhs, const eTopic& rhs)
{ 
  return lhs |= AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of eTopic enumerations.
inline UShort_t operator&(const eTopic& lhs, const eTopic& rhs)
{ 
  return AsInt(lhs) & AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of eTopic enumeration and unsigned short integer.
inline UShort_t operator&(const eTopic& lhs, const UShort_t& rhs)
{ 
  return AsInt(lhs) & rhs; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND of unsigned short integerand eTopic enumerations.
inline UShort_t operator&(const UShort_t& lhs, const eTopic& rhs)
{ 
  return lhs & AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bitwise AND assignment of unsigned short integerand eTopic enumerations.
inline UShort_t& operator&=(UShort_t& lhs, const eTopic& rhs)
{ 
  return lhs &= AsInt(rhs); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Equivalence of unsigned short integer and eTopic enumerations.
inline Bool_t operator==(const UShort_t& lhs, const eTopic& rhs)
{ 
  return (lhs == AsInt(rhs)); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Anti-equivalence of unsigned short integer and eTopic enumerations.
inline Bool_t operator!=(const UShort_t& lhs, const eTopic& rhs)
{ 
  return !(lhs == AsInt(rhs)); 
}

//  ****************************************************************************
//! Default log-topic
static const UShort_t gDefLogTopic = AsInt(eTopic::kNone);

} // end namespace Log

} // end namespace Pomona

#endif // end POMONA_LOG_TOPIC_HH

//  end LogTopic.hh
//  ############################################################################
