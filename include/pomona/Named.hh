//  ############################################################################
//! @file       Named.hh
//! @brief      Header for generic named object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_NAMED_HH
#define POMONA_NAMED_HH
#include "String.hh"  // pomona: enhanced string

namespace Pomona {

//  ****************************************************************************
//! Generic top-level named object base-class.
class Named : public Object
{
public: 
  Named(const String& name = "", const String& title = "");
  Named(const Named& other);
  Named(const Named& other, const String& newName);
  Named(Named&& other);
  Named& operator=(const Named& rhs);
  Named& operator=(Named&& rhs);
  inline virtual ~Named(){}

  // AbsObject
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  // Comparable
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable
  virtual UShort_t defaultPrintContent() const;
          void     printName  (std::ostream& os) const;
          void     printTitle (std::ostream& os) const;
  virtual void     printArgs  (std::ostream& os) const;
  virtual void     printExtras(std::ostream& os) const;
  // Object
  Bool_t        isNamed() const;
  const Char_t* getName() const;

  // accessors
  const String& name() const;
        String& name();
  const String& title() const;
        String& title();
  // setters
  void setName     (const String& name);
  void setTitle    (const String& title);
  void setNameTitle(const String& name, const String& title);
  /// checkers
  Bool_t hasName() const;
  Bool_t hasTitle() const;
  Bool_t hasNameTitle() const;
  
#ifdef CERNROOT                   // Using root
  // TObject overloads
  virtual TObject*      Clone(const Char_t* newName = "") const;
  virtual const Char_t* GetName() const;
  virtual const Char_t* GetTitle() const;
#endif                            // end using root

protected: 
  //! Name
  String mName;
  //! Title
  String mTitle;

private: 
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(CNVP(mTitle));  // Name member is set externaly
  }

private: 
  CLASS_DEF(Pomona::Named,1)
}; 

//  ****************************************************************************
//! @struct is_named
/** @brief A template struct to dertmine if the template parameter inherits 
    from Named. 
*/
template <class T>
struct is_named : std::is_base_of<Named, T>{ };

//  ****************************************************************************
//! Template function to  dertmine if inheritence from Named. 
template <class T> 
Bool_t IsNamed(const T&){ return is_named<T>::value; }

//  ****************************************************************************
//! Returns either the name-member, if available, or the class-name of an object, if not
template <class U>
String_t GetObjectName(const U& obj)
{
  std::string name(obj.className());
  try{
    const Named& named = dynamic_cast<const Named&>(obj);
    if(!named.name().isEmpty()) name = named.name(); 
  }
  catch(const std::bad_cast& bc){}
  return name;
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::Named>
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Named& rhs)
  {
    Node node;
    // node["Name"]  = rhs.mName;  // Name member is set externaly
    node["Title"] = rhs.mTitle;
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::Named& rhs)
  {
    if(!node.IsMap() || node.size() < 1) return kFALSE;
    // rhs.mName  = node["Name"] .as<String_t>();    // Name member is set externaly
    rhs.mTitle = node["Title"].as<String_t>();
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_NAMED_HH

//  end Named.hh
//  ############################################################################
