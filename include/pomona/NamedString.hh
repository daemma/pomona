//  ############################################################################
//! @file       NamedString.hh
//! @brief      Header for NamedString class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_NAMEDSTRING_HH
#define POMONA_NAMEDSTRING_HH
#include "Named.hh"  // pomona: named type
// #include "String.hh"     // pomona: enhanced string class

namespace Pomona {

//  ****************************************************************************
//! Named string object
class NamedString : public Named {
public:
  NamedString(const String& name = "", const String& title = "", 
	      const String& val = "");
  NamedString(const NamedString& other);
  NamedString(const NamedString& other, const String& newName);
  NamedString(NamedString&& other);
  NamedString& operator=(const NamedString& rhs);
  NamedString& operator=(NamedString&& rhs);
  NamedString& operator=(String&& rhs);
  NamedString& operator=(const String& rhs);
  NamedString& operator=(const Char_t* rhs);
  inline virtual ~NamedString(){}

  // AbsObject
  virtual void     clear();
  virtual Bool_t   isEmpty() const;
  // Comparable
  virtual Bool_t   isEqual(const Comparable& other) const;
  virtual Bool_t   isSortable() const;
  virtual Int_t    compare(const Comparable& other) const;
  // Printable overloads
  virtual UShort_t defaultPrintContent() const;
  virtual void     printValue (std::ostream& os) const;
  virtual void     printArgs  (std::ostream& os) const;
  virtual void     printExtras(std::ostream& os) const;

  // Accessors
  const  String& get() const;
         String& get();
  const  String& operator()() const;
         String& operator()();
  void   set(const String& val);

  // Arithmetic operators
  NamedString& operator+=(const String& rhs);
  NamedString& operator+=(const Char_t* rhs);

protected:
  //! Typed member
  String mValue;

private: 
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(CNVP(mTitle)
      ,CNVP(mValue)
      );
  }

private: 
  CLASS_DEF(Pomona::NamedString,1)
};

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert< Pomona::NamedString >
{
  //! Encode into YAML Node
  static Node encode(const Pomona::NamedString& rhs)
  {
    Node node     = convert<Pomona::Named >::encode(rhs);
    node["Value"] = convert<Pomona::String>::encode(rhs.get());
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::NamedString& rhs)
  {
    if(!node.IsMap() || node.size() < 2) return kFALSE;
    convert<Pomona::Named>::decode(node, static_cast<Pomona::Named&>(rhs));
    rhs.set(node["Value"].as<Pomona::String>());
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_NAMEDSTRING_HH

//  end NamedString.hh
//  ############################################################################
