//  ############################################################################
//! @file       Object.hh
//! @brief      Header for a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_OBJECT_HH
#define POMONA_OBJECT_HH
#include "AbsObject.hh"   // pomona: abstract base class
#include "Comparable.hh"  // pomona: abstract base class
#include "Printable.hh"   // pomona: print utility
#include "ClassDef.hh"    // pomona: class definition macro

namespace Pomona {

//  ****************************************************************************
//! Generic top-level concrete base class.
class Object : public AbsObject, public Comparable, public Printable
#ifdef CERNROOT       // Using root
	     , public TObject
#endif                // end using root
{
public: 
  Object();
  Object(const Object& other);
  Object(Object&& other);
  Object& operator=(const Object& rhs);
  Object& operator=(Object&& rhs);
  inline virtual ~Object(){}

  // AbsObject overloads
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  // Printable overloads
  virtual UShort_t defaultPrintContent() const;
          void     printClassName(std::ostream& os) const;
  virtual void     printName     (std::ostream& os) const;

  // Named helpers
  Bool_t isNamed() const;
  const Char_t* getName() const;
  
#ifdef CERNROOT         // Using root
  // TObject overloads
  virtual TObject*      Clone(const Char_t* newName = "") const;
  virtual void          Clear(Option_t* option = "");
  virtual Bool_t        IsEqual(const TObject* other) const;
  virtual Bool_t        IsSortable() const;
  virtual Int_t         Compare(const TObject* other) const;
  virtual const Char_t* GetName() const;
  virtual const Char_t* GetTitle() const;
#endif                  // end using root

private: 
  //! cerealize
  template <class Archive> void serialize(Archive&){ }

private: 
  CLASS_DEF(Pomona::Object,1)
}; 

//  ****************************************************************************
//! @struct is_object
/** @brief A template struct to dertmine if the template parameter inherits 
    from Object. 
*/
template <class T>
struct is_object : std::is_base_of<Object, T>{ };

//  ****************************************************************************
//! Template function to dertmine if inheritence from Object. 
template <class T>
const Bool_t& IsObject(const T&){ return is_object<T>::value; }

//  ****************************************************************************
//! Template function to assert Pomona::Object-tivity
//! @note Uses compiler to check for Pomona::Object inheritance
template <class U>
void AssertObject(const U&)
{
  static_assert(is_object<U>::value, 
  		"Pomona::AssertObject: Class does not inherit from Pomona::Object.");
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::Object>
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Object&)
  {
    Node node;
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node&, Pomona::Object&)
  {
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

// //  ****************************************************************************
// //! @def MEMBNAME(var) Class member name as a std::string
// #define MEMBNAME(var) 
//   String_t(#var).substr(1, String_t(#var).length())

#endif // end POMONA_OBJECT_HH

//  end Object.hh
//  ############################################################################
