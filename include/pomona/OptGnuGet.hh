//  ############################################################################
//! @file       OptGnuGet.hh
//! @brief      Interfaces for GNU 'getopt' type-defs and methods.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_GNUGETOPT_HH
#define POMONA_GNUGETOPT_HH
#include "Exception.hh"  // pomona: enhanced exception
#ifdef GNUGETOPT              // Have GNU-getopt
#include <getopt.h>           // GNU option parsing
#endif

namespace Pomona {

//  ****************************************************************************
/*! @namespace Pomona::GetOpt 
    @brief     GNU getopt interface. 
*/
namespace GetOpt {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! @typedef Option_t GNU getopt option struct.
/** The struct option structure has these fields:
    - const char *name : This field is the name of the option. It is a string.
    - int has_arg      : This field says whether the option takes an argument. 
                         It is an integer, and there are three legitimate values: 
                         no_argument, required_argument and optional_argument.
    - int *flag        : If flag is a null pointer, then the val is a value 
                         which identifies this option. Often these values are 
			 chosen to uniquely identify particular long options. 
			 If flag is not a null pointer, it should be the address of 
			 an int variable which is the flag for this option. 
                         The value in val is the value to store in the flag to 
			 indicate that the option was seen. 
    - int val          : These fields control how to report or act on the option 
                         when it occurs.
*/
#ifdef GNUGETOPT  // Have GNU-getopt
typedef struct option Option_t;
#else
struct Option_t;
#endif

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! @enum eArgType GNU-getopt "has_arg" options.
/** Says whether the option takes an argument. 
    It is an integer, and there are three legitimate values: 
    no_argument, required_argument and optional_argument.
*/
enum class eArgType : Int8_t { 
#ifdef GNUGETOPT  // Have GNU-getopt
  kNon = no_argument,       //!< no argument
  kReq = required_argument, //!< required argument
  kOpt = optional_argument  //!< optional argument
#else
  kNon = 0, //!< no argument
  kReq = 1, //!< required argument
  kOpt = 2  //!< optional argument
#endif
};


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Wrapper for GNU-getopt "getopt_long".
/** 
    @code
    int getopt_long(int argc, char *const *argv, 
                    const char *shortopts, const struct option *longopts, 
  		    int *indexptr)
    @endcode

    Decode options from the vector argv (whose length is argc). 
    The argument shortopts describes the short options to accept, 
    just as it does in getopt. 
    The argument longopts describes the long options to accept (see above).

    When getopt_long encounters a short option, it does the same thing that getopt 
    would do: it returns the character code for the option, and stores the options 
    argument (if it has one) in optarg.

    When getopt_long encounters a long option, it takes actions based on the flag 
    and val fields of the definition of that option.

    If flag is a null pointer, then getopt_long returns the contents of val to 
    indicate which option it found. You should arrange distinct values in the val 
    field for options with different meanings, so you can decode these values after 
    getopt_long returns. 
    If the long option is equivalent to a short option, you can use the short 
    option’s character code in val.

    If flag is not a null pointer, that means this option should just set a flag in 
    the program. The flag is a variable of type int that you define. Put the address 
    of the flag in the flag field. Put in the val field the value you would like 
    this option to store in the flag. In this case, getopt_long returns 0.

    For any long option, getopt_long tells you the index in the array longopts of the 
    options definition, by storing it into *indexptr. You can get the name of the 
    option with longopts[*indexptr].name. So you can distinguish among long options 
    either by the values in their val fields or by their indices. 
    You can also distinguish in this way among long options that set flags.

    When a long option has an argument, getopt_long puts the argument value in the 
    variable optarg before returning. When the option has no argument, the value in 
    optarg is a null pointer. This is how you can tell whether an optional argument 
    was supplied.

    When getopt_long has no more options to handle, it returns -1, and leaves in the 
    variable optind the index in argv of the next remaining argument.
*/
inline Int_t Parse(Int_t argc, Char_t* const* argv, 
		   const Char_t* shortopts, const Option_t* longopts, 
		   Int_t* indexptr)
{
#ifdef GNUGETOPT  // Have GNU-getopt
  return getopt_long(argc, argv, shortopts, longopts, indexptr);  
#else
  throw Exception("Not compiled with GNU-getopt", "Parse", "GetOpt");
#endif
}
 
} // end namespace GetOpt

} // end namespace Pomona

#endif // end POMONA_GNUGETOPT_HH

//  end OptGnuGet.hh
//  ############################################################################
