//  ############################################################################
//! @file       Option.hh
//! @brief      Header for command-line Option
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_OPTION_HH
#define POMONA_OPTION_HH
#include "NamedString.hh"  // pomona: base class

namespace Pomona {

//  ****************************************************************************
//! An holder for a command-line option.
class Option : public NamedString {
public:
  Option(const String& name = "", 
	 const String& title = "",
	 const String& val = String(),
	 const Char_t& shortOpt = '\0', 
	 const Bool_t& isBool = kFALSE);
  Option(const Option& other);
  Option(Option&& other);
  Option& operator=(const Option& rhs);
  Option& operator=(Option&& rhs);
  Option& operator=(const Char_t* rhs);
  inline virtual ~Option(){ }

  // AbsObject
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  // Comparable
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable
  virtual void printArgs  (std::ostream& os) const;
  virtual void printExtras(std::ostream& os) const;

  // Set/Get
  const Char_t& shortOpt() const;
  void          setShortOpt(const Char_t& shortOpt = '\0');
  const Bool_t& isBool() const;
  void          setIsBool(const Bool_t& isBool = kTRUE);

  // Extra printers
  void printValueLine(std::ostream& os, 
  		      const String& indent = "  ", const UShort_t& nw = 20) const;
  void printHelpLine (std::ostream& os, 
		      const String& indent = "  ", const UShort_t& nw = 20) const;

protected:
  //! Character for "short option" flag
  String mShortOpt;
  //! Is this option a boolean flag?
  Bool_t mIsBool;

private: 
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(CNVP(mTitle)
      ,CNVP(mValue)
      ,CNVP(mShortOpt)
      ,CNVP(mIsBool));
  }

private: 
  CLASS_DEF(Pomona::Option,1)
};

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::Option> 
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Option& rhs)
  {
    Node node = convert<Pomona::NamedString>::encode(rhs);
    node["ShortOpt"] = rhs.mShortOpt;
    node["IsBool"]   = rhs.mIsBool;
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::Option& rhs)
  {
    if(!node.IsMap() || node.size() < 4) return kFALSE;
    convert<Pomona::NamedString>::decode(node, static_cast<Pomona::NamedString&>(rhs));
    rhs.mShortOpt = node["ShortOpt"].as<Char_t>();
    rhs.mIsBool   = node["IsBool"]  .as<Bool_t>();
    return kTRUE; // static_cast<UShort_t>(lt)
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_OPTION_HH

//  end Option.hh
//  ############################################################################
