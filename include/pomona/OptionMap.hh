//  ############################################################################
//! @file       OptionMap.hh
//! @brief      Header for collection of command-line options.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_OPTIONMAP_HH
#define POMONA_OPTIONMAP_HH
#include "SMap.hh"               // pomona: String:Object map
#include "Option.hh"             // pomona: command-line option
#include "LogService.hh"         // pomona: log-message level
#include <cereal/types/map.hpp>  // cereal: stl map support

namespace Pomona {

namespace App { class Application; } // forward delcare application class

//! Alias for a std::map of option name and Option.
typedef std::map<String,Option> OptionMap_t;

//  ****************************************************************************
//! A collection of command-line options.
class OptionMap : public Object, public OptionMap_t {
public:
  OptionMap();
  OptionMap(const OptionMap& other);
  OptionMap(OptionMap&& other);
  OptionMap& operator=(const OptionMap& rhs);
  OptionMap& operator=(OptionMap&& rhs);
  inline virtual ~OptionMap(){ }

  // AbsObject overloads
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  virtual Bool_t isEqual(const Comparable& other) const;

  // Accessors
  const String& exeName() const;
  const OptionMap_t& getMap() const;
  const Char_t& findShortOpt(const String& name) const;
  const Option& at(const Char_t& shorOpt) const;
  const Option& option(const String& name) const;
        Option& option(const String& name);
  const Option& option(const Char_t& shorOpt) const;
        Option& option(const Char_t& shorOpt);
  template <typename T> T value(const String& name) const;
  template <typename T> T value(const String& name);
  template <typename T> T value(const Char_t& shorOpt) const;
  template <typename T> T value(const Char_t& shorOpt);
  Bool_t isRequested(const Char_t& shorOpt) const;
  Bool_t isRequested(const String& name) const;

  // modifiers
  void   setExeName (const String& exeName);
  Bool_t insert(const Option& opt);
  template <typename T> Bool_t insert(const Char_t& shortOpt, const String& name, 
				      const String& title = "", const T& val = T());
  Bool_t insertBool  (const Char_t& shortOpt, const String& name, 
  		      const String& title = "", const Bool_t& val = kFALSE);
  Bool_t insertInt   (const Char_t& shortOpt, const String& name, 
  		      const String& title = "", const Int_t& val = 0);
  Bool_t insertUInt  (const Char_t& shortOpt, const String& name, 
  		      const String& title = "", const UInt_t& val = 0);
  Bool_t insertDouble(const Char_t& shortOpt, const String& name, 
  		      const String& title = "", const Double_t& val = 0.);
  Bool_t insertString(const Char_t& shortOpt, const String& name, 
  		      const String& title = "", const String& val = "");
  Bool_t insertLogLevel(const Log::eLevel& val = Log::gDefLevel);
  template <typename T> void setValue(const String& name,     const T& val);
  template <typename T> void setValue(const Char_t& shortOpt, const T& val);
  Bool_t erase(const String& name);
  void parseArgs(Int_t argc, Char_t** argv);

  // print(able)s
  void printValueLines(std::ostream& os, 
  		       const String& ind = "  ", const UShort_t& nw = 0) const;
  void printHelpLines(std::ostream& os, 
  		      const String& ind = "  ", const UShort_t& nw = 0) const;

protected: 
  friend class Application;
  OptionMap_t& getMap();
  Option& at(const Char_t& shorOpt);
  Option& operator[](const Char_t& shorOpt);
  void setMap(const OptionMap_t& optmap);
  void buildDerived();

protected: 
  //! Name of the associated executable
  String                  mExeName;
  //! Prefered ordering
  std::vector<Char_t>     mOrder;
  //! Short-option character mapped to long-option name
  std::map<Char_t,String> mCharMap;
  //! Maximum length of printed name field
  UShort_t                mNameLength;
  //! Non-option command-line arguments that were found
  std::vector<String>     mNonoptArgs;
  //! Short-option character mapped to "has been requested"
  std::map<Char_t,Bool_t> mRequesteds;

private:
  //! cerealize: save
  template<class Archive>
  void save(Archive& ar) const {
    ar(CNVP(mExeName)
      ,CNVP(mOrder)
      ,cereal::make_nvp("Options", cereal::base_class<OptionMap_t>(this))
      );
  }

  //! cerealize: load
  template<class Archive>
  void load(Archive& ar) {
    // read the archive
    ar(CNVP(mExeName)
      ,CNVP(mOrder)
      ,cereal::make_nvp("Options", cereal::base_class<OptionMap_t>(this))
      );
    // build derived quantities
    buildDerived();
  }

private: 
  CLASS_DEF(Pomona::OptionMap,1)
};


//  ****************************************************************************
//  ****************************************************************************
//  Template implementations
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert a typed option into the mapping.
template <typename T> 
Bool_t OptionMap::insert(const Char_t& so, const String& n, 
			 const String& t, const T& val)
{
  Option opt(n, t);  // construct Option
  opt.setShortOpt(so);  // set short option
  String sval; sval.fromArith<T>(val); opt.set(sval);  // set value
  // Set the has-arg
  if(typeid(T) == typeid(Bool_t)) opt.setIsBool(kTRUE);
  else                            opt.setIsBool(kFALSE);
  return insert(opt);  // return success
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the value of option mapped from the name (const)
/*! @warning Throws Exception if option not found or cannot be cast. */
template <typename T>
T OptionMap::value(const String& n) const
{
  try{ return std::move( option(n).get().asArith<T>() ); }
  catch(const Exception& e){
    loutE(kInput) << "OptionMap::value(" << n << ") (const): " << e.what() << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the value of option mapped from the name 
/*! @warning Throws Exception if option not found or cannot be cast. */
template <typename T>
T OptionMap::value(const String& n) 
{
  try{ return std::move( option(n).get().asArith<T>() ); }
  catch(const Exception& e){
    loutE(kInput) << "OptionMap::value(" << n << "): " << e.what() << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the value of option mapped from the short option character (const)
/*! @warning Throws Exception if option not found or cannot be cast. */
template <typename T>
T OptionMap::value(const Char_t& so) const
{
  try{ return std::move( option(so).get().asArith<T>() ); }
  catch(const Exception& e){
    loutE(kInput) << "OptionMap::value(" << so << ") (const): " << e.what() << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the value of option mapped from the short option character
/*! @warning Throws Exception if option not found or cannot be cast. */
template <typename T>
T OptionMap::value(const Char_t& so) 
{
  try{ return std::move( option(so).get().asArith<T>() ); }
  catch(const Exception& e){
    loutE(kInput) << "OptionMap::value(" << so << "): " << e.what() << endl;
    return T();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the value of option mapped from the name 
template <typename T> 
void OptionMap::setValue(const String& n, const T& val)
{
  option(n).get().fromArith<T>(val);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the value of option mapped from the name 
template <typename T> 
void OptionMap::setValue(const Char_t& so, const T& val)
{
  option(so).get().fromArith<T>();
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive> 
struct specialize<Archive, Pomona::OptionMap, specialization::member_load_save>{}; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::OptionMap> 
{
  //! Encode into YAML Node
  static Node encode(const Pomona::OptionMap& rhs)
  {
    Node node;
    node["ExeName"] = rhs.mExeName;
    node["Order"]   = rhs.mOrder;
    node["Options"] = rhs.getMap();
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::OptionMap& rhs)
  {
    if(!node.IsMap()|| node.size() < 2) return kFALSE;
    rhs.mExeName = node["ExeName"].as<String_t>();
    rhs.mOrder   = node["Order"]  .as< std::vector<Char_t> >();
    rhs.setMap( node["Options"].as<Pomona::OptionMap_t>() );
    rhs.buildDerived();
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_OPTIONMAP_HH

//  end OptionMap.hh
//  ############################################################################

  // std::vector<Char_t>     mOrder;
