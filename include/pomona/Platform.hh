//  ############################################################################
//! @file       Platform.hh
//! @brief      Platform-specific definitions and macros
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_PLATFORM_HH
#define POMONA_PLATFORM_HH

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Compiler directives (for portability ... maybe/hopefully?).
#ifdef _WIN64
#  define POMONA_OS_WIN       // Windows
#  define POMONA_OS_WIN64     // Windows, 64-bit
#elif _WIN32
#  define POMONA_OS_WIN       // Windows
#  define POMONA_OS_WIN32     // Windows, 32-bit
#elif __APPLE__
#  define POMONA_OS_UNIX      // Unix
#  define POMONA_OS_DARWIN    // Darwin
#elif __linux
#  define POMONA_OS_UNIX      // Unix
#  define POMONA_OS_GNULINUX  // GNU/Linux
#elif __unix
#  define POMONA_OS_UNIX      // all unices not caught above
#elif __posix
#  define POMONA_OS_POSIX     // POSIX
#endif

#endif // end POMONA_PLATFORM_HH

//  end Platform.hh
//  ############################################################################
