//  ############################################################################
//! @file       Pomona.hh
//! @brief      Header for top-level namespace
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-23
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_POMONA_HH
#define POMONA_POMONA_HH
#include "Project.hh"  // pomona: library information interface

//  ****************************************************************************
/*! @mainpage

    @author     Emma Hague 
    @date       2017
    @copyright  Copyright (C) 2016-2019 "Emma Hague" <dev@daemma.io>
                GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>

    @section Overview
    Pomona is for converting data file types, among other fruitful tasks.
*/

//  ****************************************************************************
//! Top-level namespace encapsulating all (most) objects and methods.
namespace Pomona { }  // end namespace Pomona

#endif // end POMONA_POMONA_HH

//  end Pomona.hh
//  ############################################################################
