/*  ########################################################################  */
/*! @file       PomonaLinkDef.h 
    @brief      Link definitions for root loadable objects. 
    @author     "Emma Hague" <dev@daemma.io>
    @date       2017-08-15
    @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
                GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
*/
#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Pomona::AbsObject+;
#pragma link C++ class Pomona::Comparable+;
#pragma link C++ class Pomona::Printable+;
#pragma link C++ class Pomona::Object+;
#pragma link C++ class Pomona::TimePoint-;
#pragma link C++ class Pomona::Date+;
#pragma link C++ class Pomona::String+;
#pragma link C++ class Pomona::Named+;
#pragma link C++ class Pomona::NamedString+;
#pragma link C++ class Pomona::SsMap-;
#pragma link C++ class Pomona::HostPath+;
#pragma link C++ class Pomona::Option+;
#pragma link C++ class Pomona::OptionMap+;
#pragma link C++ class Pomona::Root::Plot::Attrib+;

#endif /* end __CINT__ */


/*  end PomonaLinkDef.h */
/*  ########################################################################  */
