//  ############################################################################
//! @file       Printable.hh
//! @brief      Header file for class infromation printing utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_PRINTABLE_HH
#define POMONA_PRINTABLE_HH
#include "Types.hh"  // fundamental types
#include <iosfwd>         // std: i/o-stream forward declarations

namespace Pomona {

//  ****************************************************************************
//! An infromation printing utility for classes.
/*! A 'mix-in' base class that define the standard printing methods. 
    Each Printable implementation must define methods that print the objects name, 
    class name, title, value, arguments and extras to a provided stream. 
    The definition of value is class dependent. 
    The definition of arguments is also class dependent, but should always be 
    interpreted as the names (and properties) of any external inputs of a given 
    object.
    The extras method can be used to print any properties that does not fit in any
    of the other classes. 
    Each object an also override the definitions made in defaultPrintStyle and 
    defaultPrintContents to determine what is printed (in terms of contents) 
    and how it is printed (inline,single-line or multiline) given a Print() option 
    string. 
*/
class Printable 
{
public: 
  inline Printable(){ }
  inline Printable(const Printable&)            = default;
  inline Printable(Printable&&)                 = default;
  inline Printable& operator=(const Printable&) = default;
  inline Printable& operator=(Printable&&)      = default;
  inline virtual ~Printable(){ }

  //  **************************************************************************
  //! @enum eContents Contents to be printed.
  /*! @note Values can be ORed. */
  enum eContents { 
    kNA        = 0x00,  //!< not applicable
    kAddress   = 0x01,  //!< object address in memory
    kClassName = 0x02,  //!< class-name
    kName      = 0x04,  //!< name
    kValue     = 0x08,  //!< value
    kTitle     = 0x10,  //!< class-title
    kArgs      = 0x20,  //!< arguments
    kExtras    = 0x40   //!< extras
  };
  //! Get the default print content
  inline virtual UShort_t defaultPrintContent() const 
  { return kClassName; }

  //  **************************************************************************
  //! @enum eStyle Contents to be printed.
  /*! @note Values are exclusive. */
  enum eStyle { 
    kInline     = 1, //!< in-line (no new-line)
    kSingleLine = 2, //!< single line
    kStandard   = 3, //!< standard
    kVerbose    = 4, //!< verbose
    // kYaml       = 5, //!< YAML formatted
    // kJson       = 6, //!< JSON formatted
    // kXml        = 7, //!< XML formatted
    kCsv        = 8, //!< CSV formatted
  };
  //! Get the default print style
  inline virtual eStyle defaultPrintStyle() const
  { return kInline; }


  static std::ostream& DefaultPrintStream(std::ostream* os = nullptr);
  virtual void printStream(std::ostream& os, UShort_t content, eStyle style, 
			   const String_t& indent = "") const;

                 void printAddress  (std::ostream&) const;
  inline virtual void printClassName(std::ostream&) const{ }
  inline virtual void printName     (std::ostream&) const{ }
  inline virtual void printTitle    (std::ostream&) const{ }
  inline virtual void printValue    (std::ostream&) const{ }
  inline virtual void printArgs     (std::ostream&) const{ }
  inline virtual void printExtras   (std::ostream&) const{ }

  inline virtual void printCsv      (std::ostream&, const Bool_t& /*endLine*/ = kFALSE) const{ }
  inline virtual void printCsvHead  (std::ostream&, const Bool_t& /*endLine*/ = kFALSE) const{ }

  //! Set the length of the name field
  inline static void SetNameFieldPrintLength(const UShort_t& length)
  { mNameLength = length; }

#ifdef CERNROOT  // Using root
  //! Print method which can overload TObject::Print.
  inline virtual void Print(const Option_t*) const
  { printStream(DefaultPrintStream(), defaultPrintContent(), kSingleLine); }
#endif           // end using root

protected: 
  //! Length of field for name
  static UInt_t mNameLength;
#ifdef CERNROOT  // Using root
  ClassDef(Pomona::Printable,0);
#endif           // end using root
}; 

std::ostream& operator<<(std::ostream& os, const Printable& p);

} // end namespace Pomona

#ifndef __CINT__  // Not using root-system c-interpreter
using Pomona::operator<<;
#endif

#endif // end POMONA_PRINTABLE_HH

//  end Printable.hh
//  ############################################################################
