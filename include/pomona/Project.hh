//  ############################################################################
//! @file       Project.hh
//! @brief      Header file for library/project information utility class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-09-07
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_PROJECT_HH
#define POMONA_PROJECT_HH
#include "Types.hh"         // pomona: basic types
#include "LibConfig.hh"     // pomona: library configuration

namespace Pomona {

//  ****************************************************************************
//! Information about this project (library).
/** Essentially, this is an interface for configuration & compliation options. 
    @note All member functions are static.
*/
class Project
{
public: 
  inline Project(){}
  inline virtual ~Project(){ }

  static inline String_t Name()           { return PROJECT_NAME; }
  static inline String_t Moniker()        { return PROJECT_MONIKER; }
  static inline String_t Title()          { return PROJECT_TITLE; }
  static inline String_t Author()         { return PROJECT_AUTHOR; }
  static inline String_t Year()           { return PROJECT_YEAR; }
  static inline String_t Copyright()      { return PROJECT_COPYRIGHT; }
  static inline String_t LibName()        { return PROJECT_LIBNAME; }
  static inline String_t LibVersion()     { return PROJECT_VERSION; }
  static inline UInt8_t  LibVersionMajor(){ return UInt8_t(PROJECT_VERSION_MAJOR); }
  static inline UInt8_t  LibVersionMinor(){ return UInt8_t(PROJECT_VERSION_MINOR); }
  static inline UInt8_t  LibVersionPatch(){ return UInt8_t(PROJECT_VERSION_PATCH); }
  static inline String_t InstallPrefix()  { return PROJECT_INSTALL_PREFIX; }
  static inline String_t InstallCFlags()  { return PROJECT_INSTALL_CFLAGS; }
  static inline String_t InstallLdFlags() { return PROJECT_INSTALL_LDFLAGS; }

  static inline Bool_t   HasLogMsg(){ 
#ifdef LOGMSG 
  return kTRUE;
#else
  return kFALSE;
#endif
  }

  static inline Bool_t   HasYaml(){ 
#ifdef YAMLCPP 
  return kTRUE;
#else
  return kFALSE;
#endif
  }
  static inline String_t YamlVersion(){ return PROJECT_YAML_CPP_VERSION; }

  static inline Bool_t   HasCernRoot(){ 
#ifdef CERNROOT 
  return kTRUE;
#else
  return kFALSE;
#endif
  }
  static inline String_t RootVersion(){ return PROJECT_ROOT_VERSION; }
}; 

} // end namespace Pomona

#endif // end POMONA_PROJECT_HH


//  end Projct.hh
//  ############################################################################
