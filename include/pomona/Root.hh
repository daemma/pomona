//  ############################################################################
//! @file       Root.hh
//! @brief      Header file for TROOT interface
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ROOT_HH
#define POMONA_ROOT_HH
#ifdef CERNROOT         // Using root
#include "LogLevel.hh"  // pomona: log message level enumeration
#include <iosfwd>       // io-stream forward declarations
class TROOT;            // root: forward declare THE ROOT class

namespace Pomona {

//  ****************************************************************************
/*! @namespace Pomona::Root 
    @brief     CERN root-system interface. 
*/
namespace Root {

//  ****************************************************************************
/*! @class Pomona::Root::Root 
    @brief CERN root-system interface

    This is, essentially, an API for presenting usefull methods from root's 
    TROOT class. The idea here is to both limit "#include" overhead and 
    "compartmentalize" our usage of root-system inferface methods.
    All member functions are static.
*/
class Root
{
public: 
  inline Root(){}
  inline virtual ~Root(){ }

  static TROOT& Troot();
  static String_t Name();
  static String_t Title();

  static Bool_t IsBatch();
  static void   SetBatch(const Bool_t& flag = kTRUE);

  static void SetStyle(const String_t& name);
  static void ForceStyle();

  static String_t Info();
  static void PrintInfo(std::ostream& os);
  static void PrintInfo(const Log::eLevel& level = Log::eLevel::kDebug);

private: 
  Root(const Root& other)          = delete;
  Root(Root&& other)               = delete;
  Root& operator=(const Root& rhs) = delete;
  Root& operator=(Root&& rhs)      = delete;
}; 


} // end namespace Root

} // end namespace Pomona

#else   // Not using root
namespace Pomona {
  //! root-system interface
  namespace Root { class Root; }
}  // forward declare class
#endif  // end using root

#endif  // end POMONA_ROOT_HH

//  end Root.hh
//  ############################################################################
