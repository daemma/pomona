//  ############################################################################
//! @file       RootPlotAttrib.hh
//! @brief      Header file for plot attributes
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ROOT_PLOT_ATTRIB_HH
#define POMONA_ROOT_PLOT_ATTRIB_HH
#ifndef CERNROOT         // Not using root  
// forward declare class
namespace Pomona { namespace Root { namespace Plot { class Attrib; } } }
#else                    // Using root
#include "Object.hh"     // pomona: named base class
#include "TAttLine.h"    // root: line attributes class
#include "TAttMarker.h"  // root: marker attributes class
#include "TAttFill.h"    // root: fill attributes class

namespace Pomona {

namespace Root {

//  ****************************************************************************
/*! @namespace Pomona::Root::Plot 
    @brief     CERN root-system plotting & style interface. 
*/
namespace Plot {

//  ****************************************************************************
//! Attributes for plottable objects
class Attrib : public Object, 
	       public TAttLine, public TAttMarker, public TAttFill
{
public: 
  Attrib();
  Attrib(const Attrib& other);
  Attrib(Attrib&& other);
  Attrib& operator=(const Attrib& rhs);
  Attrib& operator=(Attrib&& rhs);
  inline virtual ~Attrib(){ }

  // AbsObject overloads
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  virtual Bool_t isEqual(const Comparable& other) const;

  // Printable Methods
  virtual UShort_t defaultPrintContent() const;
  virtual void     printValue(std::ostream& os) const;

  // Set attributes of this from other
  void setColor(const Color_t& color);
  void setLine (const Color_t& color, const Style_t& style, const Width_t& width, 
		const Float_t& alpha = 1.);
  void setMark (const Color_t& color, const Style_t& style, const Size_t& size, 
		const Float_t& alpha = 1.);
  void setFill (const Color_t& color, const Style_t& style, 
		const Float_t& alpha = 1.);

  // Set other object attributes from this
  void setObjAttLine(TAttLine&   line) const;
  void setObjAttMark(TAttMarker& mark) const;
  void setObjAttFill(TAttFill&   fill) const;
  void setObjAtt    (TObject&    obj) const;

protected: 
  virtual void copyAttribs(const Attrib& other);

private:
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(CNVP(fLineColor)
      ,CNVP(fLineStyle)
      ,CNVP(fLineWidth)
      ,CNVP(fMarkerColor)
      ,CNVP(fMarkerStyle)
      ,CNVP(fMarkerSize)
      ,CNVP(fFillColor)
      ,CNVP(fFillStyle)
      );
  }

private: 
  CLASS_DEF(Pomona::Root::Plot::Attrib,1)
};

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::Root::Plot::Attrib> 
{
  //! Encode into YAML Node
  static Node encode(const Pomona::Root::Plot::Attrib& rhs)
  {
    Node node;
    node["LineColor"]   = rhs.GetLineColor();
    node["LineStyle"]   = rhs.GetLineStyle();
    node["LineWidth"]   = rhs.GetLineWidth();
    node["MarkerColor"] = rhs.GetMarkerColor();
    node["MarkerStyle"] = rhs.GetMarkerStyle();
    node["MarkerSize"]  = rhs.GetMarkerSize();
    node["FillColor"]   = rhs.GetFillColor();
    node["FillStyle"]   = rhs.GetFillStyle();
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::Root::Plot::Attrib& rhs)
  {
    // if(!node.IsMap()|| node.size() != 11) return kFALSE;
    rhs.SetLineColor  ( node["LineColor"]  .as<Short_t>() );
    rhs.SetLineStyle  ( node["LineStyle"]  .as<Short_t>() );
    rhs.SetLineWidth  ( node["LineWidth"]  .as<Short_t>() );
    rhs.SetMarkerColor( node["MarkerColor"].as<Short_t>() );
    rhs.SetMarkerStyle( node["MarkerStyle"].as<Short_t>() );
    rhs.SetMarkerSize ( node["MarkerSize"] .as<Short_t>() );
    rhs.SetFillColor  ( node["FillColor"]  .as<Short_t>() );
    rhs.SetFillStyle  ( node["FillStyle"]  .as<Short_t>() );

    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif  // end using root

#endif  // end POMONA_ROOT_PLOT_ATTRIB_HH

//  end RootPlotAttrib.hh
//  ############################################################################
