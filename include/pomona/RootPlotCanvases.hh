//  ############################################################################
//! @file       RooPlotCanvases.hh
//! @brief      Header file for collection of canvases
//! @author     Emma Hague
//! @date       2018-08-30
//! @copyright  Copyright (C) 2017 "Emma Hague" 
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ROOT_PLOT_CANVASES_HH
#define POMONA_ROOT_PLOT_CANVASES_HH
#ifndef CERNROOT      // Not using root  
// forward declare class
namespace Pomona { namespace Root { namespace Plot { class Canvases; } } }
#else                 // Using root
#include "String.hh"  // pomona: enhanced string
#include <map>        // std: map collection
class TCanvas;        // root: plot canvas

namespace Pomona {

class HostPath;
namespace App { class Application; }

namespace Root {

namespace Plot {

//  ****************************************************************************
//! String-keyed map of canvases
class Canvases : public std::map<String,TCanvas*>
{
public: 
  Canvases();
  Canvases(const Canvases& other);
  Canvases(Canvases&& other);
  Canvases& operator=(const Canvases& rhs);
  Canvases& operator=(Canvases&& rhs);
  inline virtual ~Canvases(){ }

  // Info
  void printObjects() const;

  // Modifiers
  void obliterate();
  Bool_t    insert    (TCanvas* canvas);
  Canvases& operator+=(TCanvas* rhs);
  Bool_t    insert    (const Canvases& other);
  Canvases& operator+=(const Canvases& rhs);

  // Saving
  void save(const HostPath& path, const String& ext, const Bool_t& printInfo) const;
  void save(const App::Application& app, const Bool_t& printInfo) const;
};

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

#endif  // end POMONA_ROOT_PLOT_CANVASES_HH

//  end RooPlotCanvases.hh
//  ############################################################################
