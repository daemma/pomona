//  ############################################################################
//! @file       RootPlotStyle.hh
//! @brief      Header file for root-system style utility class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_ROOT_PLOT_STYLE_HH
#define POMONA_ROOT_PLOT_STYLE_HH
#ifndef CERNROOT               // Not using root
namespace Pomona { namespace Root { namespace Plot { class Style; } } }
#else                         // Using root
#include "Types.hh"           // pomona: basic types
#include "HostPath.hh"        // pomona: host system file path
#include "RootPlotAttrib.hh"  // pomona: attributes for plotting
#include "LogLevel.hh"        // pomona: log message level enumeration
#include <iosfwd>             // std: io-stream forward declarations
class TStyle;                 // root: style class
class TH1;                    // root: histogram class
class TH1D;                   // root: 1D histogram of doubles class
class TGraphAsymmErrors;      // root: graph with asymmetric error bars
class TLegend;                // root-system legend class
class TCanvas;                // root-system plot canvas

namespace Pomona {

namespace Root {

namespace Plot {
  
//  **************************************************************************
static constexpr Double_t kGoldRat      = 1.618033989;  //!< the golden ratio
static constexpr Double_t kInvGoldRat   = 0.618033989;  //!< inverse golden ratio
static constexpr Double_t kDefSzY       = 700;          //!< #-pix-y
static constexpr Double_t kDefSzX       = 1133;         //!< #-pix-x: kDefSzY*kGoldRat
static constexpr UInt_t   kDefPalette   = 77;           //!< kDarkRainBow
static constexpr UInt_t   kDefPaletteI  = 58;           //!< kCubehelix
static constexpr UInt_t   kDefLineWidth = 2;            //!< line width
static const     String_t kDefImgExt    = ".png";       //!< image extension

//  **************************************************************************
//! @enum eStyle Plotting styles, enumerated for control
/*! @note Values are exclusive. */
enum class eStyle : UInt8_t { 
  kPomona  = 0x00, //!< Default for this library
  kInvert  = 0x01, //!< Inverted default for this library
  kPlain   = 0x02, //!< Plain Style (no colors/fill areas)
  kBold    = 0x03, //!< Bold Style
  kVideo   = 0x04, //!< Style for video presentation histograms
  kPub     = 0x05, //!< Style for Publications
  kClassic = 0x06, //!< Classic Style
  kDefault = 0x07, //!< Equivalent to Classic
  kModern  = 0x09  //!< Modern Style
};
//! Default plot style
static const eStyle kDefStyle = eStyle::kPomona;

//  ****************************************************************************
//! @enum eScheme Color schemes
/*! @note Values are exclusive. */
enum class eScheme : UInt8_t { 
  kDef  = 0x00 //!< Default
 ,kBkg  = 0x01 //!< Background
 ,kNul  = 0x02 //!< Null
 ,kAlt  = 0x03 //!< Alternative
 ,kPs0  = 0x10 //!< Generic scheme 1
 ,kPs1  = 0x20 //!< Generic scheme 2
 ,kPs2  = 0x30 //!< Generic scheme 3 
 ,kPs3  = 0x40 //!< Generic scheme 4
 ,kPs4  = 0x50 //!< Generic scheme 5
 ,kPs5  = 0x60  //!< Generic scheme 6
 ,kPs6  = 0x70 //!< Generic scheme 7
 ,kPs7  = 0x80 //!< Generic scheme 8
 ,kPs8  = 0x90 //!< Generic scheme 9
 ,kPs9  = 0xA0 //!< Generic scheme 10
 ,kPs10 = 0xB0 //!< Generic scheme 11
 ,kPs11 = 0xC0 //!< Generic scheme 12
 ,kPs12 = 0xD0 //!< Generic scheme 13
};
//! Default plot scheme
static const eScheme kDefScheme = eScheme::kDef;

//  ****************************************************************************
//! root-system style interface.
/*! This is, essentially, an API for presenting usefull methods from root's 
    TStyle class. The idea here is to both limit "#include" overhead and 
    "compartmentalize" our usage of root-system inferface methods.

    @note All member functions are static.
*/
class Style
{
public: 
  inline Style(){}
  inline virtual ~Style(){ }

  static TStyle&        Tstyle();
  static const Char_t*  Name();
  static const Char_t*  Title();
  static String_t       Info();

  static void BuildStyles();

  static const String_t& StyleName(const eStyle& es);
  static const eStyle&   StyleEnum(const String_t& name);
  static void            Set(const eStyle& es);
  static void            Force();
  static Bool_t          IsInverse();

  static Color_t    GetGenColor (const UShort_t& idx);
  static Style_t    GetGenLine  (const UShort_t& idx);
  static Style_t    GetGenFill  (const UShort_t& idx);
  static Style_t    GetGenMark  (const UShort_t& idx);
  static eScheme    GetGenScm   (const UShort_t& idx);
  static UShort_t   GetGenIdx   (const eScheme& ps);
  static Attrib GetGenAttrib(const UShort_t& idx);
  static Attrib GetSchAttrib(const eScheme& ps);

  static void SetAttrib(TObject& obj, const eScheme& ps = kDefScheme);
  static void SetAttrib(TObject& obj, const UShort_t& idx = 0);
  static void SetAxis(TH1& h, 
		      const Char_t* xTitle = "X", 
		      const Char_t* yTitle = "Y", 
		      const Char_t* zTitle = "Z");


  static TLegend* Legend(const Double_t& x1 = 0.80, const Double_t& y1 = 0.80, 
			 const Double_t& x2 = 0.98, const Double_t& y2 = 0.98, 
			 const Char_t* header = "");
  static void SaveCanvas(const TCanvas& canvas, 
			 const HostPath& path = "", const String& ext = kDefImgExt, 
			 const Bool_t& printInfo = kFALSE,
			 const HostPath& aliasPath = "");

  static TGraphAsymmErrors* PullGraph(const TH1& bkg, const TH1& frg);
  static Double_t KullbackDivergence(const TH1D& p, const TH1D& q);

  static void PrintInfo(std::ostream& os);
  static void PrintInfo(const Log::eLevel& level = Log::eLevel::kDebug);
  static void PrintStyleHelp(std::ostream& os, const String_t& indent = "");
  static void PrintExtHelp(std::ostream& os, const String_t& indent = "");

  static void DrawExamples();
  static void DrawRootExamples();

protected: 
  static Bool_t  Has (const String_t& name);
  static TStyle& Find(const String_t& name);
  static void    Set (const String_t& name);
  static void    BuildPomonaStyle();
  static void    BuildInvertStyle();

private: 
  Style(const Style& other)          = delete;
  Style(Style&& other)               = delete;
  Style& operator=(const Style& rhs) = delete;
  Style& operator=(Style&& rhs)      = delete;
}; 

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

#endif  // end POMONA_ROOT_PLOT_STYLE_HH


//  end RootPlotStyle.hh
//  ############################################################################
