//  ############################################################################
//! @file       SMap.hh
//! @brief      Header for Name-ified std::map<String,U> container
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_SMAP_HH
#define POMONA_SMAP_HH
#include "Object.hh"             // pomona: base class
#include "String.hh"             // pomona: template class
#include <cereal/types/map.hpp>  // cereal: std::map

namespace Pomona {

//! Alias for a std::map<String,U>.
template <class U> using SMap_t = std::map<String,U>;

//  ****************************************************************************
//! Object-ified std::map container
template <class U> 
class SMap : public Object, public SMap_t<U>
{
public: 
  SMap();
  SMap(std::initializer_list<typename SMap_t<U>::value_type> il);
  SMap(const SMap<U>& other);
  SMap(SMap<U>&& other);
  SMap<U>& operator=(const SMap& rhs);
  SMap<U>& operator=(SMap&& rhs);
  SMap<U>& operator=(const SMap_t<U>& rhs);
  SMap<U>& operator=(SMap_t<U>&& rhs);
  SMap<U>& operator=(std::initializer_list<typename SMap_t<U>::value_type> rhs);
  inline virtual ~SMap(){}

  // AbsObject
  virtual AbsObject*     clone() const;
  virtual const String_t className() const;
  virtual void           clear();
  virtual Bool_t         isEmpty() const;
  // Comparable
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable
  virtual UShort_t defaultPrintContent() const;
  virtual void     printValue  (std::ostream& os) const;
  virtual void     printCsv    (std::ostream& os, const Bool_t& endLine = kFALSE) const;
  virtual void     printCsvHead(std::ostream& os, const Bool_t& endLine = kFALSE) const;

  // accessors
  const SMap_t<U>& get() const;
        SMap_t<U>& get();
  const SMap_t<U>& operator()() const;
        SMap_t<U>& operator()();
  void   set(const SMap_t<U>& map);
  Bool_t contains(const String& name) const;

private: 
  //! cereal::access must be friendly
  friend class cereal::access;
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar(cereal::base_class< std::map<String,U> >(this));
    // ar(cereal::nvp("Values", cereal::base_class< std::map<String,U> >(this)));
  }

#ifdef YAMLCPP  // using yaml-cpp 
#if defined (__CINT__) || defined (__MAKECINT__)  // Using root
  //! YAML::convert must be friendly
  friend class YAML::convert< SMap<U> >; 
#endif
#endif
}; 


//  ****************************************************************************
//  ****************************************************************************
//  Template implementations
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: default
template <class U> 
SMap<U>::SMap()
  : Object()
  , SMap_t<U>()
{}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: initializer list
template <class U> 
SMap<U>::SMap(std::initializer_list<typename SMap_t<U>::value_type> il)
  : Object()
  , SMap_t<U>(il)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: copy
template <class U> 
SMap<U>::SMap(const SMap<U>& other)
  : Object(other)
  , SMap_t<U>(other)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: move
template <class U> 
SMap<U>::SMap(SMap<U>&& other)
  : Object(other)
  , SMap_t<U>(other)
{
  other.SMap<U>::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator
template <class U> 
SMap<U>& SMap<U>::operator=(const SMap& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    SMap_t<U>::operator=(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <class U> 
SMap<U>& SMap<U>::operator=(SMap&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    SMap_t<U>::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from std::list
template <class U> 
SMap<U>& SMap<U>::operator=(const SMap_t<U>& rhs)
{
  if(this != &rhs){
    SMap_t<U>::operator=(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <class U> 
SMap<U>& SMap<U>::operator=(SMap_t<U>&& rhs)
{
  if(this != &rhs){
    Object::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from initializer list
template <class U> 
SMap<U>& SMap<U>::operator=(std::initializer_list<
			    typename SMap_t<U>::value_type> rhs)
{
  SMap_t<U>::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type (const)
template <class U> 
const SMap_t<U>& SMap<U>::get() const 
{ 
  return static_cast<const SMap_t<U>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type
template <class U> 
SMap_t<U>& SMap<U>::get()       
{ 
  return static_cast<SMap_t<U>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type (const)
template <class U> 
const SMap_t<U>& SMap<U>::operator()() const 
{ 
  return static_cast<const SMap_t<U>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type
template <class U> 
SMap_t<U>& SMap<U>::operator()()       
{ 
  return static_cast<SMap_t<U>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set object from template-type
template <class U> 
void SMap<U>::set(const SMap_t<U>& m)
{ 
  SMap_t<U>::operator=(m); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clone this object
template <class U> 
AbsObject* SMap<U>::clone() const
{
  return new SMap<U>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the class-name
template <class U> 
const String_t SMap<U>::className() const
{
  return ("SMap<" + String(typeid(U).name()) + ">");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Removes all elements from the list container (which are destroyed), 
//! and leaving the container with a size of 0.
template <class U> 
void SMap<U>::clear()
{
  Object::clear();
  SMap_t<U>::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object empty?
template <class U> 
Bool_t SMap<U>::isEmpty() const
{
  if(!Object::isEmpty())     return kFALSE;
  if(!SMap_t<U>::empty()) return kFALSE;
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! equality
/*! @note Only compares object member. */
template <class U> 
Bool_t SMap<U>::isEqual(const Comparable& other) const
{
  if(isSame(other)) return kTRUE;
  try{ // attempt to cast
    const SMap<U>& o = dynamic_cast<const SMap<U>&>(other); 
    if(get() != o.get())  return kFALSE;
    return kTRUE;
  }
  catch(const std::bad_cast& bc){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
template <class U> 
Bool_t SMap<U>::isSortable() const 
{ 
  return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** @note Compares name only. */
template <class U> 
Int_t SMap<U>::compare(const Comparable&) const 
{ 
  return 0;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this containter contain the value? 
template <class U> 
Bool_t SMap<U>::contains(const String& n) const
{
  if(SMap_t<U>::count(n) > 0) return kTRUE;
  return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
template <class U> 
UShort_t SMap<U>::defaultPrintContent() const
{
  return kClassName | kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print value.
template <class U> 
void SMap<U>::printValue(std::ostream& os) const
{
  os << SMap_t<U>::size() << " [entries]";
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print CSV.
template <class U> 
void SMap<U>::printCsv(std::ostream& os, const Bool_t& el) const
{
  for(auto& sop: (*this)) sop.second.printCsv(os, el);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print the headers for a CSV file
template <class U> 
void SMap<U>::printCsvHead(std::ostream& os, const Bool_t& el) const
{
  if(SMap::size()>0) SMap::begin()->second.printCsvHead(os, el);
  // else               loutE(kEval) << "SMap<U>::printCsvHead: No entries found." << endl;
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive, class U> 
struct specialize<Archive, Pomona::SMap<U>, specialization::member_serialize>{}; 
}
// //! Cereal: Registers a derived polymorphic type 
// CEREAL_REGISTER_TYPE(Pomona::SMap<U>)


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <class U> struct convert< Pomona::SMap<U> >
{
  //! Encode into YAML Node
  static Node encode(const Pomona::SMap<U>& rhs)
  {
    Node node = convert< Pomona::SMap_t<U> >::encode(rhs);
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::SMap<U>& rhs)
  {
    if(!node.IsMap()) return kFALSE;
    convert< Pomona::SMap_t<U> >::decode(node, rhs());
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_SMAP_HH

//  end SMap.hh
//  ############################################################################
