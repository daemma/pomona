//  ############################################################################
//! @file       SsMap.hh
//! @brief      Header for a String map of String's
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_SSMAP_HH
#define POMONA_SSMAP_HH
#include "SMap.hh"  // pomona: String:Object map

namespace Pomona {

//  ****************************************************************************
//! Object-ified std::map container
class SsMap : public SMap<String>
{
public: 
  SsMap();
  SsMap(const SsMap& other);
  SsMap(SsMap&& other);
  SsMap& operator=(const SsMap& rhs);
  SsMap& operator=(SsMap&& rhs);
  inline virtual ~SsMap(){}

private: 
  CLASS_DEF(Pomona::SsMap,1)
}; 

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::SsMap>
{
  //! Encode into YAML Node
  static Node encode(const Pomona::SsMap& rhs)
  {
    Node node = convert< Pomona::SMap<Pomona::String> >::encode(rhs);
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::SsMap& rhs)
  {
    if(!node.IsMap()) return kFALSE;
    convert< Pomona::SMap<Pomona::String> >::decode(node, rhs);
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_SSMAP_HH

//  end SsMap.hh
//  ############################################################################
