//  ############################################################################
//! @file       Stopwatch.hh
//! @brief      Header for timing exectution
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_STOPWATCH_HH
#define POMONA_STOPWATCH_HH
#include "Types.hh"  // pomona: basic types
#include <chrono>         // std: chronography

namespace Pomona {

//  ****************************************************************************
//! Utility class to facilitate calculating and printing elapsed wall-clock time.
class Stopwatch {
public:
  Stopwatch();
  Stopwatch(const Stopwatch& other);
  Stopwatch(Stopwatch&& other);
  Stopwatch& operator=(const Stopwatch& rhs);
  Stopwatch& operator=(Stopwatch&& rhs);
  inline virtual ~Stopwatch() { }

  const Bool_t&   isRunning();
  void            start();
  Double_t        elapsed() const;
  const Double_t& stop();
  void            reset();
  String_t        elapsedAsString() const;
  String_t        stoppedAsString();

  static String_t DurationAsString(Double_t realt);

protected:
  Double_t calcDuration() const;

private:
  //! steady "wall clock" start time
  std::chrono::steady_clock::time_point mStartTime;  
  //! time duration
  Double_t mDuration;
  //! Has been started, e.g Is running
  Bool_t mIsRunning;
};

} // end namespace Pomona

#endif // end POMONA_STOPWATCH_HH

//  end Stopwatch.hh
//  ############################################################################
