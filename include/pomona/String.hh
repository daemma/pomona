//  ############################################################################
//! @file       String.hh
//! @brief      Header for enhanced character string class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_STRING_HH
#define POMONA_STRING_HH
#include "Object.hh"     // pomona: named object
#include "Exception.hh"  // pomona: exceptions
#include <functional>    // std: functional algorithms & methods
#include <type_traits>   // std: type traits
#include <typeinfo>      // std: typeid

namespace Pomona {

//  ****************************************************************************
//! An enhanced std::string
/*! Both std::string and ROOT::TString leave some things to be desired. 
    So, we'll roll our own with the features we want. 
*/
class String : public Object, public String_t
{
public: 
  String();
  String(const String_t& str);
  String(const String_t& str, const size_t& pos, const size_t& len = String_t::npos);
  String(const Char_t* str);
  String(const Char_t* str, size_t n);
  String(const size_t& n, const Char_t& c);
  String(const Char_t& c);
  String(const String& other);
  String(const String& str, const size_t& pos, const size_t& len = String_t::npos);
  explicit String(const Bool_t&    value);
  explicit String(const Short_t&   value);
  explicit String(const UShort_t&  value);
  explicit String(const Int_t&     value);
  explicit String(const UInt_t&    value);
  explicit String(const Int64_t&  value);
  explicit String(const UInt64_t& value);
  explicit String(const Float_t&   value);
  explicit String(const Double_t&  value);
  String(std::initializer_list<Char_t> il);
  String(String&& other);
  String& operator=(const String& rhs);
  String& operator=(const String_t& rhs);
  String& operator=(const Char_t* rhs);
  String& operator=(const Char_t& rhs);
  String& operator=(      String&& rhs);
  String& operator=(      String_t&& rhs);
  String& operator=(const Bool_t&    rhs);
  String& operator=(const Short_t&   rhs);
  String& operator=(const UShort_t&  rhs);
  String& operator=(const Int_t&     rhs);
  String& operator=(const UInt_t&    rhs);
  String& operator=(const Int64_t&  rhs);
  String& operator=(const UInt64_t& rhs);
  String& operator=(const Float_t&   rhs);
  String& operator=(const Double_t&  rhs);
  inline virtual ~String(){}

  // AbsObject Methods
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  // Comparable Methods
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable Methods
  virtual UShort_t defaultPrintContent() const;
  virtual void     printArgs (std::ostream& os) const;
  virtual void     printValue(std::ostream& os) const;

  // accessors
  virtual const String_t& get() const;
  virtual       String_t& get();
  virtual       void      set(const String_t& val);
  virtual const Char_t*   getAsChars();
  virtual       void      setFromChars(const Char_t* val);

  // search: String
  Bool_t contains  (const String& str) const;
  Bool_t beginsWith(const String& str) const;
  Bool_t endsWith  (const String& str) const;
  void   replaceAll(const String& all, const String& with);
  void   stripNamespace();

  // check character category
  Bool_t isCateg(std::function<Int_t(Int_t)> fcn, 
		 size_t idx = String_t::npos) const;
  Bool_t isPrint(size_t idx = String_t::npos) const;
  Bool_t isGraph(size_t idx = String_t::npos) const;
  Bool_t isAlNum(size_t idx = String_t::npos) const;
  Bool_t isAlpha(size_t idx = String_t::npos) const;
  Bool_t isLower(size_t idx = String_t::npos) const;
  Bool_t isUpper(size_t idx = String_t::npos) const;
  Bool_t isDigit(size_t idx = String_t::npos) const;
  Bool_t isHexad(size_t idx = String_t::npos) const;
  Bool_t isPunct(size_t idx = String_t::npos) const;
  Bool_t isSpace(size_t idx = String_t::npos) const;
  Bool_t isCntrl(size_t idx = String_t::npos) const;
  Bool_t isBlank(size_t idx = String_t::npos) const;

  // change
  void toLower();
  void toUpper();
  void removeNonGraphical();
  void trimNonGraphical();
  void removeCntrl();

  // Addition operators
  String& operator+=(const String& rhs);
  String& operator+=(const Char_t* rhs);

  // arithmetic interpreting
  template <typename T, typename = EiiA<T> > 
  T         asArith  (size_t* idx = nullptr, Int_t base = 10) const;
  Bool_t    asBool   () const;
  Short_t   asShort  (size_t* idx = nullptr, Int_t base = 10) const;
  UShort_t  asUShort (size_t* idx = nullptr, Int_t base = 10) const;
  Int_t     asInt    (size_t* idx = nullptr, Int_t base = 10) const;
  UInt_t    asUInt   (size_t* idx = nullptr, Int_t base = 10) const;
  Int64_t   asLong64 (size_t* idx = nullptr, Int_t base = 10) const;
  UInt64_t  asULong64(size_t* idx = nullptr, Int_t base = 10) const;
  Float_t   asFloat  (size_t* idx = nullptr) const;
  Double_t  asDouble (size_t* idx = nullptr) const;
  template <typename T, typename = EiiA<T> > 
  void fromArith  (const T&         value);
  void fromBool   (const Bool_t&    value);
  void fromShort  (const Short_t&   value);
  void fromUShort (const UShort_t&  value);
  void fromInt    (const Int_t&     value);
  void fromUInt   (const UInt_t&    value);
  void fromLong64 (const Int64_t&  value);
  void fromULong64(const UInt64_t& value);
  void fromFloat  (const Float_t&   value);
  void fromDouble (const Double_t&  value);

private: 
  //! save-minimal
  template <class Archive> 
  String_t save_minimal(const Archive&) const { return get(); }
  //! load-minimal
  template <class Archive> 
  void load_minimal(const Archive&, const String_t& s){ set(s); }

private: 
  CLASS_DEF(Pomona::String,1)
};

// external operators
String operator+(const String& lhs, const Char_t* rhs);
String operator+(const Char_t* lhs, const String& rhs);
String operator+(const String& lhs, const Char_t& rhs);
String operator+(const Char_t& lhs, const String& rhs);

//  ****************************************************************************
//! @struct is_string
/** @brief A template struct to dertmine if the template parameter inherits 
    from String. 
*/
template <class U>
struct is_string : std::is_base_of<String, U>{ };

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for Enable if is arithmetic type
template <typename U> 
using EiiS = typename std::enable_if<is_string<U>::value>::type;

//  ****************************************************************************
//! Template function to  dertmine if inheritence from String. 
template <class U> 
Bool_t IsString(){ return is_string<U>::value; }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Convert this string to an arithmetic type.
/*! Checks the type and calls appropriate method from std::string. 
    Returns default constructor of type if typeid doesn't match.

    For boolean type:
    If the string-length is greater than or equal to one and the first character 
    is any of {0,t,T,y,Y} then it is interpreted as true, otherwise it is 
    interpreted as false. Note that idx and base are ignored for boolean-type.

    For all other arithmetic types: 
    If idx is not a null pointer, the function also sets the value of idx to 
    the position of the first character in str after the number.

    For integral types: 
    Parses str interpreting its content as an integral number of the specified 
    base, which is returned as an int value.

    For floating-point types: 
    The specified base is ignored.

    @note The motivation for templating this method -- as oppossed to indivual 
    implementations -- is to consolidate the code into "one place" which makes 
    maintenance and debugging more straightforward.
*/
template <typename T, typename> 
T String::asArith(size_t* idx, Int_t base) const
{
  if(typeid(T) == typeid(Bool_t)){
    if( length()>=1 && ((*this)[0]=='1' || 
			(*this)[0]=='t' || (*this)[0]=='T' || 
			(*this)[0]=='y' || (*this)[0]=='Y')) 
      return kTRUE;
    else return kFALSE;
  } 
  else if(typeid(T) == typeid(Int8_t)) 
    return static_cast<Int8_t>(stoi((*this), idx, base));
  else if(typeid(T) == typeid(UInt8_t)) 
    return static_cast<UInt8_t>(stoul((*this), idx, base));
  else if(typeid(T) == typeid(Short_t)) 
    return static_cast<Short_t>(stoi((*this), idx, base));
  else if(typeid(T) == typeid(UShort_t)) 
    return static_cast<UShort_t>(stoul((*this), idx, base));
  else if(typeid(T) == typeid(Int_t)) 
    return static_cast<Int_t>(stoi((*this), idx, base));
  else if(typeid(T) == typeid(UInt_t)) 
    return static_cast<UInt_t>(stoul((*this), idx, base));
  else if(typeid(T) == typeid(Int64_t)) 
    return static_cast<Int64_t>(stoll((*this), idx, base));
  else if(typeid(T) == typeid(UInt64_t)) 
    return static_cast<UInt64_t>(stoull((*this), idx, base));
  else if(typeid(T) == typeid(Float_t)) 
    return static_cast<Float_t>(stof((*this), idx));
  else if(typeid(T) == typeid(Double_t)) 
    return static_cast<Double_t>(stod((*this), idx));
  else throw Exception("Unknown arithmetic type", "asArith", "String");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Sets the string with the representation of value.
template <typename T, typename> 
void String::fromArith(const T& value)
{
  if(typeid(T) == typeid(Bool_t)){
    if(value) set("true");
    else      set("false");
  }
  else set(std::to_string(value));
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive> 
struct specialize<Archive, Pomona::String,
		  specialization::member_load_save_minimal>{}; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::String>
{
  //! Encode into YAML Node
  static Node encode(const Pomona::String& rhs)
  {
    Node node;
    node = rhs.get();
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::String& rhs)
  {
    if(!node.IsScalar()) return kFALSE;
    rhs = node.as<String_t>();
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! <a href="http://www.cplusplus.com/reference/">std</a>
namespace std {
//! Specialize the std::hash function 
template<>
struct hash<Pomona::String>
{
   typedef Pomona::String argument_type;
   typedef size_t         result_type;       
   result_type operator()(const argument_type& x) const
   { return std::hash<String_t>()(static_cast<String_t>(x)); }
};
}

#endif // end POMONA_STRING_HH

//  end String.hh
//  ############################################################################
