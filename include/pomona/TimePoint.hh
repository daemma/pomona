//  ############################################################################
//! @file       TimePoint.hh
//! @brief      Header for TimePoint class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROORAS_TIMEPOINT_HH
#define ROORAS_TIMEPOINT_HH
#include "Object.hh"           // pomona: base class
#include <cereal/types/chrono.hpp>  // cereal: stl chronography
using std::chrono::milliseconds;
using std::chrono::seconds;

namespace Pomona {

//  ****************************************************************************
//! A point in time.
class TimePoint : public Object {
public:
  TimePoint();
  explicit TimePoint(const milliseconds& ms);
  explicit TimePoint(const seconds& s);
  TimePoint(const TimePoint& other);
  TimePoint(TimePoint&& other);
  TimePoint& operator=(const TimePoint& rhs);
  TimePoint& operator=(TimePoint&& rhs);
  TimePoint& operator=(const milliseconds& rhs);
  TimePoint& operator=(milliseconds&& rhs);
  TimePoint& operator=(const seconds& rhs);
  TimePoint& operator=(seconds&& rhs);
  TimePoint& operator=(const UInt64_t& rhs);
  TimePoint& operator=(UInt64_t&& rhs);
  inline virtual ~TimePoint(){ }

  // AbsObject overloads
  virtual void   clear();
  virtual Bool_t isEmpty() const;
  virtual Bool_t isEqual(const Comparable& other) const;
  virtual Bool_t isSortable() const;
  virtual Int_t  compare(const Comparable& other) const;
  // Printable overloads
  virtual UShort_t defaultPrintContent() const;
  virtual void     printValue  (std::ostream& os) const;
  virtual void     printCsv    (std::ostream& os, const Bool_t& endLine = kFALSE) const;
  virtual void     printCsvHead(std::ostream& os, const Bool_t& endLine = kFALSE) const;

  // Get/Set
  const milliseconds& get() const;
  UInt64_t count() const;
  void set(const milliseconds& ms);
  void set(const seconds& s);
  void set(const UInt64_t& ms);
  void setNow();
  void incOneSec();

  // Operators
  TimePoint  operator+(const TimePoint& rhs) const;
  TimePoint  operator-(const TimePoint& rhs) const;
  TimePoint& operator++();
  TimePoint  operator++(int);
  TimePoint& operator--();
  TimePoint  operator--(int);
  TimePoint& operator+=(const TimePoint& rhs);
  TimePoint& operator-=(const TimePoint& rhs);
  TimePoint& operator+=(const milliseconds& rhs);
  TimePoint& operator-=(const milliseconds& rhs);
  TimePoint& operator+=(const seconds& rhs);
  TimePoint& operator-=(const seconds& rhs);
  
  // utilities
  static TimePoint    Now();
  static milliseconds SystemNow();
  static milliseconds SteadyNow();
  static milliseconds HighResNow();
  static const TimePoint& Zero();
  static const TimePoint& OneSec();

protected:
  milliseconds& get();

private:
  //  Member, instead of base-class, so that root doesn't complain about I/O :-/
  //! Milliseconds of duration
  milliseconds mMS;

private: 
  //! save-minimal
  template <class Archive> 
  UInt64_t save_minimal(const Archive&) const { return count(); }
  //! load-minimal
  template <class Archive> 
  void load_minimal(const Archive&, const UInt64_t& val){ set(val); }

private: 
  CLASS_DEF(Pomona::TimePoint,0)
};

  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  External operators
Bool_t operator==(const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator!=(const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator< (const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator<=(const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator> (const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator>=(const TimePoint& lhs, const milliseconds& rhs);
Bool_t operator==(const TimePoint& lhs, const UInt64_t& rhs);
Bool_t operator!=(const TimePoint& lhs, const UInt64_t& rhs);
Bool_t operator< (const TimePoint& lhs, const UInt64_t& rhs);
Bool_t operator<=(const TimePoint& lhs, const UInt64_t& rhs);
Bool_t operator> (const TimePoint& lhs, const UInt64_t& rhs);
Bool_t operator>=(const TimePoint& lhs, const UInt64_t& rhs);

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive> 
struct specialize<Archive, Pomona::TimePoint, specialization::member_load_save_minimal>{}; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <> struct convert<Pomona::TimePoint>
{
  //! Encode into YAML Node
  static Node encode(const Pomona::TimePoint& rhs)
  {
    Node node;
    node = UInt64_t(rhs.count());
    return node;
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::TimePoint& rhs)
  {
    if(!node.IsScalar()) return kFALSE;
    rhs = node.as<UInt64_t>();
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end ROORAS_TIMEPOINT_HH

//  end TimePoint.hh
//  ############################################################################
