//  ############################################################################
//! @file       Types.hh
//! @brief      Fundamental types
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_TYPES_HH
#define POMONA_TYPES_HH
#include "Platform.hh"  // platform-specific definitions and macros
#include <cstddef>      // cstd: size_t, NULL, nullptr_t
#include <cstdint>      // cstd: int64, etc
#include <string>       //  std: string container

//  ****************************************************************************
typedef std::string String_t;  //!< C++ std::string container.

//  ++++++++++++++++++++++++++++++++++++
static const bool kIs64bit = (sizeof(void*)==8);  //!< Is 64-bit?
typedef void*          VoidPtr_t;  //!< Void pointer
typedef nullptr_t      NullPtr_t;  //!< Null pointer type
#ifndef NULL // not NULL
#define NULL 0
#endif       // end not NULL

//  ****************************************************************************
//  Specifically sized integers 
#ifndef POMONA_OS_WIN  // NOT Windows
typedef int8_t            Int8_t;    //!< 1-byte integer, signed
typedef uint8_t           UInt8_t;   //!< 1-byte integer, unsigned 
typedef int16_t           Int16_t;   //!< 2-byte integer, signed
typedef uint16_t          UInt16_t;  //!< 2-byte integer, unsigned 
typedef int32_t           Int32_t;   //!< 4-byte integer, signed
typedef uint32_t          UInt32_t;  //!< 4-byte integer, unsigned 
typedef int64_t           Int64_t;   //!< 8-byte integer, signed
typedef uint64_t          UInt64_t;  //!< 8-byte integer, unsigned 
#else                  // IS Windows
typedef __int8            Int8_t;    //!< 1-byte integer, signed
typedef unsigned __int8   UInt8_t;   //!< 1-byte integer, unsigned 
typedef __int16           Int16_t;   //!< 2-byte integer, signed
typedef unsigned __int16  UInt16_t;  //!< 2-byte integer, unsigned 
typedef __int32           Int32_t;   //!< 4-byte integer, signed
typedef unsigned __int32  UInt32_t;  //!< 4-byte integer, unsigned 
typedef __int64           Int64_t;   //!< 8-byte integer, signed
typedef unsigned __int64  UInt64_t;  //!< 8-byte integer, unsigned 
#endif

//  ****************************************************************************
//  Other basic types
// #if defined(CERNROOT) || !defined (__CINT__) || defined (__MAKECINT__)  // Using root
#ifdef CERNROOT  // Using root
#include "Rtypes.h"      // root: basic types
#include "RtypesCore.h"  // root: basic types, core
#else                    // Not using root

//  ++++++++++++++++++++++++++++++++++++
//  Basic & fundamental types
typedef bool           Bool_t;     //!< 1-byte boolean
typedef char           Char_t;     //!< 1-byte character, signed
typedef unsigned char  UChar_t;    //!< 1-byte character, unsigned
typedef Int16_t        Short_t;    //!< 2-byte integer, signed
typedef UInt16_t       UShort_t;   //!< 2-byte integer, unsigned
typedef Int32_t        Int_t;      //!< 4-byte integer, signed
typedef UInt32_t       UInt_t;     //!< 4-byte integer, unsigned 
typedef Int64_t        Long64_t;   //!< 8-byte integer, signed
typedef UInt64_t       ULong64_t;  //!< 8-byte integer, unsigned 
typedef float          Float_t;    //!< 4-byte floating-point, signed
typedef double         Double_t;   //!< 8-byte floating-point, signed

//  ++++++++++++++++++++++++++++++++++++
//  Extras & helpfuls
typedef unsigned char Byte_t;     //!< 1-byte character, unsigned

//  ++++++++++++++++++++++++++++++++++++
//  Boolean values
static const Bool_t kFALSE = false;  //!< false value = 0
static const Bool_t kTRUE  = true;   //!< true  value = 1

#endif // end not root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for Enable if is arithmetic type
template <typename T> 
using EiiA = typename std::enable_if<std::is_arithmetic<T>::value>::type;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for Enable if is integral type
template <typename T> 
using EiiI = typename std::enable_if<std::is_integral<T>::value>::type;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Alias for Enable if is floating-point type
template <typename T> 
using EiiF = typename std::enable_if<std::is_floating_point<T>::value>::type;

#endif // end POMONA_TYPES_HH

//  end Types.hh
//  ############################################################################
