//  ############################################################################
//! @file       USet.hh
//! @brief      Header for Object-ified std::unordered_set container
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef POMONA_USET_HH
#define POMONA_USET_HH
#include "Object.hh"                  // pomona: base class
#include <cereal/types/unordered_set.hpp>  // cereal: std::unordered_set
#include <vector>                          // std: vector for YAML I/O

namespace Pomona {

//  ****************************************************************************
//! Object-ified std::unordered_set container
template <typename T> 
class USet : public Object, public std::unordered_set<T>
{
public: 
  USet();
  USet(std::initializer_list<typename std::unordered_set<T>::value_type> il);
  USet(const USet& other);
  USet(USet&& other);
  USet& operator=(const USet& rhs);
  USet& operator=(USet&& rhs);
  USet& operator=(const std::unordered_set<T>& rhs);
  USet& operator=(std::unordered_set<T>&& rhs);
  USet& operator=(std::initializer_list<typename std::unordered_set<T>::value_type> rhs);
  inline virtual ~USet(){}

  // accessors
  virtual const std::unordered_set<T>& get() const;
  virtual       std::unordered_set<T>& get();
  virtual void set(const std::unordered_set<T>& list);
  virtual Bool_t contains(const T& val) const;

  // AbsObject overloads
  virtual AbsObject* clone() const;
  virtual const String_t   className() const;
  virtual void       clear();
  virtual Bool_t     isEmpty() const;
  // Comparable overloads
  virtual Bool_t     isEqual(const Comparable& other) const;
  virtual Bool_t     isSortable() const;
  virtual Int_t      compare(const Comparable& other) const;
  // // Printable overloads
  // virtual UShort_t defaultPrintContent() const;
  // virtual void     printValue  (std::ostream& os) const;
  // virtual void     printCsv    (std::ostream& os) const;
  // virtual void     printCsvHead(std::ostream& os) const;

private: 
  //! cereal::access must be friendly
  friend class cereal::access;
  //! cerealize
  template <class Archive> 
  void serialize(Archive& ar)
  {
    ar( cereal::make_nvp("USet", cereal::base_class< std::unordered_set<T> >(this) ) );
  }
  //! YAML::convert must be friendly
  friend class YAML::convert< USet<T> >; 

// private: 
//   CLASS_DEF(Pomona::USet,1)
}; 


//  ****************************************************************************
//  ****************************************************************************
//  Template implementations
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: default
template <typename T> 
USet<T>::USet()
  : Object()
  , std::unordered_set<T>()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: initializer list
template <typename T> 
USet<T>::USet(std::initializer_list<typename std::unordered_set<T>::value_type> il)
  : Object()
  , std::unordered_set<T>(il)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: copy
template <typename T> 
USet<T>::USet(const USet& other)
  : Object(other)
  , std::unordered_set<T>(other)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: move
template <typename T> 
USet<T>::USet(USet&& other)
  : Object(other)
  , std::unordered_set<T>(other)
{
  other.USet<T>::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator
template <typename T> 
USet<T>& USet<T>::operator=(const USet& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    std::unordered_set<T>::operator=(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <typename T> 
USet<T>& USet<T>::operator=(USet&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    std::unordered_set<T>::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from std::list
template <typename T> 
USet<T>& USet<T>::operator=(const std::unordered_set<T>& rhs)
{
  if(this != &rhs){
    std::unordered_set<T>::operator=(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator
template <typename T> 
USet<T>& USet<T>::operator=(std::unordered_set<T>&& rhs)
{
  if(this != &rhs){
    Object::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from initializer list
template <typename T> 
USet<T>& USet<T>::operator=(std::initializer_list<
			    typename std::unordered_set<T>::value_type> rhs)
{
  std::unordered_set<T>::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type (const)
template <typename T> 
const std::unordered_set<T>& USet<T>::get() const 
{ 
  return static_cast<const std::unordered_set<T>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get object as template-type
template <typename T> 
std::unordered_set<T>& USet<T>::get()       
{ 
  return static_cast<std::unordered_set<T>&>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set object from template-type
template <typename T> 
void USet<T>::set(const std::unordered_set<T>& l)
{ 
  std::unordered_set<T>::operator=(l); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clone this object
template <typename T> 
AbsObject* USet<T>::clone() const
{
  return new USet<T>(*this); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the class-name
template <typename T> 
const String_t USet<T>::className() const
{
  return ("USet<" + String(typeid(T).name()) + ">");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Removes all elements from the list container (which are destroyed), 
//! and leaving the container with a size of 0.
template <typename T> 
void USet<T>::clear()
{
  Object::clear();
  std::unordered_set<T>::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object empty?
template <typename T> 
Bool_t USet<T>::isEmpty() const
{
  if(!Object::isEmpty())     return kFALSE;
  if(!std::unordered_set<T>::empty()) return kFALSE;
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! equality
/*! @note Only compares object member. */
template <typename T> 
Bool_t USet<T>::isEqual(const Comparable& other) const
{
  if(isSame(other)) return kTRUE;
  try{ // attempt to cast
    const USet<T>& o = dynamic_cast<const USet<T>&>(other); 
    if(get() != o.get())  return kFALSE;
    return kTRUE;
  }
  catch(const std::bad_cast& bc){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
template <typename T> 
Bool_t USet<T>::isSortable() const 
{ 
  return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/*! @note Only compares object members. */
template <typename T> 
Int_t USet<T>::compare(const Comparable&) const 
{ 
  return 0;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this containter contain the value? 
template <typename T> 
Bool_t USet<T>::contains(const T& val) const
{
  if(std::unordered_set<T>::count(val) > 0) return kTRUE;
  return kFALSE;
}

} // end namespace Pomona

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
namespace cereal { 
//! cereal: specialization
template <class Archive, typename T> 
struct specialize<Archive, Pomona::USet<T>, specialization::member_serialize>{}; 
}
// //! Cereal: Registers a derived polymorphic type 
// CEREAL_REGISTER_TYPE(Pomona::USet<T>)


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef YAMLCPP    // using yaml-cpp
namespace YAML { 
template <typename T> struct convert< Pomona::USet<T> >
{
  //! Encode into YAML Node
  static Node encode(const Pomona::USet<T>& rhs)
  {
    // we should be able to write this as a std::vector/vector
    std::vector<T> vset;
    // copy contents to vector
    for(auto& t: rhs.get()) vset.push_back(t);
    // write vector to node
    Node node;
    node["USet"] = vset;
    return std::move(node);
  }

  //! Decode from YAML Node
  static Bool_t decode(const Node& node, Pomona::USet<T>& rhs)
  {
    if(!node.IsSequence()) return kFALSE;
    // read as vector
    std::vector<T> vset = node["USet"].as< std::vector<T> >();
    // copy vector to this set
    for(auto& t: vset) rhs.insert(t);
    return kTRUE;
  }
};  // end convert struct
}  // end namespace YAML
#endif  // end using yaml-cpp

#endif // end POMONA_USET_HH

//  end USet.hh
//  ############################################################################
