/*  ########################################################################  */
/*! @file       pomona.h */
/*! @brief      Header for top-level C-lanuage API entry point */
/*! @author     "Emma Hague" <dev@daemma.io> */
/*! @date       2017-11-23 */
/*! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io> */
/*!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt> */
#ifndef POMONA_POMONA_H
#define POMONA_POMONA_H
#include <stdbool.h>  // cstd: boolean type

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*  Basic library information */
const char*   pomona_name();
const char*   pomona_moniker();
const char*   pomona_title();
const char*   pomona_libname();
const char*   pomona_author();
const char*   pomona_year();
const char*   pomona_copyright();
const char*   pomona_version();
      short   pomona_version_major();
      short   pomona_version_minor();
      short   pomona_version_patch();
const char*   pomona_install_prefix();
const char*   pomona_install_cflags();
const char*   pomona_install_ldflags();
      bool    pomona_has_cernroot();
      bool    pomona_has_yaml();

#endif // end POMONA_POMONA_H

/*  end rooras.h */
/*  ########################################################################  */
