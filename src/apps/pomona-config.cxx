//  ############################################################################
//! @file       pomona-config.cxx
//! @brief      Source for library configuration executable
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-26
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "AppConfig.hh"

//  ****************************************************************************
//! Main method for configuration printer 
int main(int argc, char** argv)
{  
  Pomona::App::Config conf;
  return conf.main(argc, argv);
}

//  end pomona-config.cxx
//  ############################################################################
