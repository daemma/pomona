//  ############################################################################
//! @file       AppConfig.cxx
//! @brief      Source for configuration app
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-26
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "AppConfig.hh"  // pomona: this class
#include "Project.hh"    // pomona: project information
#include "Root.hh"       // pomona: root-system API

namespace Pomona {

namespace App {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Config::Config()
  : Application("Config")
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Add the options 
Bool_t Config::addOptions()
{
  Bool_t ret(kTRUE);
  ret &= mOpts.insertBool('h', "help",          "show help message");
  ret &= mOpts.insertBool('C', "copyright",     "output copyright");
  ret &= mOpts.insertBool('I', "info",          "output information");
  ret &= mOpts.insertBool('v', "version",       "output version number");
  ret &= mOpts.insertBool('j', "version-major", "output major version number");
  ret &= mOpts.insertBool('n', "version-minor", "output minor version number");
  ret &= mOpts.insertBool('a', "version-patch", "output patch version number");

  // from pkg-config::
  // --static                                output linker flags for static linking
  // --libs                                  output all linker flags
  // --libs-only-l                           output -l flags
  // --libs-only-other                       output other libs (e.g. -pthread)
  // --libs-only-L                           output -L flags
  // --cflags                                output all pre-processor and compiler flags
  // --cflags-only-I                         output -I flags
  // --cflags-only-other                     output cflags not covered by the cflags-only-I option  

  ret &= mOpts.insertBool('p', "prefix",       "output installation prefix");
  ret &= mOpts.insertBool('c', "cflags",       "output library c-flags");
  ret &= mOpts.insertBool('l', "ldflags",      "output ld-flags");

#ifdef YAMLCPP  // using yaml-cpp 
  ret &= mOpts.insertBool('Y', "yaml-version", "yaml: print version");
#endif          // end using yaml-cpp
#ifdef CERNROOT     // Using root
  ret &= mOpts.insertBool  ('R', "root-info",  "root: print info and exit");
#endif              // end using root

  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print program usage help text to the stream
void Config::printHelpUsage(std::ostream& os) const
{
  os << "Usage: " << mOpts.exeName() << " [OPTIONS]" << endl;
  os << "Print pomona configuration(s)" << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*! Main method. */
Int_t Config::run()
{
  // Attempt to run; exceptions shall not pass!
  try {
    if     (mOpts.value<Bool_t>("help"))              printHelpText(cout);
    else if(mOpts.value<Bool_t>("copyright"))         PrintLibCopyright(cout);
    else if(mOpts.value<Bool_t>("info"))              PrintLibInfo(cout);
    else if(mOpts.value<Bool_t>("version"))           PrintLibVersion(cout);
    else if(mOpts.value<Bool_t>("version-major"))     cout << Project::LibVersionMajor() << endl;
    else if(mOpts.value<Bool_t>("version-minor"))     cout << Project::LibVersionMinor() << endl;
    else if(mOpts.value<Bool_t>("version-patch"))     cout << Project::LibVersionPatch() << endl;
    else if(mOpts.value<Bool_t>("prefix"))            cout << Project::InstallPrefix() << endl;
    else if(mOpts.value<Bool_t>("cflags"))            cout << Project::InstallCFlags() << endl;
    else if(mOpts.value<Bool_t>("ldflags"))           cout << Project::InstallLdFlags() << endl;
#ifdef YAMLCPP  // using yaml-cpp 
    else if(mOpts.value<Bool_t>("yaml-version"))      PrintYamlVersion(cout);
#endif          // end using yaml-cpp
#ifdef CERNROOT  // Using root
    else if(mOpts.value<Bool_t>("root-info"))         Root::Root::PrintInfo(cout);
#endif           // end using root
    else{}
    return 0;
  }
  // Catch any thrown exceptions
  catch(const std::exception& e) { 
    loutF(kEval) << "Config::main: Caught exception: " << e.what() << endl;
    return -1; 
  }
}

} // end namespace App

} // end namespace Pomona

//  end AppConfig.cxx
//  ############################################################################
