//  ############################################################################
//! @file       Application.cxx
//! @brief      Source for generic application class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-25
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Application.hh"     // pomona: this class
#include "Exception.hh"       // pomona: exception handler
#include "Project.hh"         // pomona: project information
#include "Date.hh"            // pomona: calendar date helper
#include "HostSystem.hh"      // pomona: host system api
#include "LogProgressBar.hh"  // pomona: terminal progress bar
#include "LogStyle.hh"        // pomona: log message style enumeration
#include "LogService.hh"      // pomona: log message service
#include "IoParserTxt.hh"     // pomona: log message service
#include "Root.hh"            // pomona: TROOT interface
#include "RootPlotStyle.hh"   // pomona: Style interface
#ifdef CERNROOT               // Using root
#include "TRint.h"            // root-system interpreter application
#include "TRandom3.h"         // root: random numbers
#include "TBrowser.h"         // root: graphical file/object browser
#include "TCanvas.h"          // root: canvas
#endif                        // end using root

namespace Pomona {

namespace App {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Application::Application(const Char_t* appClassName)
  : Named     (appClassName, appClassName)
  , mOpts     ()
  , mTimer    ()
  , mPid      (HostSystem::Pid())
  , mLogFile  ("")
  , mOutDir   ("")
  , mOutDirTop(mOutDir)
{
  // Global log-message meta options
  logger().setStyle(Log::gDefStyle);
  // set a logging prefix
  logger().setPrefix(appClassName);
#ifdef CERNROOT                       // Using root
  Root::Plot::Style::BuildStyles();   // build the root-system styles
#endif                                // end using root
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option map (const).
const OptionMap& Application::options() const
{
  return mOpts;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option map
OptionMap& Application::options()
{
  return mOpts;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the application timer (const).
const Stopwatch& Application::timer() const
{
  return mTimer;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the application process ID (const).
const UInt_t& Application::pid() const
{
  return mPid;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Add the options 
Bool_t Application::addOptions()
{
  Bool_t ret(kTRUE);
  // Generic options
  ret &= mOpts.insertBool('h', "help",      "Print help message");
  ret &= mOpts.insertBool('H', "Help",      "Print verbose help message");
  ret &= mOpts.insertBool('v', "version",   "Print the library version.");
  ret &= mOpts.insertBool('C', "copyright", "Print the library copyright.");
  ret &= mOpts.insertBool('I', "info",      "Print the library information.");
  ret &= mOpts.insertBool('T', "no-time",   "Do not time the app");  
  ret &= mOpts.insertUInt('n', "ncpu",      "Number of CPUs to employ", 0);
  ret &= mOpts.insertString('o', "out-dir", "Output directory.", "");
  // ret &= mOpts.insertString('c', "conf-file",  "Configuration file name");  

  // logging options
  ret &= mOpts.insertLogLevel(Log::gDefLevel);
  ret &= mOpts.insertBool('S', "silent",  "Print no messages; log-level=Silent");
  ret &= mOpts.insertBool('D', "debug",   "Print debug messages; log-level=Debug");
  ret &= mOpts.insertBool('V', "verbose", "Print verbose messages; log-level=Verbose");
  ret &= mOpts.insertBool('G', "guru",    "Print guru messages; log-level=Guru");

#ifdef YAMLCPP  // using yaml-cpp 
  ret &= mOpts.insertBool('Y', "yaml-info", "yaml: print version");
#endif          // end using yaml-cpp

#ifdef CERNROOT     // Using root
  // root-system options
  ret &= mOpts.insertBool  ('b', "root-batch",       "root: batch mode");
  ret &= mOpts.insertString('L', "root-log-level",   "root: message level", "SysError");
  ret &= mOpts.insertBool  ('W', "root-browser",     "root: TBrowser");
  ret &= mOpts.insertUInt  ('E', "root-random-seed", "root: random number seed (unsigned)", 0);
  ret &= mOpts.insertBool  ('R', "root-info",        "root: print info and exit");
  ret &= mOpts.insertString('P', "root-plot-style",  "root: Canvas & color palette.", "PomonaInv");
  ret &= mOpts.insertString('X', "root-image-ext",   "root: extension (format) for saved images.", 
			    Root::Plot::kDefImgExt);
#endif              // end using root

  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print a standardized output section seperator
void Application::printSectionSep(const Char_t* sectionName) const
{
  // print first 80 characters of the section title
  Char_t ss[80];
  UInt_t idx = 0;
  while(sectionName[idx] != '\0'){
    if(idx < 80){
      ss[idx] = sectionName[idx];
      idx++;
    }
  }
  // title & tag seperator
  if(idx < 77){ ss[idx] = ' '; idx++; }
  if(idx < 77){ ss[idx] = ':'; idx++; }
  if(idx < 77){ ss[idx] = ' '; idx++; }
  // tag
  for(UInt_t i=idx; i<80; i++) ss[i] = '+';
  // end the character string
  ss[80] = '\0';
  // end the line
  loutI(kNone) << ss << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print help text to the stream
void Application::printHelpText(std::ostream& os) const
{
  printHelpUsage(os);
  os << endl; printHelpOptions(os);
  os << endl; PrintLibInfo(cout);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print verbose help text to the stream
void Application::printHelpTextVerb(std::ostream& os) const
{
  printHelpUsage(os);
  os << endl; printHelpOptions(os);
  os << endl; Log::Service::PrintLevelHelp(os, "  ");
#ifdef CERNROOT  // Using root
  os << endl; Log::Service::PrintRootLevelHelp(os, "  ");
  os << endl; Root::Plot::Style::PrintStyleHelp(os, "  ");
  os << endl; Root::Plot::Style::PrintExtHelp(os, "  ");
#endif           // end using root
  os << endl; PrintLibInfo(os);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print program usage help text to the stream
void Application::printHelpUsage(std::ostream& os) const
{
  os << "Usage: " << mOpts.exeName() << " [OPTIONS]" << endl;
  os << "This is an example application for the " 
     << Project::Moniker() << " library." << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print run-time options help to the stream.
void Application::printHelpOptions(std::ostream& os, const Char_t* ind) const
{
  os << "[OPTIONS]" << endl;
  mOpts.printHelpLines(os, ind);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print library version to the stream
void Application::PrintLibVersion(std::ostream& os)
{
  os << Project::LibVersion() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print library copyright to the stream
void Application::PrintLibCopyright(std::ostream& os)
{
  os << Project::Copyright() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print library information to the stream
void Application::PrintLibInfo(std::ostream& os)
{
  // one-liner description
  os << Project::Moniker() 
     << " v"  << Project::LibVersion() 
     << " - " << Project::Title() << endl;
  // copyright
  PrintLibCopyright(os);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print library information to the stream
void Application::PrintLibInfo()
{
  // remove prefixes from logger
  UInt8_t s = logger().style();
  logger().setStyle(static_cast<UInt8_t>(Log::eStyle::kNone));
  // one-liner description
  loutI(kNone) << Project::Moniker() 
	       << " v"  << Project::LibVersion() 
	       << " - " << Project::Title() << endl;
  // copyright
  loutI(kNone) << Project::Copyright() << endl;
  // resett the log style
  logger().setStyle(s);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print library version to the stream
void Application::PrintYamlVersion(std::ostream& os)
{
  os << Project::YamlVersion() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Run the methods the do something simple and exit
void Application::runExitableOptions() const
{
  // check for exitable options
  if     (mOpts.value<Bool_t>("help"))              printHelpText(cout);
  else if(mOpts.value<Bool_t>("Help"))              printHelpTextVerb(cout);
  else if(mOpts.value<Bool_t>("version"))           PrintLibVersion(cout);
  else if(mOpts.value<Bool_t>("copyright"))         PrintLibCopyright(cout);
  else if(mOpts.value<Bool_t>("info"))              PrintLibInfo(cout);
#ifdef YAMLCPP  // using yaml-cpp 
  else if(mOpts.value<Bool_t>("yaml-info"))         PrintYamlVersion(cout);
#endif          // end using yaml-cpp
#ifdef CERNROOT  // Using root
  else if(mOpts.value<Bool_t>("root-info"))         Root::Root::PrintInfo(cout);
#endif           // end using root
  else                                              return;
  // exit the app
  HostSystem::Exit(0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the log message level from the options
void Application::setLogLevelOptions() const
{
  Log::eLevel level = Log::gDefLevel;
  if(mOpts.isRequested("log-level")) 
    level = logger().AsLevel( mOpts.option("log-level").get() );
  else{
    if     (mOpts.value<Bool_t>("silent"))  level = Log::eLevel::kSilent;
    else if(mOpts.value<Bool_t>("debug"))   level = Log::eLevel::kDebug;
    else if(mOpts.value<Bool_t>("verbose")) level = Log::eLevel::kVerbose;
    else if(mOpts.value<Bool_t>("guru"))    level = Log::eLevel::kGuru;
    else                                    level = Log::gDefLevel;
  }
  logger().setLevel(level);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the root-system related options & configuration
void Application::setRootSysOptions() const
{
#ifdef CERNROOT  // Using root
  // root-system log level is different from Log::Service
  Log::Service::SetRootLevel(mOpts.option("root-log-level").get());
  // set batch mode
  Root::Root::SetBatch(mOpts.value<Bool_t>("root-batch"));
  // initialize random number generator
  gRandom = new TRandom3(mOpts.value<UInt_t>("root-random-seed"));
  // set the style
  if(mOpts.isRequested("root-plot-style")) 
    Root::Plot::Style::Set( Root::Plot::Style::StyleEnum( mOpts.option("root-plot-style").get() ) );
  else { 
    if(Root::Root::IsBatch()) Root::Plot::Style::Set(Root::Plot::eStyle::kPomona);
    else                      Root::Plot::Style::Set(Root::Plot::eStyle::kInvert);
  }
#endif           // end using root
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the (derived) application related options & configuration
void Application::setAppOptions()
{    
  try {
    mOutDir    = mOpts.option("out-dir").get();
    mOutDirTop = mOutDir.get();
    // // Configuration File
    // File::ParserCfg pcfg("default.cfg");
    // SsMap confs = pcfg.parseFile();
    // for(auto& cfg : confs) loutI(kFile) << cfg.first << " : " << cfg.second << endl;
  } catch(const Exception& e){
    loutA(kEval) << "Application::setAppOptions: Failed to set options: " 
		 << e.what() << endl;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Begining for main method.
void Application::begin()
{
  runExitableOptions();  // Print basic infos & exit
  setLogLevelOptions();  // Global Pomona logging level 
  setRootSysOptions();   // Root-system options && configuration
  setAppOptions();       // Application specific options
  if(!mOpts.value<Bool_t>("log-file")) PrintLibInfo();

  printSectionSep("Main");
  loutI(kEval)  << "      : " << mOpts.exeName() << " [" << mPid << "]"
  		<< " @ " << Date::NowLocal().asDescription() << endl;
  if(!mOpts.value<Bool_t>("no-time")) mTimer.start(); // timer

#ifdef CERNROOT  // Using root
  Root::Root::PrintInfo(Log::eLevel::kDebug);
  loutD(kInput) << "root-random-seed    : " << gRandom->GetSeed() << endl;
  loutD(kInput) << "root-plot-style     : " << Root::Plot::Style::Name() << endl;
#endif           // end using root
  if(logLIG(kDebug)) mOpts.printValueLines(cout);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Ending for main method.
Int_t Application::end()
{
  // Print the app evolution time?
  if(!mOpts.value<Bool_t>("no-time")){
    mTimer.stop();
    printSectionSep(("Run Time = "+mTimer.elapsedAsString()).c_str());
  } 
  // else printSectionSep("End");

#ifndef CERNROOT   // Not using root
  return 0;
#else              // Using root
  // run the root-app
  if(Root::Root::IsBatch()) return 0;
  else{
    // Open a TBrowser?
    if(mOpts.value<Bool_t>("root-browser")){
      TBrowser* browser = new TBrowser(("Pomona::"+name()).c_str(), 
  				       ("Pomona::"+name()+" Browser").c_str());
      browser->Refresh();
    }
    // Run the root-system interpreter
    static auto gTRint = new TRint(name().c_str(), NULL, 0, 0, 0, kTRUE);
    if(gTRint == nullptr){
      loutE(kEval) << "Application::end: Failed to create root interpreter" << endl;
    }
    else gTRint->Run(kTRUE);
    // return null on exit;
    return 0;
  }
#endif            // end using root
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Main method from command-line args.
Int_t Application::main(Int_t argc, Char_t** argv)
{
  // Attempt to parse aruments
  try {

#ifdef CERNROOT   // Using root
    // build the root-system styles
    Root::Plot::Style::BuildStyles();
#endif            // end using root

    // build the options and parse the input arguments
    if(!addOptions()){
      loutF(kObject) << "Application::main: Failed to add options." << endl;
      Exit(-1);
    }
    mOpts.parseArgs(argc, argv);
    logger().setPrefix(mOpts.exeName().c_str());

  } catch(const std::exception& e){
    throw Exception((String("Bad argument parsing: ")+e.what()).c_str(), 
                    "main(Int_t, Char_t**)", "Application");
  }

  // run the main
  return run();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*! Main method. 
    This serves as an example for how to write other mains. 
*/
Int_t Application::run()
{
  // Attempt to run; exceptions shall not pass!
  try {
    // run begining methods
    begin(); 

    // +++++++++++++++++++++++++++++++++++
    // Here go the body of work ... here only as examples
    printSectionSep("Main");
    Log::ProgressBar::printProgressBarExample();
#ifdef CERNROOT   // Using root
    Root::Plot::Style::DrawExamples();
#endif            // end using root

    // Finish, clean, & return
    return end();
  }
  // Catch any thrown exceptions
  catch(const std::exception& e) { 
    loutF(kEval) << "Application::main: Caught exception: " << e.what() << endl;
    return -1; 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Exit the primary process and return control to the host OS 
void Application::Exit(const Int_t& code, const Bool_t& clean)
{
  HostSystem::Exit(code, clean);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Change output directory
Bool_t Application::cdOut(const String& subDir, const Bool_t& print) const
{
  // build sub-directory
  HostPath odir = mOutDir + HostPath(subDir);
  Bool_t suc(kTRUE);
  if(!odir.exist()){
    if(HostSystem::MkDir(odir.get().c_str()) == 0){
      suc = kTRUE;
      if(print) loutN(kFile) << "Created output dir: " << odir << endl;
    }
    else{
      odir = mOutDir;
      suc  = kFALSE;
      loutW(kFile) << "Failed to create output dir: " << odir << endl;
    }
  }
  mOutDir = odir;
  if(print) loutI(kFile) << "Output directory : " << mOutDir << endl;
  return suc;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Change output directory
Bool_t Application::cdOut(const Char_t* subDir, const Bool_t& print) const
{
  return cdOut(String(subDir), print);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Change output directory to top level output dir
void Application::cdTop() const
{
  mOutDir = mOutDirTop; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Change output directory up one level
void Application::cdUp() const
{
  mOutDir = mOutDir.dirName(); 
}

#ifdef CERNROOT  // Using root
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Save a canvas.
void Application::saveCanvas(const TCanvas& c, const Bool_t& pi, const Bool_t& aod) const
{
  HostPath aliasPath("");
  if(aod) aliasPath = mOutDirTop;
  const String& extOpt = mOpts.option("root-image-ext").get();
  if(extOpt.contains(",")){
    std::vector<String> exts = Io::Parser::Txt::Tokenize(extOpt, ',');
    for(auto& ext : exts){
      String fe = ext;
      if(fe[0] != '.') fe.append(".");
      Root::Plot::Style::SaveCanvas(c, mOutDir, fe, pi, aliasPath);
    } // end token loop
  } // end multi
  else {
    String fe = extOpt;
    if(fe[0] != '.') fe.append(".");
    Root::Plot::Style::SaveCanvas(c, mOutDir, fe, pi, aliasPath);
  } // end ! multi
}
#endif           // end using root

} // end namespace App

} // end namespace Pomona

//  end Application.cxx
//  ############################################################################
