//  ############################################################################
//! @file       Date.cxx
//! @brief      Source for calendar date utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Date.hh"        // pomona: this class
#include "ClassImp.hh"    // pomona: class implementation
#include "TimePoint.hh"   // pomona: point in time (milliseconds)
#include <iomanip>        // std: out streamer manipulators
#include <sstream>        // std: stringstream
using std::chrono::system_clock;
using std::chrono::duration_cast;
using std::stringstream;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::Date)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
Date::Date()
  : Object(),
    mMSec(0), mSec(0), mMin(0), mHour(0),
    mDay(0), mMonth(0), mYear(0),
    mTzMin(0), mTzHour(0), mTzAbr("UTC"), mIsDst(kFALSE)
{
  setNow();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Tm_t (c struct tm) constructor. */
Date::Date(const Tm_t& tmDate)
  : Object(),
    mMSec(0), mSec(0), mMin(0), mHour(0),
    mDay(0), mMonth(0), mYear(0),
    mTzMin(0), mTzHour(0), mTzAbr("UTC"), mIsDst(kFALSE)
{
  set(tmDate);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Time_t (c type time_t) Unix time Stamp constructor. */
Date::Date(const Time_t& uts)
  : Object(),
    mMSec(0), mSec(0), mMin(0), mHour(0),
    mDay(0), mMonth(0), mYear(0),
    mTzMin(0), mTzHour(0), mTzAbr("UTC"), mIsDst(kFALSE)
{
  set(uts);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** TimePoint Unix time Stamp constructor. */
Date::Date(const TimePoint& uts)
  : Object(),
    mMSec(0), mSec(0), mMin(0), mHour(0),
    mDay(0), mMonth(0), mYear(0),
    mTzMin(0), mTzHour(0), mTzAbr("UTC"), 
    mIsDst(kFALSE)
{
  set(uts);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
Date::Date(const Date& other)
  : Object(other),
    mMSec(other.mMSec), mSec(other.mSec), mMin(other.mMin), mHour(other.mHour), 
    mDay(other.mDay), mMonth(other.mMonth), mYear(other.mYear), 
    mTzMin(other.mTzMin), mTzHour(other.mTzHour), mTzAbr(other.mTzAbr), 
    mIsDst(other.mIsDst)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
Date::Date(Date&& other)
  : Object(std::move(other)),
    mMSec(other.mMSec), mSec(other.mSec), mMin(other.mMin), mHour(other.mHour), 
    mDay(other.mDay), mMonth(other.mMonth), mYear(other.mYear), 
    mTzMin(other.mTzMin), mTzHour(other.mTzHour), mTzAbr(other.mTzAbr), 
    mIsDst(other.mIsDst)
{
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy assignment operator. */
Date& Date::operator=(const Date& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    mMSec = rhs.mMSec;  mSec = rhs.mSec; mMin = rhs.mMin; mHour = rhs.mHour;
    mDay = rhs.mDay; mMonth = rhs.mMonth; mYear = rhs.mYear; 
    mTzMin = rhs.mTzMin; mTzHour = rhs.mTzHour; mTzAbr = rhs.mTzAbr; 
    mIsDst = rhs.mIsDst;
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move assignment operator. */
Date& Date::operator=(Date&& rhs)
{
  if(!isSame(rhs)){
    // pilfer
    Object::operator=(std::move(rhs));
    mMSec = rhs.mMSec;  mSec = rhs.mSec; mMin = rhs.mMin; mHour = rhs.mHour;
    mDay = rhs.mDay; mMonth = rhs.mMonth; mYear = rhs.mYear; 
    mTzMin = rhs.mTzMin; mTzHour = rhs.mTzHour; mTzAbr = rhs.mTzAbr;  
    mIsDst = rhs.mIsDst;
    // destroy
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Tm_t (c struct tm) copy assignment operator. */
Date& Date::operator=(const Tm_t& rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Time_t (c type time_t) copy assignment operator. */
Date& Date::operator=(const Time_t& rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** TimePoint copy assignment operator. */
Date& Date::operator=(const TimePoint& rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void Date::clear()
{
  Object::clear();
  mMSec   = 0; 
  mSec    = 0; 
  mMin    = 0; 
  mHour   = 0;
  mDay    = 1; 
  mMonth  = 1; 
  mYear   = 1970; 
  mTzMin  = 0; 
  mTzHour = 0; 
  mIsDst  = kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t Date::isEmpty() const
{
  return (mMSec  == 0 && 
	  mSec   == 0 && 
	  mMin   == 0 && 
	  mHour  == 0 && 
	  mDay   == 1 && 
	  mMonth == 1 && 
	  mYear  == 1970);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t Date::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{ // attempt to cast to Date
      const Date& d = dynamic_cast<const Date&>(other); 
      // check members
      if(mMSec   != d.mMSec)   return kFALSE;
      if(mSec    != d.mSec)    return kFALSE;
      if(mMin    != d.mMin)    return kFALSE;
      if(mHour   != d.mHour)   return kFALSE;
      if(mDay    != d.mDay)    return kFALSE;
      if(mMonth  != d.mMonth)  return kFALSE;
      if(mYear   != d.mYear)   return kFALSE;
      if(mTzMin  != d.mTzMin)  return kFALSE;
      if(mTzHour != d.mTzHour) return kFALSE;
      if(mIsDst  != d.mIsDst)  return kFALSE;
      return kTRUE;
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t Date::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** Derived implementations should return:
    - <0 (-1) if other is "less than" this
    - >0 (+1) if other is "greater than" this
    -  0 else 
*/
Int_t Date::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return 0;
  else{
    try{ // attempt to cast to TimePoint
      const Date& d = dynamic_cast<const Date&>(other); 
      // compare members
      return asTimePoint().compare( d.asTimePoint() );
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t Date::defaultPrintContent() const
{
  return kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object class-name.
void Date::printValue(std::ostream& os) const
{
  os << asIso8601();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set to "now" according to system_clock.
void Date::setNow()
{
  set(TimePoint::Now());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set from Tm_t (c struct tm). */
void Date::set(const Tm_t& tmDate)
{
  mMSec   = 0; 
  mSec    = tmDate.tm_sec;
  mMin    = tmDate.tm_min;
  mHour   = tmDate.tm_hour;
  mDay    = tmDate.tm_mday;
  mMonth  = tmDate.tm_mon + 1;
  mYear   = tmDate.tm_year + 1900;
  // mTzMin  = 0;
  // mTzHour = 0;
  mIsDst  = (tmDate.tm_isdst > 0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set from Time_t (c type time_t) Unix Time Stamp. */
void Date::set(const Time_t& uts)
{
  // time_t is in seconds
  mMSec = 0; 
  // convert to Unix time at the GMT timezone
  const Tm_t* tmDate = gmtime(&uts);
  set(*tmDate);
  // set the time-zone values
  mTzMin  = 0;
  mTzHour = 0;
  mTzAbr  = "UTC";
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set from Time_t (c type time_t) local time. */
void Date::setLocal(const Time_t& local)
{
  // time_t is in seconds
  mMSec = 0; 
  // convert to Unix time at the GMT timezone
  const Tm_t* tmDate = localtime(&local);
  set(*tmDate);
  // set the time-zone values
  TzValues(*tmDate, mTzHour, mTzMin);
  mTzAbr  = TzAbbrev(*tmDate);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set from TimePoint Unix Time Stamp. */
void Date::set(const TimePoint& uts)
{
  // fish out chrono stuff
  system_clock::time_point tp(uts.get());
  Time_t tt = system_clock::to_time_t(tp);
  set(tt);
  // get the # milliseconds
  mMSec = UShort_t( uts.count() - 1000*duration_cast<seconds>(uts.get()).count() );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set from TimePoint local. */
void Date::setLocal(const TimePoint& local)
{
  // fish out chrono stuff
  system_clock::time_point tp(local.get());
  Time_t tt = system_clock::to_time_t(tp);
  setLocal(tt);
  // get the # milliseconds
  mMSec = UShort_t( local.count() - 1000*duration_cast<seconds>(local.get()).count() );
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get as Tm_t (c struct tm). */
Tm_t Date::asTm() const
{
  Tm_t tmDate;
  tmDate.tm_sec   = mSec;
  tmDate.tm_min   = mMin;
  tmDate.tm_hour  = mHour;
  tmDate.tm_mday  = mDay;
  tmDate.tm_mon   = mMonth - 1;
  tmDate.tm_year  = mYear - 1900;
  tmDate.tm_isdst = (mIsDst ? 1 : 0);
  return tmDate;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get as UTC Time_t (c type time_t). */
Time_t Date::asTime() const
{
  // convert as utc
  Time_t ret = Date::GmTime(asTm());
  // add time-zone offsets
  if(mTzHour != 0){
    if(mTzHour > 0) ret += mTzHour*3600 + mTzMin*60;
    else            ret += mTzHour*3600 - mTzMin*60;
  }
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get as UTC TimePoint. */
TimePoint Date::asTimePoint() const
{
  system_clock::time_point tp = system_clock::from_time_t(asTime());
  return TimePoint( duration_cast<milliseconds>(tp.time_since_epoch()) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get date as a (pseudo-) ISO-8601 String: "YYYY-MM-DD HH:MM:SS.sss+HHMM". */
String_t Date::asIso8601(Bool_t wms) const
{
  String_t ret("");
  ret += dateStr();
  ret += " ";
  ret += timeStr(wms);
  ret += tzStr();
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get date only as a String: "YYYY-MM-DD". */
String_t Date::dateStr() const
{
  stringstream ss;
  ss <<                      std::setw(4) <<           mYear  << "-";
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mMonth << "-";
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mDay;
  return ss.str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get time only as a String: "HH:MM:SS.sss". */
String_t Date::timeStr(Bool_t wms) const
{
  stringstream ss;
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mHour << ":";
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mMin  << ":";
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mSec;
  if(wms) ss << "." << std::setfill('0') << std::setw(3) << mMSec;
  return ss.str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get time-zone offset String: "+HHMM". */
String_t Date::tzStr() const
{
  stringstream ss;
  if(mTzHour >= 0 ) ss << "+" ;
  else              ss << "-";
  ss << std::setfill('0') << std::setw(2) << ( Short_t)mTzHour;
  ss << std::setfill('0') << std::setw(2) << (UShort_t)mTzMin;
  return ss.str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get date as verbose description. 
    @warning Always treated as local.
*/
String_t Date::asDescription() const
{
  // basically, to get the weekday (WTF?)
  Tm_t t = asTm();
  Time_t tt = mktime(&t);
  Tm_t* tmDate = localtime(&tt);
  return Description(*tmDate);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get UTC "zero" according to system_clock.
Date Date::Zero()
{
  return Date(TimePoint::Zero());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get UTC "now" according to system_clock.
Date Date::Now()
{
  return Date(TimePoint::Now());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get Local "now" according to system_clock.
Date Date::NowLocal()
{
  TimePoint tp = TimePoint::Now();
  Date d;
  d.setLocal(tp);
  return d;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! "Full" descritpion of a time.
String_t Date::Description(const Tm_t& tmDate)
{
  // // put it ... not available until GCC 5.x! :-(
  // stringstream ss;
  // ss << std::put_time(tmDate, "%T (%Z) on %A %B %e, %Y");
  // return ss.str();

  Char_t* str = new Char_t[128];
  strftime(str, 128, "%T (%Z) on %A %B %e, %Y", &tmDate);
  String_t tstr(str);
  if(str){ delete [] str; str = 0; }
  return tstr;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Timezone name or abbreviation.
String_t Date::TzAbbrev(const Tm_t& tmDate)
{
  Char_t* str = new Char_t[4];
  strftime(str,4,"%Z",&tmDate);
  // loutD(kInput) << "Date::TzAbbrev: " << str << endl;
  String_t tstr(str);
  if(str){ delete [] str; str = 0; }
  return tstr;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! ISO 8601 offset String from UTC in timezone (1 minute=1, 1 hour=100)
String_t Date::TzStr(const Tm_t& tmDate)
{
  Char_t* str = new Char_t[6];
  strftime(str,6,"%z",&tmDate);
  // loutD(kInput) << "Date::TzStr: " << str << endl;
  String_t tstr(str);
  if(str){ delete [] str; str = 0; }
  return tstr;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Parse the time-zone String, e.g. "+hhmm" into hours & minutes offset. */
void Date::TzParseStr(const String_t& tzStr, Int8_t& hour, UInt8_t& min)
{    
  // loutD(kInput) << "Date::TzParseStr: " <<  tzStr << endl;

  // // check length
  // if(tzStr.size() < 5){
  //   // loutE(InputArguments) << "Date::TzParseStr: Ill-formatted string, " 
  //   // 			  << "time-zone offset too short."  << endl;
  //   return;
  // }
  // // check format
  // if(tzStr[0] != '+' && tzStr[0] != '-'){
  //   // loutE(InputArguments) << "Date::TzParseStr: Ill-formatted string, "
  //   // 			  << "no sign." << endl;
  //   return;
  // }

  // extract values
  hour = Int8_t ( atoi( tzStr.substr(0,3).c_str() ) );
  min  = UInt8_t( atoi( tzStr.substr(3,2).c_str() ) );

  // check for negative
  if(tzStr[0]=='-') hour *= -1;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Parse the Tm_t struct into time-zone hours & minutes offset. */
void Date::TzValues(const Tm_t& tmDate, Int8_t& hour, UInt8_t& min)
{
  TzParseStr(TzStr(tmDate), hour, min);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Parse the Tm_t struct into time-zone seconds offset. */
Int_t Date::TzSeconds(const Tm_t& tmDate)
{
  // parse hours and minutes
  Int8_t hour(0); UInt8_t min(0);
  TzValues(tmDate, hour, min);
  // convert to seconds
  Int_t sec(0);
  if(hour < 0) sec = -(-hour*3600 + min*60);
  else         sec =    hour*3600 + min*60;
  return sec;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Returns the value of type Time_t that represents the UTC time described 
    by the tm structure timeInfo.

    Similar to standard routine "mktime" but
    using the assumption that tm struct is filled with UTC, not local, time.
   
    @param tmDate C/C++ standard library time struct.
           See <a href="cplusplus.com/reference/ctime/tm/" >tm</a>.

    @warning This version IS NOT configured to handle every possible
             weirdness of out-of-range values in the case of normalizing
	     the tm struct.
   
    @warning This version DOES NOT correctly handle values that can't be
             fit into a Time_t (i.e. beyond year 2038-01-18 19:14:07, or
	     before the start of Epoch).
*/
Time_t Date::GmTime(const Tm_t& tmDate)
{
  // check for leap-year
  Bool_t isLeapYear = (tmDate.tm_year % 4 == 0) && 
                      !( (tmDate.tm_year % 100 == 0) && 
			 (tmDate.tm_year % 400 >  0) );

  // get days in months
  static const Int_t DaysInMonths[] = { 31, (isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  // DaysInMonths[1] = isLeapYear ? 29 : 28;

  // copy input struct so that input doesn't change
  Tm_t tm_cp = tmDate;

  // reference values
  Int_t ref_tm_mon = tm_cp.tm_mon;
  Int_t ref_tm_mday = tm_cp.tm_mday;

  // count days in months past
  tm_cp.tm_yday = 0;
  for(Int_t imonth = 0; imonth < ref_tm_mon; imonth++) {
    tm_cp.tm_yday += DaysInMonths[imonth];
  }
  // day [1-31] but yday [0-365]
  tm_cp.tm_yday += ref_tm_mday - 1;

  // adjust if day in this month is more than the month has
  while(ref_tm_mday > DaysInMonths[ref_tm_mon]) {
    ref_tm_mday -= DaysInMonths[ref_tm_mon];
    ref_tm_mon++;
  }

  // *should* calculate tm_wday (0-6) here ...

  // UTC is never DST
  tm_cp.tm_isdst = 0;

  // Calculate seconds since the Epoch based on formula in
  // POSIX IEEEE Std 1003.1b-1993 pg 22
  return (tm_cp.tm_sec +
	  tm_cp.tm_min*60 +
	  tm_cp.tm_hour*3600 +
	  tm_cp.tm_yday*86400 +
	  (tm_cp.tm_year-70)*31536000 +
	  ((tm_cp.tm_year-69)/4)*86400);  
}

} // end namespace Pomona

//  end Date.cxx
//  ############################################################################
