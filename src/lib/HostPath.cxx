//  ############################################################################
//! @file       HostPath.cxx
//! @brief      Source for host system path handling
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "HostPath.hh"    // pomona: this class
#include "ClassImp.hh"    // pomona: class implementation
#include "HostSystem.hh"  // pomona: host OS interface
#include "LogService.hh"      // pomona: log message utility

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  class implementation macro
CLASS_IMP(Pomona::HostPath)
// //! Cereal: Register derived polymorphic type -- HostPath 
// CEREAL_REGISTER_TYPE(Pomona::HostPath)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
HostPath::HostPath(const String& p)
  : String(p)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Constructor from character arrays. */
HostPath::HostPath(const Char_t* p)
  : String(p)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
HostPath::HostPath(const HostPath& other)
  : String(other)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
HostPath::HostPath(HostPath&& other)
  : String(other)
{
  other.HostPath::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: copy assignment.
HostPath& HostPath::operator=(const HostPath& rhs)
{
  if(!isSame(rhs)) String::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: move assignment.
HostPath& HostPath::operator=(HostPath&& rhs)
{
  if(!isSame(rhs)){
    String::operator=(rhs);
    rhs.HostPath::clear();
  } 
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: copy assignment from String
HostPath& HostPath::operator=(const String& rhs)
{
  String::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: copy assignment from character array
HostPath& HostPath::operator=(const Char_t* rhs)
{
  String::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*! Return the directory name in pathname.
    dirName of "/user/root" is "/user". 
    In case no dir-name is specified "." is returned. 
*/
HostPath HostPath::dirName() const
{
  HostPath hp(*this); 
  hp.set( HostSystem::DirName( HostPath::get().c_str() ) );
  return hp;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*! Base name of a file name. 
    basename of "/user/root" is "root". 
*/
HostPath HostPath::baseName() const
{
  HostPath hp(*this); 
  hp.set( HostSystem::BaseName( HostPath::get().c_str() ) );
  return hp;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*! Return the extension name in pathname.
    extName of "/user/root.exe" is ".exe". 
    In case no dirname is found "" is returned. 
*/
String HostPath::extName() const
{
  size_t dotPos = get().find_last_of('.');
  if(dotPos == String::npos) return "";
  else return get().substr(dotPos, get().size());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get path-string with a part aliased
String HostPath::asAliased(const HostPath& aliasPath) const 
{
  if(aliasPath.isEmpty()) return get();
  size_t aPos = get().find(aliasPath.get());
  if(aPos == String::npos) return get();
  else{
    // String alias = "${" + aliasPath.name() + "}";
    String alias = "${...}";
    String aliased(get());
    aliased.replace(aPos, aliasPath.get().size(), alias);
    return aliased;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Check access to this path
Bool_t HostPath::access(const HostSystem::eAccessMode& mode) const
{
  return HostSystem::Access(get(), mode);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does the path exist?
Bool_t HostPath::exist() const
{
  return access(HostSystem::eAccessMode::kExists);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the path readable?
Bool_t HostPath::read() const
{
  return access(HostSystem::eAccessMode::kRead);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the path writable?
Bool_t HostPath::write() const
{
  return access(HostSystem::eAccessMode::kWrite);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the path executable?
Bool_t HostPath::execute() const
{
  return access(HostSystem::eAccessMode::kExecute);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Append a path to this and return this
HostPath& HostPath::append(const HostPath& path)
{
  String_t cpyPath(path.get());                       // copy input for non-const
  cpyPath = HostSystem::PrependPath(get().c_str(), cpyPath);  // prepend this to other
  set(cpyPath);                                       // set this to other
  return (*this);                                     // return this
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Prepend a path to this.
HostPath& HostPath::prepend(const HostPath& path)
{
  HostSystem::PrependPath(path.get().c_str(), get());
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Replace the extension
HostPath& HostPath::replaceExt(const String& ext)
{
  String newExt = ext;
  if(newExt!="" && newExt[0]!='.') newExt = '.' + ext;
  String curExt = extName();
  if(!curExt.isEmpty()) replaceAll(curExt, newExt);
  else                  append(HostPath(newExt));
  return (*this);
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Replace the extension
HostPath& HostPath::stripExt()
{
  return replaceExt("");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Compare this to other. 
    Comparison is done in terms of path hierarchy. 
    Returns: 
    - +1 if this  begins with other directory name
    - -1 if other begins with this  directory name 
    -  0 else
*/
Int_t HostPath::compare(const Comparable& other) const
{
  // check pointer
  if(isSame(other) || isEqual(other)) return 0;
  try{ // attempt cast
    const HostPath& o = dynamic_cast<const HostPath&>(other); 
    // check members
    HostPath odn(o.dirName());
    if     ( dirName() == odn                    ) return  0;
    else if( get().find_first_of(odn.get()) == 0 ) return +1;
    else if( odn.get().find_first_of(get()) == 0 ) return -1;
    else                                           return  0;
  }
  catch(const std::bad_cast& bc){ return 0; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t HostPath::defaultPrintContent() const
{
  return kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: add rhs with this and return this
HostPath& HostPath::operator+=(const String& rhs)
{
  String::operator+=(rhs);
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: add rhs with this and return this
HostPath& HostPath::operator+=(const Char_t* rhs)
{
  String::operator+=(rhs);
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: add lhs to rhs
HostPath operator+(const HostPath& lhs, const String& rhs)
{
  HostPath l(lhs);
  l += rhs;
  return std::move(lhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: add lhs to rhs
HostPath operator+(const HostPath& lhs, const Char_t* rhs)
{
  HostPath l(lhs);
  l += rhs;
  return std::move(lhs);
}

} // end namespace Pomona

//  end HostPath.cxx
//  ############################################################################
