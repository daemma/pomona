//  ############################################################################
//! @file       HostSystem.cxx
//! @brief      Source for host os-system utility class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "HostSystem.hh"  // pomona: this class
#include "LogService.hh"  // pomona: log message service
#include <cstdlib>        // cstd: general purpose functions
#include <string.h>       // cstd: string tools
#ifdef POMONA_OS_UNIX     // Using *nix
#include <unistd.h>       // posix: operating system API
#include <sys/stat.h>     // *ix: mkdir
#endif                    // end using *nix
#ifdef CERNROOT           // Using root
#include "TSystem.h"      // root: "base" class
#endif                    // end using root

namespace Pomona {

//  ****************************************************************************
//  ****************************************************************************
#ifdef CERNROOT  // Using root
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get pointer to root-system OS interface
TSystem& HostSystem::System()
{
  return *gSystem;
}
#endif            // end CERNROOT

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the current process id number
UInt_t HostSystem::Pid()
{
  return ::getpid();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Exit the application
void HostSystem::Exit(const Int_t& code, const Bool_t& performCleanup)
{
#ifdef CERNROOT  // Using root
  System().Exit(code, performCleanup);
#else            // Not using root
  if(performCleanup)  ::exit(code);
  else                ::_exit(code);
#endif           // end using root
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Execute a system command
/*! From http://www.cplusplus.com/reference/cstdlib/system/:
    Invokes the command processor to execute a command.
    If command is a null pointer, the function only checks whether a command processor 
    is available through this function, without invoking any command.

    @return:
    If command is a null pointer, the function returns a non-zero value in case a 
    command processor is available and a zero value if it is not.
    If command is not a null pointer, the value returned depends on the system and 
    library implementations, but it is generally expected to be the status code returned 
    by the called command, if supported.    
*/
Int_t HostSystem::Exec(const Char_t* cmd)
{
#ifdef CERNROOT  // Using root
  return System().Exec(cmd);
#else            // Not using root
  return ::system(cmd);
#endif           // end using root
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Make a directory
/*! https://pubs.opengroup.org/onlinepubs/009695399/functions/mkdir.html

    @return 
    Upon successful completion, mkdir() shall return 0. 
    Otherwise, -1 shall be returned, no directory shall be created, 
    and errno shall be set to indicate the error.
*/
Int_t HostSystem::MkDir(const Char_t* dir)
{
#ifdef CERNROOT  // Using root
  return System().mkdir(dir);
#else  // Not using root
#ifdef POMONA_OS_UNIX  // Using *nix
  return ::mkdir(dir, S_IFDIR);
#else  // Not using *nix
  loutC(kInput) << "HostSystem::MkDir: Not implemented for Windows w/o CERN-ROOT" << endl;
  return -1;
#endif  // end POMONA_OS_UNIX
#endif  // end CERNROOT
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Base name of a file name.
/** Base name of /user/root is root.
    But the base name of '/' is '/', 'c:\' is 'c:\' */
const Char_t* HostSystem::BaseName(const Char_t* path)
{
  return BaseNameImp(path);

  // // get as character arrays
  // const Char_t* pn = path.c_str();
  // const Char_t* bn = BaseNameImp(pn);
  // // copy to string & clean-up
  // String_t ret("");
  // if(bn){ ret = bn; }
  // return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Base name of a file name, implementation.
/** Base name of /user/root is root.
    But the base name of '/' is '/', 'c:\' is 'c:\' */
const Char_t* HostSystem::BaseNameImp(const Char_t* path)
{
#ifdef CERNROOT  // Using root
  return System().BaseName(path);
#else            // Not using root

#ifdef POMONA_OS_UNIX  // Using a Unix-type system
  // This code is copied from CERN's root-system TSystem class
  // https://root.cern.ch/doc/v608/TSystem_8cxx_source.html#l00929
  if(path){
    if (path[0] == '/' && path[1] == '\0') return path;
    char *cp; if((cp = (char*)strrchr(path, '/'))) return ++cp;
    return path;
  }
  loutE(kInput) << "HostSystem::BaseName: Empty path" << endl;
  return "";

#else  // Not POMONA_OS_UNIX
  // This code is copied from CERN's root-system TSystem class
  // https://root.cern.ch/doc/v608/TWinNTSystem_8cxx_source.html#l01173
  if (path) {
    int idx = 0;
    const char *symbol=path;
    // Skip leading blanks
    while ( (*symbol == ' ' || *symbol == '\t') && *symbol) symbol++;
    if (*symbol) {
      if (isalpha(symbol[idx]) && symbol[idx+1] == ':') idx = 2;
      if ( (symbol[idx] == '/'  ||  symbol[idx] == '\\')  &&  symbol[idx+1] == '\0') {
	//return StrDup(symbol);
	return symbol;
      }
    } else {
      // Error("BaseName", "name = 0");
      loutE(kInput) << "HostSystem::BaseName: Empty path" << endl;
      return 0;
    }
    char *cp;
    char *bslash = (char *)strrchr(&symbol[idx],'\\');
    char *rslash = (char *)strrchr(&symbol[idx],'/');
    if (cp = std::max(rslash, bslash)) {
      //return StrDup(++cp);
      return ++cp;
    }
    //return StrDup(&symbol[idx]);
    return &symbol[idx];
  }
  // Error("BaseName", "name = 0");
  loutE(kInput) << "HostSystem::BaseName: path is empty" << endl;
  return 0;

#endif           // end POMONA_OS_UNIX
#endif           // end CERNROOT
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Directory name of a file name, implementation. 
/** Return the directory name in pathname. DirName of /user/root is /user.
    In case no dirname is specified "." is returned. */
const Char_t* HostSystem::DirName(const Char_t* path)
{
  return DirNameImp(path);

  // // get as character arrays
  // const Char_t* pn = path.c_str();
  // const Char_t* bn = DirNameImp(pn);
  // // copy to string & clean-up
  // String_t ret("");
  // if(bn){ ret = bn; /*delete [] bn; bn = 0;*/ }
  // return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Directory name of a file name, implementation. 
/** Return the directory name in pathname. DirName of /user/root is /user.
    In case no dirname is specified "." is returned. */
const Char_t* HostSystem::DirNameImp(const Char_t* path)
{
#ifdef CERNROOT  // Using root
  return System().DirName(path);
#else            // Not using root

#ifdef POMONA_OS_UNIX  // Using a Unix-type system
  // This code is copied from CERN's root-system TSystem class
  // https://root.cern.ch/doc/v608/TSystem_8cxx_source.html#l00997
  if (path && strchr(path, '/')) {
    // R__LOCKGUARD2(gSystemMutex);

    static int len = 0;
    static char *buf = 0;
    int pathlen = strlen(path);
    if (pathlen > len) {
      delete [] buf;
      len = pathlen;
      buf = new char [len+1];
    }
    strcpy(buf, path);

    char *r = buf+pathlen-1;
    // First skip the trailing '/'
    while ( r>buf && *(r)=='/') { --r; }
    // Then find the next non slash
    while ( r>buf && *(r)!='/') { --r; }
    // Then skip duplicate slashes
    // Note the 'r>buf' is a strict comparison to allows '/topdir' to return '/'
    while ( r>buf && *(r)=='/') { --r; }
    // If all was cut away, we encountered a rel. path like 'subdir/'
    // and ended up at '.'.
    if (r==buf && *(r)!='/') {
      return ".";
    }
    // And finally terminate the string to drop off the filename
    *(r+1) = '\0';

    return buf;
  }
  return ".";

#else  // Not POMONA_OS_UNIX
  // This code is copied from CERN's root-system TSystem class
  // https://root.cern.ch/doc/v608/TWinNTSystem_8cxx_source.html#l02394
  // It creates output with 'new char []' operator. Returned string has to
  // be deleted.

  static char *fDirNameBuffer;
  // Delete old buffer
  if (fDirNameBuffer) { fDirNameBuffer = 0; }
  
  // Create a buffer to keep the path name
  if(path){
     if (strchr(path, '/') || strchr(path, '\\')) {
       const char *rslash = strrchr(path, '/');
       const char *bslash = strrchr(path, '\\');
       const char *r = std::max(rslash, bslash);
       const char *ptr = path;
       while (ptr <= r) {
	 if (*ptr == ':') {
	   // Windows path may contain a drive letter
	   // For NTFS ":" may be a "stream" delimiter as well
	   path =  ptr + 1;
	   break;
	 }
	 ptr++;
       }
       int len =  r - path;
       if (len > 0) {
	 fDirNameBuffer = new char[len+1];
	 memcpy(fDirNameBuffer, path, len);
	 fDirNameBuffer[len] = 0;
       }
     }
   }
   if (!fDirNameBuffer) {
     fDirNameBuffer = new char[1];
     *fDirNameBuffer = '\0'; // Set the empty default response
   }
   return fDirNameBuffer;

#endif           // end POMONA_OS_UNIX
#endif           // end CERNROOT
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Concatenate a directory and a file name, implementation. 
const Char_t* HostSystem::PrependPath(const Char_t* path, String_t& fileName)
{
  return PrependPathImp(path, fileName);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Concatenate a directory and a file name, implementation. 
const Char_t* HostSystem::PrependPathImp(const Char_t* path, String_t& fileName)
{
#ifdef CERNROOT  // Using root

  // loutV(kEval) << "HostSystem::PrependPathImp: path: " << path << endl;
  // loutV(kEval) << "HostSystem::PrependPathImp: file: " << fileName << endl;

  TString tsn(fileName);
  const Char_t* cfn = System().PrependPathName(path, tsn);
  fileName = tsn;
  return cfn;

  // TString tsn(name);
  // system().PrependPathName(path.c_str(), tsn);
  // name = tsn;
  // return name;


  // TString fn(fileName);
  // name = tsn;
  // return System().PrependPathName(path,fn);

#else            // Not using root

#ifdef POMONA_OS_UNIX  // Using a Unix-type system
  // This code is copied from CERN's root-system TSystem class
  // https://root.cern.ch/doc/v608/TUnixSystem_8cxx_source.html#l01513
   if (fileName.empty() || fileName == ".") {
      if (path) {
         fileName = path;
         if (path[strlen(path) - 1] != '/')
            fileName += '/';
      } else fileName = "";
      return fileName.c_str();
   }

   if (!path || !path[0]) path = "/";
   else if (path[strlen(path) - 1] != '/') fileName = String_t("/") + fileName;
   fileName = String_t(path) + fileName;
   return fileName.c_str();

#else  // Not POMONA_OS_UNIX
   loutA(kInput) << "HostSystem::PrependPathImp: Not implemented!" << endl;
   return "";

#endif           // end POMONA_OS_UNIX
#endif           // end CERNROOT
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Can the path be accessed using the specified mode?
Bool_t HostSystem::Access(const String_t& dir, const eAccessMode& mode)
{
#ifdef CERNROOT  // Using root
  // map eAccessMode -> EAccessMode
  EAccessMode ram = kFileExists;
  switch(mode){
  case eAccessMode::kExists  : { ram = kFileExists;        break; }
  case eAccessMode::kExecute : { ram = kExecutePermission; break; }
  case eAccessMode::kWrite   : { ram = kWritePermission;   break; }
  case eAccessMode::kRead    : { ram = kReadPermission;    break; }
  }
  // return the negation of the root method
  return !System().AccessPathName(dir.c_str(), ram);
#else  // Not using root
#ifdef POMONA_OS_UNIX  // Using *nix
  return (::access(dir.c_str(), static_cast<Int_t>(mode)) == 0);
#else  // Not using *nix
  loutC(kInput) << "HostSystem::Access: Not implemented for Windows w/o CERN-ROOT" 
		<< endl;
  return kFALSE;
#endif  // end POMONA_OS_UNIX
#endif  // end CERNROOT
}

} // end namespace Pomona


//  end HostSystem.cxx
//  ############################################################################
