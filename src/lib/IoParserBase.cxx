//  ############################################################################
//! @file       IoParserBase.cxx
//! @brief      Source for file parser helper base class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-03-31
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "IoParserBase.hh"  // pomona: this class
#include "String.hh"        // pomona: enhanced string class
#include "HostPath.hh"      // pomona: host system filie path
#include "LogService.hh"    // pomona: log-message helper
#include <fstream>          // std: fstream

namespace Pomona {

namespace Io {

namespace Parser {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: character array
Base::Base(const Char_t* p, const Bool_t& isBin)
  : mPath(p)
  , mIfs(new ifstream)
  , mFileSize(0)
  , mIsGood(kFALSE)
  , mIsBin(isBin)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: std::string
Base::Base(const String_t& p, const Bool_t& isBin)
  : mPath(p)
  , mIfs(new ifstream)
  , mFileSize(0)
  , mIsGood(kFALSE)
  , mIsBin(isBin)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: String
Base::Base(const String& p, const Bool_t& isBin)
  : mPath(p)
  , mIfs(new ifstream)
  , mFileSize(0)
  , mIsGood(kFALSE)
  , mIsBin(isBin)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: HostPath
Base::Base(const HostPath& p, const Bool_t& isBin)
  : mPath(p)
  , mIfs(new ifstream)
  , mFileSize(0)
  , mIsGood(kFALSE)
  , mIsBin(isBin)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Destructor
Base::~Base()
{
  if(mIfs){ 
    if(mIfs->is_open()) mIfs->close();
    // // Destruction should be correctly handled by unique_ptr magic!
    // delete mIfs; 
    // mIfs = nullptr; 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access path (const)
const String_t& Base::path() const
{
  return mPath;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access the size [bytes] of the file
const UInt64_t& Base::fileSize() const
{
  return mFileSize;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the size [mega-bytes] of the file
Float_t Base::fileSizeMB() const
{
  return std::move( Float_t(mFileSize) * 1.e-6 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the ifstream pointer null?
Bool_t Base::ifsIsNull() const
{
  if(mIfs == nullptr || mIfs == NULL) return kTRUE;
  else                                return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Returns whether the stream is currently associated to a file.
Bool_t Base::ifsIsOpen() const
{
  if(!ifsIsNull()) return mIfs->is_open();
  else             return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the file good, e.g. !(eofbit, failbit and badbit)?
Bool_t Base::ifsIsGood() const
{
  if(!ifsIsNull()) return mIfs->good();
  else             return kFALSE;
}

//  ++++++++++++++++++++++++++1++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does the file "look good"?
const Bool_t& Base::isGood() const
{
  return mIsGood;
}

//  ++++++++++++++++++++++++++1++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the read-mode binary?
const Bool_t& Base::isBinary() const
{
  return mIsBin;
}

//  ++++++++++++++++++++++++++1++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set whether the read-mode is binary?
void Base::setIsBinary(const Bool_t& val)
{
  mIsBin = val;
}

//  ++++++++++++++++++++++++++1++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access the input file streamer (const)
/*! @warning streamer pointer not checked. */
const ifstream& Base::ifs() const
{
  return *mIfs;
}

//  ++++++++++++++++++++++++++1++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access the input file streamer
/*! @warning streamer pointer not checked. */
ifstream& Base::ifs()
{
  return *mIfs;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the size of the file (in-stream) in bytes.
UInt64_t Base::calcSize() const
{
  if(ifsIsGood()){
    UInt64_t currentPos = curPos();  // current possition
    mIfs->seekg(0, std::ios::end);   // seek to end
    UInt64_t length = curPos();      // size at end
    mIfs->clear();                   // clear and reset state flags
    mIfs->seekg(currentPos);         // reset possition
    return length;                   // return size in bytes    
  }
  else return 0;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Current position of file pointer
/*! @warning File streamer goodness not checked. */
UInt64_t Base::curPos() const
{
  return mIfs->tellg();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Close the file streamer.
void Base::close() const
{
  if(ifsIsOpen()) mIfs->close();
}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Instantiate the input file-streamer and set the read-mode
Bool_t Base::openStream()
{
  std::ios_base::openmode mode = ifstream::in;
  if(mIsBin) mode |= ifstream::binary;
  // mIfs = new ifstream(mPath.c_str(), mode);
  mIfs->open(mPath, mode);
  return ifsIsOpen();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Open the file path
const Bool_t& Base::open()
{
  mIsGood = openStream();
  if(mIsGood){
    loutD(kFile) << "Base::open: Opened file: " << mPath << endl;
    mFileSize = calcSize();
    loutD(kFile) << "Base::open: File size [bytes]: " << mFileSize << endl;
    if(mFileSize > 0)
      loutV(kFile) << "Base::open: Start Pos: " << curPos() << endl;
    else
      loutW(kFile) << "Base::open: Empty file: " << mPath << endl;
  } // end ifsIsOpen()
  else loutE(kFile) << "Base::open: Failed to open file: " << mPath << endl;
  return mIsGood;
}

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona


//  end IoParserBase.cxx
//  ############################################################################
