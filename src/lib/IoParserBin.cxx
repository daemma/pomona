//  ############################################################################
//! @file       IoParserBin.cxx
//! @brief      Source for binary file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "IoParserBin.hh"   // pomona: this class
#include "LogService.hh"    // pomona: log-message service
#include <fstream>          // std: fstream

namespace Pomona {

namespace Io {

namespace Parser {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: character array
Bin::Bin(const Char_t* p)
  : Base(p, kTRUE)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: std::string
Bin::Bin(const String_t& p)
  : Base(p, kTRUE)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a string from the infile-stream
Bool_t Bin::readStr(String_t& val, const UInt_t& length)
{
  if(ifsIsGood() && length > 0){
    val.resize(length);
    ifs().read((Char_t*)(&val[0]), length);
    if(ifs().gcount() == length) return kTRUE;
    else{
      loutW(kFile) << "Bin::readStr: Read " << ifs().gcount() << " of " 
		   << length << " characters." << endl;
      return kFALSE;
    }
  }
  else{
    loutE(kFile) << "Bin::readStr: Not good." << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a character from the infile-stream
Bool_t Bin::readChr(Char_t& val)
{
  if(ifsIsGood()){
    ifs().read((Char_t*)(&val), 1);
    if(ifs().gcount() == 1) return kTRUE;
    else{
      loutW(kFile) << "Bin::readChr: Read " << ifs().gcount() 
		   << " characters." << endl;
      return kFALSE;
    }
  }
  else{
    loutE(kFile) << "Bin::readChr: Not good." << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a float from the infile-stream
Bool_t Bin::readFlt(Float_t& val)
{
  if(ifsIsGood()){
    ifs().read((Char_t*)(&val), 4);
    if(ifs().gcount() == 4) return kTRUE;
    else{
      loutW(kFile) << "Bin::readFlt: Read " << ifs().gcount() 
		   << " bytes." << endl;
      return kFALSE;
    }
  }
  else{
    loutE(kFile) << "Bin::readFlt: Not good." << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a signed integer from the infile-stream
Bool_t Bin::readInt(Int_t& val)
{
  if(ifsIsGood()){
    ifs().read((Char_t*)(&val), 4);
    if(ifs().gcount() == 4) return kTRUE;
    else{
      loutW(kFile) << "Bin::readInt: Read " << ifs().gcount() 
		   << " bytes." << endl;
      return kFALSE;
    }
  }
  else{
    loutE(kFile) << "Bin::readInt: Not good." << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read an unsigned integer from the infile-stream
Bool_t Bin::readInt(UInt_t& val)
{
  if(ifsIsGood()){
    ifs().read((Char_t*)(&val), 4);
    if(ifs().gcount() == 4) return kTRUE;
    else{
      loutW(kFile) << "Bin::readInt: Read " << ifs().gcount() 
		   << " bytes." << endl;
      return kFALSE;
    }
  }
  else{
    loutE(kFile) << "Bin::readInt: Not good." << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a vector of floats from the infile-stream
Bool_t Bin::readVec(std::vector<Float_t>& vals, const UInt_t& num)
{
  if(ifsIsGood()){
    vals.clear();
    Float_t hold(0.);
    for(UInt_t c=0; c<num; c++){
      if(readFlt(hold)) vals.push_back(hold);
      else              return kFALSE;
    }
    return kTRUE;
  }
  else{
    loutE(kFile) << "Bin::readVec: Not good." << endl;
    return kFALSE;
  }
}

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona


//  end IoParserBin.cxx
//  ############################################################################
