//  ############################################################################
//! @file       IoParserCsv.cxx
//! @brief      Source for text file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-03-31
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "IoParserCsv.hh"  // pomona: this class
#include "String.hh"       // pomona: enhanced string
#include <fstream>         // std: file streamer

namespace Pomona {

namespace Io {

namespace Parser {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: character array
Csv::Csv(const Char_t* p, const Char_t& d)
  : Txt(p)
  , mDelim(d)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: std::string
Csv::Csv(const String_t& p, const Char_t& d)
  : Txt(p)
  , mDelim(d)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: String
Csv::Csv(const String& p, const Char_t& d)
  : Txt(p)
  , mDelim(d)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: HostPath
Csv::Csv(const HostPath& p, const Char_t& d)
  : Txt(p)
  , mDelim(d)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the deliminator
const Char_t& Csv::delim() const
{
  return mDelim;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the deliminator
void Csv::setDelim(const Char_t& d)
{
  mDelim = d;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read and parse a line of text into a vector of Strings
Row_t Csv::getRow()
{
  Row_t line;
  // check for stream goodness
  if(!isGood()) return line;

  // helper variables
  String field;
  Char_t achar;
  Bool_t newLine(kFALSE), inQuote(kFALSE), lastCharWasAQuote(kFALSE);
  // gobble up characters and check for a new line condition
  while(!newLine && ifs().get(achar)){
    switch(achar){
    case '"':
      // begining/ending a quoted field
      newLine = kFALSE;
      if(lastCharWasAQuote){
  	lastCharWasAQuote = kFALSE;
  	field += achar;
      }
      else inQuote = !inQuote;
      break;

    case ',':
      // field deliminator
      newLine = kFALSE;
      if(inQuote) field += achar;
      else{
  	line.push_back(field);
  	field.clear();
      }
      break;

    case '\n':
    case '\r':
      // new line
      if(inQuote) field += achar;
      else{
  	if(!newLine){
  	  line.push_back(field);
  	  newLine = kTRUE;
  	}
      }
      break;

    default:
      // default is to gobble characters
      newLine = kFALSE;
      field += achar;
      break;
    }  // end char switch
  }  // end while !newLine

  // return the parsed line
  return line;
}

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona

//  end Csv.cxx
//  ############################################################################



  // std::ifstream ifs(fileName);
  // if(!ifs){
  //   oocoutE((TObject*)0,InputArguments)
  //     << "WdCsvParser::ParseRawCsv: failed to open file " 
  //     << fileName << endl;
  //   return kTRUE;
  // }

  // // get size of file:
  // ifs.seekg (0, std::ios::end);
  // size_t length = ifs.tellg();
  // ifs.seekg (0, std::ios::beg);
  // oocoutP((TObject*)0,DataHandling)
  //   << "ParseRawCsv: Reading raw data from CSV file " << fileName 
  //   << " ("  << 1.*length/1e6 << " MB) ... " << endl;

  // // parse the whole file
  // lines.clear();
  // Bool_t inQuote(kFALSE);
  // Bool_t lastCharWasAQuote(kFALSE);
  // Bool_t newLine(kFALSE);
  // TString field;
  // vector<TString> line;

  // char achar;
  // while( !ifs.eof() ){
  //   ifs.get(achar);
  //   switch (achar){
  //   case '"':
  //     newLine = kFALSE;
  //     if(lastCharWasAQuote == kTRUE){
  // 	lastCharWasAQuote = kFALSE;
  // 	field += achar;
  //     }
  //     else inQuote = !inQuote;
  //     break;

  //   case ',':
  //     newLine = kFALSE;
  //     if(inQuote == kTRUE) field += achar;
  //     else{
  // 	line.push_back(field);
  // 	field.clear();
  //     }
  //     break;

  //   case '\n':
  //   case '\r':
  //     if(inQuote == kTRUE) field += achar;
  //     else{
  // 	if(newLine == kFALSE){
  // 	  line.push_back(field);
  // 	  lines.push_back(line);
  // 	  field.clear();
  // 	  line.clear();
  // 	  newLine = kTRUE;
  // 	}
  //     }
  //     break;

  //   default:
  //     newLine = kFALSE;
  //     field.Append(achar);
  //     break;
  //   } // end char switch
  // } // endl while !eof

  // if(line.size()){
  //   if(field.Length()) line.push_back(field);
  //   lines.push_back(line);
  // }

  // // clean and return
  // ifs.close();
  // return kFALSE;


