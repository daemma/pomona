//  ############################################################################
//! @file       IoParserTxt.cxx
//! @brief      Source for text file parser helper class.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "IoParserTxt.hh"  // pomona: this class
#include "LogService.hh"     // pomona: log-message helper
#include "String.hh"     // pomona: enhanced string
#include <fstream>            // std: fstream

namespace Pomona {

namespace Io {

namespace Parser {

// This declaration should not be needed in C++17, WTF!?
constexpr UShort_t Txt::mDefBufSize;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: character array
Txt::Txt(const Char_t* p)
  : Base(p, kFALSE)
  , mBufSz(0)
  , mBuffer(nullptr)
{
  init(mDefBufSize);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: std::string
Txt::Txt(const String_t& p)
  : Base(p, kFALSE)
  , mBufSz(0)
  , mBuffer(nullptr)
{
  init(mDefBufSize);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: String
Txt::Txt(const String& p)
  : Base(p, kFALSE)
  , mBufSz(0)
  , mBuffer(nullptr)
{
  init(mDefBufSize);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: HostPath
Txt::Txt(const HostPath& p)
  : Base(p, kFALSE)
  , mBufSz(0)
  , mBuffer(nullptr)
{
  init(mDefBufSize);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Initialize memory
void Txt::init(const UShort_t& bufSz)
{
  if(bufSz == 0) loutW(kFile) << "Txt::init: Zero buffer size" << endl;
  mBufSz  = bufSz;
  mBuffer = BufPtr_t(new Char_t[mBufSz]());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the size of the buffer / initialize memory
void Txt::setBufferSize(const UShort_t& bufSz)
{
  init(bufSz);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access buffer size
const UShort_t& Txt::bufferSize() const
{
  return mBufSz;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access buffer managed pointer (const)
const BufPtr_t& Txt::bufferPtr() const
{
  return mBuffer;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access buffer managed pointer
BufPtr_t& Txt::bufferPtr()
{
  return mBuffer;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access buffer (const)
const Char_t* Txt::buffer() const
{
  return bufferPtr().get();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Access buffer
Char_t* Txt::buffer()
{
  return bufferPtr().get();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Read a line of text into a string
Bool_t Txt::getLine(String& line, const Char_t& delim)
{
  ifs().getline(mBuffer.get(), mBufSz, delim);
  line = bufferPtr().get();
  loutG(kData) << "Txt::getLine: Buffer: \"" << line <<  "\"" << endl;
  return ifsIsGood();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default buffer size
const UShort_t& Txt::DefBufferSize()
{
  return mDefBufSize;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Split into tokens
std::vector<String> Txt::Tokenize(const String& str, const Char_t& delim)
{
  std::vector<String> ret;
  String item("");
  for(size_t ind=0; ind<str.length(); ind++){
    Char_t c = str[ind];
    if(c == delim){
      ret.push_back(item);
      item.clear();
    }
    else item += c;
  }
  ret.push_back(item);
  return ret;
}

} // end namespace Parser

} // end namespace Io

} // end namespace Pomona


//  end IoParserTxt.cxx
//  ############################################################################
