//  ############################################################################
//! @file       LogProgressBar.cxx
//! @brief      Source for ProgressBar class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "LogProgressBar.hh"  // pomona: this class
#include "LogService.hh"      // pomona: logging service
#include <iostream>           // std: I/O streamers
#include <iomanip>            // std: setprecision, etc .. 
#include <chrono>             // std: c++ chronography; for example method only
#include <thread>             // std: c++ threads; for example method only

namespace Pomona {

namespace Log {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor.
ProgressBar::ProgressBar(const Log::eTopic& topic) 
  : mTopic(topic)
  , mFraction(0.)
  , mTimer()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get current progress
const Float_t& ProgressBar::fraction() const
{
  return mFraction;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print current fraction of progress
void ProgressBar::printFraction(const Float_t& frac)
{
  if(frac < 0.){
    loutW(kEval) << "ProgressBar::printFraction: Fraction < 0" << std::endl;
    printFraction(0.);
    return;
  }
  else if(frac > 1.){
    loutW(kEval) << "ProgressBar::printFraction: Fraction > 1" << std::endl;
    printFraction(1.);
    return;
  }
  else{
    //! Bar width in number of characters
    static constexpr UInt8_t mTitleWidth = 19;
    //! Bar width in number of characters
    static constexpr UInt8_t mBarWidth = 50;
    //! Past progress character
    static constexpr Char_t mPast      = '=';
    //! Current progress character
    static constexpr Char_t mCurrent   = '>';
    //! Future progress character
    static constexpr Char_t mFuture    = ' ';
    //! Bar opening character
    static constexpr Char_t mBarOpen   = '[';
    //! Bar closing character
    static constexpr Char_t mBarClose  = ']';
    
    // set the percent progress
    mFraction = frac;

    // start the stream
    std::ostream* os = &logger().log(Log::eLevel::kProgress, mTopic);
    auto oswidth = (*os).width();
    auto ospreci = (*os).precision();

    // print the time
    (*os) << std::left << std::setw(mTitleWidth);
    if(mFraction == 1.) 
      (*os) << mTimer.stoppedAsString();      // stop timer and print total time
    else {
      if(!mTimer.isRunning()) mTimer.start(); // start the timer
      (*os) << mTimer.elapsedAsString();      // print the elapsed time
    }
    (*os) << " : ";

    // print the "title" prefix
    // (*os) << std::left << std::setw(mTitleWidth) 
    // 	  << mTitle
    // 	  << resetiosflags(std::ios_base::adjustfield);
    // (*os) << " : ";

    // print the bar
    (*os) << mBarOpen;
    UInt_t pos = mBarWidth * mFraction;
    for(UInt_t i=0; i<mBarWidth; i++){
      if      (i <  pos) (*os) << mPast;
      else if (i == pos) (*os) << mCurrent;
      else               (*os) << mFuture;
    }
    (*os) << mBarClose; 

    // print the percent complete
    (*os) << std::fixed << std::setw(5) << std::setprecision(1)
	  << mFraction*100.0
	  << resetiosflags(std::ios_base::floatfield)
	  << std::setw(oswidth) << std::setprecision(ospreci)
	  << '%';

    // caraige return, for "overwriting"
    (*os) << '\r';
    // flush the contents
    (*os).flush();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print current percentage to stream
void ProgressBar::printFraction(const UShort_t& numerator, const UShort_t& denomenator)
{
  Float_t frac = 1. * numerator / denomenator;
  printFraction(frac);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print current percentage to stream
void ProgressBar::printFraction(const UInt_t& numerator, const UInt_t& denomenator)
{
  Float_t frac = 1. * numerator / denomenator;
  printFraction(frac);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print current percentage to stream
void ProgressBar::printFraction(const ULong64_t& numerator, 
				const ULong64_t& denomenator)
{
  Float_t frac = 1. * numerator / denomenator;
  printFraction(frac);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print current percentage to stream
void ProgressBar::printFinish()
{
  printFraction(1.);
  (*logger().progressStream(mTopic)) << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Return true if path exists.
void ProgressBar::printProgressBarExample()
{
  // Demonstration of progress bar
  loutP(kEval) << "Example ProgressBar :" << endl; 
  ProgressBar pb;
  UInt_t totalSteps(1000);
  UInt_t sleepTime(2*1000/totalSteps);
  for(UInt_t k=0; k<totalSteps; k++){
    pb.printFraction(k, totalSteps);
    std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
  }
  pb.printFinish();
}

} // end namespace Log

} // end namespace Pomona

//  end LogProgressBar.cxx
//  ############################################################################
