//  ############################################################################
//! @file       LogService.cxx
//! @brief      Source for log message utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "LogService.hh"         // pomona: this class
#include "LogTerminalColors.hh"  // pomona: colorizing output
#include "LogStyle.hh"        // pomona: log message style enumeration
#include "Exception.hh"       // pomona: exception handler
#include "Date.hh"            // pomona: date + time helper
#include <memory>             // std: unique_ptr
#include <map>                // std::map
#include <vector>             // std::vector
#include <fstream>            // std: file streamers
#include <iostream>           // std: i/o streamers
#include <iomanip>            // std: i/o stream manipulators
#include <stdexcept>          // std: out_of_range
#ifdef CERNROOT               // Using root
#include "TError.h"           // root: error message level
#endif                        // end using root

namespace Pomona {

namespace Log {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Map of log-level names
static const std::map<eLevel,String_t> gLLNames = 
  {
    {eLevel::kGuru,     "Guru"},
    {eLevel::kVerbose,  "Verbose"},
    {eLevel::kDebug,    "Debug"},
    {eLevel::kInfo,     "Info"},
    {eLevel::kProgress, "Progress"},
    {eLevel::kNotice,   "Notice"},
    {eLevel::kWarning,  "Warning"},
    {eLevel::kError,    "Error"},
    {eLevel::kAlert,    "Alert"},
    {eLevel::kCritical, "CRITICAL"},
    {eLevel::kFatal,    "FATAL"},
    {eLevel::kSilent,   "Silent"}
  };


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Map of log-level titles
static const std::map<eLevel,String_t> gLLTitles = 
  {
    {eLevel::kGuru,     "Print all the things!"},
    {eLevel::kVerbose,  "Debug deep dive"},
    {eLevel::kDebug,    "Useful for devs, not for ops"},
    {eLevel::kInfo,     "Normal operational messages"},
    {eLevel::kProgress, "Time intensive operational messages"},
    {eLevel::kNotice,   "Unusual event, but not an error"},
    {eLevel::kWarning,  "Strange event, error may be eminent"},
    {eLevel::kError,    "Error has occured, unexpceted behavior ahead"},
    {eLevel::kAlert,    "Substantial error that should be corrected"},
    {eLevel::kCritical, "Serious error that must be corrected immediatly"},
    {eLevel::kFatal,    "Feces in the bed!"},
    {eLevel::kSilent,   "No messages"}
  };

//  ****************************************************************************
//! Map of log-level colors/styles
/*! @note Color scheme by: "Kathryn McCulley" */
static const std::map<eLevel,TermColorMod> gLLColors = 
  {
    {eLevel::kGuru,     TermColorMod(eTermColor::kFgWhite,   eTermStyle::kDim)},
    {eLevel::kVerbose,  TermColorMod(eTermColor::kFgWhite,   eTermStyle::kRegular)},
    {eLevel::kDebug,    TermColorMod(eTermColor::kFgWhite,   eTermStyle::kBright)},
    {eLevel::kInfo,     TermColorMod(eTermColor::kFgGreen,   eTermStyle::kRegular)},
    {eLevel::kProgress, TermColorMod(eTermColor::kFgBlue,    eTermStyle::kRegular)},
    {eLevel::kNotice,   TermColorMod(eTermColor::kFgHiCyan,  eTermStyle::kRegular)},
    {eLevel::kWarning,  TermColorMod(eTermColor::kFgHiYellow,eTermStyle::kRegular)},
    {eLevel::kError,    TermColorMod(eTermColor::kFgHiRed,   eTermStyle::kRegular)},
    {eLevel::kAlert,    TermColorMod(eTermColor::kFgRed,     eTermStyle::kBright)},
    {eLevel::kCritical, TermColorMod(eTermColor::kFgHiRed,   eTermStyle::kBright)},
    {eLevel::kFatal,    TermColorMod(eTermColor::kBgRed,     eTermStyle::kBright)}
  };

//  ****************************************************************************
//! Map of log-topic names
static const std::map<eTopic,String_t> gLTNames = 
  {
    {eTopic::kNone,      ""},
    {eTopic::kInput,     "Input"},
    {eTopic::kEval,      "Eval"},
    {eTopic::kObject,    "Object"},
    {eTopic::kData,      "Data"},
    {eTopic::kFile,      "File"},
    {eTopic::kPlot,      "Plot"},
    {eTopic::kMini,      "Mini"},
    {eTopic::kFit,       "Fit"},
    {eTopic::kSimu,      "Simu"},
    {eTopic::kInterp,    "Interp"},
    {eTopic::kML,        "Learn"},
    {eTopic::kAll,       "All"}
  };

//  ****************************************************************************
//! Map of log-topic titles
static const std::map<eTopic,String_t> gLTTitles = 
  {
    {eTopic::kNone,      "None"},
    {eTopic::kInput,     "Input"},
    {eTopic::kEval,      "Evaluation"},
    {eTopic::kObject,    "Object handling"},
    {eTopic::kData,      "Data handling"},
    {eTopic::kFile,      "File I/O"},
    {eTopic::kPlot,      "Plotting"},
    {eTopic::kMini,      "Minimization"},
    {eTopic::kFit,       "Fitting"},
    {eTopic::kSimu,      "Simulation"},
    {eTopic::kInterp,    "Interpolation"},
    {eTopic::kInterp,    "Machine Learning"},
    {eTopic::kAll,       "All"}
  };

//  ****************************************************************************
//! Map of level specific message counts
static std::map<eLevel,UInt_t> gLSLevelCounts;

//  ****************************************************************************
//! Pointer to "/dev/null"
static std::unique_ptr<std::ostream> gDevNull;

//  ****************************************************************************
//! List (vector) of message streams.
static std::vector<StreamConfig> gLSStreams;

//  ****************************************************************************
//! Map of message file-streams.
static std::map<String_t,std::ofstream*> gLSFiles;
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Holder for stream configuration. 
struct StreamConfig {
public:
  friend class Service;
  inline void   addTopic   (const eTopic& newTopic) { mTopic |= newTopic; }
  inline void   removeTopic(const eTopic& oldTopic) { mTopic &= ~oldTopic; }
  inline Bool_t match(const eLevel& level, 
		      const eTopic& topic, 
		      const Object* /*obj*/)
  {
    if(!mActive)                    return kFALSE;
    if(level > mLevel)              return kFALSE;
    if((mTopic&topic) == eTopic::kNone && 
               topic  != eTopic::kNone ) return kFALSE;
    if(mUniversal)                  return kTRUE;
    // if(obj){
    //   if(mObjectName.size()>0 && 
    // 	 mObjectName != obj->getName())  return kFALSE;
    //   if(mClassName.size()>0 && 
    // 	 mClassName != obj->classType()) return kFALSE;
    // }
    return kTRUE;
  }

public:
  Bool_t         mActive;
  eLevel         mLevel;
  UShort_t       mTopic;
  Bool_t         mUniversal;
  String_t       mObjectName;
  String_t       mClassName;
  Bool_t         mInLine;
  std::ostream*  mOs;
};

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Instanton.
/** Return a reference to a singleton object that is created the
    first time this method is called. 
    Only one object will be constructed per session.
 */
Service& Service::instance() 
{
  static Service _theLogInstance;
  return _theLogInstance;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
Service::Service() 
  : mLevel(eLevel::kInfo)
  , mStyle(gDefStyle)
  , mPrefix("Pomona")
{
  // level counts
  for(auto& llns: gLLNames) gLSLevelCounts[llns.first] = 0;

  // create and check /dev/null
#ifdef POMONA_OS_UNIX
  gDevNull = std::unique_ptr<std::ostream>(new std::ofstream("/dev/null"));
#elif  POMONA_OS_WIN
  gDevNull = std::unique_ptr<std::ostream>(new std::ofstream("nul"));
#else
  std::cerr << "Log::Service::Service: Unkown operating system!" << endl;
#endif
  if(!gDevNull)
    throw Exception("Unable to find null output file stream", 
		    "Service", "Log::Service");

  // add default stream(s)
  // seperate levels to different c/c++ system-level logging streams
  addStream(eLevel::kError, &std::cerr, AsInt(eTopic::kAll));
  addStream(eLevel::kGuru,  &cout,      AsInt(eTopic::kAll));
  // // map all stream to std:cout as default
  // addStream(eLevel::kGuru, NULL, LogTopicEnumAsInt(eTopic::kAll));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Destructor. */
Service::~Service() 
{
  // Delete all std::ostreams we own
  for(auto& fs: gLSFiles){ if(fs.second){ fs.second->close(); delete fs.second; } }
  // delete /dev/null
  if(gDevNull) gDevNull.reset();
}  

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the global/default level
void Service::setLevel(const eLevel& l)
{
  mLevel = l;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the library/application prefix for all messages
const eLevel& Service::level() const
{
  return mLevel;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the level name string value
const String_t& Service::LevelName(const eLevel& level)
{
  return gLLNames.at(level);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the level title string value
const String_t& Service::LevelTitle(const eLevel& level)
{
  return gLLTitles.at(level);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the level from string value
/** @note Compares only first letters. 
    @warning Case sensitive.
*/
const eLevel& Service::AsLevel(const String_t& str)
{
  if(str.size()>1 && str[0]!='\0' ) 
    for(auto& lme: gLLNames) if(str[0] == lme.second[0]) return lme.first;
  return gDefLevel;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Formatted printing for eLevel.
void Service::PrintLevel(std::ostream& os, const eLevel& l)
{
  os << std::left << std::setw(9) 
     << LevelName(l)
     << resetiosflags(std::ios_base::adjustfield);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print log-level help information
void Service::PrintLevelHelp(std::ostream& os, const String_t& ind)
{    
  os << ind << "log-level arguments: " << endl;
  for(auto& llns: gLLNames){
    const eLevel& l = llns.first;
    os << ind << "  ";
    PrintLevel(os,l);
    os << ": " << LevelTitle(l) << endl;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print log-level example
void Service::PrintLevelExample()
{
  // hold the global log-level
  const eLevel& llHold = Service::instance().level();
  Service::instance().setLevel(eLevel::kGuru);

  loutI(kNone) << "Example log messages : " << endl;
  loutG(kInput)  << "  " 
  		 << eLevel::kGuru << ": " 
  		 << LevelTitle(eLevel::kGuru) << endl;
  loutV(kEval)   << "  " 
  		 << eLevel::kVerbose << ": " 
  		 << LevelTitle(eLevel::kVerbose) << endl;
  loutD(kObject) << "  " 
  		 << eLevel::kDebug << ": " 
  		 << LevelTitle(eLevel::kDebug) << endl;
  loutI(kData)   << "  " 
  		 << eLevel::kInfo << ": " 
  		 << LevelTitle(eLevel::kInfo) << endl;
  loutP(kSimu)   << "  " 
  		 << eLevel::kProgress << ": " 
  		 << LevelTitle(eLevel::kProgress) << endl;
  loutN(kFile)   << "  " 
  		 << eLevel::kNotice << ": " 
  		 << LevelTitle(eLevel::kNotice) << endl;
  loutW(kPlot)   << "  " 
  		 << eLevel::kWarning << ": " 
  		 << LevelTitle(eLevel::kWarning) << endl;
  loutE(kMini)   << "  " 
  		 << eLevel::kError << ": " 
  		 << LevelTitle(eLevel::kError) << endl;
  loutA(kFit)    << "  " 
  		 << eLevel::kAlert << ": " 
  		 << LevelTitle(eLevel::kAlert) << endl;
  loutC(kInterp) << "  " 
  		 << eLevel::kCritical << ": " 
  		 << LevelTitle(eLevel::kCritical ) << endl;
  loutF(kSimu)   << "  " 
  		 << eLevel::kFatal << ": " 
  		 << LevelTitle(eLevel::kFatal) << endl;

  // reseet global log-level
  Service::instance().setLevel(llHold);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the topic name string value
const String_t& Service::TopicName(const eTopic& t)
{
  return gLTNames.at(t);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the topic name string value
const String_t& Service::TopicTitle(const eTopic& t)
{
  return gLTTitles.at(t);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Formatted printing for eTopic.
void Service::PrintTopic(std::ostream& os, const eTopic& t)
{
  os << ":" 
     << std::left << std::setw(6) 
     << TopicName(t)
     << resetiosflags(std::ios_base::adjustfield);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the library/application prefix for all messages
void Service::setPrefix(const Char_t* str)
{
  mPrefix = str;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the library/application prefix for all messages
const Char_t* Service::prefix() const
{
  return mPrefix.c_str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Formatted printing for app log message prefix tag.
void Service::printPrefix(std::ostream& os) const
{
  os << "[" << prefix() << "] ";      
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set style to print any tags (message meta)
void Service::setStyle(const UInt8_t& s)
{
  mStyle = s;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set style to print any tags (message meta)
const UInt8_t& Service::style() const
{
  return mStyle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is app-prefix printing requested?
Bool_t Service::isPrintTags() const
{
  return ( mStyle != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is app-prefix printing requested?
Bool_t Service::isPrintPrefix() const
{
  return ( (mStyle & eStyle::kPrefix) != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is date printing requested?
Bool_t Service::isPrintPid() const
{
  return ( (mStyle & eStyle::kPid) != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is date printing requested?
Bool_t Service::isPrintDate() const
{
  return ( (mStyle & eStyle::kDate) != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is time printing requested?
Bool_t Service::isPrintTime() const
{
  return ( (mStyle & eStyle::kTime) != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is colorized printing requested?
Bool_t Service::isPrintColor() const
{
  return ( (mStyle & eStyle::kColor) != eStyle::kNone  );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculated the total number of messages (from all levels).
UInt_t Service::msgCount() const
{
  UInt_t cnt(0);
  for(auto& lc: gLSLevelCounts) cnt += lc.second;
  return cnt;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculated the total number of messages (from all levels).
UInt_t Service::msgCount(const eLevel& l) const
{
  return gLSLevelCounts[l];
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Add (push_back) a StreamConfig struct.
size_t Service::addStream(const StreamConfig& sc) const
{
  // Add it to list of active streams
  gLSStreams.push_back(sc);
  // Return stream identifier
  return gLSStreams.size()-1;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Add (push_back) a new (non-file) stream.
size_t Service::addStream(const eLevel& l, std::ostream* os, 
			     const UShort_t& topic) const
{
  // Create new stream object
  StreamConfig nsc;
  // basic configuration info
  nsc.mActive     = kTRUE;
  nsc.mLevel      = l;
  nsc.mTopic      = topic;
  nsc.mUniversal  = kTRUE;
  nsc.mObjectName = "";
  nsc.mClassName  = "";
  nsc.mInLine     = kTRUE;
  // out-stream configuration info
  if(!os || os->fail()) nsc.mOs = &cout;
  else                  nsc.mOs = os;
  // Return stream identifier
  return addStream(nsc);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Add (push_back) a new file-stream.
size_t Service::addStream(const eLevel& l, const Char_t* fileName, 
			  Bool_t append, const UShort_t& topic) const
{
  // Create new stream object
  StreamConfig nsc;
  // basic configuration info
  nsc.mActive     = kTRUE;
  nsc.mLevel      = l;
  nsc.mTopic      = topic;
  nsc.mUniversal  = kTRUE;
  nsc.mObjectName = "";
  nsc.mClassName  = "";
  nsc.mInLine     = kFALSE;

  // out-file-stream configuration info
  std::ofstream* ofs = 0;
  if(fileName && (fileName[0] != '\0')){
    // See if we already opened the file
    ofs = gLSFiles["outFile"];
    if(!ofs){
      // To given file name, create owned stream for it
      if(append) 
	ofs = new std::ofstream(fileName, 
				std::ios_base::out | std::ios_base::ate | 
				std::ios_base::app);
      else       
	ofs = new std::ofstream(fileName, std::ios_base::out);
      gLSFiles["outFile"] = ofs;
    } 
  }
  // check creation and assign
  if(!ofs || ofs->fail()) nsc.mOs = &cout;
  else                    nsc.mOs = ofs;

  // Return stream identifier
  return addStream(nsc);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the StreamConfig struct assiciated with id.
const StreamConfig& Service::streamConfig(Int_t id) const
{
  try{ return gLSStreams.at(id); }
  catch(const std::out_of_range& oor){ return gLSStreams[0]; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Find the index of the stream associated with parameters
Int_t Service::activeStream(eLevel l, eTopic topic, 
			       const Object* obj) const
{
  if(l > mLevel) return -1;
  for(UInt_t i=0; i< gLSStreams.size(); i++)
    if(gLSStreams[i].match(l,topic,obj))
      return i;
  return -1;  
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Retrieve a stream
std::ostream* Service::progressStream(eTopic topic)
{
#ifndef LOGMSG 
  return return gDevNull.get();
#else
  Int_t as = activeStream(eLevel::kProgress, topic);
  if(as == -1) return gDevNull.get();  // return if not found
  else         return (&gLSStreams[as])->mOs;
#endif
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Log a message
std::ostream& Service::log(const eLevel& l, const eTopic& t) const
{
#ifndef LOGMSG 
  return return gDevNull.get();
#else
  // update log-level counts
  gLSLevelCounts[l]++;
  // Get the associated stream
  Int_t as = activeStream(l, t);
  if(as == -1) return *gDevNull.get();  // return if not found
  else{
    // get the streamer
    std::ostream* os = (&gLSStreams[as])->mOs;
    // flush any previous message
    (*os).flush();
    if(isPrintTags()){
      if(isPrintDate() || isPrintTime()){  // date/time stamp
      	Date now = Date::NowLocal();
      	if(isPrintDate()){
      	  (*os) << now.dateStr();
      	  if(isPrintTime()) (*os) << " " << now.timeStr() << " ";
      	}
      	else (*os) << now.timeStr() << " ";
      }
      if(isPrintPrefix()) printPrefix(*os);            // prefix
      if(isPrintColor()) (*os) << gLLColors.at(l);     // start color
      PrintLevel(*os,l);                               // level
      PrintTopic(*os,t);                               // topic
      if(isPrintColor()) TermColorMod::ColorOff(*os);  // end color
      (*os) << " -- ";                                 // message seperator
    }
    // return the stream
    return (*os);
  }
#endif
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Out-streamer operator for eLevel
std::ostream& operator<<(std::ostream& os, const eLevel& level)
{
  os << Service::LevelName(level);
  return os;
}


//  ****************************************************************************
//  ****************************************************************************
#ifdef CERNROOT  // Using root
//  ****************************************************************************
//  ****************************************************************************

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the root-system message level
/*! Valid inputs are:
     - kInfo     = 1000
     - kWarning  = 2000
     - kError    = 3000
     - kBreak    = 4000
     - kSysError = 5000
     - kFatal    = 6000
*/
void Service::SetRootLevel(const Int_t& level)
{
  gErrorIgnoreLevel = level + 1;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the root-system message level from a character array.
/*! Valid inputs are:
     - Info
     - Warning
     - Error
     - Break
     - SysError
     - Fatal
    
    @note Compares only first letters. 
    @warning Case sensitive.
*/
void Service::SetRootLevel(const String_t& l)
{
  if     (l[0] == 'F') SetRootLevel(kFatal);
  else if(l[0] == 'S') SetRootLevel(kSysError);
  else if(l[0] == 'B') SetRootLevel(kBreak);
  else if(l[0] == 'E') SetRootLevel(kError);
  else if(l[0] == 'W') SetRootLevel(kWarning);
  else                 SetRootLevel(kInfo);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the library/application prefix for all messages
const Int_t& Service::RootLevel()
{
  return gErrorIgnoreLevel;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print log-level help information
void Service::PrintRootLevelHelp(std::ostream& os, const String_t& ind)
{    
  os << ind << "root-log-level arguments: " << endl;
  os << ind << "  Info     : Normal operational messages" << endl;
  os << ind << "  Warning  : Strange event, error may be eminent" << endl;
  os << ind << "  Break    : Breaking event, error may be eminent" << endl;
  os << ind << "  Error    : Error has occured, unexpceted behavior ahead" << endl;
  os << ind << "  SysError : System-level error has occured" << endl;
  os << ind << "  Fatal    : Feces in the bed!" << endl;
}


//  ****************************************************************************
//  ****************************************************************************
#endif  // end using root
//  ****************************************************************************
//  ****************************************************************************


} // end namespace Log

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the logging stream
std::ostream& Logger(const Log::eLevel& l, const Log::eTopic& t)
{
  return Log::Service::instance().log(l, t);
}

} // end namespace Pomona

//  end LogService.cxx
//  ############################################################################
