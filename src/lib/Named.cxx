//  ############################################################################
//! @file       Named.cxx
//! @brief      Source for generic named object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Named.hh"     // pomona: this class
#include "ClassImp.hh"  // pomona: class implementation

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::Named)
//! Cereal: Register derived polymorphic type -- NamedString 
CEREAL_REGISTER_TYPE(Pomona::Named)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Named::Named(const String& n, const String& t)
  : Object()
  , mName(n)
  , mTitle(t)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
Named::Named(const Named& other)
  : Object(other)
  , mName(other.mName)
  , mTitle(other.mTitle)    
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor, with re-name
Named::Named(const Named& other, const String& newName)
  : Object(other)
  , mName(other.mName)
  , mTitle(other.mTitle)    
{
  if(!newName.isEmpty()) setName(newName);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
Named::Named(Named&& other)
  : Object(other)
  , mName(std::move(other.mName))
  , mTitle(std::move(other.mTitle))
{
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
Named& Named::operator=(const Named& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    mName  = rhs.name();
    mTitle = rhs.title();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
Named& Named::operator=(Named&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    mName  = rhs.name();
    mTitle = rhs.title();
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void Named::clear()
{
  Object::clear();
  mName.clear();
  mTitle.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t Named::isEmpty() const
{
  if(!mName.isEmpty()) return kFALSE;
  else if(!mTitle.isEmpty()) return kFALSE;
  else return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t Named::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{  // attempt cast
      const Named& o = dynamic_cast<const Named&>(other); 
      // check members
      if(mName != o.name()) return kFALSE;
      else if(mTitle != o.title()) return kFALSE;
      else return kTRUE;
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t Named::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** Derived implementations should return:
    - <0 (-1) if other is "less than" this
    - >0 (+1) if other is "greater than" this
    -  0 else 
*/
Int_t Named::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return 0;
  else{
    try{  // attempt cast
      const Named& o = dynamic_cast<const Named&>(other); 
      return mName.compare(o.name());
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t Named::defaultPrintContent() const
{
  return kName | kTitle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object name.
void Named::printName(std::ostream& os) const
{
  os << name();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object title.
void Named::printTitle(std::ostream& os) const
{
  os << title();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object arguments.
void Named::printArgs(std::ostream& os) const
{
  os << "Name,Title"; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object extras.
void Named::printExtras(std::ostream& os) const
{
  os << "Name = " << mName  << std::endl; 
  os << "Title = " << mTitle << std::endl; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Name (const)
const String& Named::name() const
{
  return mName;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Name
String& Named::name()
{
  return mName;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Title (const)
const String& Named::title() const
{
  return mTitle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Title
String& Named::title()
{
  return mTitle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set Name
void Named::setName(const String& n)
{
  mName = n;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set Title
void Named::setTitle(const String& t)
{
  mTitle = t;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set Name & Title
void Named::setNameTitle(const String& n, const String& t)
{
  setName(n);
  setTitle(t);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is there a name?
Bool_t Named::hasName() const
{
  return !mName.isEmpty();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is there a title?
Bool_t Named::hasTitle() const
{
  return !mTitle.isEmpty();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is there a name and a title?
Bool_t Named::hasNameTitle() const
{
  return ( hasName() && hasTitle() );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this inherit from Pomona:Named?
Bool_t Named::isNamed() const
{
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the name
const Char_t* Named::getName() const
{
  return mName.c_str();
}

#ifdef CERNROOT                   // Using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::Clone
TObject* Named::Clone(const Char_t* newName) const
{
  Named* n = new Named(*this);
  if(n != nullptr){
    n->setName(newName);
    return static_cast<TObject*>(n);
  }
  else return nullptr;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::GetName: Get's the name
const Char_t* Named::GetName() const
{
  return mName.c_str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::GetTitle: Get's the title
const Char_t* Named::GetTitle() const
{
  return mTitle.c_str();
}

#endif                            // end using root

} // end namespace Pomona

//  end Named.cxx
//  ############################################################################
