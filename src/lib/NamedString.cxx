//  ############################################################################
//! @file       NamedString.cxx
//! @brief      Source for NamedString class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "NamedString.hh"  // pomona: this class
#include "ClassImp.hh"     // pomona: class implementation

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::NamedString)
//! Cereal: Register derived polymorphic type -- NamedString 
CEREAL_REGISTER_TYPE(Pomona::NamedString)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
NamedString::NamedString(const String& n, const String& t, const String& s)
  : Named(n,t)
  , mValue(s)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
NamedString::NamedString(const NamedString& other)
  : Named(other)
  , mValue(other.mValue)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor, with re-name
NamedString::NamedString(const NamedString& other, const String& newName)
  : Named(other, newName)
  , mValue(other.mValue)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
NamedString::NamedString(NamedString&& other)
  : Named(other)
  , mValue(other.mValue)
{
  other.NamedString::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
NamedString& NamedString::operator=(const NamedString& rhs)
{
  if(!isSame(rhs)){
    Named::operator=(rhs);
    set(rhs());
  } 
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
NamedString& NamedString::operator=(NamedString&& rhs)
{
  if(!isSame(rhs)){
    Named::operator=(rhs);
    set(rhs());
    rhs.Named::clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
NamedString& NamedString::operator=(const String& rhs)
{
  if(!isSame(rhs)){
    Named::operator=(rhs);
    set(rhs);
  } 
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
NamedString& NamedString::operator=(String&& rhs)
{
  if(!isSame(rhs)){
    set(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from character array.
NamedString& NamedString::operator=(const Char_t* rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void NamedString::clear()
{
  Object::clear();
  Named::clear();
  get().clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t NamedString::isEmpty() const
{
  return (Named::isEmpty() && get().isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t NamedString::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{  // attempt cast
      const NamedString& o = dynamic_cast<const NamedString&>(other); 
      // check members
      return (Named::isEqual(o) && get().isEqual(o.get()) );
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t NamedString::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** Derived implementations should return:
    - <0 (-1) if other is "less than" this
    - >0 (+1) if other is "greater than" this
    -  0 else 
*/
Int_t NamedString::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return 0;
  else{
    try{  // attempt cast
      const NamedString& o = dynamic_cast<const NamedString&>(other); 
      return get().compare(o.get());
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t NamedString::defaultPrintContent() const
{
  return kName | kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object value
void NamedString::printValue(std::ostream& os) const
{
  os << get();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object arguments.
void NamedString::printArgs(std::ostream& os) const
{
  Named::printArgs(os);
  os << ",Value"; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object extras.
void NamedString::printExtras(std::ostream& os) const
{
  Named::printExtras(os);
  os << "Value = " << mName  << std::endl; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get member string (const)
const String& NamedString::get() const 
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get member string
String& NamedString::get()       
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get member string (const)
const String& NamedString::operator()() const 
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get member string
String& NamedString::operator()()
{ 
  return mValue; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set member string
void NamedString::set(const String& val)
{ 
  mValue = val; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition assignment
/*! @note Operates on string member only */
NamedString& NamedString::operator+=(const String& rhs)
{
  get() += rhs;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition assignment
/*! @note Operates on string member only */
NamedString& NamedString::operator+=(const Char_t* rhs)
{
  get() += rhs;
  return (*this);
}

} // end namespace Pomona

//  end NamedString.cxx
//  ############################################################################
