//  ############################################################################
//! @file       lib/Object.cxx
//! @brief      Source for a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Object.hh"    // pomona: this class
#include "ClassImp.hh"  // pomona: class implementation

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::Object)
//! Cereal: Registers a derived polymorphic type 
CEREAL_REGISTER_TYPE(Pomona::Object)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Object::Object()
  : AbsObject()
  , Comparable()
  , Printable()
#ifdef CERNROOT
  , TObject()
#endif
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
Object::Object(const Object& other)
  : AbsObject(other)
  , Comparable(other)
  , Printable(other)
#ifdef CERNROOT       // Using root
  , TObject(other)
#endif                // end using root
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
Object::Object(Object&& other)
  : AbsObject(other)
  , Comparable(other)
  , Printable(other)
#ifdef CERNROOT       // Using root
  , TObject(other)
#endif                // end using root
{
  other.Object::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
Object& Object::operator=(const Object& rhs)
{
  if(!isSame(rhs)){
    AbsObject::operator=(rhs);
    Comparable::operator=(rhs);
    Printable::operator=(rhs);
#ifdef CERNROOT       // Using root
    TObject::operator=(rhs);
#endif                // end using root
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
Object& Object::operator=(Object&& rhs)
{
  if(!isSame(rhs)){
    AbsObject::operator=(rhs);
    Comparable::operator=(rhs);
    Printable::operator=(rhs);
#ifdef CERNROOT       // Using root
    TObject::operator=(rhs);
#endif                // end using root
    rhs.Object::clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void Object::clear()
{
  AbsObject::clear();
#ifdef CERNROOT
  TObject::Clear();
#endif
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t Object::isEmpty() const
{
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t Object::defaultPrintContent() const
{
  return kClassName;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object class-name.
void Object::printClassName(std::ostream& os) const
{
  os << className();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object name.
void Object::printName(std::ostream& os) const
{
  printClassName(os);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this inherit from Pomona:Named?
Bool_t Object::isNamed() const
{
  return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get's the name (overloaded in derived)
const Char_t* Object::getName() const
{
  // return className().c_str();
  return "";
}

#ifdef CERNROOT  // Using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::Clone
TObject* Object::Clone(const Char_t*) const
{
  return dynamic_cast<TObject*>(clone());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::Clear: Call clear
void Object::Clear(Option_t*)
{
  clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::IsEqual: Is this equal to the other?
Bool_t Object::IsEqual(const TObject* other) const 
{ 
  try{ 
    // attempt to cast to Pomona::Object
    const Object& o = dynamic_cast<const Object&>(*other); 
    return isEqual(o);
  }
  catch(const std::bad_cast& bc){ return TObject::IsEqual(other); }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t Object::IsSortable() const 
{ 
  return isSortable();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::Compare: Compare this object to other.
Int_t Object::Compare(const TObject* other) const 
{ 
  try{ 
    // attempt to cast to Pomona::Object
    const Object& o = dynamic_cast<const Object&>(*other); 
    return compare(o);
  }
  catch(const std::bad_cast& bc){ return TObject::Compare(other); }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::GetName: Get's the name (should be overloaded in derived)
const Char_t* Object::GetName() const
{
  return TObject::GetName();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! TObject::GetTitle: Get's the title (should be overloaded in derived)
const Char_t* Object::GetTitle() const
{
  return TObject::GetTitle();
}

#endif  // end using root

} // end namespace Pomona

//  end Object.cxx
//  ############################################################################
