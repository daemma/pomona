//  ############################################################################
//! @file       Option.cxx
//! @brief      Source for command-line Option
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Option.hh"     // pomona: this class
#include "OptGnuGet.hh"  // pomona: GNU getopt helpers
#include "ClassImp.hh"   // pomona: class implementation
#include <iomanip>       // stl: setw

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::Option)
//! Cereal: Register derived polymorphic type
CEREAL_REGISTER_TYPE(Pomona::Option)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
Option::Option(const String& n, const String& t, const String& v,
	       const Char_t& so, const Bool_t& ib)
  : NamedString(n,t,v)
  , mShortOpt(so)
  , mIsBool(ib)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
Option::Option(const Option& other)
  : NamedString(other)
  , mShortOpt(other.mShortOpt)
  , mIsBool(other.mIsBool)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
Option::Option(Option&& other)
  : NamedString(std::move(other))
  , mShortOpt(std::move(other.mShortOpt))
  , mIsBool(std::move(other.mIsBool))
{
  other.Option::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy assignment operator. */
Option& Option::operator=(const Option& rhs)
{
  if(this != &rhs){
    NamedString::operator=(rhs);
    mShortOpt = rhs.mShortOpt;
    mIsBool   = rhs.mIsBool;
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move assignment operator. */
Option& Option::operator=(Option&& rhs)
{
  if(this != &rhs){
    NamedString::operator=(rhs);
    mShortOpt = rhs.mShortOpt;
    mIsBool   = rhs.mIsBool;
    rhs.Option::clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from character array.
Option& Option::operator=(const Char_t* rhs)
{    
  NamedString::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear
void Option::clear()
{
  NamedString::clear();
  mShortOpt = '\0';
  mIsBool   = kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are the members empty
Bool_t Option::isEmpty() const
{
  if     (!NamedString::isEmpty()) return kFALSE;
  else if(mShortOpt[0] != '\0')    return kFALSE;
  else                             return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object equal to other?
Bool_t Option::isEqual(const Comparable& other) const
{
  // check pointer
  if(isSame(other)) return kTRUE;
  try{ // attempt cast
    const Option& o = dynamic_cast<const Option&>(other); 
    // check members
    if     (!NamedString::isEqual(other)) return kFALSE;
    else if(mShortOpt[0] != o.shortOpt()) return kFALSE;
    else if(mIsBool      != o.isBool())   return kFALSE;
    else                                  return kTRUE;
  }
  catch(const std::bad_cast& bc){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t Option::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Compare this to other. 
    Returns: 
    - -1 if this has more cps-live than other, 
    - +1 if this has less
    -  0 else
*/
Int_t Option::compare(const Comparable& other) const
{
  // check pointer
  if(isSame(other)) return 0;
  // check members
  try{  // attempt to cast
    const Option& o = dynamic_cast<const Option&>(other); 
    if     (mShortOpt[0] < o.shortOpt()) return -1;
    else if(mShortOpt[0] > o.shortOpt()) return  1;
    else                              return  0;
  }
  catch(const std::bad_cast& bc){ return 0; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object arguments.
void Option::printArgs(std::ostream& os) const
{
  NamedString::printArgs(os);
  os << ",ShortOpt,IsBool"; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object extras.
void Option::printExtras(std::ostream& os) const
{
  NamedString::printExtras(os);
  os << "ShortOpt = " << mShortOpt << std::endl; 
  os << "IsBool = " << mIsBool << std::endl; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Retrieve the option short option character
const Char_t& Option::shortOpt() const
{
   return mShortOpt[0]; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the short options character
void Option::setShortOpt(const Char_t& s)
{
  mShortOpt = s;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this option boolean?
const Bool_t& Option::isBool() const
{
  return mIsBool;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set this option boolean
void Option::setIsBool(const Bool_t& val)
{
  mIsBool = val;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print a line of help text
void Option::printValueLine(std::ostream& os, 
			    const String& ind, const UShort_t& nw) const
{
  os << std::left << ind << std::setw(nw) << name();
  os << " : " << get();
  os << resetiosflags(std::ios_base::adjustfield);
  os << std::endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print a line of help text
void Option::printHelpLine(std::ostream& os, 
			   const String& ind, const UShort_t& nw) const
{
  os << std::left << ind << "-" << shortOpt();
  os << "|--" << std::setw(nw) << name();
  os << resetiosflags(std::ios_base::adjustfield);
  if(!isBool())    os << "  [ARG]";
  else             os << "       ";
  os << "  " << title();
  os << std::endl;
}

} // end namespace Pomona


//  end Option.cxx
//  ############################################################################
