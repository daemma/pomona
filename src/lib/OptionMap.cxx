//  ############################################################################
//! @file       OptionMap.cxx
//! @brief      Source for collection of command-line options.
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "OptionMap.hh"   // pomona: this class
#include "ClassImp.hh"    // pomona: class implementation
#include "HostSystem.hh"  // pomona: host os utilitites
#include "LogService.hh"  // pomona: log message utility
#include "OptGnuGet.hh"   // pomona: GNU getopt helpers

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  class implementation macro
CLASS_IMP(Pomona::OptionMap)
// //! Cereal: Register derived polymorphic type
// CEREAL_REGISTER_TYPE(Pomona::OptionMap)

namespace Pomona {

//! Alias for an entry in an OptionMap.
typedef std::pair<String,Option> OptionMapEntry_t;
//! Alias for an iterator over entries in an OptionMap_t
typedef OptionMap_t::iterator IterOM_t;
//! Alias for a const iterator over entries in an OptionMap_t
typedef OptionMap_t::const_iterator CIterOM_t;
//! Default (meaningless) short option character
static constexpr Char_t gDefShortOpt = '\0';

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
OptionMap::OptionMap()
  : Object()
  , OptionMap_t()
  , mExeName("")
  , mOrder()
  , mCharMap()
  , mNameLength(0)
  , mNonoptArgs()
  , mRequesteds()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
OptionMap::OptionMap(const OptionMap& other)
  : Object(other)
  , OptionMap_t(other)
  , mExeName(other.mExeName)
  , mOrder(other.mOrder)
  , mCharMap(other.mCharMap)
  , mNameLength(other.mNameLength)
  , mNonoptArgs(other.mNonoptArgs)
  , mRequesteds(other.mRequesteds)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
OptionMap::OptionMap(OptionMap&& other)
  : Object(other)
  , OptionMap_t(other)
  , mExeName(other.mExeName)
  , mOrder(other.mOrder)
  , mCharMap(other.mCharMap)
  , mNameLength(other.mNameLength)
  , mNonoptArgs(other.mNonoptArgs)
  , mRequesteds(other.mRequesteds)
{
  other.OptionMap::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy assignment operator. */
OptionMap& OptionMap::operator=(const OptionMap& rhs)
{
  if(this != &rhs){
    OptionMap_t::operator=(rhs);
    mExeName    = rhs.mExeName;
    mOrder      = rhs.mOrder;
    mCharMap    = rhs.mCharMap;
    mNameLength = rhs.mNameLength;
    mNonoptArgs = rhs.mNonoptArgs;
    mRequesteds = rhs.mRequesteds;
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move assignment operator. */
OptionMap& OptionMap::operator=(OptionMap&& rhs)
{
  if(this != &rhs){
    OptionMap_t::operator=(rhs);
    mExeName    = rhs.mExeName;
    mOrder      = rhs.mOrder;
    mCharMap    = rhs.mCharMap;
    mNameLength = rhs.mNameLength;
    mNonoptArgs = rhs.mNonoptArgs;
    mRequesteds = rhs.mRequesteds;
    rhs.OptionMap::clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear ("zero") the members.
void OptionMap::clear()
{
  OptionMap_t::clear();
  mExeName.clear();
  mOrder.clear();
  mCharMap.clear();
  mNameLength = 0;
  mNonoptArgs.clear();
  mRequesteds.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are the derived members filled?
Bool_t OptionMap::isEmpty() const 
{ 
  if(OptionMap_t::size() != 0)return kFALSE;
  if(!mExeName.isEmpty())     return kFALSE;
  if(mOrder.size() != 0)      return kFALSE;
  if(mCharMap.size() != 0)    return kFALSE;
  if(mNameLength != 0)        return kFALSE;
  if(mNonoptArgs.size() != 0) return kFALSE;
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object equal to other?
Bool_t OptionMap::isEqual(const Comparable& other) const 
{ 
  // check pointer
  if(isSame(other)) return kTRUE;
  try{ // attempt cast
    const OptionMap& o = dynamic_cast<const OptionMap&>(other); 
    // check members
    if(OptionMap_t::size() != o.size()) return kFALSE;
    if(getMap()    != o.getMap())       return kFALSE;
    if(mExeName    != o.mExeName)       return kFALSE;
    if(mOrder      != o.mOrder)         return kFALSE;
    if(mCharMap    != o.mCharMap)       return kFALSE;
    if(mNameLength != o.mNameLength)    return kFALSE;
    return kTRUE;
  }
  catch(const std::bad_cast& bc){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the name of the associated executable
const String& OptionMap::exeName() const
{
  return mExeName;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the executable name
void OptionMap::setExeName(const String& n)
{
  mExeName = n;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option map (const)
const OptionMap_t& OptionMap::getMap() const
{
  return static_cast<const OptionMap_t&>(*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option map
OptionMap_t& OptionMap::getMap()
{
  return static_cast<OptionMap_t&>(*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Find the short-option character assoicated with name
const Char_t& OptionMap::findShortOpt(const String& n) const
{
  for(auto& cnp: mCharMap) if(cnp.second == n) return cnp.first;
  return gDefShortOpt;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option mapped from the short option character (const)
/*! @warning throws Exception if input not found. */
const Option& OptionMap::at(const Char_t& so) const
{
  try{
    const String& n = mCharMap.at(so);
    return OptionMap_t::at( n );
  }
  catch(std::out_of_range& oor){ 
    throw Exception("Input character out of range", "at", "OptionMap"); 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option mapped from the short option character (const)
/*! @warning throws Exception if input not found. */
Option& OptionMap::at(const Char_t& so)
{
  try{
    const String& n = mCharMap.at(so);
    return OptionMap_t::at( n );
  }
  catch(std::out_of_range& oor){ 
    throw Exception("Input character out of range", "at", "OptionMap"); 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option mapped from the short option character (const)
/*! @note Creates map element if not found. */
Option& OptionMap::operator[](const Char_t& so)
{
  try{
    const String& n = mCharMap.at(so);
    return OptionMap_t::operator[]( n );
  }
  catch(std::out_of_range& oor){ 
    throw Exception("Input character out of range", "operator[]", "OptionMap"); 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the (abstract) option mapped from the name (const)
/*! @warning throws Exception if input not found. */
const Option& OptionMap::option(const String& n) const
{
  try{ return OptionMap_t::at( n ); }
  catch(std::out_of_range& oor){ 
    throw Exception("Input string out of range", "option", "OptionMap"); 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the (abstract) option mapped from the name
/*! @warning throws Exception if input not found. */
Option& OptionMap::option(const String& n)
{
  try{ return OptionMap_t::at( n ); }
  catch(std::out_of_range& oor){ 
    throw Exception("Input string out of range", "option", "OptionMap"); 
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option mapped from the short option character (const)
/*! @warning throws Exception if input not found. */
const Option& OptionMap::option(const Char_t& so) const
{
  return OptionMap::at(so);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the option mapped from the short option character (const)
/*! @warning throws Exception if input not found. */
Option& OptionMap::option(const Char_t& so)
{
  return OptionMap::at(so);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Has this option been requested on the command-line?
Bool_t OptionMap::isRequested(const Char_t& so) const
{
  try{ return mRequesteds.at(so); }
  catch(const std::out_of_range& oor){ return kFALSE; }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Has this option been requested on the command-line?
Bool_t OptionMap::isRequested(const String& n) const
{
  return isRequested( findShortOpt(n) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the option map
void OptionMap::setMap(const OptionMap_t& optmap)
{
  static_cast<OptionMap_t&>(*this) = optmap;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert an option into the mapping.
Bool_t OptionMap::insert(const Option& opt)
{
  // check if short-option character is already present
  for(auto& c: mOrder){ 
    if(c == opt.shortOpt()) {
      loutE(kInput) << "OptionMap::insert: Option with short-opt '" << c 
		    << "' already present, not inserted." << endl;
      return kFALSE;
    }
  }
  
  // insert into the object mapping
  std::pair<IterOM_t,Bool_t> ret = 
    OptionMap_t::insert( OptionMapEntry_t(opt.name(),opt) );
  if(!ret.second){
    loutW(kInput) << "OptionMap::insert: Object with key=\"" << opt.name()
		  << "\" not inserted." << endl;
    return kFALSE;
  }
  else{
    // add to character ordering
    mOrder.push_back(opt.shortOpt());
    // insert character-map entry
    mCharMap[opt.shortOpt()] = opt.name();
    // update name-length
    UShort_t nl(opt.name().size());
    if(nl > mNameLength) mNameLength = nl;
    // return success
    return kTRUE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Erase an option from the mapping.
Bool_t OptionMap::erase(const String& n)
{
  // find the option entry
  try{ 
    const Option& opt = OptionMap_t::at( n );
    const Char_t optc = opt.shortOpt();
    // erase the entry
    if(OptionMap_t::erase(n) > 0){
      // remove character from ordering
      for(std::vector<Char_t>::iterator oIter = mOrder.begin(); oIter<mOrder.end(); oIter++){
	if(optc == (*oIter)){ mOrder.erase(oIter); break; }
	else continue;
      }
      // erase character-map entry
      mCharMap.erase(optc);
      // update name-length
      UShort_t nl(n.size());
      if(nl >= mNameLength){
	mNameLength = 0;
	for(auto& op : (*this)) 
	  if(op.second.name().size() >= mNameLength)
	    mNameLength = op.second.name().size();
      }
      // return success
      return kTRUE;
    }
    else{
      loutE(kInput) << "OptionMap::erase: Failed to erase map entry: " << n << endl;
      return kFALSE;      
    }
  }
  catch(std::out_of_range& oor){ 
    loutE(kInput) << "OptionMap::erase: Name not found: " << n << endl;
    return kFALSE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert a boolean option into the mapping.
Bool_t OptionMap::insertBool(const Char_t& so, const String& n, 
			     const String& t, const Bool_t& v)
{
  return insert<Bool_t>(so,n,t,v);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert an integer option into the mapping.
Bool_t OptionMap::insertInt(const Char_t& so, const String& n, 
			    const String& t, const Int_t& v)
{
  return insert<Int_t>(so,n,t,v);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert an unsigned integer option into the mapping.
Bool_t OptionMap::insertUInt(const Char_t& so, const String& n, 
			     const String& t, const UInt_t& v)
{
  return insert<UInt_t>(so,n,t,v);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert a double option into the mapping.
Bool_t OptionMap::insertDouble(const Char_t& so, const String& n, 
			       const String& t, const Double_t& v)
{
  return insert<Double_t>(so,n,t,v);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert a string option into the mapping.
Bool_t OptionMap::insertString(const Char_t& so, const String& n, 
			       const String& t, const String& v)
{
  return insert( Option(n,t,v,so,kFALSE) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert a log-level option to the list
Bool_t OptionMap::insertLogLevel(const Log::eLevel& val)
{
  return insertString('l', "log-level", "Verbosity of log messages.", 
		      Log::Service::LevelName(val));
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get (build) GNU option struct from this option
/*! @note GetOpt::Option_t::name must be filled by derived class. */
GetOpt::Option_t GetGnuOption(const Option& opt)
{
  GetOpt::Option_t go;
  go.name    = opt.name().c_str();
  go.has_arg = Int_t(opt.isBool() ? GetOpt::eArgType::kNon : 
		                    GetOpt::eArgType::kReq );
  go.flag    = 0;
  go.val     = opt.shortOpt();
  return go;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get (build) GNU NULL option struct
GetOpt::Option_t GetNullGnuOption()
{
  GetOpt::Option_t go;
  go.name    = 0;
  go.has_arg = 0;
  go.flag    = 0;
  go.val     = 0;
  return go;
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Parse and set command-line arguments
void OptionMap::parseArgs(Int_t argc, Char_t** argv)
{
  // Check inputs
  if(argc < 1 || !argv){
    loutE(kInput) << "OptionMap::parseArgs: Argument count < 1" << endl;
    return;
  }

  // Get associated executable name
  if(argv[0] && argv[0][0]!='\0') mExeName = HostSystem::BaseName(argv[0]);
  if(mExeName.isEmpty())          mExeName = argv[0];


#ifdef GNUGETOPT  // Have GNU-getopt

  String shortOpts;  // GNU getopt formatted string of short option flags
  std::vector<GetOpt::Option_t> gnuOpts;  // GNU getopt formatted option type
  for(auto& optPair : (*this)){
    const Option& opt = optPair.second;
    shortOpts += opt.shortOpt();
    if(!opt.isBool()) shortOpts += ':';
    gnuOpts.push_back(GetGnuOption(opt));
    mRequesteds[opt.shortOpt()] = kFALSE;
  }
  gnuOpts.push_back(GetNullGnuOption());  // add null terminator

  // Set-up GNU getopt stuff and things
  opterr = 0; // turn off GNU getopt error logging
  Int_t  oi(0);   // getopt_long stores the option index here.
  Char_t oc(-1);  // getopt_long stores the option char-flag here.

  // read options
  while(kTRUE){
    // Call GNU getopt_long parser
    oc = GetOpt::Parse(argc, argv, shortOpts.c_str(), &gnuOpts[0], &oi);
    if     (oc ==  -1) break;  // end of the options.
    else if(oc == '?') loutW(kInput) << "Unrecognized option encountered: " 
				     << argv[optind-1] << endl;
    else{ // oc is good, let's parse it!
      try{  // Attempt to fetch; exception thrown if not found
  	Option& opt = option(oc);  // get the option from the map
	mRequesteds[oc] = kTRUE; // this option has been requested
  	// set the option value from a character string
  	if(opt.isBool())    opt = "true";
  	else if(optarg)     opt = optarg;
  	else loutW(kInput) << "OptionMap::parseArgs: Argument required for: " 
  			   << "-"   << opt.shortOpt() 
  			   << "|--" << opt.name() << endl;
      }
      catch(const std::out_of_range& oor) { 
  	loutW(kInput) << "Undefined option encountered: " << oc << endl;
  	continue; 
      } // end try/catch
    } // end option parse
  } // end while true

  // Gobble up non-option arguments
  if(optind < argc) while(optind < argc) mNonoptArgs.push_back(argv[optind++]);

#else  // not have GNU-getopt
  throw Exception("Must have GNU getopt", "parseArgs", "OptionMap");
#endif  // end have GNU-getopt
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print values lines
void OptionMap::printValueLines(std::ostream& os, 
				const String& ind, const UShort_t& nw) const
{
  UShort_t cnw(nw);
  if(cnw == 0) cnw = mNameLength;
  for(auto& c: mOrder) option(c).printValueLine(os,ind,cnw);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print lines of help text
void OptionMap::printHelpLines(std::ostream& os, 
			       const String& ind, const UShort_t& nw) const
{
  UShort_t cnw(nw);
  if(cnw == 0) cnw = mNameLength;
  for(auto& c: mOrder) option(c).printHelpLine(os,ind,cnw);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print lines of help textI/O helper for building derived members
void OptionMap::buildDerived()
{
  for(auto& optPair : (*this)){
    const Option& opt = optPair.second;
    // insert entry in character map
    mCharMap[opt.shortOpt()] = opt.name();
    // update name-length
    if(opt.name().size() > mNameLength) mNameLength = opt.name().size();
  } 
}

} // end namespace Pomona

//  end OptionMap.cxx
//  ############################################################################
