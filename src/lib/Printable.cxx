//  ############################################################################
//! @file       Printable.cxx
//! @brief      Source file for class infromation printing utility
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Printable.hh"  // this class
#include <iostream>           // std: in/out streamers
#include <iomanip>            // std: stream manipulators

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set default length of field for name
UInt_t Printable::mNameLength = 0;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print stream
/*! Return a reference to the current default stream to use in TObject::Print(). 
    Use the optional parameter to specify a new default stream (a reference to 
    the old one is still returned). This method allows subclasses to provide an 
    inline implementation of TObject::Print() without pulling in iostream.h.
*/
std::ostream& Printable::DefaultPrintStream(std::ostream* os)
{
  // define a static default stream as std::cout
  static std::ostream* gDefaultPrintStream = &std::cout;
  // keep old stream
  std::ostream& old = *gDefaultPrintStream;
  // assign new default, if not null
  if(os != nullptr) gDefaultPrintStream = os;
  // return reference to the old stream
  return old;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Main printing method.
void Printable::printStream(std::ostream& os, UShort_t c, eStyle s, 
			    const String_t& ind) const
{
  if(s == kCsv ){ printCsv(os); return; }
  else{
    // indent, if not inline
    if(s != kInline) os << ind;
    // address
    if(c & kAddress){ os << "@"; printAddress(os); if(c != kAddress) os << " "; }
    // class-name
    if(c & kClassName){ printClassName(os); if(c != kClassName) os << "::"; }
    // object name
    if(c & kName){ 
      if(mNameLength > 0) os << std::setw(mNameLength);
      printName(os); 
    }
    // arguments
    if(c & kArgs){ os<< "("; printArgs(os); os << ")"; }
    // value
    if(c & kValue){ if(c & kName){ os << " = "; } printValue(os); }
    // Extras
    if(c & kExtras){ if(c != kExtras){ os << " "; }  printExtras(os); }
    // Title
    if(c & kTitle){
      if(c == kTitle) printTitle(os); 
      else{ os << " \""; printTitle(os); os << "\""; }
    }
    // end the line
    if(s != kInline) os << std::endl;
  } // end not CSV
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object memory address.
void Printable::printAddress(std::ostream& os) const
{
  os << this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: Printable ostream
std::ostream& operator<<(std::ostream& os, const Printable& p)
{
  p.printStream(os, p.defaultPrintContent(), p.defaultPrintStyle());
  return os; 
}

} // end namespace Pomona


//  end Printable.cxx
//  ############################################################################
