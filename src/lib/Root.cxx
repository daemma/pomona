//  ############################################################################
//! @file       Root.cxx
//! @brief      Source file for TROOT interface
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifdef CERNROOT           // Using root
#include "Root.hh"        // pomona: this class
#include "LogService.hh"  // pomona: log message service
#include "TROOT.h"        // THE ROOT!

namespace Pomona {

namespace Root {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get refference to global root-system interface
TROOT& Root::Troot()
{
  return *gROOT;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the application name
String_t Root::Name()
{
  return Troot().GetName();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the application title
String_t Root::Title()
{
  return Troot().GetTitle();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Currently in batch mode?
Bool_t Root::IsBatch()
{
  return Troot().IsBatch();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set batch mode
void Root::SetBatch(const Bool_t& flag)
{
  Troot().SetBatch(flag);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set current style to a named style.
void Root::SetStyle(const String_t& name)
{
  Troot().SetStyle(name.c_str());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Force current style onto all objects.
void Root::ForceStyle()
{
  Troot().ForceStyle();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String of CERN root-system information
String_t Root::Info()
{
  return ( 
	  String_t(Troot().GetVersion()) + 
	  String_t(" [") + std::to_string(Troot().GetVersionCode()) + 
	  String_t("] ") + std::to_string(Troot().GetVersionDate()) 
	 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print CERN root-system information to the stream
void Root::PrintInfo(std::ostream& os)
{
  // os << "root-system-version : " << Root::Info() << endl;
  os << Root::Info() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print CERN root-system information to debug log-level
void Root::PrintInfo(const Log::eLevel& l)
{
  Logger(l,Log::eTopic::kEval) << "root-system-version : " << Root::Info() << endl;
}

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

//  end Root.cxx
//  ############################################################################
