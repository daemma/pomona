//  ############################################################################
//! @file       RootPlotAttrib.cxx
//! @brief      Source file for plot attributes
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifdef CERNROOT               // Using root
#include "RootPlotAttrib.hh"  // pomona: this class
#include "RootPlotStyle.hh"   // pomona: plot styles
#include "ClassImp.hh"        // pomona: class implementation
#include "LogService.hh"      // pomona: log message utility

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::Root::Plot::Attrib)

namespace Pomona {

namespace Root {

namespace Plot {

//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Attrib::Attrib()
  : Object()
  , TAttLine()
  , TAttMarker()
  , TAttFill()
{
  // set to library default
  setLine(kBlue+2, kSolid,      2);
  setMark(kBlue+2, kFullCircle, 1);
  setFill(kBlue-7, kFDotted2,   0.8);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
Attrib::Attrib(const Attrib& other)
  : Object(other)
  , TAttLine()
  , TAttMarker()
  , TAttFill()
{
  copyAttribs(other);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
Attrib::Attrib(Attrib&& other)
  : Object(other)
  , TAttLine()
  , TAttMarker()
  , TAttFill()
{
  copyAttribs(other);
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
Attrib& Attrib::operator=(const Attrib& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    copyAttribs(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
Attrib& Attrib::operator=(Attrib&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    copyAttribs(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy attributes from other
void Attrib::copyAttribs(const Attrib& other)
{
  fLineColor   = other.GetLineColor();
  fLineStyle   = other.GetLineStyle();
  fLineWidth   = other.GetLineWidth();
  fMarkerColor = other.GetMarkerColor();
  fMarkerStyle = other.GetMarkerStyle();
  fMarkerSize  = other.GetMarkerSize();
  fFillColor   = other.GetFillColor();
  fFillStyle   = other.GetFillStyle();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void Attrib::clear()
{
  Object    ::clear();
  TAttLine  ::ResetAttLine();
  TAttMarker::ResetAttMarker();
  TAttFill  ::ResetAttFill();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t Attrib::isEmpty() const
{
  if     (fLineColor!=1   || fLineStyle!=1   || fLineWidth!=1)  return kFALSE;
  else if(fMarkerColor!=1 || fMarkerStyle!=1 || fMarkerSize!=1) return kFALSE;
  else if(fFillColor!=1   || fFillStyle!=0)                     return kFALSE;
  else return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t Attrib::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else if(!Object::isSame(other)) return kFALSE;
  else{
    try{  // attempt cast
      const Attrib& o = dynamic_cast<const Attrib&>(other); 
      // check members
      if(fLineColor   != o.GetLineColor())   return kFALSE;
      if(fLineStyle   != o.GetLineStyle())   return kFALSE;
      if(fLineWidth   != o.GetLineWidth())   return kFALSE;
      if(fMarkerColor != o.GetMarkerColor()) return kFALSE;
      if(fMarkerStyle != o.GetMarkerStyle()) return kFALSE;
      if(fMarkerSize  != o.GetMarkerSize())  return kFALSE;
      if(fFillColor   != o.GetFillColor())   return kFALSE;
      if(fFillStyle   != o.GetFillStyle())   return kFALSE;
      return kTRUE;
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t Attrib::defaultPrintContent() const
{
  return kName | kTitle | kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object class-name.
void Attrib::printValue(std::ostream& os) const
{
  os << fLineColor;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set color for all attributes
void Attrib::setColor(const Color_t& color)
{
  TAttLine  ::SetLineColor  (color);
  TAttMarker::SetMarkerColor(color);
  TAttFill  ::SetFillColor  (color);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set attributes for line
void Attrib::setLine(const Color_t& color, const Style_t& style, 
			 const Width_t& width, const Float_t& alpha)
{
  TAttLine::SetLineColor     (color);
  TAttLine::SetLineStyle     (style);
  TAttLine::SetLineWidth     (width);
  TAttLine::SetLineColorAlpha(color, alpha);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set attributes for line
void Attrib::setMark(const Color_t& color, const Style_t& style, 
			 const Size_t& size, const Float_t& alpha)
{
  TAttMarker::SetMarkerColor     (color);
  TAttMarker::SetMarkerStyle     (style);
  TAttMarker::SetMarkerSize      (size);
  TAttMarker::SetMarkerColorAlpha(color, alpha);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set attributes for fill
void Attrib::setFill(const Color_t& color, const Style_t& style, 
			 const Float_t& alpha)
{
  TAttFill::SetFillColor     (color);
  TAttFill::SetFillStyle     (style);
  TAttFill::SetFillColorAlpha(color, alpha);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set other object line attributes from this
void Attrib::setObjAttLine(TAttLine& line) const
{
  line.SetLineColor(fLineColor);
  line.SetLineStyle(fLineStyle);
  line.SetLineWidth(fLineWidth);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set other object marker attributes from this
void Attrib::setObjAttMark(TAttMarker& mark) const
{
  mark.SetMarkerColor(fMarkerColor);
  mark.SetMarkerStyle(fMarkerStyle);
  mark.SetMarkerSize (fMarkerSize);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set other object fill attributes from this
void Attrib::setObjAttFill(TAttFill& fill) const
{
  fill.SetFillColor(fFillColor);
  fill.SetFillStyle(fFillStyle);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set other object attributes from this
void Attrib::setObjAtt(TObject& obj) const
{
  try { setObjAttLine(dynamic_cast<TAttLine&>(obj)); } 
  catch(const std::bad_cast& bc){ }
  try { setObjAttMark(dynamic_cast<TAttMarker&>(obj)); } 
  catch(const std::bad_cast& bc){ }
  try { setObjAttFill(dynamic_cast<TAttFill&>(obj)); } 
  catch(const std::bad_cast& bc){ }
}

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

//  end RootPlotAttrib.cxx
//  ############################################################################
