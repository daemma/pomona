//  ############################################################################
//! @file       RootPlotCanvases.cxx
//! @brief      Source file for collection of canvases
//! @author     Emma Hague
//! @date       2018-08-30
//! @copyright  Copyright (C) 2017 "Emma Hague" 
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifdef CERNROOT                 // Using root
#include "RootPlotCanvases.hh"  // pomona: this class
#include "RootPlotStyle.hh"     // pomona: plot styles
#include "LogService.hh"        // pomona: log message utility
#include "Application.hh"       // pomona: log message utility
#include "TCanvas.h"            // root: Plotting canvas

namespace Pomona {

namespace Root {

namespace Plot {

//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default constructor.
Canvases::Canvases()
  : std::map<String,TCanvas*>()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
Canvases::Canvases(const Canvases& other)
  : std::map<String,TCanvas*>(other)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
Canvases::Canvases(Canvases&& other)
  : std::map<String,TCanvas*>(other)
{
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
Canvases& Canvases::operator=(const Canvases& rhs)
{
  if(&rhs != this) std::map<String,TCanvas*>::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
Canvases& Canvases::operator=(Canvases&& rhs)
{
  if(&rhs != this){
    std::map<String,TCanvas*>::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print list of objects
void Canvases::printObjects() const
{
  for(auto& ncp : (*this)){
    const String& nme = ncp.first;
    loutI(kPlot) << nme << endl;
    const TCanvas* can = ncp.second;
    TIter next(can->GetListOfPrimitives()); TObject* cur;
    while((cur = next()))
      loutI(kPlot) << "\t" << cur->ClassName() << "::" << cur->GetName() << endl;
  } // end canvas loop
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear the collection and delete all root objects contained
/*! @warning This is can lead to segfaults! */
void Canvases::obliterate()
{
  for(auto& ncp : (*this)){
    TCanvas* can = ncp.second;
    TIter next(can->GetListOfPrimitives()); TObject* cur;
    while((cur = next())) if(cur){ delete cur; cur = nullptr; }
    // TCanvas* cObj = (TCanvas*)Root::Root::Troot().FindObject(can->GetName());
    // if(cObj){ delete cObj; cObj = nullptr; }
    if(can){ delete can; can = nullptr; }
  } // end canvas loop
  // clear the collection
  std::map<String,TCanvas*>::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert (add) a canvas to this map
Bool_t Canvases::insert(TCanvas* c)
{
  std::pair<String,TCanvas*> ncp(String(c->GetName()),c);
  return std::map<String,TCanvas*>::insert( ncp ).second;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert (add) an other canvas-map to this map (operator)
Canvases& Canvases::operator+=(TCanvas* rhs)
{
  insert(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert (add) an other canvas-map to this map
Bool_t Canvases::insert(const Canvases& o)
{
  Bool_t suc(kFALSE);
  for(auto & cnp : o) suc |= insert(cnp.second);
  return suc;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Insert (add) an other canvas-map to this map (operator)
Canvases& Canvases::operator+=(const Canvases& rhs)
{
  insert(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Save all canvases to image files
void Canvases::save(const HostPath& path, const String& ext, const Bool_t& pi) const
{
  for(auto & ncp : (*this)) Style::SaveCanvas(*ncp.second, path, ext, pi);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Save all canvases to image files
void Canvases::save(const App::Application& app, const Bool_t& pi) const
{
  for(auto & ncp : (*this)) app.saveCanvas(*ncp.second, pi);
}

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

//  end RootPlotCanvases.cxx
//  ############################################################################
