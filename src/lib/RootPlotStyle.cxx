//  ############################################################################
//! @file       RootPlotStyle.cxx
//! @brief      Source file for root-system style utility class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifdef CERNROOT                // Using root
#include "RootPlotStyle.hh"    // pomona: this class
#include "Root.hh"             // pomona: root-system interface
#include "LogService.hh"       // pomona: log message service
#include "TStyle.h"            // root: TStyle
#include "TROOT.h"             // root: THE ROOT
#include "TVirtualMutex.h"     // root: mutex
#include "TMath.h"             // root: mathematical methods
#include "TH1.h"               // root: histogram
#include "TLegend.h"           // root: legend
#include "TCanvas.h"           // root: graphics\plot canvas
#include "TRandom.h"           // root: random numbers
#include "TGraphAsymmErrors.h" // root: graph with asymetric error bars
#include <iomanip>             // std: I/O manipulation

namespace Pomona {

namespace Root {

namespace Plot {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Global static map of plot-style names
static const std::map<eStyle,String_t> gStyleNames = 
  {{eStyle::kPomona,  "Pomona"}
  ,{eStyle::kInvert,  "Invert"}
  ,{eStyle::kPlain,   "Plain"}
  ,{eStyle::kBold,    "Bold"}
  ,{eStyle::kVideo,   "Video"}
  ,{eStyle::kPub,     "Pub"}
  ,{eStyle::kClassic, "Classic"}
  ,{eStyle::kDefault, "Default"}
  ,{eStyle::kModern,  "Modern"}
  };

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Offset for dark colors (for light pallet)
static const UShort_t gDColOff = 1;
//! Global static holder for array of different colors
static const std::vector<Color_t> gColors = 
  { kBlack
  , kRed+gDColOff,     kBlue+gDColOff,  kGreen+gDColOff+1
  , kMagenta+gDColOff, kCyan+gDColOff,  kYellow+gDColOff
  , kPink+gDColOff,    kAzure+gDColOff, kSpring+gDColOff 
  , kViolet+gDColOff,  kTeal+gDColOff,  kOrange+gDColOff 
  };
//! Offset for bright colors (for dark pallet)
static const UShort_t gBColOff = 7;
//! Global static holder for array of different colors
static const std::vector<Color_t> gColorsInv = 
  { kWhite
  , kRed-gBColOff,     kBlue-gBColOff,  kGreen-gBColOff
  , kMagenta-gBColOff, kCyan-gBColOff,  kYellow-gBColOff
  , kPink-gBColOff,    kAzure-gBColOff, kSpring-gBColOff 
  , kViolet-gBColOff,  kTeal-gBColOff,  kOrange-gBColOff
  };
//! Global static holder for array of different line styles
static const std::vector<Style_t> gLines = 
  { 1
  , 2, 3, 4
  , 5, 6, 7
  , 8, 9, 10 
  , 2, 3, 4
  };
//! Global static holder for array of different fill styles
static const std::vector<Style_t> gFills = 
  { kFDotted3
  , kFHatched1,   kHatched2, kFHatched3
  , kFHatched4,   kFStars1,  kFStars2
  , kFWaves1,     kFWaves2,  kFMondrian 
  , kFSnowflakes, kFCircles, kFAlhambra 
  };
//! Global static holder for array of different marker styles
static const std::vector<Style_t> gMarks = 
  { kFullCircle
  , kFullSquare, kFullTriangleUp, kFullTriangleDown
  , kFullStar,   kFullDiamond,    kOpenCircle
  , kOpenSquare, kOpenTriangleUp, kOpenTriangleDown
  , kOpenStar,   kOpenCross,      kOpenDiamond
  };

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get refference to global root-system current style
TStyle& Style::Tstyle()
{
  return *gStyle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the current style name
const Char_t* Style::Name()
{
  return Tstyle().GetName();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the current style title
const Char_t* Style::Title()
{
  return Tstyle().GetTitle();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String of CERN root-system information
String_t Style::Info()
{
  return Name();
}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the level name string value
const String_t& Style::StyleName(const eStyle& es)
{
  try{ return gStyleNames.at(es); }
  catch(std::out_of_range& oor){
    loutE(kInput) << "Style::StyleName: Failed to find input style enumerator: " 
		  << static_cast<UShort_t>(es) << endl;
    return gStyleNames.at(eStyle::kPomona);
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the level from string value
/** @warning Case sensitive. */
const eStyle& Style::StyleEnum(const String_t& n)
{
  if(n.size()>1 && n[0]!='\0' ) 
    for(auto& lme: gStyleNames) if(n == lme.second) return lme.first;
  loutN(kInput) << "Style::StyleEnum: Style named '" << n << "' not found"
		<< ", using '" << StyleName(kDefStyle) << "'" << endl;
  return kDefStyle;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Has the named style been built?
Bool_t Style::Has(const String_t& name)
{
  TStyle* s = Root::Troot().GetStyle(name.c_str());
  if(s != 0) return kTRUE;
  else       return kFALSE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get (a refference to) a named style.
TStyle& Style::Find(const String_t& name)
{
  TStyle* s = Root::Troot().GetStyle(name.c_str());
  if(s != 0) return *s;
  else{
    loutW(kEval) << "Style::findStyle: '" << name 
		 << "' not found, returning current style." << endl;
    return Style::Tstyle();
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set current style to a named style.
void Style::Set(const eStyle& es)
{
  Set( StyleName(es) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set current style to a named style.
void Style::Set(const String_t& name)
{
  if(Has(name)){
    Root::Troot().SetStyle(name.c_str());
    if(Style::IsInverse()) gStyle->SetPalette(kDefPaletteI);
    else                   gStyle->SetPalette(kDefPalette);
  }
  else
    loutW(kInput) << "Root::Style::Set: Unknown style: '" << name 
		  << "', using '" << Name() << "'" << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Force current style onto all objects.
void Style::Force()
{
  Root::Troot().ForceStyle();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! True if the style is "inverted"
Bool_t Style::IsInverse()
{
  return String(Name()).contains("Inv");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Build the default styles for this library
void Style::BuildStyles()
{
  BuildPomonaStyle();
  BuildInvertStyle();
  Set(eStyle::kInvert);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Build the default style for this library
void Style::BuildPomonaStyle()
{
  // check that this style has not already been built
  if(!Has("Pomona")){ 
    // construct the style
    TStyle s("Pomona", "Pomona Style");  
    // copy from built-in style
    Find("Modern").Copy(s);

    s.SetCanvasPreferGL (kFALSE);
    s.SetCanvasDefW     (kDefSzX);
    s.SetCanvasDefH     (kDefSzY);
    s.SetCanvasColor    (kWhite);
    s.SetPadColor       (kWhite);
    s.SetPadLeftMargin  (0.08); 
    s.SetPadRightMargin (0.04); 
    s.SetPadTopMargin   (0.06); 
    s.SetPadGridX       (kTRUE);
    s.SetPadGridY       (kTRUE);
    s.SetOptStat        (0);
    s.SetTitleFontSize  (0.04);
    s.SetHistLineColor  (kBlue+2);
    s.SetHistLineStyle  (kSolid);
    s.SetHistLineWidth  (2);
    s.SetHistFillColor  (kBlue-7);
    s.SetHistFillStyle  (kFDotted2);
    s.SetFuncColor      (kBlue+2);
    s.SetFuncStyle      (kSolid);
    s.SetFuncWidth      (2);
    s.SetPalette        (kDefPalette);
    s.SetNumberContours (255);

    // Add to the global list
    {
      R__LOCKGUARD2(gROOTMutex);
      Root::Troot().GetListOfStyles()->Add((TStyle*)s.Clone());
    }
  } // end style not found
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Build the default inverted style for this library
void Style::BuildInvertStyle()
{
  // check that this style has not already been built
  if(!Has("Invert")){ 
    // check that library default has been built
    if(!Has("Pomona")) BuildPomonaStyle();
    // construct the style
    TStyle s("Invert", "Pomona Inverted Style");
    // copy from pre-built style
    Find("Pomona").Copy(s);

    // set values
    s.SetCanvasColor     (kBlack);
    s.SetPadColor        (kBlack);
    s.SetFrameLineColor  (kWhite);
    s.SetTextColor       (kWhite);
    s.SetTitleTextColor  (kWhite);
    s.SetAxisColor       (kWhite, "X");
    s.SetTitleColor      (kWhite, "X");
    s.SetLabelColor      (kWhite, "X");
    s.SetAxisColor       (kWhite, "Y");
    s.SetTitleColor      (kWhite, "Y");
    s.SetLabelColor      (kWhite, "Y");
    s.SetAxisColor       (kWhite, "Z");
    s.SetTitleColor      (kWhite, "Z");
    s.SetLabelColor      (kWhite, "Z");
    s.SetLineColor       (kWhite);
    s.SetLineColorAlpha  (kWhite, 1.0);
    s.SetMarkerColor     (kWhite);
    s.SetMarkerColorAlpha(kWhite, 1.0);
    s.SetFillColor       (kGray+2);
    s.SetFillColorAlpha  (kGray+2, 0.5);
    s.SetHistLineColor   (kCyan);
    s.SetHistFillColor   (kCyan-3);
    s.SetFuncColor       (kCyan);
    s.SetLegendFillColor (kBlack);
    s.SetPalette         (kDefPaletteI);
    s.SetNumberContours  (255);

    // Add to the global list
    {
      R__LOCKGUARD2(gROOTMutex);
      Root::Troot().GetListOfStyles()->Add((TStyle*)s.Clone());
    }
  } // end style not found
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get a predefined color
Color_t Style::GetGenColor(const UShort_t& idx)
{
  if(IsInverse()) return gColorsInv[idx % gColorsInv.size()];
  else            return gColors   [idx % gColors   .size()];
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get a predefined line-style
Style_t Style::GetGenLine(const UShort_t& idx)
{
  return gLines[idx % gLines.size()];
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get a predefined fill-style
Style_t Style::GetGenFill(const UShort_t& idx)
{
  return gFills[idx % gFills.size()];
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get a predefined marker-style
Style_t Style::GetGenMark(const UShort_t& idx)
{
  return gMarks[idx % gMarks.size()];
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the plot scheme of a generic index
eScheme Style::GetGenScm(const UShort_t& idx)
{
  UShort_t mod = (idx % gColors.size());
  switch(mod){
  case 0  : return eScheme::kPs0;
  case 1  : return eScheme::kPs1;
  case 2  : return eScheme::kPs2;
  case 3  : return eScheme::kPs3;
  case 4  : return eScheme::kPs4;
  case 5  : return eScheme::kPs5;
  case 6  : return eScheme::kPs6;
  case 7  : return eScheme::kPs7;
  case 8  : return eScheme::kPs8;
  case 9  : return eScheme::kPs9;
  case 10 : return eScheme::kPs10;
  case 11 : return eScheme::kPs11;
  case 12 : return eScheme::kPs12;
  default : return eScheme::kPs0;
  } // end switch on scheme
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the index of a generic plot scheme
UShort_t Style::GetGenIdx(const eScheme& ps)
{
  switch(ps){
  case eScheme::kPs1 : return 1;
  case eScheme::kPs2 : return 2;
  case eScheme::kPs3 : return 3;
  case eScheme::kPs4 : return 4;
  case eScheme::kPs5 : return 5;
  case eScheme::kPs6 : return 6;
  case eScheme::kPs7 : return 7;
  case eScheme::kPs8 : return 8;
  case eScheme::kPs9 : return 9;
  case eScheme::kPs10: return 10;
  case eScheme::kPs11: return 11;
  case eScheme::kPs12: return 12;
  default            : return 0;
  } // end switch on scheme
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get a predefined attribute
Attrib Style::GetGenAttrib(const UShort_t& idx)
{
  return GetSchAttrib( GetGenScm(idx) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get schemed attributes
Attrib Style::GetSchAttrib(const eScheme& ps)
{
  Attrib pa;
  switch(ps){
  case eScheme::kDef : 
    pa.setLine(kBlue+2, kSolid,      kDefLineWidth);
    pa.setMark(kBlue+2, kFullCircle, 1);
    pa.setFill(kBlue-7, kFDotted2,   0.8);
    break;
  case eScheme::kBkg : 
    pa.setLine(kGray+3, kDotted,     kDefLineWidth);
    pa.setMark(kGray+3, kOpenCircle, 1);
    pa.setFill(kGray+1, kFDotted2,   0.8);
    break;
  case eScheme::kNul : 
    pa.setLine(kBlue+2, kDashed,           kDefLineWidth);
    pa.setMark(kBlue+2, kFullTriangleDown, 1.5);
    pa.setFill(kBlue-7, kHatched2);
    break;
  case eScheme::kAlt : 
    pa.setLine(kRed+2, kSolid,          kDefLineWidth);
    pa.setMark(kRed+2, kFullTriangleUp, 1.5);
    pa.setFill(kRed-7, kFHatched1);
    break;

  case eScheme::kPs0 : 
    pa.setLine(gColors[0],    gLines[0], kDefLineWidth);
    pa.setMark(gColors[0],    gMarks[0], 1);
    pa.setFill(gColorsInv[0], gFills[0]);
    break;
  case eScheme::kPs1 : 
    pa.setLine(gColors[1],    gLines[1], kDefLineWidth);
    pa.setMark(gColors[1],    gMarks[1], 1);
    pa.setFill(gColorsInv[1], gFills[1]);
    break;
  case eScheme::kPs2 : 
    pa.setLine(gColors[2],    gLines[2], kDefLineWidth);
    pa.setMark(gColors[2],    gMarks[2], 1);
    pa.setFill(gColorsInv[2], gFills[2]);
    break;
  case eScheme::kPs3 : 
    pa.setLine(gColors[3],    gLines[3], kDefLineWidth);
    pa.setMark(gColors[3],    gMarks[3], 1);
    pa.setFill(gColorsInv[3], gFills[3]);
    break;
  case eScheme::kPs4 : 
    pa.setLine(gColors[4],    gLines[4], kDefLineWidth);
    pa.setMark(gColors[4],    gMarks[4], 1);
    pa.setFill(gColorsInv[4], gFills[4]);
    break;
  case eScheme::kPs5 : 
    pa.setLine(gColors[5],    gLines[5], kDefLineWidth);
    pa.setMark(gColors[5],    gMarks[5], 1);
    pa.setFill(gColorsInv[5], gFills[5]);
    break;
  case eScheme::kPs6 : 
    pa.setLine(gColors[6],    gLines[6], kDefLineWidth);
    pa.setMark(gColors[6],    gMarks[6], 1);
    pa.setFill(gColorsInv[6], gFills[6]);
    break;
  case eScheme::kPs7 : 
    pa.setLine(gColors[7],    gLines[7], kDefLineWidth);
    pa.setMark(gColors[7],    gMarks[7], 1);
    pa.setFill(gColorsInv[7], gFills[7]);
    break;
  case eScheme::kPs8 : 
    pa.setLine(gColors[8],    gLines[8], kDefLineWidth);
    pa.setMark(gColors[8],    gMarks[8], 1);
    pa.setFill(gColorsInv[8], gFills[8]);
    break;
  case eScheme::kPs9 : 
    pa.setLine(gColors[9],    gLines[9], kDefLineWidth);
    pa.setMark(gColors[9],    gMarks[9], 1);
    pa.setFill(gColorsInv[9], gFills[9]);
    break;
  case eScheme::kPs10 : 
    pa.setLine(gColors[10],    gLines[10], kDefLineWidth);
    pa.setMark(gColors[10],    gMarks[10], 1);
    pa.setFill(gColorsInv[10], gFills[10]);
    break;
  case eScheme::kPs11 : 
    pa.setLine(gColors[11],    gLines[11], kDefLineWidth);
    pa.setMark(gColors[11],    gMarks[11], 1);
    pa.setFill(gColorsInv[11], gFills[11]);
    break;
  case eScheme::kPs12 : 
    pa.setLine(gColors[12],    gLines[12], kDefLineWidth);
    pa.setMark(gColors[12],    gMarks[12], 1);
    pa.setFill(gColorsInv[12], gFills[12]);
    break;

  default : 
    loutA(kInput) << "Attrib::GetSchemed: Unknown scheme." << endl;
    break;
  } // end switch eScheme

  // modify for inverted plotting-style
  if(Style::IsInverse()){
    switch(ps){
    case eScheme::kDef : 
      pa.setColor    (kWhite); 
      pa.SetFillColor(kGray+2);
      break;
    case eScheme::kBkg : 
      pa.setColor    (kGray); 
      pa.SetFillColor(kGray+2);
      break;
    case eScheme::kNul : 
      pa.setColor    (kCyan); 
      pa.SetFillColor(kCyan-2);
      break;
    case eScheme::kAlt : 
      pa.setColor    (kMagenta); 
      pa.SetFillColor(kMagenta-4);
      break;

    case eScheme::kPs0 : 
      pa.setColor    (gColorsInv[0]); 
      pa.SetFillColor(gColors   [0]);
      break;
    case eScheme::kPs1 : 
      pa.setColor    (gColorsInv[1]); 
      pa.SetFillColor(gColors   [1]);
      break;
    case eScheme::kPs2 : 
      pa.setColor    (gColorsInv[2]); 
      pa.SetFillColor(gColors   [2]);
      break;
    case eScheme::kPs3 : 
      pa.setColor    (gColorsInv[3]); 
      pa.SetFillColor(gColors   [3]);
      break;
    case eScheme::kPs4 : 
      pa.setColor    (gColorsInv[4]); 
      pa.SetFillColor(gColors   [4]);
      break;
    case eScheme::kPs5 : 
      pa.setColor    (gColorsInv[5]); 
      pa.SetFillColor(gColors   [5]);
      break;
    case eScheme::kPs6 : 
      pa.setColor    (gColorsInv[6]); 
      pa.SetFillColor(gColors   [6]);
      break;
    case eScheme::kPs7 : 
      pa.setColor    (gColorsInv[7]); 
      pa.SetFillColor(gColors   [7]);
      break;
    case eScheme::kPs8 : 
      pa.setColor    (gColorsInv[8]); 
      pa.SetFillColor(gColors   [8]);
      break;
    case eScheme::kPs9 : 
      pa.setColor    (gColorsInv[9]); 
      pa.SetFillColor(gColors   [9]);
      break;
    case eScheme::kPs10 : 
      pa.setColor    (gColorsInv[10]); 
      pa.SetFillColor(gColors   [10]);
      break;
    case eScheme::kPs11 : 
      pa.setColor    (gColorsInv[11]); 
      pa.SetFillColor(gColors   [11]);
      break;
    case eScheme::kPs12 : 
      pa.setColor    (gColorsInv[12]); 
      pa.SetFillColor(gColors   [12]);
      break;

    default : 
      loutA(kInput) << "Attrib::GetSchemed: Unknown inverse scheme." << endl;
      break;
    } // end switch eScheme
  } // end is inverse

  // return constructed attribute scheme
  return pa;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set attributes for scheme
void Style::SetAttrib(TObject& obj, const eScheme& scheme)
{
  // use Attrib class
  Attrib pa = Style::GetSchAttrib(scheme);
  pa.setObjAtt(obj);

  // Text is not covered in Attrib class
  if(IsInverse()){
    try { dynamic_cast<TAttText&>(obj).SetTextColor(kWhite); } 
    catch(const std::bad_cast& bc){ }
  }
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set attributes for scheme
void Style::SetAttrib(TObject& obj, const UShort_t& idx)
{
  SetAttrib(obj, GetGenScm(idx));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Set a histogram axis style */
void Style::SetAxis(TH1& h, 
		    const Char_t* xTitle, const Char_t* yTitle, const Char_t* zTitle)
{
  TAxis* xAxis = h.GetXaxis();
  xAxis->SetTitle(xTitle);
  xAxis->CenterTitle(kTRUE);
  TAxis* yAxis = h.GetYaxis();
  yAxis->SetTitle(yTitle);
  yAxis->CenterTitle(kTRUE);
  TAxis* zAxis = h.GetZaxis();
  zAxis->SetTitle(zTitle);
  zAxis->CenterTitle(kTRUE);
  zAxis->SetTitleOffset(1.10);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Get a generic legend object */
TLegend* Style::Legend(const Double_t& x1, const Double_t& y1, 
		       const Double_t& x2, const Double_t& y2, 
		       const Char_t* header)
{
  TLegend* leg = new TLegend(x1, y1, x2, y2, header);
  // SetAttributes(*leg);
  if(IsInverse()){ leg->SetFillColor(kBlack);  leg->SetTextColor(kWhite); }
  return leg;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Save a canvas (static).
void Style::SaveCanvas(const TCanvas& canvas, const HostPath& path, 
		       const String& ext, const Bool_t& printInfo,
		       const HostPath& aliasPath)
{
  String cname(canvas.GetName());
  if(Style::IsInverse()) cname += "_Dark";
  cname += ext;
  HostPath filePath(path);
  filePath += cname;
  canvas.SaveAs(filePath.get().c_str());
  if(printInfo){
    if(aliasPath.get() != "") 
      loutI(kPlot) << "Saved canvas: " << filePath.asAliased(aliasPath) << endl;  
    else
      loutI(kPlot) << "Saved canvas: " << filePath << endl;  
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the bin-wise pull (static) 
TGraphAsymmErrors* Style::PullGraph(const TH1& bkg, const TH1& frg)
{
  const UInt_t nBins    = bkg.GetNbinsX();
  const UInt_t nBinsFrg = frg.GetNbinsX();
  if(nBins != nBinsFrg){
    loutW(kInput) << "Style::Pull: Unequal number of bins " 
		  << nBins << " != " << nBinsFrg << endl;
    if(nBins > nBinsFrg){
      loutE(kInput) << "Style::Pull: Too many background bins " 
		    << nBins << " != " << nBinsFrg << endl;
      return nullptr;
    }
  }

  TGraphAsymmErrors* g = new TGraphAsymmErrors();
  // initialize graph values
  for(UInt_t c=0; c<nBins; c++){
    const UInt_t bNum = c+1;
    g->SetPoint     (c, frg.GetBinCenter(bNum), 0.);
    // const Double_t hbw = 0.5*frg.GetBinWidth(bNum);
    g->SetPointError(c, 0., 0., 0., 0.);
  }
  // set graph values
  for(UInt_t c=0; c<nBins; c++){
    UInt_t bNum = c+1;
    const Double_t bbc  = bkg.GetBinContent (bNum); if(bbc  <= 0.) continue;
    const Double_t fbc  = frg.GetBinContent (bNum); if(fbc  <= 0.) continue;
    const Double_t fbeu = frg.GetBinErrorUp (bNum); if(fbeu <= 0.) continue;
    const Double_t fbel = frg.GetBinErrorLow(bNum); if(fbel <= 0.) continue;

    Double_t pull = fbc - bbc;
    // if(TMath::Abs(pull) > 1.e10) continue;
    if     (pull > 0.) pull /= fbeu;
    else if(pull < 0.) pull /= fbel;
    else               pull  = 0.;
    g->SetPoint(c, frg.GetBinCenter(bNum), pull);
    
    // // error up
    // Double_t pullUp = (fbc + fbeu) - bbc;    
    // if     (pullUp > 0.) pullUp /= fbeu;
    // else if(pullUp < 0.) pullUp /= fbel;
    // else                 pullUp  = 0.;
    // const Double_t pullUpE = TMath::Abs(pullUp-pull);
    // // error low
    // Double_t pullLow = (fbc + fbeu) - bbc;    
    // if     (pullLow > 0.) pullLow /= fbeu;
    // else if(pullLow < 0.) pullLow /= fbel;
    // else                 pullLow  = 0.;
    // const Double_t pullLowE = TMath::Abs(pull-pullLow);
    // // set errors
    // const Double_t hbw = 0.5*frg.GetBinWidth(bNum);
    // g->SetPointError(c, hbw, hbw, pullLowE, pullUpE);
  }

  return g;
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the Kullback–Leibler divergence (static) 
/*! See https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence 

    In the context of machine learning, D_KL(P‖Q) is often called the information 
    gain achieved if Q is used instead of P. By analogy with information theory, 
    it is also called the relative entropy of P with respect to Q. 
    In the context of coding theory, DKL(P‖Q) can be construed as measuring the 
    expected number of extra bits required to code samples from P using a code 
    optimized for Q rather than the code optimized for P.
*/
Double_t Style::KullbackDivergence(const TH1D& p, const TH1D& q)
{
  // check and set-up
  Double_t ret(0.);
  const UInt_t nBp = p.GetNbinsX();
  const UInt_t nBq = q.GetNbinsX();
  if(nBp != nBq){
    loutE(kInput) << "Style::KullbackDivergence: Unequal number of bins " 
		  << nBp << " != " << nBq << endl;
    return 0.;
  }
  // calculate stat
  for(UInt_t c=0; c<nBp; c++){
    Double_t pbc = p.GetBinContent(c+1); if(pbc <= 0.) continue;
    Double_t qbc = q.GetBinContent(c+1); if(qbc <= 0.) continue;
    ret += pbc * TMath::Log2(qbc/pbc);
  }

  // // copy to extract normalized arrays
  // auto pNorm = new TH1D(p); 
  // pNorm->Scale(1./pNorm->Integral());
  // pNorm->SetName( Form("%s_KLDiv",p.GetName()) );
  // auto qNorm = new TH1D(q); 
  // qNorm->Scale(1./qNorm->Integral());
  // qNorm->SetName( Form("%s_KLDiv",q.GetName()) );
  // for(UInt_t c=0; c<nBp; c++){
  //   Double_t pbc = pNorm->GetBinContent(c+1); if(pbc <= 0.) continue;
  //   Double_t qbc = qNorm->GetBinContent(c+1); if(qbc <= 0.) continue;
  //   ret += pbc * TMath::Log2(qbc/pbc);
  // }
  // // clean-up
  // if(pNorm){ delete pNorm; pNorm = nullptr; }
  // if(qNorm){ delete qNorm; qNorm = nullptr; }

  // return the value
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print style information to the stream
void Style::PrintInfo(std::ostream& os)
{
  os << "plot-style : " << Info() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print style information to debug log-level
void Style::PrintInfo(const Log::eLevel& l)
{
  Logger(l,Log::eTopic::kEval) << "plot-style : " << Info() << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print help text for available styles
void Style::PrintStyleHelp(std::ostream& os, const String_t& ind)
{
  os << ind << "plot-style arguments: " << endl;
  // Current style
  String csName(Name());  
  // Iterate over available styles
  TIter next(Root::Troot().GetListOfStyles()); TStyle *s;
  while ( (s = (TStyle*)next()) ){
    // if(!TString(s->GetTitle()).Contains("Pomona")) continue;
    // else {
      os << ind << "  " << std::left << std::setw(9) << s->GetName() 
	        << resetiosflags(std::ios_base::adjustfield) 
		<< " : " << s->GetTitle() << "" << endl;
    // }
  }
  // Return style to previous current style
  Set(csName);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print help text for available image extensions
void Style::PrintExtHelp(std::ostream& os, const String_t& ind)
{
  os << ind << "image-ext arguments: " << endl;
  os << ind << "  .png  : Portable Network Graphics" << endl;
  os << ind << "  .svg  : Scalable Vector Graphics" << endl;
  os << ind << "  .gif  : Graphics Interchange Format" << endl;
  os << ind << "  .jpg  : Joint Photographic experts Group" << endl;
  os << ind << "  .tiff : Tagged Image File Format" << endl;
  os << ind << "  .pdf  : Portable Document Format" << endl;
  os << ind << "  .ps   : PostScript" << endl;
  os << ind << "  .eps  : Encapsulated PostScript" << endl;
  os << ind << "  .xml  : eXtensable Markup Language" << endl;
  os << ind << "  .tex  : TeX" << endl;
  os << ind << "  .xpm  : X(11) PixMap" << endl;
  os << ind << "  .C    : C++ macro" << endl;
  os << ind << "  .cxx  : C++ macro" << endl;
  os << ind << "  .root : Root" << endl;
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Draw examples of available styles
void Style::DrawExamples()
{
  // Current style
  String_t csName(Name());
  const UInt_t nSim = 10000;

  // List header
  loutI(kEval) << std::left << std::setw(11) << "Styles" 
	       << resetiosflags(std::ios_base::adjustfield) << " : " << endl;
  // Iterate over available styles
  TIter next(Root::Troot().GetListOfStyles()); TStyle *s;
  while ( (s = (TStyle*)next()) ){
    if(!TString(s->GetTitle()).Contains("Pomona")) continue;
    else {
      // Set the style for new objects
      Root::Troot().SetStyle(s->GetName());
      const Char_t* sName  = Name();
      const Char_t* sTitle = Title();

      // print style information
      loutI(kEval) << "  " << std::left << std::setw(9) << sName 
		   << resetiosflags(std::ios_base::adjustfield) 
		   << " : \"" << sTitle << "\"" << endl;

      // Canvas for plotting the style
      TCanvas* canvas = new TCanvas(sName, sTitle);
      const Char_t* hTitle = (String_t(sName) + ": " + String_t(sTitle)).c_str();
      TLegend* leg = Legend();
      
      // Create and attribute histograms
      TH1D* hAlt = new TH1D("hAlt", hTitle, 128, -6., 6.);
      SetAttrib(*hAlt, eScheme::kAlt);
      SetAxis(*hAlt, "X [arb. units]", "Counts per Bin");
      leg->AddEntry(hAlt, "Alt", "LPF");
      TH1D* hNul = (TH1D*)hAlt->Clone("hNul");
      SetAttrib(*hNul, eScheme::kNul);
      leg->AddEntry(hNul, "Null", "LPF");
      TH1D* hBkg = (TH1D*)hAlt->Clone("hBkg");
      SetAttrib(*hBkg, eScheme::kBkg);
      leg->AddEntry(hBkg, "Bkg", "LPF");

      // Fill and draw histograms
      for(UInt_t i=0;i<nSim;i++) hAlt->Fill(gRandom->Gaus(-1,1));
      hAlt->Draw();
      hAlt->Draw("SAME PE");
      for(UInt_t i=0;i<nSim;i++) hNul->Fill(gRandom->Gaus(1,1));
      hNul->Draw("SAME");
      hNul->Draw("SAME PE");
      for(UInt_t i=0;i<nSim;i++) hBkg->Fill(gRandom->Gaus(0,1));
      hBkg->Draw("SAME");
      hBkg->Draw("SAME PE");

      // update the stuffs
      leg->Draw("SAME");
      gPad->RedrawAxis();
      canvas->Update();
    }
  }

  // Return style to previous current style
  Set(csName);
  //printInfo(eLogLevel::kDebug);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Draw examples of available styles
void Style::DrawRootExamples()
{
  // Current style
  String_t csName = gStyle->GetName();

  // Histo for plotting style examples
  TH1D* histo = new TH1D("histo", "A histogram", 128, -6., 6.);
  histo->FillRandom("gaus", 10000);

  // List header
  loutI(kEval) << std::left << std::setw(10) 
	       << "Styles" 
	       << resetiosflags(std::ios_base::adjustfield)
	       << " : " << endl;
  // Iterate over available styles
  TIter next(Root::Troot().GetListOfStyles()); TStyle *s;
  while ((s = (TStyle*) next())){
    if(TString(s->GetTitle()).Contains("Pomona")) continue;
    else {
      // print style information
      loutI(kEval) << "  " 
		   << std::left << std::setw(8) 
		   << s->GetName() 
		   << resetiosflags(std::ios_base::adjustfield)
		   << " : " << s->GetTitle() 
		   << endl;
      // Set the style for new objects
      Root::Troot().SetStyle(s->GetName());
      // Canvas for plotting the style
      TCanvas* canvas = new TCanvas(s->GetName(), s->GetTitle());
      canvas->UseCurrentStyle();
      // Set histogram to show useful information
      histo->SetNameTitle(s->GetName(), (String_t(s->GetName()) + ": " + 
					 String_t(s->GetTitle())).c_str() );
      histo->GetXaxis()->SetTitle("X");
      histo->GetYaxis()->SetTitle("Counts per Bin");
      histo->UseCurrentStyle();
      histo->Draw();
      histo->Draw();
      canvas->Update();
    }
  }

  // Return style to previous current style
  Set(csName.c_str());
  PrintInfo(Log::eLevel::kDebug);
} 

} // end namespace Plot

} // end namespace Root

} // end namespace Pomona

#endif  // end using root

//  end RootPlotStyle.cxx
//  ############################################################################
