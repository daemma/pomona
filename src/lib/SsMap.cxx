//  ############################################################################
//! @file       lib/SsMap.cxx
//! @brief      Source for SsMap class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "SsMap.hh"      // pomona: this class
#include "ClassImp.hh"   // pomona: class implementation
#ifdef CERNROOT          // Using root
#include "TBuffer.h"     // root: I/O buffer
#endif                   // end using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::SsMap)
// register the cereal-ization
CEREAL_REGISTER_TYPE(Pomona::SsMap)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
SsMap::SsMap()
  : SMap<String>()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
SsMap::SsMap(const SsMap& other)
  : SMap<String>(other)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
SsMap::SsMap(SsMap&& other)
  : SMap<String>(std::move(other))
{ 
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy assignment operator. */
SsMap& SsMap::operator=(const SsMap& rhs)
{
  if(!isSame(rhs)){
    SMap<String>::operator=(rhs);
    set(rhs());
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move assignment operator. */
SsMap& SsMap::operator=(SsMap&& rhs)
{
  if(!isSame(rhs)){
    SMap<String>::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

#ifdef CERNROOT  // Using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! root file i/o streamer
void SsMap::Streamer(TBuffer& buff)
{
  if(buff.IsReading()){
    buff.ReadClassBuffer(Pomona::SsMap::Class(),this);
    ULong64_t sz(0);
    buff.ReadULong64(sz);
    for(ULong64_t i=0; i<sz; ++i){
      String key; key.Streamer(buff);
      String val; val.Streamer(buff);
      operator[](key) = val;
    }
  } else {
    buff.WriteClassBuffer(Pomona::SsMap::Class(),this);    
    buff.WriteULong64(size());
    for(auto& snp : (*this)){
      String key = snp.first;  key.Streamer(buff);
      String val = snp.second; val.Streamer(buff);
    }
  }
}

#endif           // end using root

} // end namespace Pomona

//  end SsMap.cxx
//  ############################################################################
