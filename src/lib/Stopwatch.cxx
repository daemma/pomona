//  ############################################################################
//! @file       Stopwatch.cxx
//! @brief      Source for timing exectution
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Stopwatch.hh"  // pomona: this class

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor.
Stopwatch::Stopwatch() 
  : mStartTime(),
    mDuration(0.),
    mIsRunning(false)
{ }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy constructor.
Stopwatch::Stopwatch(const Stopwatch& other) 
  : mStartTime(other.mStartTime),
    mDuration(other.mDuration)
{ }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move constructor.
Stopwatch::Stopwatch(Stopwatch&& other) 
  : mStartTime(std::move(other.mStartTime)),
    mDuration(std::move(other.mDuration))
{ }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
Stopwatch& Stopwatch::operator=(const Stopwatch& rhs)
{
  if(this != &rhs){
    mStartTime = rhs.mStartTime;
    mDuration  = rhs.mDuration;
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
Stopwatch& Stopwatch::operator=(Stopwatch&& rhs)
{
  if(this != &rhs){
    mStartTime = std::move(rhs.mStartTime);
    mDuration  = std::move(rhs.mDuration);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Is the stopwatch running?
const Bool_t& Stopwatch::isRunning()
{
  return mIsRunning;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Start the stopwatch.
void Stopwatch::start()
{
  mStartTime = std::chrono::steady_clock::now();
  mIsRunning = true;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Current elapsed time.
Double_t Stopwatch::elapsed() const
{
  return calcDuration();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Stop the stopwatch.
const Double_t& Stopwatch::stop()
{
  mDuration = elapsed();
  mIsRunning = false;
  return mDuration;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Reset the duration.
void Stopwatch::reset()
{
  mDuration = 0.;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Calculate the duration between start-time and now.
Double_t Stopwatch::calcDuration() const
{
  return std::chrono::duration_cast<std::chrono::duration<Double_t>>
    (std::chrono::steady_clock::now() - mStartTime).count();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Get elapsed time as a HH:MM:SS.sss string.
String_t Stopwatch::elapsedAsString() const
{
  return DurationAsString(elapsed());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Stop the timer and get stopped time as a HH:MM:SS.sss string.
String_t Stopwatch::stoppedAsString()
{
  return DurationAsString(stop());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++_
//! Convert a duration time to a HH:MM:SS.sss string.
String_t Stopwatch::DurationAsString(Double_t realt)
{
  // calc hrs, mins, secs
  int hours = int(realt / 3600);
  realt -= hours * 3600;
  int min   = int(realt / 60);
  realt -= min * 60;
  if(realt < 0) realt = 0.;

  // write to formatted c-string
  char* srealt = new char[64];
  if(hours > 0)
    sprintf(srealt, "%d:%02d:%02d [hh:mm:ss]", hours, min, UInt_t(realt));
  else if(min >0 )
    sprintf(srealt, "%02d:%02d [mm:ss]", min, UInt_t(realt));
  else
    sprintf(srealt, "%02.2f [sec]", realt);    
  // write to string
  String_t rts(srealt);

  // clean & return
  delete [] srealt;
  return rts;
}

} // end namespace Pomona

//  end Stopwatch.cxx
//  ############################################################################
