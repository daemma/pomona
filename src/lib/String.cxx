//  ############################################################################
//! @file       String.cxx
//! @brief      Source for enhanced character string class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "String.hh"    // pomona: this class
#include "ClassImp.hh"  // pomona: class implementation
#include <ostream>      // std: out streamer
#include <ctype.h>      // std: character handling functions
#include <iomanip>      // std: setiosflags

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::String)
// //! Cereal: Register derived polymorphic type -- NamedString 
// CEREAL_REGISTER_TYPE(Pomona::String)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: default
String::String()
  : Object()
  , String_t()
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: std::string
String::String(const String_t& s)
  : Object()
  , String_t(s)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Substring constructor.
/** Copies the portion of str that begins at the character position pos and spans 
    len characters (or until the end of str, if either str is too short or 
    if len is string::npos).
*/
String::String(const String_t& s, const size_t& pos, const size_t& len)
  : Object()
  , String_t(s, pos, len)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: const Char_t*
String::String(const Char_t* s)
  : Object()
  , String_t(s)
{
  // cout << "String::String: Char_t: " << s << endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! From buffer constructor.
/** Copies the first n characters from the array of characters pointed by s. */
String::String(const Char_t* s, size_t n)
  : Object()
  , String_t(s, n)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Fill constructor
/** Fills the string with n consecutive copies of character c. */
String::String(const size_t& n, const Char_t& c)
  : Object(),
    String_t(n, c)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Single character constructor
/** Fills the string with n consecutive copies of character c. */
String::String(const Char_t& c)
  : Object()
  , String_t(1, c)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: copy
String::String(const String& other)
  : Object(other)
  // , String_t()
  , String_t(other.get())
  // , String_t(static_cast<const String_t&>(other))
  // , String_t(dynamic_cast<const String_t&>(other))
{
  // cout << "String::String: other: " << other << endl;
  // try{
  //   String_t::operator=( );
    
  // }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Substring constructor.
/** Copies the portion of str that begins at the character position pos and spans 
    len characters (or until the end of str, if either str is too short or 
    if len is string::npos).
*/
String::String(const String& o, const size_t& pos, const size_t& len)
  : Object()
  , String_t(o, pos, len)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Boolean constructor
String::String(const Bool_t& value)
  : Object(),
    String_t()
{
  fromBool(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Short integer constructor
String::String(const Short_t& value)
  : Object()
  , String_t()
{
  fromShort(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Unsigned short integer constructor
String::String(const UShort_t& value)
  : Object(),
    String_t()
{
  fromUShort(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Integer constructor
String::String(const Int_t& value)
  : Object()
  , String_t()
{
  fromInt(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Unsigned integer constructor
String::String(const UInt_t& value)
  : Object()
  , String_t()
{
  fromUInt(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Long integer constructor
String::String(const Int64_t& value)
  : Object()
  , String_t()
{
  fromLong64(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Unsigned long integer constructor
String::String(const UInt64_t& value)
  : Object()
  , String_t()
{
  fromULong64(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Float constructor
String::String(const Float_t& value)
  : Object()
  , String_t()
{
  fromFloat(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Double constructor
String::String(const Double_t& value)
  : Object()
  , String_t()
{
  fromDouble(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Initializer list constructor
/** Copies each of the characters in il, in the same order. */
String::String(std::initializer_list<Char_t> il)
  : Object(),
    String_t(il)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Constructor: move
String::String(String&& other)
  : Object(other)
  , String_t(other.get())
{
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator.
String& String::operator=(const String& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    String_t::operator=(rhs);
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from std::string
String& String::operator=(const String_t& rhs)
{
  String_t::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from c-string
String& String::operator=(const Char_t* rhs)
{
  String_t::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from character
String& String::operator=(const Char_t& rhs)
{
  String_t::operator=(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator.
String& String::operator=(String&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    String_t::operator=(rhs);
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Move assignment operator from std::string
String& String::operator=(String_t&& rhs)
{
  String_t::operator=(rhs);
  rhs.clear();
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from boolean
String& String::operator=(const Bool_t& rhs)
{
  fromBool(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from short
String& String::operator=(const Short_t& rhs)
{
  fromShort(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from unsigned short
String& String::operator=(const UShort_t& rhs)
{
  fromUShort(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from int
String& String::operator=(const Int_t& rhs)
{
  fromInt(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from unsigned int
String& String::operator=(const UInt_t& rhs)
{
  fromUInt(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from long
String& String::operator=(const Int64_t& rhs)
{
  fromLong64(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from unsigned long
String& String::operator=(const UInt64_t& rhs)
{
  fromULong64(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from float
String& String::operator=(const Float_t& rhs)
{
  fromFloat(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy assignment operator from 
String& String::operator=(const Double_t& rhs)
{
  fromDouble(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void String::clear()
{
  Object::clear();
  String_t::clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t String::isEmpty() const 
{
  return ( Object::isEmpty() &&
	   String_t::size() == 0 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t String::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{ // attempt to cast to TimePoint
      const String& o = dynamic_cast<const String&>(other); 
      // check members
      if(get() != o.get())       return kFALSE;
      return kTRUE;
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t String::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** Uses std::string::compare. */
Int_t String::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{  // attempt cast
      const String& o = dynamic_cast<const String&>(other); 
      return String_t::compare(o.get());
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t String::defaultPrintContent() const
{
  return kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object arguments.
void String::printArgs(std::ostream& os) const
{
  os << "String";
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print value.
void String::printValue(std::ostream& os) const
{
  os << get();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the std::string (const)
const String_t& String::get() const
{
  return static_cast<const String_t&>(*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the std::string
String_t& String::get()
{
  return static_cast<String_t&>(*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the std::string
void String::set(const String_t& s)
{
  String_t::operator=(s);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the std::string as a character array.
const Char_t* String::getAsChars()
{
  return get().c_str();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the std::string from a character array.
void String::setFromChars(const Char_t* val)
{
  set(val);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this string contain str?
Bool_t String::contains(const String& s) const
{
  return (find(s) != String_t::npos);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this string begin with str?
Bool_t String::beginsWith(const String& s) const
{
  return (find(s) == 0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Does this string end with str?
Bool_t String::endsWith(const String& s) const
{
  return (  contains(s)
	 && length()-rfind(s) == s.length());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Replace all occurances of "all" with "with"
void String::replaceAll(const String& all, const String& with)
{
  const size_t& lenAll = all.length();
  String strCopy(*this);                 // make a copy 
  size_t pos = strCopy.find(all);        // find the first instance
  while(pos != String_t::npos){
    strCopy.replace(pos, lenAll, with);  // replace at this position
    pos = strCopy.find(all);             // find next position
  }
  // copy modified string to this
  set(strCopy);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Replace all occurances of "all" with "with"
void String::stripNamespace()
{
  String strCopy(*this);                    // make a copy 
  size_t pos = strCopy.find_last_of("::");  // find the last "::"
  strCopy.replace(0, pos+1, "");            // replace with empty
  set(strCopy);                             // copy modified string to this
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx pass the character category (functional)?
/*! Default index (idx = std::string::npos) checks entire string.
    See <a href="cplusplus.com/reference/cctype/">ctype.h</a>.
*/
Bool_t String::isCateg(std::function<Int_t(Int_t)> fcn, size_t idx) const
{
  if(idx != String_t::npos) return (fcn((*this)[idx]) != 0);  
  else{
    for(auto& c: (*this)) if(fcn(c) == 0) return kFALSE;
    return kTRUE;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx printable?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isPrint(size_t idx) const
{
  return isCateg(isprint, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx graphical?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isGraph(size_t idx) const
{
  return isCateg(isgraph, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx alpha-numeric?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isAlNum(size_t idx) const
{
  return isCateg(isalnum, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx alphabetic?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isAlpha(size_t idx) const
{
  return isCateg(isalpha, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx lower-case?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isLower(size_t idx) const
{
  return isCateg(islower, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx upper-case?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isUpper(size_t idx) const
{
  return isCateg(isupper, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx digit?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isDigit(size_t idx) const
{
  return isCateg(isdigit, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx hexadecimal?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isHexad(size_t idx) const
{
  return isCateg(isxdigit, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx punctuation?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isPunct(size_t idx) const
{
  return isCateg(ispunct, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx a space?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isSpace(size_t idx) const
{
  return isCateg(isspace, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx control?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isCntrl(size_t idx) const
{
  return isCateg(iscntrl, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is the character at position idx blank?
/*! Default index (idx = std::string::npos) checks entire string */
Bool_t String::isBlank(size_t idx) const
{
  return isCateg(isblank, idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Convert to all lower-case (where possible).
void String::toLower()
{
  for(size_t idx=0; idx<length(); idx++)
    (*this)[idx] = tolower((*this)[idx]);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Convert to all upper-case (where possible).
void String::toUpper()
{
  for(size_t idx=0; idx<length(); idx++)
    (*this)[idx] = toupper((*this)[idx]);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Remove all non-graphical characters.
void String::removeNonGraphical()
{
  String s;
  for(auto& c: (*this)) if(isgraph(c)) s += c;
  set(s);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Remove leading and trailing non-graphical characters.
void String::trimNonGraphical()
{
  // find first graphical
  size_t begIdx(0);
  for(; begIdx<length(); begIdx++) if(isgraph( (*this)[begIdx] )) break;
  // find last graphical
  size_t endIdx(length());
  for(; endIdx>begIdx;   endIdx--) if(isgraph( (*this)[endIdx] )) break;
  // set to the trimmed sub-string
  set(substr(begIdx, endIdx-begIdx+1));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Remove all control characters.
void String::removeCntrl()
{
  String s;
  for(auto& c: (*this)) if(!iscntrl(c)) s += c;
  set(s);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition assignment
/*! @note Operates on string member only */
String& String::operator+=(const String& rhs)
{
  String_t::operator+=(rhs.get());
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition assignment
/*! @note Operates on string member only */
String& String::operator+=(const Char_t* rhs)
{
  String_t::operator+=(rhs);
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as a boolean.
Bool_t String::asBool() const
{
  return asArith<Bool_t>();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as a short integer.
Short_t String::asShort(size_t* idx, Int_t base) const
{
  return asArith<Short_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as an unsigned short integer.
UShort_t String::asUShort(size_t* idx, Int_t base) const
{
  return asArith<UShort_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as an integer.
Int_t String::asInt(size_t* idx, Int_t base) const
{
  return asArith<Int_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as an unsigned integer.
UInt_t String::asUInt(size_t* idx, Int_t base) const
{
  return asArith<UInt_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as a long integer.
Int64_t String::asLong64(size_t* idx, Int_t base) const
{
  return asArith<Int64_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as an unsigned long integer.
UInt64_t String::asULong64(size_t* idx, Int_t base) const
{
  return asArith<UInt64_t>(idx, base);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as a float.
Float_t String::asFloat(size_t* idx) const
{
  return asArith<Float_t>(idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String as a double.
Double_t String::asDouble(size_t* idx) const
{
  return asArith<Double_t>(idx);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from a boolean.
void String::fromBool(const Bool_t& value)
{
  return fromArith<Bool_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from a short integer.
void String::fromShort(const Short_t& value)
{
  return fromArith<Short_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from an unsigned short integer.
void String::fromUShort(const UShort_t& value)
{
  return fromArith<UShort_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from an integer.
void String::fromInt(const Int_t& value)
{
  return fromArith<Int_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from an unsigned integer.
void String::fromUInt(const UInt_t& value)
{
  return fromArith<UInt_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from a long integer.
void String::fromLong64(const Int64_t& value)
{
  return fromArith<Int64_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from an unsigned long integer.
void String::fromULong64(const UInt64_t& value)
{
  return fromArith<UInt64_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from a float.
void String::fromFloat(const Float_t& value)
{
  return fromArith<Float_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the string from a double.
void String::fromDouble(const Double_t& value)
{
  return fromArith<Double_t>(value);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Character array and String concatentation.
String operator+(const Char_t* lhs, const String& rhs)
{
  String ret(lhs);
  ret += rhs;
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String and Character array concatentation.
String operator+(const String& lhs, const Char_t* rhs)
{
  String ret(lhs);
  ret += rhs;
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Character and String concatentation.
String operator+(const Char_t& lhs, const String& rhs)
{
  String ret(lhs);
  ret += rhs;
  return ret;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! String and Character concatentation.
String operator+(const String& lhs, const Char_t& rhs)
{
  String ret(lhs);
  ret += rhs;
  return ret;
}

} // end namespace Pomona

//  end String.cxx
//  ############################################################################
