//  ############################################################################
//! @file       lib/TimePoint.cxx
//! @brief      Source for TimePoint class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "TimePoint.hh"  // pomona: this class
#include "ClassImp.hh"   // pomona: class implementation
#include <ostream>       // std: out streamer
#include <typeinfo>      // std: bad_cast
#ifdef CERNROOT          // Using root
#include "TBuffer.h"     // root: I/O buffer
#endif                   // end using root

using namespace std::chrono;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
CLASS_IMP(Pomona::TimePoint)

namespace Pomona {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Default constructor. */
TimePoint::TimePoint()
  : Object()
  , mMS()
{
  setNow();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::milliseconds constructor. */
TimePoint::TimePoint(const milliseconds& ms)
  : Object()
  , mMS()
{
  set(ms);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::seconds constructor. */
TimePoint::TimePoint(const seconds& s)
  : Object()
  , mMS()
{
  set(s);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy constructor. */
TimePoint::TimePoint(const TimePoint& other)
  : Object(other)
  , mMS(other.mMS)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move constructor. */
TimePoint::TimePoint(TimePoint&& other)
  : Object(std::move(other))
  , mMS(std::move(other.mMS))
{ 
  other.clear();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Copy assignment operator. */
TimePoint& TimePoint::operator=(const TimePoint& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    set(rhs.count());
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** Move assignment operator. */
TimePoint& TimePoint::operator=(TimePoint&& rhs)
{
  if(!isSame(rhs)){
    Object::operator=(rhs);
    set(rhs.get());
    rhs.clear();
  }
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::milliseconds copy assignment operator. */
TimePoint& TimePoint::operator=(const milliseconds& rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::milliseconds move assignment operator. */
TimePoint& TimePoint::operator=(milliseconds&& rhs)
{
  set(rhs);
  rhs = milliseconds::zero();
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::seconds copy assignment operator. */
TimePoint& TimePoint::operator=(const seconds& rhs)
{
  set(rhs);
  return *this;
}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::seconds move assignment operator. */
TimePoint& TimePoint::operator=(seconds&& rhs)
{
  set(rhs);
  rhs = seconds::zero();
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::milliseconds copy assignment operator. */
TimePoint& TimePoint::operator=(const UInt64_t& rhs)
{
  set(rhs);
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** std::chrono::milliseconds move assignment operator. */
TimePoint& TimePoint::operator=(UInt64_t&& rhs)
{
  set(rhs);
  rhs = 0;
  return *this;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Clear: set values to defaults
void TimePoint::clear()
{
  Object::clear();
  mMS = milliseconds::zero();
  // milliseconds::operator=(milliseconds::zero());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Are all the values equal to default?
Bool_t TimePoint::isEmpty() const
{
  // return (count() == milliseconds::zero().count());
  return (count() == TimePoint::Zero().count());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this equal to the other?
Bool_t TimePoint::isEqual(const Comparable& other) const 
{ 
  if(isSame(other)) return kTRUE;
  else{
    try{
      // const milliseconds& tp = dynamic_cast<const milliseconds&>(other); 
      const TimePoint& tp = dynamic_cast<const TimePoint&>(other); 
      return (count() == tp.count());
    }
    catch(const std::bad_cast& bc){ return kFALSE; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is this object sortable?
Bool_t TimePoint::isSortable() const 
{ 
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Compare this object to other. 
/** Derived implementations should return:
    - <0 (-1) if this is smaller than other
    - >0 (+1) if this is larger  than other
    -  0 else 
*/
Int_t TimePoint::compare(const Comparable& other) const 
{ 
  if(isSame(other)) return 0;
  else{
    try{ // attempt to cast to TimePoint
      // const milliseconds& tp = dynamic_cast<const milliseconds&>(other); 
      const TimePoint& tp = dynamic_cast<const TimePoint&>(other); 
      // compare members
      if     (count() < tp.count()) return -1;
      else if(count() > tp.count()) return  1;
      else                          return  0;
    }
    catch(const std::bad_cast& bc){ return 0; }
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get the default print content
UShort_t TimePoint::defaultPrintContent() const
{
  return kValue;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object value.
void TimePoint::printValue(std::ostream& os) const
{
  os << count();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object value to csv line.
void TimePoint::printCsv(std::ostream& os, const Bool_t& el) const
{
  printValue(os);
  if(el) os << std::endl;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Print object name to csv line.
void TimePoint::printCsvHead(std::ostream& os, const Bool_t& el) const
{
  os << "\""; printName(os); os << "\""; 
  if(el) os << std::endl;
}

#ifdef CERNROOT  // Using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! root file i/o streamer
void TimePoint::Streamer(TBuffer& buff)
{
  if(buff.IsReading()){
    buff.ReadClassBuffer(Pomona::TimePoint::Class(),this);
    ULong64_t val(0);
    buff.ReadULong64(val);
    set(val);
  } else {
    buff.WriteClassBuffer(Pomona::TimePoint::Class(),this);
    buff.WriteULong64(count());
  }
}

#endif           // end using root

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get value
milliseconds& TimePoint::get()
{
  return mMS;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get value
const milliseconds& TimePoint::get() const
{
  return mMS;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get value (const)
UInt64_t TimePoint::count() const
{
  return get().count();
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set value.
void TimePoint::set(const milliseconds& ms)
{
  mMS = ms;
  // milliseconds::operator=(ms);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set value.
void TimePoint::set(const seconds& s)
{
  mMS = duration_cast<milliseconds>(s);
  // milliseconds::operator=(duration_cast<milliseconds>(s));
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set value.
void TimePoint::set(const UInt64_t& ms)
{
  mMS = milliseconds(ms);
  // milliseconds msh(ms);
  // milliseconds::operator=(msh);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set to "now" according to system_clock.
void TimePoint::setNow()
{
  mMS = SystemNow();
  // milliseconds::operator=(SystemNow());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Increment by one second
void TimePoint::incOneSec()
{
  (*this) += seconds(1);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition
TimePoint TimePoint::operator+(const TimePoint& rhs) const
{
  TimePoint tp(*this);
  tp += rhs;
  return tp;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition
TimePoint TimePoint::operator-(const TimePoint& rhs) const
{
  TimePoint tp(*this);
  tp -= rhs;
  return tp;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: Prefix increment
TimePoint& TimePoint::operator++()
{
  ++get();
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: Postfix increment
TimePoint TimePoint::operator++(int)
{
  get()++;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: Prefix decrement
TimePoint& TimePoint::operator--()
{
  --get();
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: Postfix decrement
TimePoint TimePoint::operator--(int)
{
  get()--;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition
TimePoint& TimePoint::operator+=(const TimePoint& rhs)
{
  get() += rhs.get();
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: subtraction
TimePoint& TimePoint::operator-=(const TimePoint& rhs)
{
  get() -= rhs.get();
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition milliseconds
TimePoint& TimePoint::operator+=(const milliseconds& rhs)
{
  get() += rhs;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: subtraction milliseconds
TimePoint& TimePoint::operator-=(const milliseconds& rhs)
{
  get() -= rhs;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: addition seconds
TimePoint& TimePoint::operator+=(const seconds& rhs)
{
  get() += rhs;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: subtraction seconds
TimePoint& TimePoint::operator-=(const seconds& rhs)
{
  get() -= rhs;
  return (*this);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get "now" according to system_clock.
TimePoint TimePoint::Now()
{
  return TimePoint(SystemNow());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get "now" according to system_clock.
milliseconds TimePoint::SystemNow()
{
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get "now" according to steady_clock.
milliseconds TimePoint::SteadyNow()
{
  return duration_cast<milliseconds>(steady_clock::now().time_since_epoch());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get "now" according to high_resolution_clock.
milliseconds TimePoint::HighResNow()
{
  return duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get zero
const TimePoint& TimePoint::Zero()
{
  static const TimePoint _TimePointZero(milliseconds::zero());
  return _TimePointZero;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Get one second
const TimePoint& TimePoint::OneSec()
{
  static const TimePoint _TimePointOneSec(seconds(1));
  return _TimePointOneSec;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: equivalence with milliseconds
Bool_t operator==(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() == rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: anti-equivalence with milliseconds
Bool_t operator!=(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() != rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: less than milliseconds
Bool_t operator<(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() < rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: less than, or equal, milliseconds
Bool_t operator<=(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() <= rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: greater than milliseconds
Bool_t operator>(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() > rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: greater than, or equal, milliseconds
Bool_t operator>=(const TimePoint& lhs, const milliseconds& rhs)
{
  return (lhs.get() >= rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: equivalence with UInt64_t
Bool_t operator==(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() == rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: anti-equivalence with UInt64_t
Bool_t operator!=(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() != rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: less than UInt64_t
Bool_t operator<(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() < rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: less than, or equal, UInt64_t
Bool_t operator<=(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() <= rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: greater than UInt64_t
Bool_t operator>(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() > rhs);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Operator: greater than, or equal, UInt64_t
Bool_t operator>=(const TimePoint& lhs, const UInt64_t& rhs)
{
  return (lhs.count() >= rhs);
}

} // end namespace Pomona

//  end TimePoint.cxx
//  ############################################################################
