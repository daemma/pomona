//  ############################################################################
//! @file       capi.cxx
//! @brief      Source for top-level C-lanuage API
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-23
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "pomona.h"              // pomona: this API
#include "Project.hh"       // pomona: library information interface
using namespace Pomona;

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Name of this library/package */
const char* pomona_name()
{
  return Project::Name().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Moniker (alternative name) of this library/package */
const char* pomona_moniker()
{
  return Project::Moniker().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Title (description) of this library/package */
const char* pomona_title()
{
  return Project::Title().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library author */
const char* pomona_author()
{
  return Project::Author().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library year */
const char* pomona_year()
{
  return Project::Year().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library copyright */
const char* pomona_copyright()
{
  return Project::Copyright().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library-name of this library/package */
const char* pomona_libname()
{
  return Project::LibName().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library version number */
const char* pomona_version()
{
  return Project::LibVersion().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library major version number */
short pomona_version_major()
{
  return short(Project::LibVersionMajor());
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library minor version number */
short pomona_version_minor()
{
  return short(Project::LibVersionMinor());
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library patch version number */
short pomona_version_patch()
{
  return short(Project::LibVersionPatch());
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library install prefix */
const char* pomona_install_prefix()
{
  return Project::InstallPrefix().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library install c-flags */
const char* pomona_install_cflags()
{
  return Project::InstallCFlags().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library install ld-flags */
const char* pomona_install_ldflags()
{
  return Project::InstallLdFlags().c_str();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library has been compiled with CERN-ROOT support */
bool pomona_has_cernroot()
{
  return Project::HasCernRoot();
}

/*  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
/*! Library has been compiled with YAML file support */
bool pomona_has_yaml()
{
  return Project::HasYaml();
}

//  end capi.cxx
//  ############################################################################
