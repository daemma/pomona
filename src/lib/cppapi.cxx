//  ############################################################################
//! @file       cppapi.cxx
//! @brief      Source for top-level C++ API
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-24
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Pomona.hh"             // pomona: this API

namespace Pomona { }  // end namespace Pomona


//  end cppapi.cxx
//  ############################################################################
