//  ############################################################################
//! @file       test/Date.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-01
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Date.hh"        // pomona: this class
#include "TimePoint.hh"   // pomona: time-stamp class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "Date";
static const Tm_t    gTmZero    = {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0};
static const Time_t  gTimeZero  = 0;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, DefaultCtor) 
{
  Date obj;
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(DateTest, TmCtor)
{
  Date obj(gTmZero);
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    0);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1900);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, TimeCtor)
{
  Date obj(gTimeZero);
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    1);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1970);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, TimePointCtor)
{
  TimePoint tp;
  Date obj(tp);
  EXPECT_GE(obj.mMSec,   0);
  EXPECT_GE(obj.mSec,    0);
  EXPECT_GE(obj.mMin,    0);
  EXPECT_GE(obj.mHour,   0);
  EXPECT_GT(obj.mDay,    0);
  EXPECT_GT(obj.mMonth,  0);
  EXPECT_GE(obj.mYear,   2019);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, CopyCtor) 
{
  Date obj;
  Date objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(DateTest, MoveCtor)
{
  Date obj;
  Date objCopy(obj);
  Date objMove(std::move(obj));
  EXPECT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(DateTest, CopyAssign)
{
  Date obj;
  Date objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(DateTest, MoveAssign)
{
  Date obj;
  Date objCopy(obj);
  Date objMove = std::move(obj);
  EXPECT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(DateTest, Clone) {
  Date obj;
  ASSERT_NO_THROW({
      dynamic_cast<Date*>(obj.clone());
    });
  Date* objCln = dynamic_cast<Date*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(DateTest, ClassName) {
  Date obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(DateTest, IsA) {
  Date obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(DateTest, IsSameClass) {
  Date obj;
  Date objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(DateTest, ClearEmpty) {
  Date obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, IsSame) 
{
  Date obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(DateTest, IsEqual) 
{
  Date obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(DateTest, IsSortable) 
{
  Date obj;
  ASSERT_TRUE(obj.isSortable());
}

TEST(DateTest, Compare)
{
  Date obj;
  EXPECT_EQ(obj.compare(obj), 0);  // tautology
  Date objCopy(obj);
  EXPECT_EQ(obj.compare(objCopy), 0); // tautology

  Date obj1;
  Date obj2(obj1); obj2.mDay++;
  Date obj3(obj2); obj3.mDay++;
  EXPECT_EQ(obj2.compare(obj1),  1);
  EXPECT_EQ(obj2.compare(obj3), -1);
}

TEST(DateTest, InClassComparisonOperators)
{  
  Date obj1(TimePoint::Zero());
  const Date obj2;
  EXPECT_FALSE( (obj1 == obj2) ); 
  EXPECT_TRUE ( (obj1 != obj2) ); 
  EXPECT_TRUE ( (obj1 <  obj2) ); 
  EXPECT_TRUE ( (obj1 <= obj2) ); 
  EXPECT_FALSE( (obj1 >  obj2) ); 
  EXPECT_FALSE( (obj1 >= obj2) ); 
}

TEST(DateTest, OutClassComparisonOperators) 
{
  const Date obj1(TimePoint::Zero());
  const Date obj2;
  EXPECT_FALSE( (obj1 == obj2) ); 
  EXPECT_TRUE ( (obj1 != obj2) ); 
  EXPECT_TRUE ( (obj1 <  obj2) ); 
  EXPECT_TRUE ( (obj1 <= obj2) ); 
  EXPECT_FALSE( (obj1 >  obj2) ); 
  EXPECT_FALSE( (obj1 >= obj2) ); 
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, DefaultPrintContent)
{
  Date obj;
  EXPECT_EQ(obj.defaultPrintContent(), Printable::eContents::kValue);
}

TEST(DateTest, PrintClassName)
{
  Date obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(DateTest, PrintName)
{
  Date obj;
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(DateTest, PrintValue)
{
  const Char_t* zval = "1970-01-01 00:00:00.000+0000";
  Date obj(TimePoint::Zero());
  std::stringstream ss;
  obj.printValue(ss);
  ASSERT_STREQ(ss.str().c_str(), zval);
  ss.str("");
  ss << obj;
  ASSERT_STREQ(ss.str().c_str(), zval);
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Setters
TEST(DateTest, SetTm)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.set(gTmZero);
    });
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    0);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1900);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, SetTime)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.set(gTimeZero);
    });
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    1);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1970);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, SetLocalTime)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.setLocal(gTimeZero);
    });
  EXPECT_GE(obj.mMSec,   0);
  EXPECT_GE(obj.mSec,    0);
  EXPECT_GE(obj.mMin,    0);
  EXPECT_GE(obj.mHour,   0);
  EXPECT_GT(obj.mDay,    0);
  EXPECT_GT(obj.mMonth,  0);
  EXPECT_GT(obj.mYear,   0);
}

TEST(DateTest, SetTimePoint)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.set(TimePoint::Zero());
    });
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    1);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1970);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

TEST(DateTest, SetLocalTimePoint)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.setLocal(TimePoint::Zero());
    });
  EXPECT_GE(obj.mMSec,   0);
  EXPECT_GE(obj.mSec,    0);
  EXPECT_GE(obj.mMin,    0);
  EXPECT_GE(obj.mHour,   0);
  EXPECT_GT(obj.mDay,    0);
  EXPECT_GT(obj.mMonth,  0);
  EXPECT_GT(obj.mYear,   0);
}

TEST(DateTest, SetNow)
{
  Date obj;
  ASSERT_NO_THROW({
      obj.setNow();
    });
  EXPECT_GE(obj.mMSec,   0);
  EXPECT_GE(obj.mSec,    0);
  EXPECT_GE(obj.mMin,    0);
  EXPECT_GE(obj.mHour,   0);
  EXPECT_GT(obj.mDay,    0);
  EXPECT_GT(obj.mMonth,  0);
  EXPECT_GE(obj.mYear,   2019);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, Zero)
{
  ASSERT_NO_THROW({
      Date::Zero();
    });
  const Date obj = Date::Zero();
  EXPECT_EQ(obj.mMSec,   0);
  EXPECT_EQ(obj.mSec,    0);
  EXPECT_EQ(obj.mMin,    0);
  EXPECT_EQ(obj.mHour,   0);
  EXPECT_EQ(obj.mDay,    1);
  EXPECT_EQ(obj.mMonth,  1);
  EXPECT_EQ(obj.mYear,   1970);
  EXPECT_EQ(obj.mTzMin,  0);
  EXPECT_EQ(obj.mTzHour, 0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, IsNamed)
{
  Date obj;
  ASSERT_FALSE(obj.isNamed());
}

TEST(DateTest, GetName)
{
  Date obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, Is_Object)
{
  ASSERT_TRUE( is_object<Date>::value );
}

TEST(DateTest, IsObject)
{
  Date obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(DateTest, JsonIO)
{
  Date obj;
  Io::Archive<Date> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/date.json") );

  Date objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/date.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(DateTest, XmlIO)
{
  Date obj;
  Io::Archive<Date> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/date.xml") );

  Date objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/date.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(DateTest, CnbIO)
{
  Date obj;
  Io::Archive<Date> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/date.cnb") );

  Date objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/date.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(DateTest, YamlIO)
{
  Date obj;
  Io::Archive<Date> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/date.yaml") );

  Date objFromFile(Date::Zero());
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/date.yaml") );
  ASSERT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(DateTest, RootIO)
{
  Date obj;
  Io::Archive<Date> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/date.root") );

  Date objFromFile(Date::Zero());
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/date.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(DateTest, RootTreeWrite)
{
  Date objt;
  TFile rf("test-data/dateTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tDate", "Example tree with date");
  tree->Branch("date", "Pomona::Date", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt.mSec++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(DateTest, RootTreeRead)
{
  TFile rf("test-data/dateTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tDate");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  Date* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("date", &objt));
  Date objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, Date::Zero());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev.mSec, objt->mSec-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

TEST(DateTest, RootClone) 
{
  Date obj;
  ASSERT_NO_THROW({
      dynamic_cast<Date*>(obj.Clone());
    });
  Date* objCln = dynamic_cast<Date*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(DateTest, RootClear) 
{
  Date obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

TEST(DateTest, RootIsEqual) 
{
  Date obj;
  Date tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

TEST(DateTest, RootIsSortable) 
{
  Date obj;
  ASSERT_TRUE(obj.IsSortable());
}

TEST(DateTest, RootCompare) 
{
  Date obj;
  Date tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

TEST(DateTest, RootGetName) 
{
  Date obj;
  EXPECT_STREQ(obj.GetName(), "Pomona::Date");
}

TEST(DateTest, RootGetTitle) 
{
  Date obj;
  EXPECT_STREQ(obj.GetTitle(), "");
}

#endif                  // end using root

} // end namespace

//  end Date.cxx
//  ############################################################################
