//  ############################################################################
//! @file       test/HostPath.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-02
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "HostPath.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "HostPath";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, DefaultCtor) 
{
  HostPath obj("thispath");
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(HostPathTest, CopyCtor) 
{
  HostPath obj;
  HostPath objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(HostPathTest, MoveCtor)
{
  HostPath obj;
  HostPath objCopy(obj);
  HostPath objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(HostPathTest, CopyAssign)
{
  HostPath obj;
  HostPath objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(HostPathTest, MoveAssign)
{
  HostPath obj;
  HostPath objCopy(obj);
  HostPath objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(HostPathTest, Clone) {
  HostPath obj;
  ASSERT_NO_THROW({
      dynamic_cast<HostPath*>(obj.clone());
    });
  HostPath* objCln = dynamic_cast<HostPath*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(HostPathTest, ClassName) {
  HostPath obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(HostPathTest, IsA) {
  HostPath obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(HostPathTest, IsSameClass) {
  HostPath obj;
  HostPath objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(HostPathTest, ClearEmpty) {
  HostPath obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, IsSame) 
{
  HostPath obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(HostPathTest, IsEqual) 
{
  HostPath obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(HostPathTest, IsSortable) 
{
  HostPath obj;
  ASSERT_TRUE(obj.isSortable());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, DefaultPrintContent)
{
  HostPath obj;
  ASSERT_EQ(obj.defaultPrintContent(), 
	    Printable::eContents::kValue);
}

TEST(HostPathTest, PrintClassName)
{
  HostPath obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(HostPathTest, PrintValue)
{
  HostPath obj("thisvalue");
  std::stringstream ss;
  obj.printValue(ss);
  ASSERT_STREQ(ss.str().c_str(), "thisvalue");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, AddAssignString)
{
  HostPath obj1("path");
  String obj2("more");
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"pathmore");
}

TEST(HostPathTest, AddAssignChar)
{
  HostPath obj1("path");
  const Char_t* obj2 = "more";
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"pathmore");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, IsNamed)
{
  HostPath obj;
  ASSERT_FALSE(obj.isNamed());
}

TEST(HostPathTest, GetName)
{
  HostPath obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, Is_Object)
{
  ASSERT_TRUE( is_object<HostPath>::value );
}

TEST(HostPathTest, IsObject)
{
  HostPath obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostPathTest, JsonIO)
{
  HostPath obj("thispath");
  Io::Archive<HostPath> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/hostpath.json") );

  HostPath objFromFile("");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/hostpath.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(HostPathTest, XmlIO)
{
  HostPath obj("thispath");
  Io::Archive<HostPath> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/hostpath.xml") );

  HostPath objFromFile("");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/hostpath.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(HostPathTest, CnbIO)
{
  HostPath obj("thispath");
  Io::Archive<HostPath> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/hostpath.cnb") );

  HostPath objFromFile("");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/hostpath.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(HostPathTest, YamlIO)
{
  HostPath obj("thispath");
  Io::Archive<HostPath> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/hostpath.yaml") );

  HostPath objFromFile("");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/hostpath.yaml") );
  EXPECT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(HostPathTest, RootIO)
{
  HostPath obj("thispath");
  Io::Archive<HostPath> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/hostpath.root") );

  HostPath objFromFile("");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/hostpath.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(HostPathTest, RootTreeWrite)
{
  HostPath objt("thispath");
  TFile rf("test-data/hostpath.tree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tHostPath", "Example tree with named");
  tree->Branch("hostpath", "Pomona::HostPath", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt[0]++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(HostPathTest, RootTreeRead)
{
  TFile rf("test-data/hostpath.tree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tHostPath");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  HostPath* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("hostpath", &objt));
  HostPath objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, HostPath());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev[0], (*objt)[0]-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

//! Clone
TEST(HostPathTest, RootClone) {
  HostPath obj;
  ASSERT_NO_THROW({
      dynamic_cast<HostPath*>(obj.Clone());
    });
  HostPath* objCln = dynamic_cast<HostPath*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(HostPathTest, RootClear) {
  HostPath obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(HostPathTest, RootIsEqual) {
  HostPath obj;
  HostPath tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(HostPathTest, RootIsSortable) {
  HostPath obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(HostPathTest, RootCompare)
{
  HostPath obj;
  HostPath tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(HostPathTest, RootGetName)
{
  HostPath obj("thisvalue");
  EXPECT_STREQ(obj.GetName(), "Pomona::HostPath");
}

//! GetTitle
TEST(HostPathTest, RootGetTitle)
{
  HostPath obj("thisvalue");
  EXPECT_STREQ(obj.GetTitle(), "");
}

#endif                  // end using root

} // end namespace

//  end HostPath.cxx
//  ############################################################################
