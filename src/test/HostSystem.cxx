//  ############################################################################
//! @file       test/HostSystem.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-02
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "HostSystem.hh"  // pomona: this class
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream

namespace {

using namespace Pomona;

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(HostSystemTest, Pid) 
{
  HostSystem hs;
  ASSERT_GT(hs.Pid(), 1);  // Process ID > 1
}

TEST(HostSystemTest, Exec) 
{
  HostSystem hs;
  // If command is a null pointer, the function returns a non-zero value in case a 
  // command processor is available and a zero value if it is not.
  ASSERT_NE(hs.Exec(nullptr), 0);
  // If command is not a null pointer, the value returned depends on the system and 
  // library implementations, but it is generally expected to be the status code returned 
  // by the called command, if supported.    
  const Char_t* shellCmd = "ls";
  ASSERT_EQ(hs.Exec(shellCmd), 0);
}

TEST(HostSystemTest, MkDir) 
{
  HostSystem hs;
  // Upon successful completion, mkdir() shall return 0. 
  // Otherwise, -1 shall be returned, no directory shall be created, 
  // and errno shall be set to indicate the error.
  const Char_t* dir = "HostSystemTest_dir";
  Int_t status = hs.MkDir(dir);
  ASSERT_EQ(status, 0);
  // clean up
  if(status == 0){
    ASSERT_EQ(hs.MkDir(dir), -1);
    ASSERT_EQ(hs.Exec("rmdir HostSystemTest_dir"), 0);
  }
}

TEST(HostSystemTest, BaseName) 
{
  HostSystem hs;
  // Base name of /user/root is root.
  // But the base name of '/' is '/', 'c:\' is 'c:\'
  const Char_t* dir = "/user/root";
  const Char_t* bn = hs.BaseName(dir);
  ASSERT_STREQ(bn, "root");
}

TEST(HostSystemTest, DirName) 
{
  HostSystem hs;
  // DirName of /user/root is /user
  const Char_t* dir = "/user/root";
  const Char_t* bn = hs.DirName(dir);
  ASSERT_STREQ(bn, "/user");
}

TEST(HostSystemTest, PrependPath) 
{
  HostSystem hs;
  const Char_t* path  = "/user/";
        String_t file = "root";
  const Char_t* pf = hs.PrependPath(path, file);
  ASSERT_STREQ(pf, "/user/root");
}


} // end namespace

//  end HostSystem.cxx
//  ############################################################################
