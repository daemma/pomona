//  ############################################################################
//! @file       test/Named.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-02
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Named.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "Named";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, DefaultCtor) 
{
  Named obj("Named", "This is named");
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(NamedTest, CopyCtor) 
{
  Named obj;
  Named objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(NamedTest, MoveCtor)
{
  Named obj;
  Named objCopy(obj);
  Named objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(NamedTest, CopyAssign)
{
  Named obj;
  Named objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(NamedTest, MoveAssign)
{
  Named obj;
  Named objCopy(obj);
  Named objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(NamedTest, Clone) {
  Named obj;
  ASSERT_NO_THROW({
      dynamic_cast<Named*>(obj.clone());
    });
  Named* objCln = dynamic_cast<Named*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(NamedTest, ClassName) {
  Named obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(NamedTest, IsA) {
  Named obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(NamedTest, IsSameClass) {
  Named obj;
  Named objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(NamedTest, ClearEmpty) {
  Named obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, IsSame) 
{
  Named obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(NamedTest, IsEqual) 
{
  Named obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(NamedTest, IsSortable) 
{
  Named obj;
  ASSERT_TRUE(obj.isSortable());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, DefaultPrintContent)
{
  Named obj;
  ASSERT_EQ(obj.defaultPrintContent(), 
	    Printable::eContents::kName | Printable::eContents::kTitle);
}

TEST(NamedTest, PrintClassName)
{
  Named obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(NamedTest, PrintName)
{
  Named obj("ThisName");
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), "ThisName");
}

TEST(NamedTest, PrintTitle)
{
  Named obj("ThisName", "This is a title");
  std::stringstream ss;
  obj.printTitle(ss);
  ASSERT_STREQ(ss.str().c_str(), "This is a title");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, IsNamed)
{
  Named obj;
  ASSERT_TRUE(obj.isNamed());
}

TEST(NamedTest, GetName)
{
  Named obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, Is_Object)
{
  ASSERT_TRUE( is_object<Named>::value );
}

TEST(NamedTest, IsObject)
{
  Named obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedTest, JsonIO)
{
  Named obj("ThisName", "This is a title");
  Io::Archive<Named> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/named.json") );

  Named objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/named.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedTest, XmlIO)
{
  Named obj("ThisName", "This is a title");
  Io::Archive<Named> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/named.xml") );

  Named objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/named.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedTest, CnbIO)
{
  Named obj("ThisName", "This is a title");
  Io::Archive<Named> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/named.cnb") );

  Named objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/named.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(NamedTest, YamlIO)
{
  Named obj("ThisName", "This is a title");
  Io::Archive<Named> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/named.yaml") );

  Named objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/named.yaml") );
  ASSERT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(NamedTest, RootIO)
{
  Named obj("ThisName", "This is a title");
  Io::Archive<Named> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/named.root") );

  Named objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/named.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedTest, RootTreeWrite)
{
  Named objt("anamedthisis", "A named this is");
  TFile rf("test-data/namedTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tNamed", "Example tree with named");
  tree->Branch("named", "Pomona::Named", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt.name()[0]++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(NamedTest, RootTreeRead)
{
  TFile rf("test-data/namedTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tNamed");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  Named* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("named", &objt));
  Named objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, Named());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev.name()[0], (*objt).name()[0]-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

//! Clone
TEST(NamedTest, RootClone) {
  Named obj;
  ASSERT_NO_THROW({
      dynamic_cast<Named*>(obj.Clone());
    });
  Named* objCln = dynamic_cast<Named*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(NamedTest, RootClear) {
  Named obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(NamedTest, RootIsEqual) {
  Named obj;
  Named tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(NamedTest, RootIsSortable) {
  Named obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(NamedTest, RootCompare) {
  Named obj;
  Named tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(NamedTest, RootGetName) {
  Named obj("ThisName", "This is a title");
  EXPECT_STREQ(obj.GetName(), "ThisName");
}

//! GetTitle
TEST(NamedTest, RootGetTitle) {
  Named obj("ThisName", "This is a title");
  EXPECT_STREQ(obj.GetTitle(), "This is a title");
}

#endif                  // end using root

} // end namespace

//  end Named.cxx
//  ############################################################################
