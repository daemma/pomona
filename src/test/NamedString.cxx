//  ############################################################################
//! @file       test/NamedString.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-02
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "NamedString.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "NamedString";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, DefaultCtor) 
{
  NamedString obj("NamedString", "This is named");
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(NamedStringTest, CopyCtor) 
{
  NamedString obj;
  NamedString objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(NamedStringTest, MoveCtor)
{
  NamedString obj;
  NamedString objCopy(obj);
  NamedString objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(NamedStringTest, CopyAssign)
{
  NamedString obj;
  NamedString objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(NamedStringTest, MoveAssign)
{
  NamedString obj;
  NamedString objCopy(obj);
  NamedString objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(NamedStringTest, Clone) {
  NamedString obj;
  ASSERT_NO_THROW({
      dynamic_cast<NamedString*>(obj.clone());
    });
  NamedString* objCln = dynamic_cast<NamedString*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(NamedStringTest, ClassName) {
  NamedString obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(NamedStringTest, IsA) {
  NamedString obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(NamedStringTest, IsSameClass) {
  NamedString obj;
  NamedString objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(NamedStringTest, ClearEmpty) {
  NamedString obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, IsSame) 
{
  NamedString obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(NamedStringTest, IsEqual) 
{
  NamedString obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(NamedStringTest, IsSortable) 
{
  NamedString obj;
  ASSERT_TRUE(obj.isSortable());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, DefaultPrintContent)
{
  NamedString obj;
  ASSERT_EQ(obj.defaultPrintContent(), 
	    Printable::eContents::kName | Printable::eContents::kValue);
}

TEST(NamedStringTest, PrintClassName)
{
  NamedString obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(NamedStringTest, PrintName)
{
  NamedString obj("ThisName");
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), "ThisName");
}

TEST(NamedStringTest, PrintTitle)
{
  NamedString obj("ThisName", "This is a title");
  std::stringstream ss;
  obj.printTitle(ss);
  ASSERT_STREQ(ss.str().c_str(), "This is a title");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, AddAssignString)
{
  NamedString obj1("NamedString", "This is a named string", "value");
  String obj2("more");
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"valuemore");
}

TEST(NamedStringTest, AddAssignChar)
{
  NamedString obj1("NamedString", "This is a named string", "value");
  const Char_t* obj2 = "more";
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"valuemore");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, IsNamed)
{
  NamedString obj;
  ASSERT_TRUE(obj.isNamed());
}

TEST(NamedStringTest, GetName)
{
  NamedString obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, Is_Object)
{
  ASSERT_TRUE( is_object<NamedString>::value );
}

TEST(NamedStringTest, IsObject)
{
  NamedString obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(NamedStringTest, JsonIO)
{
  NamedString obj("ThisName", "This is a title", "thisvalue");
  Io::Archive<NamedString> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/namedstring.json") );

  NamedString objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/namedstring.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedStringTest, XmlIO)
{
  NamedString obj("ThisName", "This is a title", "thisvalue");
  Io::Archive<NamedString> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/namedstring.xml") );

  NamedString objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/namedstring.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedStringTest, CnbIO)
{
  NamedString obj("ThisName", "This is a title", "thisvalue");
  Io::Archive<NamedString> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/namedstring.cnb") );

  NamedString objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/namedstring.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(NamedStringTest, YamlIO)
{
  NamedString obj("ThisName", "This is a title", "thisvalue");
  Io::Archive<NamedString> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/namedstring.yaml") );

  NamedString objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/namedstring.yaml") );
  EXPECT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(NamedStringTest, RootIO)
{
  NamedString obj("ThisName", "This is a title", "thisvalue");
  Io::Archive<NamedString> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/namedstring.root") );

  NamedString objFromFile("ThisName");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/namedstring.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(NamedStringTest, RootTreeWrite)
{
  NamedString objt("anamedthisis", "A named this is", "stringvalue");
  TFile rf("test-data/namedStringTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tNamedString", "Example tree with named");
  tree->Branch("namedstring", "Pomona::NamedString", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt()[0]++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(NamedStringTest, RootTreeRead)
{
  TFile rf("test-data/namedStringTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tNamedString");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  NamedString* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("namedstring", &objt));
  NamedString objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, NamedString());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev()[0], (*objt)()[0]-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

//! Clone
TEST(NamedStringTest, RootClone) {
  NamedString obj;
  ASSERT_NO_THROW({
      dynamic_cast<NamedString*>(obj.Clone());
    });
  NamedString* objCln = dynamic_cast<NamedString*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(NamedStringTest, RootClear) {
  NamedString obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(NamedStringTest, RootIsEqual) {
  NamedString obj;
  NamedString tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(NamedStringTest, RootIsSortable) {
  NamedString obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(NamedStringTest, RootCompare) {
  NamedString obj;
  NamedString tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(NamedStringTest, RootGetName) {
  NamedString obj("ThisName", "This is a title");
  EXPECT_STREQ(obj.GetName(), "ThisName");
}

//! GetTitle
TEST(NamedStringTest, RootGetTitle) {
  NamedString obj("ThisName", "This is a title");
  EXPECT_STREQ(obj.GetTitle(), "This is a title");
}

#endif                  // end using root

} // end namespace

//  end NamedString.cxx
//  ############################################################################
