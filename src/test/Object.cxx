//  ############################################################################
//! @file       test/Object.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Object.hh"      // pomona: this class
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream

namespace {

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! First light: extremely trivial
TEST(ObjectTest, FirstLight) {
  ASSERT_EQ(0, 0);  // trivial tautology :-)
}

using namespace Pomona;
static const Char_t* gClassName = "Object";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default contructor
TEST(ObjectTest, DefaultCtor) {
  Object obj;
  ASSERT_EQ(obj, obj);  // tautology
}

//! Copy contructor
TEST(ObjectTest, CopyCtor) {
  Object obj;
  Object objCpy(obj);
  ASSERT_NE(obj, objCpy);  // not equal since they are not the same address
}

//! Move contructor
TEST(ObjectTest, MoveCtor) {
  Object obj;
  Object objCpy(std::move(obj));
  ASSERT_NE(obj, objCpy);      // not equal since they are not the same address
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//! Copy assignment operator
TEST(ObjectTest, CopyAssign) {
  Object obj;
  Object objCpy = obj;
  ASSERT_NE(obj, objCpy);  // not equal since they are not the same address
}

//! Move assignment operator
TEST(ObjectTest, MoveAssign) {
  Object obj;
  Object objCpy = std::move(obj);
  ASSERT_NE(obj, objCpy);  // not equal since they are not the same address
  EXPECT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject

//! clone
TEST(ObjectTest, Clone) {
  Object obj;
  ASSERT_NO_THROW({
      dynamic_cast<Object*>(obj.clone());
    });
  Object* objCln = dynamic_cast<Object*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! className
TEST(ObjectTest, ClassName) {
  Object obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

//! isA
TEST(ObjectTest, IsA) {
  Object obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

//! isSameClass
TEST(ObjectTest, IsSameClass) {
  Object obj;
  Object objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

//! clear
TEST(ObjectTest, Clear) {
  Object obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! isEmpty
TEST(ObjectTest, Empty) {
  Object obj;
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Comparable

//! isSame
TEST(ObjectTest, IsSame) {
  Object obj;
  ASSERT_TRUE(obj.isSame(obj));
}

//! isEqual
TEST(ObjectTest, IsEqual) {
  Object obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

//! isSortable
TEST(ObjectTest, IsSortable) {
  Object obj;
  ASSERT_FALSE(obj.isSortable());
}

//! compare
TEST(ObjectTest, Compare) {
  Object obj;
  ASSERT_EQ(obj.compare(obj), 0);  // tautology
}

//! InClassOperators
TEST(ObjectTest, InClassComparisonOperators) {
  Object obj1;
  const Object obj2;
  ASSERT_FALSE( (obj1 == obj2) ); 
  ASSERT_TRUE ( (obj1 != obj2) ); 
  ASSERT_FALSE( (obj1 <  obj2) ); 
  ASSERT_TRUE ( (obj1 <= obj2) ); 
  ASSERT_FALSE( (obj1 >  obj2) ); 
  ASSERT_TRUE ( (obj1 >= obj2) ); 
}

//! OutClassOperators
TEST(ObjectTest, OutClassComparisonOperators) {
  const Object obj1;
  const Object obj2;
  ASSERT_FALSE( (obj1 == obj2) ); 
  ASSERT_TRUE ( (obj1 != obj2) ); 
  ASSERT_FALSE( (obj1 <  obj2) ); 
  ASSERT_TRUE ( (obj1 <= obj2) ); 
  ASSERT_FALSE( (obj1 >  obj2) ); 
  ASSERT_TRUE ( (obj1 >= obj2) ); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Printable

//! defaultPrintContent
TEST(ObjectTest, DefaultPrintContent) {
  Object obj;
  ASSERT_EQ(obj.defaultPrintContent(), Printable::eContents::kClassName);
}

//! printClassName
TEST(ObjectTest, PrintClassName) {
  Object obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

//! printName
TEST(ObjectTest, PrintName) {
  Object obj;
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! isNamed
TEST(ObjectTest, IsNamed) {
  Object obj;
  ASSERT_FALSE(obj.isNamed());
}

//! getName
TEST(ObjectTest, GetName) {
  Object obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! is_object
TEST(ObjectTest, Is_Object) {
  ASSERT_TRUE( is_object<Object>::value );
}

//! IsObject
TEST(ObjectTest, IsObject) {
  Object obj;
  ASSERT_TRUE( IsObject(obj) );
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef CERNROOT         // Using root

//! Clone
TEST(ObjectTest, RootClone) {
  Object obj;
  ASSERT_NO_THROW({
      dynamic_cast<TObject*>(obj.Clone());
    });
  TObject* objCln = dynamic_cast<TObject*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(ObjectTest, RootClear) {
  Object obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(ObjectTest, RootIsEqual) {
  Object obj;
  TObject tobj;
  ASSERT_FALSE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(ObjectTest, RootIsSortable) {
  Object obj;
  ASSERT_FALSE(obj.IsSortable());
}

//! Compare
TEST(ObjectTest, RootCompare) {
  Object obj;
  TObject tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(ObjectTest, RootGetName) {
  Object obj;
  EXPECT_STREQ(obj.GetName(), "Pomona::Object");
}

//! GetTitle
TEST(ObjectTest, RootGetTitle) {
  Object obj;
  EXPECT_STREQ(obj.GetTitle(), "");
}

#endif                  // end using root

} // end namespace

//  end Object.cxx
//  ############################################################################
