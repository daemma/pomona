//  ############################################################################
//! @file       test/Option.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-02
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Option.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "Option";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, DefaultCtor) 
{
  Option obj("Option", "This is named");
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(OptionTest, CopyCtor) 
{
  Option obj;
  Option objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(OptionTest, MoveCtor)
{
  Option obj;
  Option objCopy(obj);
  Option objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(OptionTest, CopyAssign)
{
  Option obj;
  Option objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(OptionTest, MoveAssign)
{
  Option obj;
  Option objCopy(obj);
  Option objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(OptionTest, Clone) {
  Option obj;
  ASSERT_NO_THROW({
      dynamic_cast<Option*>(obj.clone());
    });
  Option* objCln = dynamic_cast<Option*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(OptionTest, ClassName) {
  Option obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(OptionTest, IsA) {
  Option obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(OptionTest, IsSameClass) {
  Option obj;
  Option objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(OptionTest, ClearEmpty) {
  Option obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, IsSame) 
{
  Option obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(OptionTest, IsEqual) 
{
  Option obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(OptionTest, IsSortable) 
{
  Option obj;
  ASSERT_TRUE(obj.isSortable());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, DefaultPrintContent)
{
  Option obj;
  ASSERT_EQ(obj.defaultPrintContent(), 
	    Printable::eContents::kName | Printable::eContents::kValue);
}

TEST(OptionTest, PrintClassName)
{
  Option obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(OptionTest, PrintName)
{
  Option obj("ThisOption");
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), "ThisOption");
}

TEST(OptionTest, PrintTitle)
{
  Option obj("ThisOption", "This is a title");
  std::stringstream ss;
  obj.printTitle(ss);
  ASSERT_STREQ(ss.str().c_str(), "This is a title");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, AddAssignString)
{
  Option obj1("Option", "This is a named string", "value");
  String obj2("more");
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"valuemore");
}

TEST(OptionTest, AddAssignChar)
{
  Option obj1("Option", "This is a named string", "value");
  const Char_t* obj2 = "more";
  obj1 += obj2;
  ASSERT_EQ(obj1.get(),"valuemore");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, IsNamed)
{
  Option obj;
  ASSERT_TRUE(obj.isNamed());
}

TEST(OptionTest, GetName)
{
  Option obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, Is_Object)
{
  ASSERT_TRUE( is_object<Option>::value );
}

TEST(OptionTest, IsObject)
{
  Option obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(OptionTest, JsonIO)
{
  Option obj("ThisOption", "This is a title", "thisvalue", 'T');
  Io::Archive<Option> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/option.json") );

  Option objFromFile("ThisOption");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/option.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(OptionTest, XmlIO)
{
  Option obj("ThisOption", "This is a title", "thisvalue", 'T');
  Io::Archive<Option> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/option.xml") );

  Option objFromFile("ThisOption");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/option.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(OptionTest, CnbIO)
{
  Option obj("ThisOption", "This is a title", "thisvalue", 'T');
  Io::Archive<Option> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/option.cnb") );

  Option objFromFile("ThisOption");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/option.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(OptionTest, YamlIO)
{
  Option obj("ThisOption", "This is a title", "thisvalue", 'T');
  Io::Archive<Option> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/option.yaml") );

  Option objFromFile("ThisOption");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/option.yaml") );
  EXPECT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(OptionTest, RootIO)
{
  Option obj("ThisOption", "This is a title", "thisvalue", 'T');
  Io::Archive<Option> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/option.root") );

  Option objFromFile("ThisOption");
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/option.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(OptionTest, RootTreeWrite)
{
  Option objt("anamedthisis", "A named this is", "stringvalue", 'T');
  TFile rf("test-data/namedStringTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tOption", "Example tree with named");
  tree->Branch("namedstring", "Pomona::Option", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt()[0]++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(OptionTest, RootTreeRead)
{
  TFile rf("test-data/namedStringTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tOption");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  Option* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("namedstring", &objt));
  Option objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, Option());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev()[0], (*objt)()[0]-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

//! Clone
TEST(OptionTest, RootClone) {
  Option obj;
  ASSERT_NO_THROW({
      dynamic_cast<Option*>(obj.Clone());
    });
  Option* objCln = dynamic_cast<Option*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(OptionTest, RootClear) {
  Option obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(OptionTest, RootIsEqual) {
  Option obj;
  Option tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(OptionTest, RootIsSortable) {
  Option obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(OptionTest, RootCompare) {
  Option obj;
  Option tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(OptionTest, RootGetName) {
  Option obj("ThisOption", "This is a title");
  EXPECT_STREQ(obj.GetName(), "ThisOption");
}

//! GetTitle
TEST(OptionTest, RootGetTitle) {
  Option obj("ThisOption", "This is a title");
  EXPECT_STREQ(obj.GetTitle(), "This is a title");
}

#endif                  // end using root

} // end namespace

//  end Option.cxx
//  ############################################################################
