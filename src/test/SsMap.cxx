//  ############################################################################
//! @file       test/SsMap.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-01
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "SsMap.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
// static const Char_t* gClassName = "SsMap";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(SsMapTest, DefaultCtor) 
{
  SsMap obj;
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(SsMapTest, CopyCtor) 
{
  SsMap obj;
  SsMap objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(SsMapTest, MoveCtor)
{
  SsMap obj;
  SsMap objCopy(obj);
  SsMap objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(SsMapTest, CopyAssign)
{
  SsMap obj;
  SsMap objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(SsMapTest, MoveAssign)
{
  SsMap obj;
  SsMap objCopy(obj);
  SsMap objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(SsMapTest, JsonIO)
{
  SsMap obj; obj["thisstring"] = "This is a string";
  Io::Archive<SsMap> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/ssmap.json") );

  SsMap objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/ssmap.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(SsMapTest, XmlIO)
{
  SsMap obj; obj["thisstring"] = "This is a string";
  Io::Archive<SsMap> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/ssmap.xml") );

  SsMap objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/ssmap.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(SsMapTest, CnbIO)
{
  SsMap obj; obj["thisstring"] = "This is a string";
  Io::Archive<SsMap> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/ssmap.cnb") );

  SsMap objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/ssmap.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(SsMapTest, YamlIO)
{
  SsMap obj; obj["thisstring"] = "This is a string";
  Io::Archive<SsMap> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/ssmap.yaml") );

  SsMap objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/ssmap.yaml") );
  ASSERT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

// TEST(SsMapTest, RootIO)
// {
//   SsMap obj; obj["thisstring"] = "This is a string";
//   Io::Archive<SsMap> ioArc(obj, eArchive::kRoo);
//   ASSERT_TRUE( ioArc.write("test-data/ssmap.root") );

//   SsMap objFromFile;
//   ioArc.set(objFromFile);
//   ASSERT_TRUE( ioArc.read("test-data/ssmap.root") );
//   ASSERT_EQ  ( obj, objFromFile );
// }

// TEST(SsMapTest, RootTreeWrite)
// {
//   SsMap obj; obj["thisstring"] = "This is a string";
//   TFile rf("test-data/stringTree.root", "RECREATE"); rf.cd();
//   TTree* tree = new TTree("tSsMap", "Example tree with string");
//   tree->Branch("ssmap", "Pomona::SsMap", &objt);
//   for(UShort_t i=0; i<10; ++i){
//     objt[0]++;
//     tree->Fill();
//   }
//   ASSERT_TRUE(tree->GetEntries() == 10);
//   // Close and clean up
//   rf.Write();
//   rf.Close();
//   // if(tree != nullptr){ delete tree; tree = nullptr; }
// }

// TEST(SsMapTest, RootTreeRead)
// {
//   TFile rf("test-data/stringTree.root", "READ"); rf.cd();
//   ASSERT_FALSE(rf.IsZombie()); 
//   TTree* tree = (TTree*)rf.Get("tSsMap");
//   ASSERT_FALSE(tree == nullptr);
//   ASSERT_TRUE (tree->GetEntries() == 10);
//   SsMap* objt = nullptr;
//   ASSERT_EQ(0, tree->SetBranchAddress("ssmap", &objt));
//   SsMap objPrev;  
//   for(UShort_t i=0; i<10; ++i){
//     tree->GetEntry(i);
//     ASSERT_NE(*objt, "");
//     if(i == 0) objPrev = *objt;
//     else{
//       ASSERT_EQ(objPrev[0], (*objt)[0]-1);
//       objPrev = *objt;
//     }
//   }
//   // Close and clean up
//   rf.Close();
// }

#endif                  // end using root

} // end namespace

//  end SsMap.cxx
//  ############################################################################
