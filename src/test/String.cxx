//  ############################################################################
//! @file       test/String.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-04-01
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "String.hh"      // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "String";

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, DefaultCtor) 
{
  String obj;
  ASSERT_EQ(obj, obj);  // tautology
}

TEST(StringTest, CopyCtor) 
{
  String obj;
  String objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

TEST(StringTest, MoveCtor)
{
  String obj;
  String objCopy(obj);
  String objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(StringTest, CopyAssign)
{
  String obj;
  String objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(StringTest, MoveAssign)
{
  String obj;
  String objCopy(obj);
  String objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(StringTest, Clone) {
  String obj;
  ASSERT_NO_THROW({
      dynamic_cast<String*>(obj.clone());
    });
  String* objCln = dynamic_cast<String*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(StringTest, ClassName) {
  String obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(StringTest, IsA) {
  String obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(StringTest, IsSameClass) {
  String obj;
  String objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(StringTest, ClearEmpty) {
  String obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, IsSame) 
{
  String obj;
  ASSERT_TRUE(obj.isSame(obj));
}

TEST(StringTest, IsEqual) 
{
  String obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

TEST(StringTest, IsSortable) 
{
  String obj;
  ASSERT_TRUE(obj.isSortable());
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, DefaultPrintContent)
{
  String obj;
  ASSERT_EQ(obj.defaultPrintContent(), Printable::eContents::kValue);
}

TEST(StringTest, PrintClassName)
{
  String obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(StringTest, PrintName)
{
  String obj;
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(StringTest, PrintValue)
{
  const Char_t* zval = "value";
  String obj(zval);
  std::stringstream ss;
  obj.printValue(ss);
  ASSERT_STREQ(ss.str().c_str(), zval);
  ss.str("");
  ss << obj;
  ASSERT_STREQ(ss.str().c_str(), zval);
}


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Setters

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, IsNamed)
{
  String obj;
  ASSERT_FALSE(obj.isNamed());
}

TEST(StringTest, GetName)
{
  String obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, Is_Object)
{
  ASSERT_TRUE( is_object<String>::value );
}

TEST(StringTest, IsObject)
{
  String obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(StringTest, JsonIO)
{
  String obj("this is a string");
  Io::Archive<String> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/string.json") );

  String objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/string.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(StringTest, XmlIO)
{
  String obj("this is a string");
  Io::Archive<String> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/string.xml") );

  String objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/string.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(StringTest, CnbIO)
{
  String obj("this is a string");
  Io::Archive<String> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/string.cnb") );

  String objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/string.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(StringTest, YamlIO)
{
  String obj("this is a string");
  Io::Archive<String> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/string.yaml") );

  String objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/string.yaml") );
  ASSERT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(StringTest, RootTreeWrite)
{
  String objt("astringthisis");
  TFile rf("test-data/stringTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tString", "Example tree with string");
  tree->Branch("string", "Pomona::String", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt[0]++;
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(StringTest, RootTreeRead)
{
  TFile rf("test-data/stringTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tString");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  String* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("string", &objt));
  String objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, "");
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev[0], (*objt)[0]-1);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

TEST(StringTest, RootIO)
{
  String obj("this is a string");
  Io::Archive<String> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/string.root") );

  String objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/string.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

//! Clone
TEST(StringTest, RootClone) {
  String obj;
  ASSERT_NO_THROW({
      dynamic_cast<String*>(obj.Clone());
    });
  String* objCln = dynamic_cast<String*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(StringTest, RootClear) {
  String obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(StringTest, RootIsEqual) {
  String obj;
  String tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(StringTest, RootIsSortable) {
  String obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(StringTest, RootCompare) {
  String obj;
  String tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(StringTest, RootGetName) {
  String obj;
  EXPECT_STREQ(obj.GetName(), "Pomona::String");
}

//! GetTitle
TEST(StringTest, RootGetTitle) {
  String obj;
  EXPECT_STREQ(obj.GetTitle(), "");
}

#endif                  // end using root

} // end namespace

//  end String.cxx
//  ############################################################################
