//  ############################################################################
//! @file       test/TimePoint.cxx
//! @brief      Source for testing a generic object base-class
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "TimePoint.hh"   // pomona: this class
#include "IoArchive.hh"   // pomona: file I/O
#include "gtest/gtest.h"  // gTest
#include <sstream>        // std::stringstream
#ifdef CERNROOT           // Using root
#include "TFile.h"        // root: file
#include "TTree.h"        // root: tree
#endif                    // end using root

namespace {

using namespace Pomona;
using namespace Io;
static const Char_t* gClassName = "TimePoint";
static const milliseconds msZero = milliseconds::zero();

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Default contructor
TEST(TimePointTest, DefaultCtor) {
  TimePoint obj;
  ASSERT_EQ(obj, obj);  // tautology
}

//! std::milliseconds contructor
TEST(TimePointTest, MillisecCtor)
{
  TimePoint obj(milliseconds(10));
  ASSERT_EQ(obj.count(), 10);  // tautology
}

//! std::seconds contructor
TEST(TimePointTest, SecCtor)
{
  TimePoint obj(seconds(10));
  ASSERT_EQ(obj.count(), 10000);  // tautology
}

//! Copy contructor
TEST(TimePointTest, CopyCtor) {
  TimePoint obj;
  TimePoint objCpy(obj);
  ASSERT_EQ(obj, objCpy);
}

//! Move contructor
TEST(TimePointTest, MoveCtor)
{
  TimePoint obj;
  TimePoint objCopy(obj);
  TimePoint objMove(std::move(obj));
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

//! Copy assignment operator
TEST(TimePointTest, CopyAssign)
{
  TimePoint obj;
  TimePoint objCpy = obj;
  ASSERT_EQ(obj, objCpy);
}

TEST(TimePointTest, MoveAssign)
{
  TimePoint obj;
  TimePoint objCopy(obj);
  TimePoint objMove = std::move(obj);
  ASSERT_EQ(objCopy, objMove);
  ASSERT_TRUE(obj.isEmpty());  // original opbject should now be empty
}

TEST(TimePointTest, CopyAssignMillisec)
{
  milliseconds m10(10);
  TimePoint obj;
  obj = m10;
  ASSERT_EQ(obj.count(), 10);
}

TEST(TimePointTest, MoveAssignMillisec)
{
  milliseconds m10(10);
  TimePoint obj;
  obj = std::move(m10);
  ASSERT_EQ(obj.count(), 10);
  ASSERT_EQ(m10, msZero);
}

TEST(TimePointTest, CopyAssignSec)
{
  seconds m10(10);
  TimePoint obj;
  obj = m10;
  ASSERT_EQ(obj.count(), 10000);
}

TEST(TimePointTest, MoveAssignSec)
{
  seconds m10(10);
  TimePoint obj;
  obj = std::move(m10);
  ASSERT_EQ(obj.count(), 10000);
  ASSERT_EQ(m10, seconds::zero());
}

TEST(TimePointTest, CopyAssignUInt64)
{
  UInt64_t m10(10);
  TimePoint obj;
  obj = m10;
  ASSERT_EQ(obj.count(), 10);
}

TEST(TimePointTest, MoveAssignUInt64)
{
  UInt64_t m10(10);
  TimePoint obj;
  obj = std::move(m10);
  ASSERT_EQ(obj.count(), 10);
  ASSERT_EQ(m10, 0);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  AbsObject
TEST(TimePointTest, Clone) {
  TimePoint obj;
  ASSERT_NO_THROW({
      dynamic_cast<TimePoint*>(obj.clone());
    });
  TimePoint* objCln = dynamic_cast<TimePoint*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

TEST(TimePointTest, ClassName) {
  TimePoint obj;
  ASSERT_STREQ(obj.className().c_str(), gClassName);
}

TEST(TimePointTest, IsA) {
  TimePoint obj;
  ASSERT_TRUE(obj.isA(gClassName));
}

TEST(TimePointTest, IsSameClass) {
  TimePoint obj;
  TimePoint objO;
  ASSERT_TRUE(obj.isSameClass(objO));
}

TEST(TimePointTest, ClearEmpty) {
  TimePoint obj;
  obj.clear();
  ASSERT_TRUE(obj.isEmpty());
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Comparable

//! isSame
TEST(TimePointTest, IsSame) {
  TimePoint obj;
  ASSERT_TRUE(obj.isSame(obj));
}

//! isEqual
TEST(TimePointTest, IsEqual) {
  TimePoint obj;
  ASSERT_TRUE(obj.isEqual(obj));
}

//! isSortable
TEST(TimePointTest, IsSortable) {
  TimePoint obj;
  ASSERT_TRUE(obj.isSortable());
}

//! compare
TEST(TimePointTest, Compare)
{
  TimePoint obj;
  ASSERT_EQ(obj.compare(obj), 0);  // tautology
  TimePoint objCopy(obj);
  ASSERT_EQ(obj.compare(objCopy), 0);  

  TimePoint obj1(milliseconds(10));
  TimePoint obj2(milliseconds(20));
  TimePoint obj3(milliseconds(30));
  ASSERT_EQ(obj2.compare(obj1),  1);
  ASSERT_EQ(obj2.compare(obj3), -1);
}

//! InClassOperators
TEST(TimePointTest, InClassComparisonOperators)
{
  TimePoint obj1(msZero);
  const TimePoint obj2;
  ASSERT_FALSE( (obj1 == obj2) ); 
  ASSERT_TRUE ( (obj1 != obj2) ); 
  ASSERT_TRUE ( (obj1 <  obj2) ); 
  ASSERT_TRUE ( (obj1 <= obj2) ); 
  ASSERT_FALSE( (obj1 >  obj2) ); 
  ASSERT_FALSE( (obj1 >= obj2) ); 
}

//! OutClassOperators
TEST(TimePointTest, OutClassComparisonOperators) {
  const TimePoint obj1(msZero);
  const TimePoint obj2;
  ASSERT_FALSE( (obj1 == obj2) ); 
  ASSERT_TRUE ( (obj1 != obj2) ); 
  ASSERT_TRUE ( (obj1 <  obj2) ); 
  ASSERT_TRUE ( (obj1 <= obj2) ); 
  ASSERT_FALSE( (obj1 >  obj2) ); 
  ASSERT_FALSE( (obj1 >= obj2) ); 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Printable
TEST(TimePointTest, DefaultPrintContent)
{
  TimePoint obj;
  ASSERT_EQ(obj.defaultPrintContent(), Printable::eContents::kValue);
}

TEST(TimePointTest, PrintClassName)
{
  TimePoint obj;
  std::stringstream ss;
  obj.printClassName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(TimePointTest, PrintName)
{
  TimePoint obj;
  std::stringstream ss;
  obj.printName(ss);
  ASSERT_STREQ(ss.str().c_str(), gClassName);
}

TEST(TimePointTest, PrintValue)
{
  TimePoint obj(milliseconds(10));
  std::stringstream ss;
  obj.printValue(ss);
  ASSERT_STREQ(ss.str().c_str(), "10");
  ss.str("");
  ss << obj;
  ASSERT_STREQ(ss.str().c_str(), "10");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Setters
TEST(TimePointTest, SetMillisec)
{
  TimePoint obj;
  obj.set(milliseconds(10));
  ASSERT_EQ(obj.count(), 10);
}

TEST(TimePointTest, SetSec)
{
  TimePoint obj;
  obj.set(seconds(10));
  ASSERT_EQ(obj.count(), 10000);
}

TEST(TimePointTest, SetUInt64)
{
  TimePoint obj;
  obj.set(UInt64_t(10));
  ASSERT_EQ(obj.count(), 10);
}

TEST(TimePointTest, SetNow)
{
  TimePoint obj;
  ASSERT_NO_THROW({
      obj.setNow();
    });
  ASSERT_TRUE( obj > TimePoint::Zero() );
}

TEST(TimePointTest, IncOneSec)
{
  TimePoint obj;   const UInt64_t oc = obj.count();
  obj.incOneSec(); const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc+1000 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, PrefixIncrement)
{
  TimePoint obj; const UInt64_t oc = obj.count();
  ++obj;         const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc+1 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, PostfixIncrement)
{
  TimePoint obj; const UInt64_t oc = obj.count();
  obj++;         const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc+1 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, PrefixDecrement)
{
  TimePoint obj; const UInt64_t oc = obj.count();
  --obj;         const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc-1 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, PostfixDecrement)
{
  TimePoint obj; const UInt64_t oc = obj.count();
  obj--;         const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc-1 );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, AddAssign)
{
  TimePoint obj;              const UInt64_t oc = obj.count();
  obj += TimePoint::OneSec(); const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc+1000 );
  obj += TimePoint::Zero();
  ASSERT_EQ( obj.count(), nc );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, SubAssign)
{
  TimePoint obj;              const UInt64_t oc = obj.count();
  obj -= TimePoint::OneSec(); const UInt64_t nc = obj.count();
  ASSERT_EQ( nc, oc-1000 );
  obj -= TimePoint::Zero();
  ASSERT_EQ( obj.count(), nc );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, SystemNow)
{
  milliseconds ms;
  ASSERT_NO_THROW({
      ms = TimePoint::SystemNow();
    });
  ASSERT_TRUE( ms > msZero );
}

TEST(TimePointTest, SteadyNow)
{
  milliseconds ms;
  ASSERT_NO_THROW({
      ms = TimePoint::SteadyNow();
    });
  ASSERT_TRUE( ms > msZero );
}

TEST(TimePointTest, HighResNow)
{
  milliseconds ms;
  ASSERT_NO_THROW({
      ms = TimePoint::HighResNow();
    });
  ASSERT_TRUE( ms > msZero );
}

TEST(TimePointTest, Zero)
{
  ASSERT_NO_THROW({
      TimePoint::Zero();
    });
  const TimePoint& z = TimePoint::Zero();
  ASSERT_TRUE( z == msZero );
}

TEST(TimePointTest, OneSec)
{
  ASSERT_NO_THROW({
      TimePoint::OneSec();
    });
  const TimePoint& os = TimePoint::OneSec();
  ASSERT_TRUE( os.count() == 1000 );
}
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, IsNamed)
{
  TimePoint obj;
  ASSERT_FALSE(obj.isNamed());
}

TEST(TimePointTest, GetName)
{
  TimePoint obj;
  ASSERT_STREQ(obj.getName(), "");
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, Is_Object)
{
  ASSERT_TRUE( is_object<TimePoint>::value );
}

TEST(TimePointTest, IsObject)
{
  TimePoint obj;
  ASSERT_TRUE( IsObject(obj) );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TEST(TimePointTest, JsonIO)
{
  TimePoint obj;
  Io::Archive<TimePoint> ioArc(obj, eArchive::kJsn);
  ASSERT_TRUE( ioArc.write("test-data/timepoint.json") );

  TimePoint objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/timepoint.json") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(TimePointTest, XmlIO)
{
  TimePoint obj;
  Io::Archive<TimePoint> ioArc(obj, eArchive::kXml);
  ASSERT_TRUE( ioArc.write("test-data/timepoint.xml") );

  TimePoint objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/timepoint.xml") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(TimePointTest, CnbIO)
{
  TimePoint obj;
  Io::Archive<TimePoint> ioArc(obj, eArchive::kCnb);
  ASSERT_TRUE( ioArc.write("test-data/timepoint.cnb") );

  TimePoint objFromFile;
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/timepoint.cnb") );
  ASSERT_EQ  ( obj, objFromFile );
}

#ifdef YAMLCPP  // using yaml-cpp 

TEST(TimePointTest, YamlIO)
{
  TimePoint obj;
  Io::Archive<TimePoint> ioArc(obj, eArchive::kYml);
  ASSERT_TRUE( ioArc.write("test-data/timepoint.yaml") );

  TimePoint objFromFile(TimePoint::Zero());
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/timepoint.yaml") );
  ASSERT_EQ  ( obj, objFromFile );
}

#endif          // end using yaml-cpp

#ifdef CERNROOT  // Using root

TEST(TimePointTest, RootIO)
{
  TimePoint obj;
  Io::Archive<TimePoint> ioArc(obj, eArchive::kRoo);
  ASSERT_TRUE( ioArc.write("test-data/timepoint.root") );

  TimePoint objFromFile(TimePoint::Zero());
  ioArc.set(objFromFile);
  ASSERT_TRUE( ioArc.read("test-data/timepoint.root") );
  ASSERT_EQ  ( obj, objFromFile );
}

TEST(TimePointTest, RootTreeWrite)
{
  TimePoint objt;
  TFile rf("test-data/timepointTree.root", "RECREATE"); rf.cd();
  TTree* tree = new TTree("tTimePoint", "Example tree with timepoint");
  tree->Branch("timepoint", "Pomona::TimePoint", &objt);
  for(UShort_t i=0; i<10; ++i){
    objt.incOneSec();
    tree->Fill();
  }
  ASSERT_TRUE(tree->GetEntries() == 10);
  // Close and clean up
  rf.Write();
  rf.Close();
  // if(tree != nullptr){ delete tree; tree = nullptr; }
}

TEST(TimePointTest, RootTreeRead)
{
  TFile rf("test-data/timepointTree.root", "READ"); rf.cd();
  ASSERT_FALSE(rf.IsZombie()); 
  TTree* tree = (TTree*)rf.Get("tTimePoint");
  ASSERT_FALSE(tree == nullptr);
  ASSERT_TRUE (tree->GetEntries() == 10);
  TimePoint* objt = nullptr;
  ASSERT_EQ(0, tree->SetBranchAddress("timepoint", &objt));
  TimePoint objPrev;  
  for(UShort_t i=0; i<10; ++i){
    tree->GetEntry(i);
    ASSERT_NE(*objt, TimePoint::Zero());
    if(i == 0) objPrev = *objt;
    else{
      ASSERT_EQ(objPrev.count(), objt->count()-1000);
      objPrev = *objt;
    }
  }
  // Close and clean up
  rf.Close();
}

//! Clone
TEST(TimePointTest, RootClone) {
  TimePoint obj;
  ASSERT_NO_THROW({
      dynamic_cast<TimePoint*>(obj.Clone());
    });
  TimePoint* objCln = dynamic_cast<TimePoint*>(obj.clone());
  ASSERT_NE(objCln, nullptr);
}

//! Clear
TEST(TimePointTest, RootClear) {
  TimePoint obj;
  obj.Clear();
  ASSERT_TRUE(obj.isEmpty());
}

//! IsEqual
TEST(TimePointTest, RootIsEqual) {
  TimePoint obj;
  TimePoint tobj;
  ASSERT_TRUE(obj.IsEqual(&tobj));
}

//! IsSortable
TEST(TimePointTest, RootIsSortable) {
  TimePoint obj;
  ASSERT_TRUE(obj.IsSortable());
}

//! Compare
TEST(TimePointTest, RootCompare) {
  TimePoint obj;
  TimePoint tobj;
  ASSERT_EQ(obj.Compare(&tobj), 0);
}

//! GetName
TEST(TimePointTest, RootGetName) {
  TimePoint obj;
  EXPECT_STREQ(obj.GetName(), "Pomona::TimePoint");
}

//! GetTitle
TEST(TimePointTest, RootGetTitle) {
  TimePoint obj;
  EXPECT_STREQ(obj.GetTitle(), "");
}

#endif                  // end using root

} // end namespace

//  end TimePoint.cxx
//  ############################################################################
