//  ############################################################################
//! @file       test/Types.cxx
//! @brief      Source for testing fundamental types
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2019-09-29
//! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "Types.hh"       // pomona: these methods
#include "gtest/gtest.h"  // gTest

namespace {

using std::is_signed;
using std::is_unsigned;
using std::is_integral;
  
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! 64-bit
TEST(TypesTest, Is64bit) {
  ASSERT_TRUE(kIs64bit);  // we expect only 64-bit archetectures
}

//! null
TEST(TypesTest, Null) {
  ASSERT_TRUE(std::is_null_pointer<NullPtr_t>::value);  // null pointer
  ASSERT_EQ(0, NULL);                                   // null value
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Int8
TEST(TypesTest, Int8) {
  ASSERT_EQ(1, sizeof(Int8_t));             // 1 byte
  ASSERT_TRUE(is_integral<Int8_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Int8_t>::value);    // signed   
}

//! UInt8
TEST(TypesTest, UInt8) {
  ASSERT_EQ(1, sizeof(UInt8_t));             // 1 byte
  ASSERT_TRUE(is_integral<UInt8_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UInt8_t>::value);  // unsigned   
}

//! Int16
TEST(TypesTest, Int16) {
  ASSERT_EQ(2, sizeof(Int16_t));             // 2 byte
  ASSERT_TRUE(is_integral<Int16_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Int16_t>::value);    // signed   
}

//! UInt16
TEST(TypesTest, UInt16) {
  ASSERT_EQ(2, sizeof(UInt16_t));             // 2 byte
  ASSERT_TRUE(is_integral<UInt16_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UInt16_t>::value);  // unsigned   
}

//! Int32
TEST(TypesTest, Int32) {
  ASSERT_EQ(4, sizeof(Int32_t));             // 4 byte
  ASSERT_TRUE(is_integral<Int32_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Int32_t>::value);    // signed   
}

//! UInt32
TEST(TypesTest, UInt32) {
  ASSERT_EQ(4, sizeof(UInt32_t));             // 4 byte
  ASSERT_TRUE(is_integral<UInt32_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UInt32_t>::value);  // unsigned   
}

//! Int64
TEST(TypesTest, Int64) {
  ASSERT_EQ(8, sizeof(Int64_t));             // 8 byte
  ASSERT_TRUE(is_integral<Int64_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Int64_t>::value);    // signed   
}

//! UInt64
TEST(TypesTest, UInt64) {
  ASSERT_EQ(8, sizeof(UInt64_t));             // 8 byte
  ASSERT_TRUE(is_integral<UInt64_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UInt64_t>::value);  // unsigned   
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Bool
TEST(TypesTest, Bool) {
  ASSERT_EQ(1, sizeof(Bool_t));             // 1 byte
  ASSERT_TRUE(is_integral<Bool_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<Bool_t>::value);  // unsigned   
  ASSERT_TRUE (kTRUE);   // true   
  ASSERT_FALSE(kFALSE);  // false   
}

//! Char
TEST(TypesTest, Char) {
  ASSERT_EQ(1, sizeof(Char_t));             // 1 byte
  ASSERT_TRUE(is_integral<Char_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Char_t>::value);    // signed   
}

//! UChar
TEST(TypesTest, UChar) {
  ASSERT_EQ(1, sizeof(UChar_t));             // 1 byte
  ASSERT_TRUE(is_integral<UChar_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UChar_t>::value);  // unsigned   
}

//! Byte
TEST(TypesTest, Byte) {
  ASSERT_EQ(1, sizeof(Byte_t));             // 1 byte
  ASSERT_TRUE(is_integral<Byte_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<Byte_t>::value);  // unsigned   
}

//! Short
TEST(TypesTest, Short) {
  ASSERT_EQ(2, sizeof(Short_t));             // 2 byte
  ASSERT_TRUE(is_integral<Short_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Short_t>::value);    // signed   
}

//! UShort
TEST(TypesTest, UShort) {
  ASSERT_EQ(2, sizeof(UShort_t));             // 2 byte
  ASSERT_TRUE(is_integral<UShort_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UShort_t>::value);  // unsigned   
}

//! Int
TEST(TypesTest, Int) {
  ASSERT_EQ(4, sizeof(Int_t));             // 4 byte
  ASSERT_TRUE(is_integral<Int_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Int_t>::value);    // signed   
}

//! UInt
TEST(TypesTest, UInt) {
  ASSERT_EQ(4, sizeof(UInt_t));             // 4 byte
  ASSERT_TRUE(is_integral<UInt_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<UInt_t>::value);  // unsigned   
}

//! Long64
TEST(TypesTest, Long64) {
  ASSERT_EQ(8, sizeof(Long64_t));             // 8 byte
  ASSERT_TRUE(is_integral<Long64_t>::value);  // integer   
  ASSERT_TRUE(is_signed<Long64_t>::value);    // signed   
}

//! ULong64
TEST(TypesTest, ULong64) {
  ASSERT_EQ(8, sizeof(ULong64_t));             // 8 byte
  ASSERT_TRUE(is_integral<ULong64_t>::value);  // integer   
  ASSERT_TRUE(is_unsigned<ULong64_t>::value);  // unsigned   
}

//! Float
TEST(TypesTest, Float) {
  ASSERT_EQ(4, sizeof(Float_t));                        // 4 byte
  ASSERT_TRUE(std::is_floating_point<Float_t>::value);  // floating 
  ASSERT_TRUE(is_signed<Float_t>::value);               // signed   
}

//! Double
TEST(TypesTest, Double) {
  ASSERT_EQ(8, sizeof(Double_t));                        // 8 byte
  ASSERT_TRUE(std::is_floating_point<Double_t>::value);  // floating
  ASSERT_TRUE(is_signed<Double_t>::value);               // signed   
}

} // end namespace

//  end Types.cxx
//  ############################################################################
