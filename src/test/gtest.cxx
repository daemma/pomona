//  ############################################################################################
//! @file       gtest.cxx
//! @brief      Source for main testing 
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2019-03-31
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "gtest/gtest.h"  // gTest

//  ****************************************************************************
//! Main method for testing
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
//  end Object.cxx
//  ############################################################################
